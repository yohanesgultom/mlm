define({
  "title": "Good Health",
  "name": "good-health",
  "version": "0.0.1",
  "description": "Good Health API doc",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-12-12T10:26:45.321Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
