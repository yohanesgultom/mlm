const routes = [{
    path: '/',
    component: resolve => require(['./components/auth/login.vue'], resolve),
    meta: {
        title: 'Login',
        breadcrumb: ``
    }
}, {
    path: '/login',
    component: resolve => require(['./components/auth/login.vue'], resolve),
    meta: {
        title: 'Login',
        breadcrumb: ``
    }
}, {
    path: '/register',
    component: resolve => require(['./components/auth/register.vue'], resolve),
    meta: {
        title: 'Register',
        breadcrumb: ``
    }
}, {
    path: '/lockscreen',
    component: resolve => require(['./components/auth/lockscreen.vue'], resolve),
    meta: {
        title: 'Lockscreen',
        breadcrumb: ``
    }
}, {
    path: '/forgot_password',
    component: resolve => require(['./components/auth/forgot_password.vue'], resolve),
    meta: {
        title: 'Forgot Password',
        breadcrumb: ``
    }
}, {
    path: '/reset_password',
    component: resolve => require(['./components/auth/reset_pass.vue'], resolve),
    meta: {
        title: 'Reset Password',
        breadcrumb: ``
    }
}, {
    path: '*',
    component: resolve => require(['./components/pages/404.vue'], resolve),
    meta: {
        title: '404',
        breadcrumb: ``
    }
},{
    path: '/admin',
    meta: {
        title: 'Dashboard',
        breadcrumb: [{
            text: '<i class="ti-home"></i> Dashboard 1',
            href: '/',
        }]
    },
    component: resolve => require(['./default.vue'], resolve),
    children: [{
        path: 'index1',
        component: resolve => require(['./components/dashboard/index.vue'], resolve),
        meta: {
            title: 'Dashboard',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Dashboard 1',
                href: '/',
            }]
        }
    }, {
        path: 'index2',
        component: resolve => require(['./components/dashboard/index2.vue'], resolve),
        meta: {
            title: 'Dashboard2',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Dashboard 2',
                href: '#/index2',
            }]
        }
    },{
        path: 'country',
        component: resolve => require(['./components/master/country/index.vue'], resolve),
        meta: {
            title: 'Country',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Dashboard 2',
                href: '#/index2',
            }]
        }
    },{
        path: 'religion',
        component: resolve => require(['./components/master/religion/index.vue'], resolve),
        meta: {
            title: 'Religion',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Religion',
                href: '#/index2',
            }]
        }
    },{
        path: 'bank',
        component: resolve => require(['./components/master/bank/index.vue'], resolve),
        meta: {
            title: 'Bank',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Bank',
                href: '#/index2',
            }]
        }
    },{
        path: 'bank-account',
        component: resolve => require(['./components/master/bank_account/index.vue'], resolve),
        meta: {
            title: 'Bank Account',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Bank Account',
                href: '#/index2',
            }]
        }
    },{
        path: 'product-type',
        component: resolve => require(['./components/master/product_type/index.vue'], resolve),
        meta: {
            title: 'Product Type',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Product Type',
                href: '#/index2',
            }]
        }
    },{
        path: 'member-type',
        component: resolve => require(['./components/master/member_types/index.vue'], resolve),
        meta: {
            title: 'Member Type',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Member Type',
                href: '#/index2',
            }]
        }
    },{
        path: 'member-types/:member_type_id/packages/create',
        name: 'package.create',
        component: resolve => require(['./components/master/package/_form.vue'], resolve),
        meta: {
            title: 'Package',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Package',
                href: '#/index2',
            }]
        }
    },{
        path: 'member-types/:member_type_id/packages/:id',
        name: 'package.edit',
        component: resolve => require(['./components/master/package/_form.vue'], resolve),
        meta: {
            title: 'Package',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Package',
                href: '#/index2',
            }]
        }
    },{
        path: 'member-types/:member_type_id/packages',
        name: 'package.index',
        component: resolve => require(['./components/master/package/index.vue'], resolve),
        meta: {
            title: 'Package',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Package',
                href: '#/index2',
            }]
        }
    },{
        path: 'network-center-type',
        component: resolve => require(['./components/master/network_center_type/index.vue'], resolve),
        meta: {
            title: 'Network Center Type',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Network Center Type',
                href: '#/index2',
            }]
        }
    },{
        path: 'tags',
        component: resolve => require(['./components/master/tags/index.vue'], resolve),
        meta: {
            title: 'Tags',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Tags',
                href: '#/index2',
            }]
        }
    },{
        path: 'achievements',
        component: resolve => require(['./components/master/achievements/index.vue'], resolve),
        meta: {
            title: 'Achievements',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Achievements',
                href: '#/index2',
            }]
        }
    },{
        path: 'warehouse',
        component: resolve => require(['./components/warehouse/index.vue'], resolve),
        meta: {
            title: 'Warehouse',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Warehouse',
                href: '#/index2',
            }]
        }
    },{
        path: 'warehouse/:warehouse_id/stock/:id/transfer',
        name: 'warehouse.stock.transfer',
        component: resolve => require(['./components/warehouse/stocks/parts/transfer.vue'], resolve),
        meta: {
            title: 'Warehouse Transfer',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Warehouse Transfer',
                href: '#/index2',
            }]
        }
    },{
        path: 'product',
        name: 'product.index',
        component: resolve => require(['./components/products/index.vue'], resolve),
        meta: {
            title: 'Product',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Product',
                href: '#/index2',
            }]
        }
    },{
        path: 'product/create',
        name: 'product.create',
        component: resolve => require(['./components/products/create.vue'], resolve),
        meta: {
            title: 'Product Create',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Product',
                href: '#/index2',
            }]
        }
    },{
        path: 'product/edit/:id',
        name: 'product.edit',
        component: resolve => require(['./components/products/edit.vue'], resolve),
        meta: {
            title: 'Product Edit',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Product',
                href: '#/index2',
            }]
        }
    },{
        path: 'product/show/:id',
        name: 'product.show',
        component: resolve => require(['./components/products/show.vue'], resolve),
        meta: {
            title: 'Product Edit',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Product',
                href: '#/index2',
            }]
        }
    },{
        path: 'registrations',
        name: 'registrations.index',
        component: resolve => require(['./components/registrations/index.vue'], resolve),
        meta: {
            title: 'Registrations',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Registrations',
                href: '#/index2',
            }]
        }
    },{
        path: 'registrations/create',
        name: 'Registrations.create',
        component: resolve => require(['./components/registrations/create.vue'], resolve),
        meta: {
            title: 'Registrations Create',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Registrations',
                href: '#/index2',
            }]
        }
    },{
        path: 'registrations/edit/:id',
        name: 'registrations.edit',
        component: resolve => require(['./components/registrations/edit.vue'], resolve),
        meta: {
            title: 'Registrations Edit',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Registrations',
                href: '#/index2',
            }]
        }
    },{
        path: 'member',
        name: 'member.index',
        component: resolve => require(['./components/members/index.vue'], resolve),
        meta: {
            title: 'Members',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Member',
                href: '#/index2',
            }]
        }
    },{
        path: 'member/create',
        name: 'member.create',
        component: resolve => require(['./components/members/create.vue'], resolve),
        meta: {
            title: 'Member Create',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Member',
                href: '#/index2',
            }]
        }
    },{
        path: 'member/edit/:id',
        name: 'member.edit',
        component: resolve => require(['./components/members/edit.vue'], resolve),
        meta: {
            title: 'Member Edit',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Member',
                href: '#/index2',
            }]
        }
    },{
        path: 'member/show/:id',
        name: 'member.show',
        component: resolve => require(['./components/members/show.vue'], resolve),
        meta: {
            title: 'Member Show',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Member',
                href: '#/index2',
            }]
        }
    },{
        path: 'wallet/:member_id',
        name: 'wallet.index',
        component: resolve => require(['./components/members/wallets/index.vue'], resolve),
        meta: {
            title: 'Wallets',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Wallets',
                href: '#/index2',
            }]
        }
    },{
        path: 'attachments/:member_id',
        name: 'attachments.index',
        component: resolve => require(['./components/members/attachments/index.vue'], resolve),
        meta: {
            title: 'Attachments',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Attachments',
                href: '#/index2',
            }]
        }
    },{
        path: 'pairing-counters/:member_id',
        name: 'pairing-counters.index',
        component: resolve => require(['./components/members/pairing_counter/index.vue'], resolve),
        meta: {
            title: 'Pairing Counter',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Pairing Counters',
                href: '#/index2',
            }]
        }
    },{
        path: 'stock-members/:member_id',
        name: 'stocks.index',
        component: resolve => require(['./components/members/stocks/index.vue'], resolve),
        meta: {
            title: 'Stocks',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Stocks',
                href: '#/index2',
            }]
        }
    },{
        path: 'warehouse/stock-warehouses/:warehouse_id',
        name: 'stock_warehouse.index',
        component: resolve => require(['./components/warehouse/stocks/index.vue'], resolve),
        meta: {
            title: 'Stocks',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Stocks',
                href: '#/index2',
            }]
        }
    },{
        path: 'warehouse/stock-warehouses/:warehouse_id/stockLogs',
        name: 'stock_warehouse.stockLogs.create',
        component: resolve => require(['./components/warehouse/stocks/parts/formlogs.vue'], resolve),
        meta: {
            title: 'Warehouse Stock',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Stocks',
                href: '#/index2',
            }]
        }
    },{
        path: 'downline-members/parent',
        name: 'downline_members.parent.index',
        component: resolve => require(['./components/downline_members/index.vue'], resolve),
        meta: {
            title: 'Downline Members Parent',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Downline Members Parent',
                href: '#/index2',
            }]
        }
    },{
        path: 'downline-members/sponsor',
        name: 'downline_members.sponsor.index',
        component: resolve => require(['./components/downline_members/sponsor.vue'], resolve),
        meta: {
            title: 'Downline Members Sponsor',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Downline Members Sponsor',
                href: '#/index2',
            }]
        }
    },{
        path: 'downline-members/detail',
        name: 'downline_members.detail.index',
        component: resolve => require(['./components/downline_members/detail.vue'], resolve),
        meta: {
            title: 'Downline Members Details',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Downline Members Details',
                href: '#/index2',
            }]
        }
    },{
        path: 'orders',
        name: 'orders.index',
        component: resolve => require(['./components/orders/index.vue'], resolve),
        meta: {
            title: 'Orders',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Orders',
                href: '#/index2',
            }]
        }
    },{
        path: 'orders/create',
        name: 'orders.create',
        component: resolve => require(['./components/orders/create.vue'], resolve),
        meta: {
            title: 'Orders Create',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Orders Create',
                href: '#/index2',
            }]
        }
    },{
        path: 'orders/edit/:id',
        name: 'orders.edit',
        component: resolve => require(['./components/orders/edit.vue'], resolve),
        meta: {
            title: 'Orders Edit',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Orders Edit',
                href: '#/index2',
            }]
        }
    },{
        path: 'orders/show/:id',
        name: 'orders.show',
        component: resolve => require(['./components/orders/show.vue'], resolve),
        meta: {
            title: 'Orders Edit',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Orders Edit',
                href: '#/index2',
            }]
        }
    },{
        path: 'companies/',
        name: 'company.index',
        component: resolve => require(['./components/company/index.vue'], resolve),
        meta: {
            title: 'Companies',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Companies',
                href: '#/index2',
            }]
        }
    },{
        path: 'companies/:company_id/articles/',
        name: 'articles.index',
        component: resolve => require(['./components/company/articles/index.vue'], resolve),
        meta: {
            title: 'Articles',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Articles',
                href: '#/index2',
            }]
        }
    },{
        path: 'companies/:company_id/articles/create',
        name: 'articles.create',
        component: resolve => require(['./components/company/articles/_form.vue'], resolve),
        meta: {
            title: 'Articles Create',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Articles Create',
                href: '#/index2',
            }]
        }
    },{
        path: 'companies/:company_id/articles/edit/:id',
        name: 'articles.edit',
        component: resolve => require(['./components/company/articles/_form.vue'], resolve),
        meta: {
            title: 'Articles Edit',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Articles Edit',
                href: '#/index2',
            }]
        }
    },{
        path: 'deliveries',
        name: 'deliveries.index',
        component: resolve => require(['./components/deliveries/index.vue'], resolve),
        meta: {
            title: 'Deliveries List',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Deliveries List',
                href: '#/index2',
            }]
        }
    },{
        path: 'deliveries/create',
        name: 'deliveries.create',
        component: resolve => require(['./components/deliveries/parts/_form.vue'], resolve),
        meta: {
            title: 'Deliveries Create',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Deliveries Create',
                href: '#/index2',
            }]
        }
    },{
        path: 'deliveries/edit/:id',
        name: 'deliveries.edit',
        component: resolve => require(['./components/deliveries/parts/_form.vue'], resolve),
        meta: {
            title: 'Deliveries Edit',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Deliveries Edit',
                href: '#/index2',
            }]
        }
    },{
        path: 'payments',
        name: 'payments.index',
        component: resolve => require(['./components/payments/index'], resolve),
        meta: {
            title: 'Payment',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Payment',
                href: '#/index2',
            }]
        }
    },{
        path: 'deposits',
        name: 'deposits.index',
        component: resolve => require(['./components/deposit/index'], resolve),
        meta: {
            title: 'Deposit',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Deposit',
                href: '#/index2',
            }]
        }
    }, {
        path: 'bonus-logs',
        name: 'bonus.index',
        component: resolve => require(['./components/bonuslog/index'], resolve),
        meta: {
            title: 'Bonus Logs',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Bonus Log',
                href: '#/index2',
            }]
        }
    },{
        path: 'bonus-logs/show',
        name: 'bonus.show',
        component: resolve => require(['./components/bonuslog/show'], resolve),
        meta: {
            title: 'Bonus Logs Show',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Bonus Log',
                href: '#/index2',
            }]
        }
    },{
        path: 'reports/auditlog',
        name: 'reports.auditlog.index',
        component: resolve => require(['./components/reports/auditlogs/index.vue'], resolve),
        meta: {
            title: 'Report Audit Logs',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Report Audit Logs',
                href: '#/index2',
            }]
        }
    },{
        path: 'reports/order',
        name: 'reports.order.index',
        component: resolve => require(['./components/reports/orders/index.vue'], resolve),
        meta: {
            title: 'Report Orders List',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Report Orders List',
                href: '#/index2',
            }]
        }
    },{
        path: 'reports/downloads',
        name: 'reports.downloads.index',
        component: resolve => require(['./components/reports/downloads/index.vue'], resolve),
        meta: {
            title: 'Report Download ',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Report Download ',
                href: '#/index2',
            }]
        }
    },{
        path: 'reports/product-stock-logs',
        name: 'reports.stock-log.index',
        component: resolve => require(['./components/reports/product_stock_logs/index.vue'], resolve),
        meta: {
            title: 'Report Product Stock Logs List',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Report Product Stock Logs List',
                href: '#/index2',
            }]
        }
    },{
        path: 'reports/product-stocks',
        name: 'reports.stocks.index',
        component: resolve => require(['./components/reports/product_stocks/index.vue'], resolve),
        meta: {
            title: 'Report Product Stocks',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Report Product Stocks List',
                href: '#/index2',
            }]
        }
    },{
        path: 'reports/warehouses',
        name: 'reports.warehouses.index',
        component: resolve => require(['./components/reports/warehouses/index.vue'], resolve),
        meta: {
            title: 'Report Warehouses',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Report Warehouses List',
                href: '#/index2',
            }]
        }
    },{
        path: 'topup',
        name: 'topup.create',
        component: resolve => require(['./components/topup/create.vue'], resolve),
        meta: {
            title: 'Topup Wallet',
            breadcrumb: [{
                text: '<i class="ti-home"></i> TOPUP Wallet Create',
                href: '#/index2',
            }]
        }
    },{
        path: 'transactions',
        name: 'transactions.index',
        component: resolve => require(['./components/transactions/index.vue'], resolve),
        meta: {
            title: 'Transactions',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Transactions List',
                href: '#/index2',
            }]
        } 
    },{
        path: 'transactions/:id',
        name: 'transactions.transfer',
        component: resolve => require(['./components/transactions/transfer.vue'], resolve),
        meta: {
            title: 'Transactions Transfer',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Transactions Transfer',
                href: '#/index2',
            }]
        } 
    },{
        path: 'transactions/:id/split',
        name: 'transactions.split',
        component: resolve => require(['./components/transactions/split.vue'], resolve),
        meta: {
            title: 'Transactions Split',
            breadcrumb: [{
                text: '<i class="ti-home"></i> Transactions Split',
                href: '#/index2',
            }]
        } 
    }]
}]

export default routes
