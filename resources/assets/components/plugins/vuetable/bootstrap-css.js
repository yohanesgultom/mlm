export default {
	tableClass: 'table table-striped',
	loadingClass: 'loading',
	ascendingIcon: 'glyphicon glyphicon-chevron-up',
	descendingIcon: 'glyphicon glyphicon-chevron-down',
	handleIcon: 'glyphicon glyphicon-menu-hamburger',
	pagination: {
		paginationClass: 'pagination',
		infoClass: 'pull-left',
		wrapperClass: 'vuetable-pagination pull-right',
		activeClass: 'btn-primary',
		disabledClass: 'disabled',
		pageClass: 'btn btn-border',
		linkClass: 'btn btn-border',
		icons: {
			first: '',
			prev: '',
			next: '',
			last: '',
		},
	},
}