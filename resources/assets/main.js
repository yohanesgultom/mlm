import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store/store'
import App from './App'
import routes from './routes'
import BootstrapVue from 'bootstrap-vue'
import lodash from 'lodash'
import VueLodash from 'vue-lodash'

Vue.use(VueLodash, lodash)
Vue.use(VueRouter)
Vue.use(BootstrapVue)

// Check local storage to handle refreshes
if (window.localStorage) {

    var localUserString = window.localStorage.getItem('user') || 'null'
    var localUser = JSON.parse(localUserString)

    if (localUser && store.state.user !== localUser) {
    	// store.dispatch('setToken', window.localStorage.getItem('token'))
		  store.dispatch('setUser', localUser)
		//   store.dispatch('setAuthority', JSON.parse(window.localStorage.getItem('authority')))
          store.dispatch('setLoggedIn', true)
		//   store.dispatch('setSelectedStore', window.localStorage.getItem('selectedStore'))
    }
}


const router = new VueRouter({
    routes,
    linkActiveClass: 'active'
})

router.beforeEach((to, from, next) => {
    store.commit("routeChange", "start");
    next()
})

router.afterEach((to, from) => {
    document.title = to.meta.title
    store.commit("routeChange", "end");
    if (window.innerWidth < 992) {
        store.commit('left_menu', "close");
    } else {
        store.commit('left_menu', "open");
    }
})
new Vue({
    router,
    store,
    template: '<App/>',
    components: {
        App
    }
}).$mount('#app')
