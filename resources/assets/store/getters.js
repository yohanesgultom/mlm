export default {

	loggedIn: state  => state.auth.loggedIn,
	authUser: state  => state.auth.user,
	// authToken: state => state.auth.token,
	authorities: state => state.auth.authorities
}
