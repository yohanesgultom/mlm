export default {
	setUser({commit}, data){
        commit('SET_USER', data)
    },
    // setToken({commit}, data){
    //     commit('SET_TOKEN', data)
    // },
    setAuthority({commit}, data){
        commit('SET_AUTHORITY', data)
    },
    setLoggedIn({commit}, loggedIn) {
        commit('SET_LOGGED_IN', loggedIn)
    },
    setLogout({commit}) {
        commit('SET_LOGOUT')
    }
}
 