let mutations = {
    left_menu(state, option) {
        if (option == "open") {
            state.left_open = true
        } else if (option == "close") {
            state.left_open = false
        } else if (option == "toggle") {
            state.left_open = !state.left_open
        }
        if (state.left_open) {
            document.getElementsByTagName("body")[0].classList.remove("left-hidden")
        } else {
            document.getElementsByTagName("body")[0].classList.add("left-hidden")
        }
    },
    rightside_bar(state, option) {
        if (option == "open") {
            state.right_open = true
        } else if (option == "close") {
            state.right_open = false
        } else if (option == "toggle") {
            state.right_open = !state.right_open
        }
        if (state.right_open) {
            document.getElementsByTagName("body")[0].classList.add("sidebar-right-opened")
        } else {
            document.getElementsByTagName("body")[0].classList.remove("sidebar-right-opened")
        }
    },
    routeChange(state, loader) {
        if (loader == "start") {
            state.preloader = true
        } else if (loader == "end") {
            state.preloader = false
        }
    },

    /**
     * Set Vuex for Auth
     */
    SET_LOGGED_IN(state, loggedIn) {
        state.auth.loggedIn = loggedIn
    },
    SET_USER(state, user) {
        state.auth.user = user
    },
    SET_AUTHORITY(state, authorities) {
        state.auth.authorities = authorities
    },
    // SET_TOKEN(state, token) {
    //     state.auth.token = token
    // },

    SET_LOGOUT(state) {
        state.auth.loggedIn    = false
        state.auth.user        = null
        state.auth.token       = null
        state.auth.authorities = null

		if (window.localStorage) {
            localStorage.clear()
        }
    },
    // End Auth
    
}
export default mutations
