import 'es6-promise/auto';
import Vue from 'vue';
import Vuex from 'vuex';
import mutations from '../store/mutations';
import state from '../store/state';
import getters from '../store/getters';
import actions from '../store/actions';

Vue.use(Vuex);

//=======vuex store start===========
const store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions
});
//=======vuex store end===========
export default store
