import axios from 'axios';
import config from '@/config'
// import store from '@/store'


export const HTTP = axios.create({
  	baseURL: config.baseURL,
  	headers: {
    	'X-CSRF-TOKEN': Laravel.csrfToken,
        'Accept': "application/json"
  	}
})