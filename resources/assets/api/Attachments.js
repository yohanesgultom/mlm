import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl,

    postData(member_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/members/'+member_id+'/attachments', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id, member_id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/members/'+ member_id +'/attachments/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(id, member_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/attachments', params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, member_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/members/'+ member_id +'/attachments/'+ id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id, member_id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/members/'+member_id+'/attachments/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
	},
    
    download(id, member_id) {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/members/'+member_id+'/attachments/'+ id +'/download').then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    }

}