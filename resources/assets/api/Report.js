import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    getOrders(params = '') {
        return new Promise((resolve, reject) => {
            return HTTP.get(baseUrl + '/reports/orders' + params ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    getProductLogs(params) {
    	return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/reports/productStockLogs', params ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    getProductStocks(params) {
    	return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/reports/productStocks', params ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    getWarehouses(params) {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/reports/warehouses', params ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },


}