import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl,

    postData(member_type_id, params) {
        return new Promise((resolve, reject) => {

            return HTTP.post(baseUrl + '/memberTypes/'+member_type_id+'/packages', params).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id, member_type_id) {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/memberTypes/'+member_type_id+'/packages/' + id ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    getData(member_type_id, params) {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/memberTypes/'+member_type_id+'/packages', params ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(member_type_id, id, params) {
        return new Promise((resolve, reject) => {

            return HTTP.put(baseUrl + '/memberTypes/'+ member_type_id +'/packages/'+ id, params ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id, member_type_id) {
        return new Promise((resolve, reject) => {

            return HTTP.delete(baseUrl + '/memberTypes/'+ member_type_id +'/packages/'+ id).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },
    
    getStatus() {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/packages/statuses' ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    getType() {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/packages/types' ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    }
}