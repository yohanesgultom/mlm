import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

	pathUrl: pathUrl + '/transactions',

	showData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/transactions/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
				reject(error);
			})

		})
	},

	transfer(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/transactions/'+ id +'/transfer', params).then((response) => {
				resolve(response);
			}).catch((error) => {
				reject(error);
			})
		})	
	}

}