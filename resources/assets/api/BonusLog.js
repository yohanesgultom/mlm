import { HTTP } from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/bonusLogs',

    postData( params ) {
        return new Promise((resolve, reject) => {
            return HTTP.post(baseUrl + '/bonusLogs/calculateBonus', params).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })
        })
    },

    showData(id) {
        return new Promise((resolve, reject) => {
            return HTTP.get(baseUrl + '/bonusLogs/' + id).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })
        })
    },

    getData(id, params) {
        return new Promise((resolve, reject) => {
            return HTTP.get(baseUrl + '/bonusLogs', params).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })
        })
    }

}