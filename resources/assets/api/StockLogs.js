import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl,

    postData(warehouse_id, params) {
        return new Promise((resolve, reject) => {

            return HTTP.post(baseUrl + '/warehouses/'+warehouse_id+'/stockLogs', params).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    showData(warehouse_id, id) {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/warehouses/'+ warehouse_id + '/stockLogs/'+ id ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    getData(warehouse_id, params) {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/warehouses/'+ warehouse_id + '/stockLogs', params).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    }

}