import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/registrations',
    postData(params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/registrations', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/registrations/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(params = '') {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/registrations' + params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.put(baseUrl + '/registrations/'+id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/registrations/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getStatus() {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/registrations/statuses' ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

}