import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/products',

    postData(params) {
        console.log(params.values())
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/products', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/products/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(params = '') {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/products' + params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, params) {
		return new Promise((resolve, reject) => {
            return HTTP.post(baseUrl + '/products/'+ id, params ).then((response) => {
                console.log(params.values())
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/products/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
	}

}