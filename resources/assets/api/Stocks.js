import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl,

    postData(member_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/members/'+member_id+'/stocks', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(member_id, id) {
		return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/members/' + member_id + '/stocks/' + id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(member_id, params) {
		return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/members/' + member_id + '/stocks', params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, member_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.put(baseUrl + '/members/'+ member_id +'/stocks/'+ id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id, member_id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/members/'+ member_id +'/stocks/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
	}

}