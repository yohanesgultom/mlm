import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/members',
    postData(params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/members', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/members/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(params = '') {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/members' + params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.put(baseUrl + '/members/'+id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/members/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    downlines(id, params) {
        return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/members/'+ id + '/downlines'+ '?by='+ params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    }

}