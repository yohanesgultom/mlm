export const Auth = require('./Auth').default
export const Country = require('./Country').default
export const Member = require('./Member').default
export const Religion = require('./Religion').default
export const Product = require('./Product').default
export const Achievements = require('./Achievements').default
export const Bank = require('./Bank').default
export const BankAccounts = require('./BankAccounts').default
export const ProductType = require('./ProductType').default
export const MemberType = require('./MemberType').default
export const NetworkCenterType = require('./NetworkCenterType').default
export const Tags = require('./Tags').default
export const Warehouse = require('./Warehouse').default
export const Wallets = require('./Wallet').default
export const Attachments = require('./Attachments').default
export const PairingCounter = require('./PairingCounter').default
export const Stocks = require('./Stocks').default
export const StockWarehouse = require('./StockWarehouse').default
export const Orders = require('./Order').default
export const Company = require('./Company').default
export const Article = require('./Article').default
export const Delivery = require('./Delivery').default
export const Payment = require('./Payment').default
export const Deposit = require('./Deposit').default
export const BonusLog = require('./BonusLog').default
export const Transaction = require('./Transaction').default
export const Report = require('./Report').default
export const Registration = require('./Registration').default
export const Package = require('./Package').default
export const Auditlogs = require('./Auditlogs').default
export const StockLogs = require('./StockLogs').default

export default {
    Auth,
    Country,
    Member,
    Religion,
    Product,
    Achievements,
    Bank,
    ProductType,
    MemberType,
    NetworkCenterType,
    Tags,
    BankAccounts,
    Warehouse,
    Wallets,
    Attachments,
    PairingCounter,
    Stocks,
    StockWarehouse,
    Company,
    Article,
    Delivery,
    Payment,
    Deposit,
    BonusLog,
    Transaction,
    Report,
    Registration,
    Package,
    Auditlogs,
    StockLogs
}
