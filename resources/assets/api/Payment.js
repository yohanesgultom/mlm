import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/payments',

    postData(params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/payments', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/payments/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/payments', params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.put(baseUrl + '/payments/'+ id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/payments/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    getStatus() {
        return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/payments/statuses').then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getType() {
        return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/payments/types').then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    }

}