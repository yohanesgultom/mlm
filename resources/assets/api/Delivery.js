import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/deliveries',
    postData(params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/deliveries', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/deliveries/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(params) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/deliveries', params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.put(baseUrl + '/deliveries/'+id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/deliveries/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    getStatus() {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/deliveries/statuses' ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

}