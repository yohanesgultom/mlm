import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/warehouses',

    postData(params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/warehouses', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/warehouses/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(params) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/warehouses', params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.put(baseUrl + '/warehouses/'+ id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/warehouses/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
	},

    transfer(id, params) {
        return new Promise((resolve, reject) => {

            return HTTP.post(baseUrl + '/warehouses/'+ id +'/transfer', params).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    sell(id, params) {
        return new Promise((resolve, reject) => {

            return HTTP.post(baseUrl + '/warehouses/'+ id +'/sell', params).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    }

}