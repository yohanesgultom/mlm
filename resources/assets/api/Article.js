import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl,

    postData(company_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/companies/'+company_id+'/articles', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id, company_id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/companies/'+company_id+'/articles/' + id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(id, company_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/companies/'+company_id+'/articles', params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, company_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/companies/'+ company_id +'/articles/'+ id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id, company_id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/companies/'+ company_id +'/articles/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    getStatus() {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/articles/statuses' ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    },

    getType() {
        return new Promise((resolve, reject) => {

            return HTTP.get(baseUrl + '/articles/types' ).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })

        })
    }
}