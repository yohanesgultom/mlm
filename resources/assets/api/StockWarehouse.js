import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl,

    postData(warehouse_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/warehouses/'+warehouse_id+'/stocks', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(warehouse_id, id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/warehouses/'+ warehouse_id + '/stocks/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/stocks', params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, warehouse_id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.put(baseUrl + '/warehouses/'+ warehouse_id +'/stocks/'+ id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id, warehouse_id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/warehouses/'+ warehouse_id +'/stocks/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
	}

}