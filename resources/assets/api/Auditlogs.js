import { HTTP } from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/auditLogs',

    showData(id) {
        return new Promise((resolve, reject) => {
            return HTTP.get(baseUrl + '/auditLogs/' + id).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })
        })
    },

    getData(id, params) {
        return new Promise((resolve, reject) => {
            return HTTP.get(baseUrl + '/auditLogs', params).then((response) => {
                resolve(response);
            }).catch((error) => {
                reject(error);
            })
        })
    }

}