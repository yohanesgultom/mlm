import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/memberTypes',

    postData(params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/memberTypes', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/memberTypes/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/memberTypes', params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.put(baseUrl + '/memberTypes/'+ id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/memberTypes/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
	}

}