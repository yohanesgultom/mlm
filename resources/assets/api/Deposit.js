import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL
let pathUrl = baseUrl

export default {

    pathUrl: pathUrl + '/deposits',

    postData(params) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/deposits', params).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    showData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/deposits/'+ id ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    getData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/deposits', params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },

    updateData(id, params) {
		return new Promise((resolve, reject) => {

			return HTTP.put(baseUrl + '/deposits/'+ id, params ).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    deleteData(id) {
		return new Promise((resolve, reject) => {

			return HTTP.delete(baseUrl + '/deposits/'+ id).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    getStatus() {
        return new Promise((resolve, reject) => {

			return HTTP.get(baseUrl + '/deposits/statuses').then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    }

}