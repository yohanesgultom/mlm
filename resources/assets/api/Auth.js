import {HTTP} from '@/http-common.js'
import config from '@/config'

let baseUrl = config.baseURL

export default {

	login(formData) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/login', formData).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
	},
	
	forgot_password(formData) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/password/email', formData).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
	},
	
	reset_password(formData) {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/password/reset', formData).then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
    },
    
    logout() {
		return new Promise((resolve, reject) => {

			return HTTP.post(baseUrl + '/logout').then((response) => {
				resolve(response);
			}).catch((error) => {
                reject(error);
            })

        })
	}
}