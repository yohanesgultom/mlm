import sys
import json
import MySQLdb
from datetime import datetime
from openpyxl import load_workbook

OUTPUT_FILE = 'bonus_logs.sql'
COL_INDEX = {
    'id': 0, 'nama': 1, 'karir': 2, 'posisi': 3, 'db': 4, 'fsb': 5, 'asb': 6, 'lmb': 7, 'ldb': 8
}
SQL_TPL = "INSERT INTO bonus_logs (member_id, process_date, total, details, close_point, created_at) VALUES ({});"
SQL_CHECK_TPL = "SELECT id FROM members WHERE id = {}"

def quote(val, default='null'):
	if not val:
		return default
	else:
		return "'" + str(val).replace("'", "\'") + "'"

def build_insert_query(row, cursor):
    member_id = row[COL_INDEX['id']].value
    num_rows = cursor.execute(SQL_CHECK_TPL.format(quote(member_id)))
    if num_rows != 1:
        raise Exception('Invalid member_id {}'.format(member_id))
    total = row[COL_INDEX['db']].value + row[COL_INDEX['fsb']].value + row[COL_INDEX['asb']].value + row[COL_INDEX['lmb']].value + row[COL_INDEX['ldb']].value
    details = {
        'discount_bonus': row[COL_INDEX['db']].value,
        'first_sales_bonus': row[COL_INDEX['fsb']].value,
        'achievement_share_bonus': row[COL_INDEX['asb']].value,
        'leadership_development_bonus': row[COL_INDEX['ldb']].value,
        'leadership_matching_bonus': row[COL_INDEX['lmb']].value,
    }
    values = [
        quote(member_id),
        quote(process_date),
        quote(total, quote('0')),
        quote(json.dumps(details)),
        '1' if total > 0 else '0',
        'NOW()',
    ]
    return SQL_TPL.format(', '.join(values))

if __name__ == '__main__':

    input_file = sys.argv[1]
    process_date = sys.argv[2]
    output_file = OUTPUT_FILE

    db = MySQLdb.connect('localhost', 'root', 'root', 'greenvit')
    cursor = db.cursor(MySQLdb.cursors.DictCursor)

    wb = load_workbook(input_file)
    ws = wb.active
    with open(output_file, 'wb') as f: 
        f.write('SET autocommit = 0;\n')
        f.write('START TRANSACTION;\n')
        f.write('\n')
        for row in ws.iter_rows(min_row=2):
            try: 
                f.write(build_insert_query(row, cursor) + '\n')
            except Exception as e:
                print(e)
        f.write('\n')
        f.write('COMMIT;\n')

    db.close()