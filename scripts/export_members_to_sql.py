import sys
import MySQLdb

output_file = sys.argv[1]
db = MySQLdb.connect('localhost', 'root', 'root', 'greenvit5')
process_date = '2018-12-31'

ROLE_MAP = {
	'Administrator': 1,
	'Manager': 1,
	'Registered': 2,
	'Super Administrator': 1,
}

GENDER_MAP = {
	'L': 1,
	'P': 0,
	'N': 'null',
}

ACHIEVEMENTS_MAP = {
	'AM':'SPV',
	'D':'D',
	'DI':'DI',
	'GM':'GM',
	'M':'M',
	'MD':'MD',
	'PD':'PD',
	'QM':'QM',
	'S':'SPV',
	'SM':'SM',
	'SPV':'SPV',
}

def sanitize(sanitizer, values):
	for k, v in values.iteritems():
		if v and isinstance(v, str):
			v = v.decode('utf-8','ignore').encode('utf-8')
			values[k] = sanitizer(v)
	return values

def quote(val):
	if not val:
		return 'null'
	else:
		return "'" + str(val).replace("'", "\'") + "'"


def build_user_insert_query(d):
	sql = "INSERT INTO users (id,name,username,email,role,password,verified,enabled,last_access_at,status,created_at,updated_at,remember_token,company_id) VALUES ({});"
	values = [
		'null',
		quote(d['nama_lengkap']),
		quote(d['member_id']),
		quote(d['email']),
		'2', # member
		quote("$2y$10$Zl8pu/no4z4feYZLBQTcnuYsr9rKGIMA8OGTgRLVg2DSWOVnE2Tzm"), # secret
		str(1),
		str(1),
		'null',
		str(0) if d['block'] == 1 else str(1),
		'NOW()',
		'null',
		'null',
		str(1),
	]
	return sql.format(','.join(values))


def build_member_insert_query(d):
	address = []
	if d['alamat1']:
		address.append(d['alamat1'])
	if d['alamat2']:
		address.append(d['alamat2'])
	address = ', '.join(address) if address else None

	sql = "INSERT INTO members (id, user_id, parent_id, sponsor_id, member_type_id, network_center_type_id, name_printed, birth_place, birth_date, reg_date, reg_bv, bonus_date, achievement_id, highest_achievement_id, gender, official_id, tax_id, current_personal_points, current_personal_bonus, current_group_points, current_group_bonus, address, address_rt, address_rw, address_zipcode, address_city, address_state, country_id, phone_home, phone_office, phone_mobile, phone_fax, beneficiary_name, beneficiary_relation, bank_account_no, bank_account_name, bank_account_city, bank_account_branch, image_path, bank_id, status, created_at, updated_at) VALUES ({});"
	values = [
		quote(d['member_id']),
		'LAST_INSERT_ID()',
		quote(d['upline_id']),
		quote(d['sponsor_id']),
		"(SELECT id FROM member_types WHERE name = 'BRONZE')",
		'null',
		quote(d['nama_di_kartu']),
		quote(d['tmp_lahir']),
		quote(d['tgl_lahir'].isoformat().split(".")[0]) if d['tgl_lahir'] else 'null',
		quote(d['tgl_join'].isoformat().split(".")[0]) if d['tgl_join'] else 'null',
		str(0),
		quote(d['tgl_bonus'].isoformat().split(".")[0]) if d['tgl_bonus'] else 'null',
		"(SELECT id FROM achievements WHERE upper(code) = '{}')".format(ACHIEVEMENTS_MAP[d['career_code'].upper()]) if d['career_code'] else 'null',
		"(SELECT id FROM achievements WHERE upper(code) = '{}')".format(ACHIEVEMENTS_MAP[d['posisi_tertinggi'].upper()]) if d['posisi_tertinggi'] else 'null',
		str(GENDER_MAP[d['jenis_kelamin']]) if d['jenis_kelamin'] else 'null',
		quote(d['ktp_sim']),
		quote(d['npwp']),
		str(0),
		str(0),
		str(0),
		str(0),
		quote(address),
		quote(d['rt']),
		quote(d['rw']),
		quote(d['kodepos']),
		quote(d['kota']),
		quote(d['propinsi']),
		"(SELECT id FROM countries WHERE lower(name) = '{}')".format(d['negara'].lower()) if d['negara'] else 'null',
		quote(d['telp_rumah']),
		quote(d['telp_kantor']),
		quote(d['no_hp']),
		quote(d['fax']),
		quote(d['ahli_waris']),
		quote(d['ahli_waris_status']),
		quote(d['bank_account_number']),
		quote(d['bank_account_owner']),
		quote(d['bank_city']),
		quote(d['bank_branch']),
		'null',
		"(SELECT id FROM banks WHERE lower(name) = '{}')".format(d['bank_name'].lower()) if d['bank_name'] else 'null',
		str(2) if d['block'] == 1 else str(1),
		'NOW()',
		'null',
	]
	return sql.format(','.join(values))

def build_wallet_insert_query(d):
	address = []
	if d['alamat1']:
		address.append(d['alamat1'])
	if d['alamat2']:
		address.append(d['alamat2'])
	address = ', '.join(address) if address else None

	sqls = []
	# create 3 wallets
	for i in range(3):
		sql = "INSERT INTO wallets (type, balance, member_id, created_at) VALUES ({});"
		values = [
			"'{}'".format(i),
			'0',
			quote(d['member_id']),
			'NOW()',
		]
		sqls.append(sql.format(','.join(values)))
	return '\n'.join(sqls)

# def build_member_update_query(d):
# 	sql = """UPDATE members, (SELECT id FROM members WHERE id = {0}) as parents, (SELECT id FROM members WHERE id = {1}) as sponsors SET members.parent_id = parents.id, members.sponsor_id = sponsors.id WHERE members.id = {2};"""
# 	return sql.format(quote(d['upline_id']), quote(d['sponsor_id']), quote(d['member_id']))

cursor = db.cursor(MySQLdb.cursors.DictCursor)

sql = """SELECT 
	m.member_id, 
	m.nama_lengkap,
	m.nama_di_kartu,
	m.sponsor_id, 
	m.upline_id, 
	m.jenis_kelamin,
	m.tmp_lahir,
	m.tgl_lahir,
	m.ktp_sim,
	m.agama,	
	m.ahli_waris,	
	m.ahli_waris_status,	
	m.fax,
	m.telp_rumah,
	m.telp_kantor,
	m.no_hp,
	m.email,    
	m.alamat1,
	m.alamat2,
	m.rt,
	m.rw,
	m.kota,
	m.propinsi,
	m.kodepos,
	m.zone,
	m.negara,
	m.website,
	m.npwp,
	m.bank_name,
	m.bank_branch,
	m.bank_city,
	m.bank_account_number,
	m.bank_account_owner,
	m.career_code,
    c.posisi_tertinggi,
	m.tgl_join,
	m.tgl_bonus,
	m.status as member_status,
	m.block
FROM jos_mlm_users m
JOIN jos_mlm_career_history c ON m.member_id = c.member_id AND c.process_date = '{}';
"""

cursor.execute(sql.format(process_date))
updates = []
with open(output_file, 'wb') as f: 
	f.write('SET autocommit = 0;\n')
	f.write('SET FOREIGN_KEY_CHECKS=0;\n')
	f.write('START TRANSACTION;\n')
	f.write('\n')
	f.write('-- insert users and members\n')
	f.write('-- password: secret\n')
	f.write('\n')
	for row in cursor.fetchall():
		row = sanitize(db.escape_string, row)
		sql_user = build_user_insert_query(row)
		sql_member = build_member_insert_query(row)	
		sql_wallets = build_wallet_insert_query(row)	
		f.write(sql_user + '\n')
		f.write(sql_member + '\n')
		f.write(sql_wallets + '\n')
	f.write('\n')
	f.write('COMMIT;\n')
	f.write('SET FOREIGN_KEY_CHECKS=1;\n')

db.close()