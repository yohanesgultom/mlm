<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Auditable;

class Product extends Model
{
    use Auditable;

    protected $fillable = [
        'name',
        'desc',
        'desc_short',
        'image_path',
        'brochure_file',
        'product_type_id',
        'base_price',
        'retail_price',
        'bonus_value',
        'point_value',
        'weight',
        'created_by',
        'updated_by',
    ];    

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->created_by = \Auth::id();
        });

        static::updating(function($model)
        {
            $model->updated_by = \Auth::id();
        });

    }

    public function product_type()
    {
        return $this->belongsTo('App\ProductType');
    }    
    
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }    
    
    public function warehouse_stocks()
    {
        return $this->hasMany('App\WarehouseStock')->orderBy('warehouse_id');
    }

    public function warehouse_stock_logs()
    {
        return $this->hasMany('App\WarehouseStockLog');
    }

    public function packages()
    {
        return $this->hasManyThrough('App\Package', 'App\PackageProduct');
    }

    public function scopeJoinProductTypes($query)
    {
        return $query->join('product_types', 'products.product_type_id', 'product_types.id');
    }

    public function scopeSameCompany($query)
    {
        return $query->ofCompany(\Auth::user()->company_id);
    }

    public function scopeOfCompany($query, $company_id)
    {
        return $query
            ->select('products.*')
            ->joinProductTypes()            
            ->where('product_types.company_id', $company_id);
    }    
}
