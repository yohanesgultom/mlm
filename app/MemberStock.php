<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberStock extends Model
{
    protected $fillable = [
        'qty',
        'exp_date',
        'member_id',
        'product_id',
    ];

    protected $dates = [
        'exp_date',
    ];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function scopeOwned($query)
    {
        return $query->where('member_id', \Auth::user()->member->id);
    }

    public function scopeOwnedBy($query, $member_id)
    {
        return $query->where('member_id', $member_id);
    }

    public function scopeSameCompany($query)
    {
        return $query
            ->select('member_stocks.*')
            ->join('members', 'member_id', '=', 'members.id')
            ->join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', \Auth::user()->company_id);
    } 

}
