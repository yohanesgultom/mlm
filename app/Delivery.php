<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    const STATUSES = [
        'UNPAID' => 0,
        'PAID' => 1,
        'PROCESSED' => 2,
        'DELIVERED' => 3,
    ];

    protected $with = ['country'];

    protected $appends = ['status_name'];

    protected $fillable = [
        'cost',
        'delivery_date',
        'courier',
        'courier_ref',
        'address',
        'address_zipcode',
        'address_city',
        'address_state',
        'status',
        'country_id',
        'member_id',
    ];

    protected $dates = [
        'delivery_date',
    ];
    
    public function country()
    {
        return $this->belongsTo('App\Country');
    }
    
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function items()
    {
        return $this->hasMany('App\DeliveryItem');
    }    

    public function scopeSameCompany($query)
    {
        return $query
            ->select('deliveries.*')
            ->join('members', 'deliveries.member_id', '=', 'members.id')
            ->join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', \Auth::user()->company_id);
    }     

    public function getStatusNameAttribute()
    {
        return array_flip(static::STATUSES)[$this->status];
    }    
}
