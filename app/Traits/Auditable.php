<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Auth;
use App\AuditLog;
use App\User;

trait Auditable
{
    public static function bootAuditable()
    {
        static::creating(function ($model) {
            $user = Auth::user();            
            if (!empty($user) && $user->role != User::ROLES['MEMBER']) {                
                AuditLog::create([
                    'action' => 'create',
                    'ip' => session('ip'),
                    'path' => session('path'),
                    'model' => get_class($model),
                    'userrole' => array_flip(User::ROLES)[$user->role],
                    'username' => $user->name,
                    'change' => $model->getDirty(),
                    'company_id' => $user->company_id,
                ]);
            }
        });

        static::updating(function ($model) {
            $user = Auth::user();
            if (!empty($user) && $user->role != User::ROLES['MEMBER']) {
                $usertype = get_class($user);
                AuditLog::create([
                    'action' => 'update',
                    'ip' => session('ip'),
                    'path' => session('path'),
                    'model' => get_class($model),
                    'userrole' => array_flip(User::ROLES)[$user->role],
                    'username' => $user->name,
                    'before' => $model->getOriginal(),
                    'change' => $model->getDirty(),
                    'company_id' => $user->company_id,
                ]);
            }
        });

        static::deleting(function ($model) {
            $user = Auth::user();
            if (!empty($user) && $user->role != User::ROLES['MEMBER']) {
                AuditLog::create([
                    'action' => 'delete',
                    'ip' => session('ip'),
                    'path' => session('path'),
                    'model' => get_class($model),
                    'userrole' => array_flip(User::ROLES)[$user->role],
                    'username' => $user->name,
                    'before' => $model->getOriginal(),
                    'company_id' => $user->company_id,
                ]);
            }
        });
    }
}