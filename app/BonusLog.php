<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BonusLog extends Model
{
    const CYCLES = [
        'MONTHLY' => 'MONTHLY',
        'WEEKLY' => 'WEEKLY',
    ];

    protected $fillable = [
        'cycle',
        'close_point',
        'pass_up',
        'total',
        'process_date',
        'details',
        'logs',
        'data',
        'transfer_date',
        'bank_account_no',
        'bank_account_name',
        'bank_account_city',
        'bank_account_branch',
        'bank_name',
        'member_id',
    ];

    protected $casts = [
        'close_point' => 'boolean',
        'details' => 'array',
        'logs' => 'array',
        'data' => 'array',
    ];

    protected $dates = [
        'process_date',
        'transfer_date',
    ];

    protected $hidden = ['logs'];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function scopeOwned($query)
    {
        return $query->ownedBy(\Auth::user()->member->id);
    }

    public function scopeOwnedBy($query, $member_id)
    {
        return $query->where('member_id', $member_id);
    }

    public function scopeByCompany($query, $company_id)
    {
        return $query
            ->select('bonus_logs.*')
            ->join('members', 'bonus_logs.member_id', '=', 'members.id')
            ->join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', $company_id);
    } 

    public function scopeSameCompany($query)
    {
        return $query->byCompany(\Auth::user()->company_id);
    } 

}
