<?php

function from_cache($key, $expired_at, $func, $params = [])
{
    $val = cache($key);
    if (empty($val)) {
        $val = call_user_func_array($func, $params);
        cache([$key => $val], $expired_at);
    }
    return $val;
}
