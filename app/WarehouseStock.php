<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseStock extends Model
{
    protected $fillable = [
        'qty', // physically available but may not be available (booked by members)
        'available', // phsyically and virtually available
        'exp_date',
        'warehouse_id',
        'product_id',
    ];

    protected $dates = [
        'exp_date',
    ];

    public function warehouse()
    {
        return $this->belongsTo('App\Warehouse');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function scopeSameCompany($query)
    {
        return $query->ofCompany(\Auth::user()->company_id);
    }

    public function scopeOfCompany($query, $company_id)
    {
        return $query->whereHas('warehouse', function ($q) use ($company_id) {
            $q->where('company_id', $company_id);
        });
    }    

    public function scopeCentral($query, $product_ids)
    {
        $central = \App\Warehouse::central()->select('id')->first();
        if (empty($central)) {
            throw new \Exception('No central warehouse defined');
        } 
        return $query
            ->where('warehouse_stocks.warehouse_id', $central->id)
            ->whereIn('warehouse_stocks.product_id', $product_ids);
    }

    public function scopeRegular($query, $id, $product_ids)
    {
        return $query
            ->where('warehouse_stocks.warehouse_id', $id)
            ->whereIn('warehouse_stocks.product_id', $product_ids);
    }

}
