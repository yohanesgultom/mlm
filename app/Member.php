<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Auditable;

class Member extends Model
{
    use Auditable;

    public $incrementing = false;
    
    protected $with = [
        'user',
        'member_type',
        'achievement',
        'highest_achievement',
    ];

    const GENDERS = [
        'FEMALE' => 0,
        'MALE' => 1,
    ];

    const STATUSES = [
        'NEW' => 0,
        'ACTIVE' => 1,
        'SUSPENDED' => 2,
    ];
    
    protected $fillable = [
        'id',
        'user_id',
        'parent_id',
        'sponsor_id',
        'member_type_id',
        'network_center_type_id',
        'name_printed',
        'reg_date',
        'reg_bv',
        'birth_date',
        'bonus_date',
        'achievement_id',
        'highest_achievement_id',
        'gender',
        'official_id',
        'tax_id',
        'current_personal_points',
        'current_personal_bonus',
        'current_group_points',
        'current_group_bonus',
        'address',
        'address_rt',
        'address_rw',
        'address_zipcode',
        'address_city',
        'address_state',
        'country_id',
        'phone_home',
        'phone_office',
        'phone_mobile',
        'phone_fax',
        'beneficiary_name',
        'beneficiary_relation',
        'bank_account_no',
        'bank_account_name',
        'bank_account_city',
        'bank_account_branch',
        'bank_id',
        'image_path',
        'status',
    ];

    protected $dates = [
        'reg_date',
        'birth_date',
        'bonus_date',
    ];

    protected $casts = [
        'id' => 'string',
    ];

    protected $appends = ['gender_name'];

    public static function boot()
    {
        parent::boot();
    
        static::creating(function ($model) {
            if (empty($model->achievement_id)) {
                $lowest_achievement = \App\Achievement::orderBy('order', 'asc')
                    ->select('id')
                    ->first();
                if (!empty($lowest_achievement)) {
                    $model->achievement_id = $lowest_achievement->id;
                }                
            }
        });
    
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function achievement()
    {
        return $this->belongsTo('App\Achievement', 'achievement_id');
    }

    public function highest_achievement()
    {
        return $this->belongsTo('App\Achievement', 'highest_achievement_id');
    }

    public function member_type()
    {
        return $this->belongsTo('App\MemberType');
    }

    function upline(){
        return $this->belongsTo('App\Member', 'parent_id');
    }

    function downlines(){
        return $this->hasMany('App\Member', 'parent_id');
    }

    function sponsor(){
        return $this->belongsTo('App\Member', 'sponsor_id');
    }

    function sponsoreds(){
        return $this->hasMany('App\Member', 'sponsor_id');
    }

    function wallets(){
        return $this->hasMany('App\Wallet');
    }
    
    function pairingCounters(){
        return $this->hasMany('App\PairingCounter');
    }

    public function attachments()
    {
        return $this->hasMany('App\Attachment');
    }

    public function stocks()
    {
        return $this->hasMany('App\MemberStock');
    }

    public function deliveries()
    {
        return $this->hasMany('App\Delivery');
    }
    
    public function scopePublic($query)
    {
        return $query->select(
            'members.id',
            'user_id',
            'parent_id',
            'sponsor_id',
            'member_type_id',
            'name_printed',
            'reg_date',
            'current_personal_points',
            'current_personal_bonus',
            'current_group_points',
            'current_group_bonus',
            'achievement_id',
            'highest_achievement_id',
            'gender',
            'image_path',
            'members.status'
        );
    }

    public function scopeJoinUsers($query)
    {
        return $query->select('members.*')
            ->join('users', 'members.user_id', 'users.id');
    }

    public function scopeSameCompany($query)
    {
        return $query->where('users.company_id', \Auth::user()->company_id);
    }

    public function scopeByCompany($query, $company_id)
    {
        return $query->where('users.company_id', $company_id);
    }

    public function getGenderNameAttribute()
    {
        return !empty($this->attributes['gender']) ? array_flip(static::GENDERS)[$this->attributes['gender']] : null;
    }
}
