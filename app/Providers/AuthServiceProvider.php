<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
        Passport::tokensExpireIn(now()->addDays(30));
        Passport::refreshTokensExpireIn(now()->addDays(30));

        Gate::define('access-superadmin-functions', function ($user) {
            return in_array($user->role, [
                \App\User::ROLES['SUPERADMIN'],
            ]);
        });
        
        Gate::define('access-admin-functions', function ($user) {
            return in_array($user->role, [
                \App\User::ROLES['ADMIN'],
                \App\User::ROLES['SUPERADMIN'],
                \App\User::ROLES['IT'],
                \App\User::ROLES['SALESADMIN'],
                \App\User::ROLES['FINANCE'],
                \App\User::ROLES['MARKETING'],
                \App\User::ROLES['LOGISTICS'],
            ]);
        });

        // TODO: not being used until member can access trough web
        // if member never access through web, these can be removed
        Gate::define('access-member-functions', function ($user) {
            return in_array($user->role, [
                \App\User::ROLES['MEMBER'],
                \App\User::ROLES['ADMIN'],
                \App\User::ROLES['SUPERADMIN'],
            ]);
        });
    }
}
