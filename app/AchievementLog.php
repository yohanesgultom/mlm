<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievementLog extends Model
{
    protected $fillable = [
        'downlines_count',
        'downlines_pv',
        'code',
        'process_date',
        'member_id',
    ];

    protected $casts = [
        'downlines_count' => 'array',
        'downlines_pv' => 'array',
    ];

    protected $dates = [
        'process_date',
    ];
}
