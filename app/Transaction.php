<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Auditable;

class Transaction extends Model
{
    use Auditable;
    
    const TYPES = [
        'REGULAR' => 0,
        'ZERO_POINT_BONUS' => 1,
    ];

    protected $with = ['product'];

    protected $appends = ['type_name'];

    protected $fillable = [
        'code',
        'type',
        'desc',
        'qty',
        'process_date',
        'bonus',
        'points',
        'point_date',
        'exp_date',
        'transferred',
        'transfer_from_id',
        'transfer_from_member_id',
        'product_id',
        'order_id',
        'member_stock_id',
    ];

    protected $dates = [
        'process_date',
        'point_date',
        'exp_date',
    ];

    protected $casts = [
        'transferred' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            $product = \App\Product::find($model->product_id);
            $model->amount = $model->qty * $product->base_price;
            // default types
            $model->type = $model->type ?? static::TYPES['REGULAR'];
            // handle types
            if ($model->type == static::TYPES['ZERO_POINT_BONUS']) {
                $model->points = 0;
                $model->bonus = 0;    
            } else {
                $model->points = $model->qty * $product->point_value;
                $model->bonus = $model->qty * $product->bonus_value;    
            }
        });
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function memberStock()
    {
        return $this->belongsTo('App\MemberStock');
    }

    public function getTypeNameAttribute()
    {
        return $this->type ? array_flip(static::TYPES)[$this->type] : null;
    }    
}
