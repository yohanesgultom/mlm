<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberType extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'order',
        'join_rules',
        'discount_amount',
        'discount_percentage',
        'company_id',        
    ];

    protected $casts = [
        'join_rules' => 'array'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function packages()
    {
        return $this->hasMany('App\Package');
    }

    public function scopeSameCompany($query)
    {
        return $query->where('company_id', \Auth::user()->company_id);
    }     
    
}
