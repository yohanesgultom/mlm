<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'company_id',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function scopeSameCompany($query)
    {
        return $query->where('company_id', \Auth::user()->company_id);
    }    
}
