<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{

    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image_path',
        'company_id',
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function scopeSameCompany($query)
    {
        return $query->where('company_id', \Auth::user()->company_id);
    }
    
}
