<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NetworkCenterType extends Model
{
    protected $fillable = [
        'name',
        'fee_percentage',
        'company_id',
        'created_by',
        'updated_by',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->created_by = \Auth::id();
        });

        static::updating(function($model)
        {
            $model->updated_by = \Auth::id();
        });

    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function scopeSameCompany($query)
    {
        return $query->where('company_id', \Auth::user()->company_id);
    }
}
