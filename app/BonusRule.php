<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Auditable;

class BonusRule extends Model
{
    use Auditable;

    const CYCLES = [
        'MONTHLY' => 'MONTHLY',
        'WEEKLY' => 'WEEKLY',
    ];

    protected $fillable = [
        'name',
        'type',
        'cycle',
        'variables',
        'disabled',
        'company_id',
    ];

    protected $casts = [
        'variables' => 'array',
        'disabled' => 'boolean',
    ];
    
}
