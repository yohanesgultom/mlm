<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Auditable;

class Order extends Model
{
    use Auditable;

    const STATUSES = [
        'UNPAID' => 0,
        'COMPLETED' => 2,
        'CANCELED' => 3,
    ];    

    protected $appends = ['status_name'];

    protected $fillable = [
        'invoice_no',
        'invoice_date',
        'bill_no',
        'wallet_id',
        'warehouse_id',
        'status',
        'admin_id',
    ];
    
    protected $dates = [
        'invoice_date',
    ];

    public function wallet()
    {
        return $this->belongsTo('App\Wallet');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
    
    public function admin()
    {
        return $this->belongsTo('App\User', 'admin_id');
    }    

    public function scopeSameCompany($query)
    {
        return $this->ofCompany(\Auth::user()->company_id);
    }

    public function scopeOfCompany($query, $company_id)
    {
        return $query
            ->select('orders.*')
            ->join('wallets', 'wallet_id', '=', 'wallets.id')
            ->join('members', 'wallets.member_id', '=', 'members.id')
            ->join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', $company_id);
    }

    public function getStatusNameAttribute()
    {
        return array_flip(static::STATUSES)[$this->status];
    }    
}
