<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    const storage_dir = 'attachments';

    const TYPES = [
        'OTHER' => 0,
        'KTP' => 1,
        'NPWP' => 2,
    ];

    public $fillable = [
        'member_id',
        'desc',
        'type',
        'path',
    ];

    protected $hidden = [
        'path'
    ];

    protected $appends = ['type_name'];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function scopeOwned($query)
    {
        return $query->where('member_id', \Auth::user()->member->id);
    }

    public function scopeOwnedBy($query, $member_id)
    {
        return $query->where('member_id', $member_id);
    }

    public function scopeSameCompany($query)
    {
        return $query
            ->select('attachments.*')
            ->join('members', 'member_id', '=', 'members.id')
            ->join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', \Auth::user()->company_id);
    }    

    public function setFileAttribute($uploadedFile)
    {
        $this->attributes['filename'] = $uploadedFile->getClientOriginalName();
        $this->attributes['mime'] = $uploadedFile->getClientMimeType();
    }

    public function getTypeNameAttribute()
    {
        return array_flip(static::TYPES)[$this->attributes['type']];
    }
}
