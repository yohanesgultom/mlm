<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    const TYPES = [
        'ARTICLE' => 0,
        'NEWS' => 1,
        'TESTIMONIAL' => 2,
        'TIPS' => 3,
    ];

    const STATUSES = [
        'DRAFT' => 0,
        'PUBLISHED' => 1,
    ];
    
    protected $fillable = [
        'title',
        'slug',
        'type',
        'status',
        'html',
        'image_path',
        'publish_date',
        'created_by',
        'updated_by',
        'company_id',
    ];

    protected $dates = [
        'publish_date',
    ];

    protected $appends = ['type_name', 'status_name'];

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->slug = str_slug($model->title);
            $model->created_by = \Auth::id() ?: $model->created_by;
        });

        static::updating(function($model)
        {
            $model->slug = str_slug($model->title);
            $model->updated_by = \Auth::id() ?: $model->updated_by;
        });

    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function scopeSameCompany($query)
    {
        return $query->where('company_id', \Auth::user()->company_id);
    }

    public function scopeOwnedBy($query, $company_id)
    {
        return $query->where('company_id', $company_id);
    }

    public function getTypeNameAttribute()
    {
        return array_flip(static::TYPES)[$this->type];
    }

    public function getStatusNameAttribute()
    {
        return array_flip(static::STATUSES)[$this->status];
    }
}
