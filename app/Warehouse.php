<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    const TYPES = [
        'REGULAR' => 0,
        'CENTRAL' => 1,
    ];

    public $incrementing = false;

    protected $fillable = [
        'id',
        'name',
        'type',
        'address',
        'address_city',
        'address_state',
        'address_zipcode',
        'country_id',
        'company_id',
    ];    
    
    protected $appends = ['type_name'];

    public static function boot()
    {
        parent::boot();

        static::saving(function($model) {            
            $model->type = $model->type ?? static::TYPES['REGULAR'];
        });
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function stocks()
    {
        return $this->hasMany('App\WarehouseStock');
    }

    public function stockLogs()
    {
        return $this->hasMany('App\WarehouseStockLog');
    }

    public function scopeOfCompany($query, $company_id)
    {
        return $query->where('company_id', $company_id);
    }    

    public function scopeSameCompany($query)
    {
        return $query->ofCompany(\Auth::user()->company_id);
    }

    public function scopeCentral($query)
    {
        return $query->where('type', static::TYPES['CENTRAL']);
    }

    public function scopeRegular($query)
    {
        return $query->where('type', static::TYPES['REGULAR']);
    }

    public function getTypeNameAttribute()
    {
        return !empty($this->attributes['type']) ? array_flip(static::TYPES)[$this->attributes['type']] : null;
    }
}
