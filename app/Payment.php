<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const TYPES = [
        'OTHER' => 0,
        'REGISTRATION' => 1,
        'TOPUP' => 2,
    ];

    const STATUSES = [
        'UNPAID' => 0,
        'PAID' => 1,
    ];

    protected $fillable = [
        'type',
        'desc',
        'amount',
        'payment_date',
        'status',
        'member_id',
        'wallet_id',
    ];

    protected $dates = [
        'payment_date',
    ];

    protected $appends = [
        'type_name',
        'status_name',
    ];

    protected $casts = [
        'data' => 'array',
    ];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function getTypeNameAttribute()
    {
        return array_flip(static::TYPES)[$this->attributes['type']];
    }

    public function getStatusNameAttribute()
    {
        return array_flip(static::STATUSES)[$this->attributes['status']];
    }
    
    public function scopeSameCompany($query)
    {
        return $query
            ->select('payments.*')
            ->join('members', 'member_id', '=', 'members.id')
            ->join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', \Auth::user()->company_id);
    }
}
