<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Company;
use App\BonusRule;
use App\MemberType;
use App\Business\MemberService;
use App\Business\AchievementService;
use App\Business\BonusService;
use Log;
use DB;
use Carbon\Carbon;


class CalculateBonus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    public $company_id;
    public $delete_logs;
    public $date_start;
    public $date_end;
    public $process_date;
    public $weekly_date_start;
    public $weekly_date_end;
    public $weekly_process_date;

    public function __construct(
        $company_id=null,
        $delete_logs=true,
        $date_start=null,
        $date_end=null,
        $process_date=null,
        $weekly_date_start=null,
        $weekly_date_end=null,
        $weekly_process_date=null
    )
    {
        $this->company_id = $company_id;
        $this->delete_logs = $delete_logs;
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->process_date = $process_date;
        $this->weekly_date_start = $weekly_date_start;
        $this->weekly_date_end = $weekly_date_end;
        $this->weekly_process_date = $weekly_process_date;
    }

    public function handle()
    {
        if (!empty($this->company_id)) {
            $this->calculate($this->company_id, $this->delete_logs);
        } else {
            $companies = Company::select('id')->get();        
            foreach ($companies as $c) {
                try {
                    $this->calculate($c->id, $this->delete_logs);
                } catch(\Exception $e) {
                    Log::error("Error when calculating bonus for company {$c->id}");
                    Log::error($e->getTraceAsString());
                }
            }    
        }
    }

    public function calculate($company_id, $delete_logs)
    {
        $company = Company::find($company_id);        
        $member_service = new MemberService($company_id);
        $achievement_service = new AchievementService($company_id);

        $start = microtime(true);
        Log::info("Starting calculate bonus job for company id {$company_id}..");
        $dates = BonusService::getDefaultCurrentBonusDates($company);

        // monthly dates
        $date_start = $this->date_start ?? $dates['date_start'];
        $date_end = $this->date_end ?? $dates['date_end'];
        $process_date = $this->process_date ?? $dates['process_date'];

        // weekly dates
        $weekly_date_start = $this->weekly_date_start ?? $dates['weekly_date_start'];
        $weekly_date_end = $this->weekly_date_end ?? $dates['weekly_date_end'];
        $weekly_process_date = $this->weekly_process_date ?? $dates['weekly_process_date'];

        // expose for testing
        $this->process_date = $process_date;
        
        Log::debug([
            '$date_start' => $date_start->toDateTimeString(),
            '$date_end' => $date_end->toDateTimeString(),
            '$process_date' => $process_date->toDateTimeString(),
            '$weekly_date_start' => $weekly_date_start->toDateTimeString(),
            '$weekly_date_end' => $weekly_date_end->toDateTimeString(),
            '$weekly_process_date' => $weekly_process_date->toDateTimeString(),
        ]);

        // delete logs
        if ($delete_logs) {
            Log::info("Deleting achievement and bonus logs with process_date {$process_date->toDateTimeString()}..");
            $this->deleteLogs($company_id, $process_date);    
            Log::info("Deleting pairing counters with process_date {$weekly_process_date->toDateTimeString()}..");
            $this->deleteWeeklyLogs($company_id, $weekly_process_date);
        }

        // update current bonus and points
        Log::info("Updating members current points and bonus from transactions point_date {$date_start->toDateTimeString()} to {$date_end->toDateTimeString()}..");
        $member_service->updateCurrentPointAndBonusValues($date_start, $date_end);

        // update current group bonus and points and personal achievements
        Log::info("Creating achievement logs with process_date {$process_date->toDateTimeString()}..");
        $achievement_service->updateAchievements($process_date);
        
        // calculate MONTHLY bonus
        Log::info("Creating MONTHLY bonus logs with process_date {$process_date->toDateTimeString()}..");
        $cycle = BonusRule::CYCLES['MONTHLY'];
        $bonus_rules = BonusRule::where('company_id', $company_id)->where('cycle', $cycle)->get();
        $bonus_service = new BonusService($bonus_rules, $date_start, $date_end, $process_date);        
        $bonus_service->calculateAll();

        // check if trinary bonus configured
        $trinary_bonus_rule = BonusRule::where('company_id', $company_id)
            ->where('cycle', BonusRule::CYCLES['WEEKLY'])
            ->where('type', '\App\Business\TrinaryBonusCalculator')
            ->select('bonus_rules.*')
            ->first();
        if (!empty($trinary_bonus_rule) && !empty($trinary_bonus_rule->variables['member_type_name'])) {
            $trinary_member_type = MemberType::where('company_id', $company_id)
                ->where('name', $trinary_bonus_rule->variables['member_type_name'])
                ->first();

            if (!empty($trinary_member_type)) {
                // update pairing counters
                // must be after achievement update because
                // close point status affects calculation
                Log::info("Updating members pairing counters for registration date between {$weekly_date_start->toDateTimeString()} to {$weekly_date_end->toDateTimeString()}..");
                $member_service->updatePairingCounters(
                    $trinary_member_type->id,
                    $trinary_bonus_rule->variables['max_pair_count'],
                    $weekly_date_start,
                    $weekly_date_end,
                    $weekly_process_date
                );

                // calculate WEEKLY bonus
                Log::info("Creating WEEKLY bonus logs with process_date {$weekly_process_date->toDateTimeString()}..");
                $cycle = BonusRule::CYCLES['WEEKLY'];
                $bonus_rules = BonusRule::where('company_id', $company_id)->where('cycle', $cycle)->get();
                $bonus_service = new BonusService($bonus_rules, $weekly_date_start, $weekly_date_end, $weekly_process_date);
                $bonus_service->calculateAll();        
            }
        }

        $duration = microtime(true) - $start;
        Log::info("Completed calculate bonus job for company id {$company_id} in {$duration} s");
    }

    /**
     * Delete achievments and bonus logs
     * This method is not working on SQLite3 due to 
     * the lack of support for <code>delete ... from ... join</code>
     */
    public function deleteLogs($company_id, $process_date)
    {
        DB::table('achievement_logs')
            ->join('members', 'members.id', 'achievement_logs.member_id')
            ->join('users', function ($join) use ($company_id) {
                $join->on('users.id', 'members.user_id')->where('users.company_id', $company_id);
            })
            ->where('achievement_logs.process_date', $process_date->toDateTimeString())
            ->delete();

        DB::table('bonus_logs')
            ->join('members', 'members.id', 'bonus_logs.member_id')
            ->join('users', function ($join) use ($company_id) {
                $join->on('users.id', 'members.user_id')->where('users.company_id', $company_id);
            })
            ->where('bonus_logs.process_date', $process_date->toDateTimeString())
            ->delete();
    }

    public function deleteWeeklyLogs($company_id, $process_date)
    {
        DB::table('pairing_counters')
            ->join('members', 'members.id', 'pairing_counters.member_id')
            ->join('users', function ($join) use ($company_id) {
                $join->on('users.id', 'members.user_id')->where('users.company_id', $company_id);
            })
            ->where('pairing_counters.process_date', $process_date->toDateTimeString())
            ->delete();
    }
}
