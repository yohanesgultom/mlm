<?php 

namespace App\Business;

use App\Member;
use App\Business\MemberService;

class LeadershipBonusCalculator extends BonusCalculator
{
    /**
     * Get total LDB for each downline generations
     * 
     * @param Array $downlines direct (1st) generation downlines
     * @param Array $downline_bonus_logs of direct (1st) generation downlines bonus_log
     * @param Integer $min_order minimum achievement order of a leader
     * @return Array Total LDB per generations sorted ascending from direct (1st) generation
     */
    public function getMatchingLDB($downlines, $downline_bonus_logs, $min_order)
    {
        $matching_ldb = [];
        $direct_matching_ldb = 0;
        if (!empty($downlines)) {
            if (count($downlines) != count($downline_bonus_logs)) {
                throw new \Exception("downlines and bonus_downline_bonus_logs have different length");
            }
            for ($i = 0; $i < count($downlines); $i++) {
                // get downline's matching ldb
                $indirect_matching_ldb = $downline_bonus_logs->get($i)['data']['matching_ldb'];
                if (count($matching_ldb) <= 0) {
                    $matching_ldb = $indirect_matching_ldb;
                } else {
                    // sum indirect downline's matching ldb
                    for ($j = 0; $j < count($indirect_matching_ldb); $j++) {
                        // handle uneven tree
                        if (count($matching_ldb) <= $j) {
                            $matching_ldb[] = $indirect_matching_ldb[$j];
                        } else {
                            $matching_ldb[$j] += $indirect_matching_ldb[$j];
                        }                        
                    }
                }
    
                // count this downline only when his/her achievement
                // is equal or higher than minimum achievement
                if ($downlines->get($i)->achievement->order >= $min_order) {
                    $direct_matching_ldb += $downline_bonus_logs->get($i)['details']['leadership_development_bonus'];
                }            
            }    
        }
        // prepend count to array
        array_unshift($matching_ldb, $direct_matching_ldb);
        return $matching_ldb;
    }

    public function sortVariables()
    {        
        foreach ($this->variables_list as $key => $val) {
            for ($i = 0; $i < count($val); $i++) {
                $achievement = $val[$i]['achievement_type'];
                $this->variables_list[$key][$i]['min_achievement_order'] = $this->achievement_orders[$achievement];
            }
    
            // sort desc by achievment order
            usort($this->variables_list[$key], function($a, $b) {
                return $a['min_achievement_order'] > $b['min_achievement_order'] ? -1 : 1;
            });   
        }
    }
    
    public function getMatchedVariables(Member $member)
    {
        $variables = [];
        $this->sortVariables();
        foreach ($this->variables_list as $key => $val) {
            foreach ($val as $vars) {
                if ($member->achievement->order >= $vars['min_achievement_order']) {
                    $variables[$key] = $vars;
                    break;
                }
            }
        }        
        return $variables;
    }

    /**
     * Calculate leadership development bonus (LDB) for given member
     */
    public function calculateLDB($development_vars, $member, $downlines, $bonus_logs)
    {
        $leadership_development_bonus = 0;
        $ldb_claimables = [];
        $logs = collect([]);        
        $royalty_pct = $development_vars['royalty_pct'] ?? 2;
        for ($i = 0; $i < count($downlines); $i++) {                    
            
            // claim personal bv
            $downline = $downlines[$i];
            $bonus_from_personal = round($development_vars['bonus_pct'] * 0.01 * $downline->current_personal_bonus);
            $log = "leadership_development_bonus {$downline->id} = {$development_vars['bonus_pct']} * 0.01 * {$downline->current_personal_bonus}";
            if (!array_key_exists($development_vars['bonus_pct'], $ldb_claimables)) {
                $ldb_claimables[$development_vars['bonus_pct']] = 0;
            }
            $ldb_claimables[$development_vars['bonus_pct']] += $downline->current_personal_bonus;
            
            // get claimable ldb map
            $bonus_from_group = 0;
            if (array_key_exists('ldb_claimables', $bonus_logs[$i]['data'])) {
                foreach ($bonus_logs[$i]['data']['ldb_claimables'] as $bonus_pct => $gbv) {
                    if ($development_vars['bonus_pct'] < $bonus_pct) {
                        // can not claim
                        if (!array_key_exists($bonus_pct, $ldb_claimables)) {
                            $ldb_claimables[$bonus_pct] = 0;
                        }
                        $ldb_claimables[$bonus_pct] += $gbv;
                    } else if ($development_vars['bonus_pct'] == $bonus_pct) {
                        // claim royalty point
                        $bonus_from_group += $royalty_pct * 0.01 * $gbv;
                        $log .= " + ({$royalty_pct} * 0.01 * {$gbv})";
                        $ldb_claimables[$development_vars['bonus_pct']] += $gbv;
                    } else { // $development_vars['bonus_pct'] > $bonus_pct
                        $bonus_from_group += ($development_vars['bonus_pct'] - $bonus_pct) * 0.01 * $gbv;
                        $log .= " + (({$development_vars['bonus_pct']} - {$bonus_pct}) * 0.01 * {$gbv})";
                        $ldb_claimables[$development_vars['bonus_pct']] += $gbv;
                    }
                }
            }
            $bonus = $bonus_from_personal + $bonus_from_group;
            $leadership_development_bonus += $bonus;
            $logs->push("{$log} = {$bonus}");
        }

        return [
            'leadership_development_bonus' => $leadership_development_bonus,
            'ldb_claimables' => $ldb_claimables,
            'logs' => $logs,
        ];
    }

    /**
     * Calculate leadership matching bonus (LMB) for given member
     */
    public function calculateLMB($matching_vars, $member, $downlines, $bonus_logs)
    {
        $leadership_matching_bonus = 0;
        $logs = collect([]);
        $matching_rules = $this->variables_list['matching'];
        $min_order = $matching_rules[count($matching_rules) - 1]['min_achievement_order'];
        // get count of leaders per generation
        $matching_ldb = $this->getMatchingLDB($downlines, $bonus_logs, $min_order);                
        // iterate bonus percentage from 1st generation
        $bonus_pct = $matching_vars['bonus_pct'];
        // get min value to avoid index error
        $min_count = min(count($bonus_pct), count($matching_ldb));                    
        for ($i = 0; $i < $min_count; $i++) {
            $bonus = $bonus_pct[$i] * 0.01 * $matching_ldb[$i];
            $leadership_matching_bonus += $bonus;
            $gen = $i + 1;
            $logs->push("leadership_matching_bonus G{$gen} = {$bonus_pct[$i]} * 0.01 * {$matching_ldb[$i]} = {$bonus}");
        }
        return [
            'leadership_matching_bonus' => $leadership_matching_bonus,
            'matching_ldb' => $matching_ldb,
            'logs' => $logs,
        ];
    }

    public function calculate(Member $member, $downlines = null, $bonus_logs = null)
    {
        $logs = collect([]);
        $leadership_development_bonus = 0;
        $leadership_matching_bonus = 0;
        $matching_ldb = [];
        $ldb_claimables = [];

        try {
            if (!empty($member->achievement)) {
                $variables = $this->getMatchedVariables($member);        
                if (empty($variables) || (!array_key_exists('development', $variables) && !array_key_exists('matching', $variables))) {
                    $logs->push("does not match any rules");
                } else {
                    // only when downlines available
                    if (!empty($bonus_logs) && !empty($downlines) && count($downlines) > 0) {
                        if (count($bonus_logs) != count($downlines)) {
                            throw new \Exception("downlines and bonus_logs have different length");
                        }

                        // leadership development bonus (LDB)
                        $development_vars = $variables['development'];
                        $res = $this->calculateLDB($development_vars, $member, $downlines, $bonus_logs);
                        $leadership_development_bonus = $res['leadership_development_bonus'];
                        $ldb_claimables = $res['ldb_claimables'];
                        $logs = $logs->concat($res['logs']);

                        // leadership matching bonus  
                        // can only be calculated if LDB variables also exist
                        if (array_key_exists('matching', $variables)) {
                            $matching_vars = $variables['matching'];
                            $res = $this->calculateLMB($matching_vars, $member, $downlines, $bonus_logs);
                            $leadership_matching_bonus = $res['leadership_matching_bonus'];
                            $matching_ldb = $res['matching_ldb'];
                            $logs = $logs->concat($res['logs']);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $name = get_class($this);
            \Log::error("{$name} error for member_id {$member->id}: {$e->getMessage()}", $e->getTrace());
        }

        return [            
            'logs' => $logs->toArray(),
            'details' => [
                'leadership_development_bonus' => $leadership_development_bonus,
                'leadership_matching_bonus' => $leadership_matching_bonus,
            ],
            'pass_up' => 0,
            'data' => [
                'matching_ldb' => $matching_ldb,
                'ldb_claimables' => $ldb_claimables,
            ],
            'total' => $leadership_development_bonus + $leadership_matching_bonus,
        ];
    }
}