<?php 

namespace App\Business;

use App\Member;
use App\Business\MemberService;

class DiscountBonusCalculator extends BonusCalculator
{
    public function sortVariables()
    {        
        for ($i = 0; $i < count($this->variables_list); $i++) {
            $vars = $this->variables_list[$i];
            $this->variables_list[$i]['min_member_type_order'] = $this->member_type_orders[$vars['min_member_type']];
            $this->variables_list[$i]['min_achievement_order'] = $this->achievement_orders[$vars['min_achievement']];
        }

        // sort desc by member_type then achievment
        usort($this->variables_list, function($a, $b) {
            if ($a['min_member_type_order'] == $b['min_member_type_order']) {
                return $a['min_achievement_order'] > $b['min_achievement_order'] ? -1 : 1;                
            } else {
                return $a['min_member_type_order'] > $b['min_member_type_order'] ? -1 : 1;
            }            
        });
    }

    public function getMatchedVariables(Member $member)
    {
        $variables = null;
        $this->sortVariables();
        foreach ($this->variables_list as $vars) {
            if ($member->member_type->order >= $vars['min_member_type_order'] 
                && $member->achievement->order >= $vars['min_achievement_order']) {
                    $variables = $vars;
                    break;
                }
        }
        return $variables;
    }

    public function calculate(Member $member, $downlines = null, $bonus_logs = null)
    {
        $discount_bonus_personal = 0;
        $discount_bonus_group = 0;
        $logs = collect([]);

        try {
            if (!empty($member->member_type) && !empty($member->achievement)) {
                $variables = $this->getMatchedVariables($member);
                if (!empty($variables)) {
                    $discount_bonus_personal = $member->current_personal_bonus * $variables['personal_bv_pct'] / 100;
                    $logs->push("discount_bonus_personal = {$member->current_personal_bonus} * {$variables['personal_bv_pct']} / 100 = {$discount_bonus_personal}");
                    if (array_key_exists('sponsored_bv_pct', $variables)) {
                        $direct_downlines = MemberService::getDirectDownlines($member->id, 'sponsor_id');
                        if (!empty($direct_downlines)) {
                            $direct_downlines_bv = 0;
                            foreach ($direct_downlines as $d) {
                                $direct_downlines_bv += $d->current_personal_bonus;
                            }
                            $discount_bonus_group = $direct_downlines_bv * $variables['sponsored_bv_pct'] / 100;
                            $logs->push("discount_bonus_group = {$direct_downlines_bv} * {$variables['sponsored_bv_pct']} / 100 = {$discount_bonus_group}");
                        }    
                    }
                } else {
                    $logs->push("does not match any rules");
                }        
            }
        } catch (\Exception $e) {
            $name = get_class($this);
            \Log::error("{$name} error for member_id {$member->id}: {$e->getMessage()}", $e->getTrace());
        }

        return [            
            'logs' => $logs->toArray(),
            'details' => [
                'discount_bonus' => $discount_bonus_group + $discount_bonus_personal,
            ], 
            'pass_up' => $discount_bonus_group,
            'data' => [],
            'total' => $discount_bonus_group + $discount_bonus_personal
        ];
    }
}