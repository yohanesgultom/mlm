<?php 

namespace App\Business;

use App\Member;
use App\Business\MemberService;
use Carbon\Carbon;
use DB;

class FirstSalesBonusCalculator extends BonusCalculator
{
    public function sortVariables()
    {        
        for ($i = 0; $i < count($this->variables_list); $i++) {
            $vars = $this->variables_list[$i];
            $this->variables_list[$i]['min_member_type_order'] = $this->member_type_orders[$vars['min_member_type']];
        }

        // sort desc by member_type
        usort($this->variables_list, function($a, $b) {
            return $a['min_member_type_order'] > $b['min_member_type_order'] ? -1 : 1;            
        });
    }

    public function getMatchedVariables(Member $member)
    {
        $variables = null;
        $this->sortVariables();
        foreach ($this->variables_list as $vars) {
            if ($member->member_type->order >= $vars['min_member_type_order']) {
                $variables = $vars;
                break;
            }
        }
        return $variables;
    }

    public function calculate(Member $member, $downlines = null, $bonus_logs = null)
    {
        $logs = collect([]);
        $variables = [];
        $total = 0;

        try {
            if (!empty($member->member_type) && !empty($member->achievement)) {
                $variables = $this->getMatchedVariables($member);
                if (!empty($variables)) {
                    $first_sales = $this->getFirstSales($member->id, $variables['sponsored_member_type'], $this->date_start, $this->date_end);
                    if (count($first_sales) <= 0) {
                        $logs->push("first_sales_bonus = no first sales");
                    }
                    foreach ($first_sales as $fs) {
                        $fsb = $fs->reg_bv * $variables['join_factor'];
                        $base_bv = $fsb < $variables['max_bv'] ? $variables['base_bv'] : 0;
                        $fsb = $fsb + $base_bv;
                        $total += $fsb;
                        $logs->push("first_sales_bonus {$fs->id} = {$fs->reg_bv} * {$variables['join_factor']} + {$base_bv} = {$fsb}");
                    }            
                } else {
                    $logs->push("first_sales_bonus = does not match any rules");
                } 
            }
        } catch (\Exception $e) {
            $name = get_class($this);
            \Log::error("{$name} error for member_id {$member->id}: {$e->getMessage()}", $e->getTrace());
        }

        return [            
            'logs' => $logs->toArray(),
            'details' => [
                'first_sales_bonus' => $total,
            ],
            'pass_up' => $total,
            'data' => [],
            'total' => $total,
        ];
    }

    public function getFirstSales($sponsor_id, $sponsored_member_type, $date_start, $date_end)
    {
        $date_start = Carbon::instance($date_start);
        $date_end = Carbon::instance($date_end);
        return DB::table('members')
            ->select('members.id', 'users.name', 'members.reg_bv')
            ->join('users', 'users.id', 'members.user_id')
            ->join('member_types', 'member_types.id', 'members.member_type_id')
            ->where('members.sponsor_id', $sponsor_id)
            ->where('members.reg_date', '>=', $date_start)
            ->where('members.reg_date', '<=', $date_end)
            ->where('member_types.name', $sponsored_member_type)
            ->get();
    }
}