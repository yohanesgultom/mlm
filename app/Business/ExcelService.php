<?php

namespace App\Business;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;
use App\BonusLog;
use App\Order;
use App\Transaction;
use App\Package;

class ExcelService
{
    public static function safeVal($a, $key, $default)
    {
        return array_key_exists($key, $a) ? $a[$key] : $default;
    }

    public static function getBonusReportPath($company_id, Carbon $process_date)
    {
        // get data
        $bonus_logs = BonusLog::join('members', 'bonus_logs.member_id', 'members.id')
            ->join('users', 'members.user_id', 'users.id')
            ->join('achievements', 'achievements.id', 'members.achievement_id')
            ->where('users.company_id', $company_id)
            ->where('bonus_logs.process_date', $process_date)
            ->select(
                'members.id as member_id',
                'users.name as member_name',
                'members.tax_id as npwp',
                'achievements.code as achievement_code',
                'bonus_logs.bank_name',
                'bonus_logs.bank_account_no',
                'bonus_logs.bank_account_name',
                'bonus_logs.bank_account_city',
                'bonus_logs.bank_account_branch',
                'bonus_logs.total',
                'bonus_logs.details'
            )
            ->orderBy('members.id')
            ->get();

        // create spreadsheet
        $app_name = config('app.name');
        $now = Carbon::now();        
        $title = "{$app_name} Bonus Report {$process_date->format('Ymd')}";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
            ->setCreator($app_name)
            ->setTitle($title)
            ->setDescription($title);
        $sheet = $spreadsheet->setActiveSheetIndex(0);

        // header
        $col = 1;        
        foreach ([
            'Member ID',
            'Nama',
            'Achievement',
            'Discount Bonus',
            'First Sales Bonus', 
            'Achievement Share Bonus',
            'Leadership Matching Bonus',
            'Leadership Development Bonus',
            'Total',
            'NPWP',
            'Nama Rekening Bank',
            'Nama Bank',
            'No Rekening Bank',
            'Cabang Bank',
            'Kota Bank',
        ] as $val) {
        $sheet->setCellValueByColumnAndRow($col, 1, $val)
            ->getStyleByColumnAndRow($col++, 1)
            ->getFont()
            ->setBold(true);
        }

        // write data
        $row = 2;
        foreach ($bonus_logs as $i => $b) {
            $col = 1;
            $sheet->setCellValueByColumnAndRow($col++, $row, $b->member_id);
            $sheet->setCellValueByColumnAndRow($col++, $row, $b->member_name);
            $sheet->setCellValueByColumnAndRow($col++, $row, $b->achievement_code);
            $sheet->setCellValueByColumnAndRow($col, $row, static::safeVal($b->details, 'discount_bonus', 0))
                ->getStyleByColumnAndRow($col++, $row)->getNumberFormat()->setFormatCode('#,##0');
            $sheet->setCellValueByColumnAndRow($col, $row, static::safeVal($b->details, 'first_sales_bonus', 0))
                ->getStyleByColumnAndRow($col++, $row)->getNumberFormat()->setFormatCode('#,##0');
            $sheet->setCellValueByColumnAndRow($col, $row, static::safeVal($b->details, 'achievement_share_bonus', 0))
                ->getStyleByColumnAndRow($col++, $row)->getNumberFormat()->setFormatCode('#,##0');
            $sheet->setCellValueByColumnAndRow($col, $row, static::safeVal($b->details, 'leadership_matching_bonus', 0))
                ->getStyleByColumnAndRow($col++, $row)->getNumberFormat()->setFormatCode('#,##0');
            $sheet->setCellValueByColumnAndRow($col, $row, static::safeVal($b->details, 'leadership_development_bonus', 0))
                ->getStyleByColumnAndRow($col++, $row)->getNumberFormat()->setFormatCode('#,##0');
            $sheet->setCellValueByColumnAndRow($col, $row, $b->total)
                ->getStyleByColumnAndRow($col++, $row)->getNumberFormat()->setFormatCode('#,##0');
            $sheet->setCellValueByColumnAndRow($col++, $row, $b->npwp ?? '');
            $sheet->setCellValueByColumnAndRow($col++, $row, $b->bank_account_name ?? '');
            $sheet->setCellValueByColumnAndRow($col++, $row, $b->bank_name ?? '');
            $sheet->setCellValueByColumnAndRow($col++, $row, $b->bank_account_no ?? '');
            $sheet->setCellValueByColumnAndRow($col++, $row, $b->bank_account_branch ?? '');
            $sheet->setCellValueByColumnAndRow($col++, $row, $b->bank_account_city ?? '');
            $row++;
        }

        // set column size
        for ($i = 1; $i <= $col; $i++) {
            $sheet->getColumnDimensionByColumn($i)->setAutoSize(true);
        }

        // save as
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $path = base_path("/tmp/bonus_report_{$process_date->format('Ymd')}.xlsx");
        $writer->save($path);
        return $path;
    }

    public static function getTransactionsReportPath($company_id, Carbon $start_date, Carbon $end_date)
    {
        $transactions = Transaction::join('orders', 'orders.id', 'transactions.order_id')
            ->join('wallets', 'wallets.id', 'orders.wallet_id')
            ->join('members', 'members.id', 'wallets.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->leftJoin('users as admins', 'admins.id', 'orders.admin_id')
            ->join('products', 'products.id', 'transactions.product_id')
            ->where('orders.status', Order::STATUSES['COMPLETED'])
            ->where('transactions.transferred', false)
            ->where('users.company_id', $company_id)
            ->whereBetween('orders.created_at', [$start_date, $end_date])
            ->select(
               'transactions.id', 
               'transactions.transfer_from_member_id', 
               'transactions.qty', 
               'transactions.amount',
               'transactions.points',
               'transactions.bonus',
               'transactions.process_date',
               'orders.id as order_id', 
               'orders.warehouse_id', 
               'orders.invoice_no', 
               'orders.status',
               'orders.created_at',
               'products.id as product_id', 
               'products.name as product_name', 
               'products.base_price as product_price', 
               'members.id as member_id', 
               'users.name as member_name', 
               'admins.username as admin_name'
            )
            ->orderBy('orders.created_at')
            ->get();

        $packages_map = Package::join('package_product', 'package_product.package_id', 'packages.id')
            ->pluck('packages.name', 'package_product.product_id')
            ->all();

        // create spreadsheet
        $app_name = config('app.name');
        $now = Carbon::now();        
        $title = "{$app_name} Transactions Report {$start_date->format('Ymd')} {$end_date->format('Ymd')}";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
            ->setCreator($app_name)
            ->setTitle($title)
            ->setDescription($title);
        $sheet = $spreadsheet->setActiveSheetIndex(0);

        // header
        $col = 1;        
        foreach ([
            'Order ID',
            'No Nota',
            'Transaction ID',
            'Member ID',
            'Nama Member', 
            'Transferred',
            'Transferred From',
            'Warehouse ID',
            'Product ID',
            'Nama Produk',
            'Qty',
            'Harga',
            'Subtotal',
            'PV',
            'Join Package',
            'Order Status',
            'Tanggal Proses',
            'Tanggal Pembuatan',
            'Username',
        ] as $val) {
        $sheet->setCellValueByColumnAndRow($col, 1, $val)
            ->getStyleByColumnAndRow($col++, 1)
            ->getFont()
            ->setBold(true);
        }

        // write data
        $row = 2;
        $order_status_map = array_flip(Order::STATUSES);
        foreach ($transactions as $i => $t) {
            $col = 1;
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->order_id); // 'Order ID',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->invoice_no); // 'No Nota',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->id); // 'Transaction ID',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->member_id); // 'Member ID',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->member_name); // 'Nama Member', 
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->transfer_from_member_id ? 'Y' : 'N'); // 'Transferred',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->transfer_from_member_id); // 'Transferred From',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->warehouse_id); // 'Warehouse ID',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->product_id); // 'Product ID',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->product_name); // 'Nama Produk',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->qty); // 'Qty',
            $sheet->setCellValueByColumnAndRow($col, $row, $t->product_price)
                ->getStyleByColumnAndRow($col++, $row)->getNumberFormat()->setFormatCode('#,##0'); // 'Harga',
            $sheet->setCellValueByColumnAndRow($col, $row, $t->amount)
                ->getStyleByColumnAndRow($col++, $row)->getNumberFormat()->setFormatCode('#,##0'); // 'Subtotal',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->points); // 'PV',
            $sheet->setCellValueByColumnAndRow($col++, $row, array_key_exists($t->product_id, $packages_map) ? $packages_map[$t->product_id] : null); // 'Join Package',
            $sheet->setCellValueByColumnAndRow($col++, $row, $order_status_map[$t->status]); // 'Order Status',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->process_date); // 'Tanggal Proses',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->created_at->toDateTimeString()); // 'Tanggal Pembuatan',
            $sheet->setCellValueByColumnAndRow($col++, $row, $t->admin_name); // 'Username',
            $row++;
        }

        // set column size
        for ($i = 1; $i <= $col; $i++) {
            $sheet->getColumnDimensionByColumn($i)->setAutoSize(true);
        }

        // save as
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $path = base_path("/tmp/transactions_report_{$start_date->format('Ymd')}_{$end_date->format('Ymd')}.xlsx");
        $writer->save($path);
        return $path;

    }
}
