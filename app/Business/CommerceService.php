<?php
namespace App\Business;

use Carbon\Carbon;
use App\Product;
use App\MemberStock;
use App\Warehouse;
use App\WarehouseStock;
use App\WarehouseStockLog;

class CommerceService
{   
    /**
     * Validate and (optionally) filter transaction fields
     */
    public static function validateTransactions($transactions, $wallet, $warehouse_id, $filter_fields = false)
    {
        // collect data and filter transaction fields
        $product_ids = [];
        foreach ($transactions as $id => $t) {
            if ($t['qty'] <= 0) {
                throw new \Exception('Transaction quantity must be > 0');
            }
            // get allowed fields only
            if ($filter_fields) {
                $transactions[$id] = array_intersect_key($t, array_flip(['id', 'product_id', 'qty']));
            }
            $product_ids[] = $t['product_id'];
        }       

        // validate
        if (count($product_ids) <= 0) {
            throw new \Exception('No product purchased');
        } else if (array_unique($product_ids) != $product_ids) {
            throw new \Exception('Product ids not unique');
        } else {
            static::validateBalance($product_ids, $transactions, $wallet->balance);
            static::validateWarehouseStock($product_ids, $transactions, $warehouse_id);
        }

        // valid transactions
        return $transactions;
    }    

    /**
     * validate total against wallet balance
     */
    public static function validateBalance($product_ids, $transactions, $balance)
    {
        $products = Product::whereIn('id', $product_ids)->get();
        $total = 0;
        foreach ($transactions as $t) {
            $p = $products->where('id', $t['product_id'])->first();
            $subtotal = $p->base_price * $t['qty'];
            $total += $subtotal;
        }
        if ($balance < $total) {
            throw new \Exception('Insufficient wallet balance');
        }
    }

    /**
     * validate against central stock
     */
    public static function validateWarehouseStock($product_ids, $transactions, $warehouse_id)
    {
        $query = WarehouseStock::sameCompany();
        $query = empty($warehouse_id) ? $query->central($product_ids) : $query->where('warehouse_id', $warehouse_id);
        $stocks = $query->where('warehouse_stocks.available', '>', 0)
            ->orderBy('warehouse_stocks.product_id', 'asc')
            ->orderBy('warehouse_stocks.exp_date', 'asc')
            ->get();

        $unavailables = array_filter($transactions, function ($t) use ($stocks) {            
            $stock = static::getStockToSubtract($stocks, $t['product_id'], $t['qty']);
            return empty($stock);
        });

        if (count($unavailables) > 0) {
            $warehouse_id = $warehouse_id ?? 'central';
            throw new \Exception('Insufficient stock in warehouse '.$warehouse_id.' for transactions: '.json_encode($unavailables));
        }
    }

    /**
     * Validate delivery items
     */
    public static function validateDeliveryItems($delivery_items, $member_id)
    {
        // merge delivery items qty by product
        $item_map = [];
        foreach ($delivery_items as $id => $item) {
            if ($item['qty'] <= 0) {
                throw new \Exception('Delivery item quantity must be > 0');
            }
            if (!array_key_exists($item['product_id'], $item_map)) {
                $item_map[$item['product_id']] = 0;
            }
            $item_map[$item['product_id']] += $item['qty'];

            // get allowed fields only
            $delivery_items[$id] = array_intersect_key($item, array_flip(['id', 'product_id',  'qty']));
        }
        
        // compare with available stocks
        $stocks = MemberStock::where('member_id', $member_id)
            ->where('qty', '>', 0)
            ->groupBy('product_id')
            ->selectRaw('product_id, sum(qty) qty')->get();
        foreach ($item_map as $product_id => $qty) {
            $stock = $stocks->where('product_id', $product_id)->first();
            if (empty($stock) || $stock->qty < $qty) {
                throw new \Exception('Insufficient stock for product_id: '.$product_id);
            }    
        }
        return $delivery_items;
    }    

    public static function subtractBalanceAndAddStocks($order, $wallet)
    {
        // substract sum from wallet
        $order->load('transactions'); 
        $sum = $order->transactions()->sum('amount');
        $wallet->balance -= $sum;
        $wallet->save();

        $product_ids = $order->transactions->pluck('product_id')->all();
        $warehouse_stocks = collect([]);
        if (empty($order->warehouse_id)) {
            // get central stocks        
            $warehouse_stocks = WarehouseStock::sameCompany()->central($product_ids)->get();
        } else {
            $warehouse_stocks = WarehouseStock::sameCompany()->regular($order->warehouse_id, $product_ids)->get();
        }

        // update warehouse and member stock
        foreach ($order->transactions as $t) {    
            $warehouse_stock = static::getStockToSubtract($warehouse_stocks, $t->product_id, $t->qty);
            
            // recheck warehouse stock
            if (empty($warehouse_stock)) {
                throw new \Exception("Insufficient stock in central for product_id {$t->product_id} exp_date {$t->exp_date}");
            }

            // create member stock if indirect sale
            if (empty($warehouse_id)) {
                $member_stock = MemberStock::create([
                    'qty' => $t->qty,
                    'exp_date' => $warehouse_stock->exp_date,
                    'product_id' => $t->product_id,
                    'member_id' => $wallet->member_id,
                ]);    
                // for refund reference
                $t->member_stock_id = $member_stock->id;
            }

            // for refund reference
            $t->exp_date = $warehouse_stock->exp_date;
            $t->save();            

            // creating log automatically updates stock
            WarehouseStockLog::create([
                'warehouse_id' => $warehouse_stock->warehouse_id,
                'product_id' => $warehouse_stock->product_id,
                'process_date' => Carbon::now(),
                'qty_before' => $warehouse_stock->qty,
                'qty_in' => 0,
                'qty_out' => 0,
                'available_before' => $warehouse_stock->available,
                'available_in' => 0,
                'available_out' => $t->qty, // reduce availability
                'exp_date' => $warehouse_stock->exp_date,
                'member_stock_id' => $t->member_stock_id,
            ]);

        }
    }

    public static function refundBalanceAndRemoveStocks($order, $wallet)
    {
        // refund to wallet
        $sum = $order->transactions()->sum('amount');
        $wallet->balance += $sum;
        $wallet->save();

        // get warehouse stocks
        $product_ids = $order->transactions->pluck('product_id')->all();
        $warehouse_stocks = collect([]);
        if (empty($order->warehouse_id)) {
            // get central stocks        
            $warehouse_stocks = WarehouseStock::sameCompany()->central($product_ids)->get();
        } else {
            $warehouse_stocks = WarehouseStock::sameCompany()->regular($order->warehouse_id, $product_ids)->get();
        }

        // restore stocks
        foreach ($order->transactions as $t) {
            // find stock with same product_id and exp_date
            // (expected to be unique)
            $warehouse_stock = $warehouse_stocks
                ->where('product_id', $t->product_id)
                ->where('exp_date', $t->exp_date)
                ->first();
                            
            // add central stock
            // creating log automatically update stock
            WarehouseStockLog::create([
                'warehouse_id' => $warehouse_stock->warehouse_id,
                'product_id' => $warehouse_stock->product_id,
                'process_date' => Carbon::now(),
                'qty_before' => $warehouse_stock->qty,
                'qty_in' => 0,
                'qty_out' => 0,
                'available_before' => $warehouse_stock->available,
                'available_in' => $t->qty, // restore available
                'available_out' => 0,
                'exp_date' => $warehouse_stock->exp_date,
            ]);
            
            // remove member stock
            if (!empty($t->member_stock_id)) {
                $t->member_stock_id = null;
                $t->save();            
                MemberStock::destroy($t->member_stock_id);    
            }
        }

    }    

    /**
     * Reduce member stock's quantity on delivery
     */
    public static function reduceStockOnDelivery($delivery_items, $member_id, $company_id)
    {
        $product_ids = array_map(function ($item) { return $item['product_id']; }, $delivery_items);

        // load matching stocks
        $warehouse_stocks = static::getCentralStockAvailability($product_ids, $company_id);
        $member_stocks = MemberStock::where('member_id', $member_id)
            ->whereIn('product_id', $product_ids)
            ->where('qty', '>', 0)
            ->orderBy('exp_date', 'asc')
            ->orderBy('qty', 'asc')
            ->get();

        foreach ($delivery_items as $item) {
            // filter stocks
            $ware_stocks = $warehouse_stocks->where('product_id', $item['product_id']);
            $stocks = $member_stocks->where('product_id', $item['product_id']);

            if (empty($stocks) || $item['qty'] > $stocks->sum('qty')) {
                throw new \Exception('Insufficient member stock for product_id: '.$item['product_id']);
            }

            if (empty($ware_stocks) || $item['qty'] > $ware_stocks->sum('qty')) {                
                throw new \Exception('Insufficient warehouse stock for product_id: '.$item['product_id']);
            }

            // update stocks
            $qty = $item['qty'];                    
            foreach ($stocks as $s) {                
                if ($qty <= 0) break;
                
                $qty_taken = 0;
                if ($qty > $s->qty) {
                    $qty_taken = $s->qty;
                    $qty -= $s->qty;
                    $s->qty = 0;
                } else {
                    $qty_taken = $qty;
                    $qty = 0;
                    $s->qty -= $qty;
                }
                $w_s = $ware_stocks->where('exp_date', $s->exp_date)->where('qty', '>', $qty_taken)->first();
                if (empty($w_s)) {
                    throw new \Exception('Insufficient warehouse stock for product_id: '.$item['product_id']);
                }
                $w_s->qty -= $qty_taken;
                $s->save();
                $w_s->save();
            }
        }
    }

    /**
     * Get stock from collection to sell based on exp_date and availability
     */
    public static function getStockToSubtract($stocks, $product_id, $qty, $exp_date = null)
    {
        $filtered = $stocks->where('product_id', $product_id);
        // if exp_date provided use it to filter
        if (!empty($exp_date)) {            
            $filtered = $filtered->where('exp_date', $exp_date);
        }
        // find available stock with earliest exp_date
        return $filtered
            ->where('available', '>=', $qty)
            ->sortBy('exp_date')
            ->first();
    }

    /**
     * Get stock from database to sell based on exp_date and availability
     */
    public static function getStockToSubtractFromDatabase($warehouse_id, $product_id, $qty, $exp_date = null)
    {
        $query = WarehouseStock::where('warehouse_id', $warehouse_id)
            ->where('product_id', $product_id);
        if (!empty($exp_date)) {
            $query = $query->where('exp_date', $exp_date);
        }
        // find available stock with earliest exp_date
        return $query
            ->where('available', '>=', $qty)
            ->orderBy('exp_date')
            ->first();
    }

    /**
     * Get central warehouse stock availability
     * for given product ids
     */
    public static function getCentralStockAvailability($product_ids, $company_id)
    {
        return WarehouseStock::ofCompany($company_id)
            ->central($product_ids)
            ->orderBy('warehouse_stocks.product_id', 'asc')
            ->orderBy('warehouse_stocks.exp_date', 'asc')
            ->get();
    }

    /**
     * Validate warehouse_id in order input
     */
    public static function validateOrderWarehouse($input)
    {
        if (array_key_exists('warehouse_id', $input) && !empty($input['warehouse_id'])) {
            $warehouse = Warehouse::sameCompany()->find($input['warehouse_id']);
            if (!$warehouse) {
                throw new \Exception('Invalid warehouse_id '.$input['warehouse_id']);    
            }
            if ($warehouse->type != Warehouse::TYPES['REGULAR']) {                
                // threat as indirect sale
                $input['warehouse_id'] = null;
            } // else, threat as direct sale
        } else {
            $input['warehouse_id'] = null;
        }
        return $input;
    }
}