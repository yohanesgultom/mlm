<?php

namespace App\Business;

use App\Member;

abstract class BonusCalculator
{
    /**
     * Company that gives the bonus
     */    
    public $company_id;

    /**
     * Variables list of a bonus rule
     */
    public $variables_list;

    /**
     * Map of member type code with order as key
     */
    public $member_type_orders;

    /**
     * Map of achievement code with order as key
     */
    public $achievement_orders;

    /**
     * Earliest reg_date for first sales bonus
     */
    public $date_start;

    /**
     * Latest reg_date for first sales bonus
     */
    public $date_end;

    /**
     * Bonus log process_date
     */
    public $process_date;

    public function __construct(
        $company_id,
        $variables_list,
        $member_type_orders,
        $achievement_orders,
        $date_start,
        $date_end,
        $process_date
    )
    {
        $this->company_id = $company_id;
        $this->variables_list = $variables_list;
        $this->member_type_orders = $member_type_orders;
        $this->achievement_orders = $achievement_orders;
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->process_date = $process_date;
    }

    /**
     * Sort bonus rule variables to easily get first match
     */
    abstract public function sortVariables();
    /**
     * Get first match bonus rule variables from sorted variables
     */
    abstract public function getMatchedVariables(Member $member);
    /**
     * User matched bonus rule variables to calculate bonus
     * 
     * @param Member $member
     * @param Collection $bonus_logs Collection of BonusLogs of direct downlines (by parent_id/sponsor_id)
     */
    abstract public function calculate(Member $member, $downlines = null, $bonus_logs = null);
}