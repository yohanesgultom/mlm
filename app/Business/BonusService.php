<?php 

namespace App\Business;

use App\Company;
use App\BonusLog;
use App\BonusRule;
use App\Member;
use App\MemberType;
use App\Achievement;
use DB;
use Log;
use Carbon\Carbon;

class BonusService
{
    public $company_id;
    public $date_start;
    public $date_end;
    public $process_date;
    public $member_type_orders;
    public $achievement_orders;
    public $bonus_rules;

    /**
     * $bonus_rules must belong to a company only
     */
    public function __construct($bonus_rules, $date_start, $date_end, $process_date) {
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->process_date = $process_date;
        $this->bonus_rules = $bonus_rules;
        $this->member_type_orders = [];
        $this->achievement_orders = [];
        if (count($bonus_rules) > 0) {
            $this->company_id = $bonus_rules->first()->company_id;
            // all bonus rules must have same cycle
            // so the 1st one can represent the rest
            $this->cycle = $bonus_rules->get(0)->cycle;
            $member_types = MemberType::where('company_id', $this->company_id)->get();
            $achievements = Achievement::where('company_id', $this->company_id)->get();
            $this->member_type_orders = $member_types->mapWithKeys(function ($item) {
                return [$item->name => $item->order];
            });
            $this->achievement_orders = $achievements->mapWithKeys(function ($item) {
                return [$item->code => $item->order];
            });   
        }        
    }


    /**
     * Calculate bonus for all members starting from root members
     * (does not have parent)
     */
    public function calculateAll()
    {
        // by sponsor tree
        $roots = Member::joinUsers()
            ->byCompany($this->company_id)
            ->whereNull('members.sponsor_id')
            ->get();
        foreach ($roots as $root) {
            $this->calculateBySponsorTree($root);
        }        

        // by member tree
        $roots = Member::joinUsers()
            ->byCompany($this->company_id)
            ->whereNull('members.parent_id')
            ->get();
        foreach ($roots as $root) {
            $this->calculateByMemberTree($root);
        }
    }

    public function calculateBySponsorTree(Member $member)
    {
        $total = 0;
        $pass_up = 0;
        $logs = [];
        // get pass up from sponsor
        $sponsoreds = $member->sponsoreds;
        $bonus_logs = collect([]);
        foreach ($sponsoreds as $s) {
            $bonus_log = $this->calculateBySponsorTree($s);
            $bonus_logs->push($bonus_logs);
            if ($bonus_log->pass_up > 0) {
                array_push($logs, "sponsored_pass_up from {$s->id} = {$bonus_log->pass_up}");
                $pass_up += $bonus_log->pass_up;
            }
        }
        $total += $pass_up;
        return $this->calculateOwnBonus(
            $member,
            $sponsoreds,
            $bonus_logs,
            $pass_up,
            $total,
            $logs,
            'sponsor_id'
        );
    }

    public function calculateByMemberTree(Member $member)
    {
        $total = 0;
        $pass_up = 0;
        $logs = [];
        $downlines = $member->downlines;
        $bonus_logs = collect([]);
        foreach ($downlines as $d) {
            $bonus_log = $this->calculateByMemberTree($d);
            $bonus_logs->push($bonus_log);
            if ($bonus_log->pass_up > 0) {
                array_push($logs, "pass_up from {$d->id} = {$bonus_log->pass_up}");
                $pass_up += $bonus_log->pass_up;
            }
        }
        $total += $pass_up;
        return $this->calculateOwnBonus(
            $member,
            $downlines,
            $bonus_logs,
            $pass_up,
            $total,
            $logs,
            'parent_id'
        );
    }

    public function calculateOwnBonus($member, $downlines, $bonus_logs, $pass_up, $total, $logs, $tree_field)
    {
        $close_point = false;
        $details = [];
        $data = [];    
        $logs[] = "tree_field = {$tree_field}";

        if (empty($member->member_type)) {
            $logs[] = 'empty or invalid member_type_id';
        } else {
            $logs[] = "member_type = {$member->member_type->name}";
        }
        
        if (empty($member->achievement)) {
            $logs[] = 'empty or invalid achievement_id';
        } else {
            $close_point = $member->current_personal_points >= $member->achievement->maintenance_pv;
            $logs[] = "achievement = {$member->achievement->code}";
            $logs[] = "close_point = {$member->current_personal_points} >= {$member->achievement->maintenance_pv}";
        }

        // calculate own bonus
        $bonus_rules = $this->bonus_rules->where('tree_field', $tree_field)->all();
        foreach ($bonus_rules as $bonus_rule){
            $calculator_class = $bonus_rule->type;
            $calculator = new $calculator_class(
                $this->company_id,
                $bonus_rule->variables,
                $this->member_type_orders,
                $this->achievement_orders,
                $this->date_start,
                $this->date_end,
                $this->process_date
            );
            $bonus = $calculator->calculate($member, $downlines, $bonus_logs);
            if (!$close_point) {                    
                $pass_up += $bonus['pass_up'];
            }                                
            $total += $bonus['total'];
            $details = array_merge($details, $bonus['details']);
            $logs = array_merge($logs, $bonus['logs']);
            $data = array_merge($data, $bonus['data']);
        }

        // create or update bonus log
        $bonus_log = BonusLog::where('process_date', $this->process_date)
            ->where('member_id', $member->id)
            ->first();
        if (empty($bonus_log)) {
            $bonus_log = BonusLog::create([
                'member_id' => $member->id,
                'process_date' => $this->process_date,
                'close_point' => $close_point,
                'total' => $close_point ? $total : 0,
                'pass_up' => $close_point ? 0 : $pass_up,
                'details' => $details,
                'logs' => $logs,
                'data' => $data,
                'cycle' => $this->cycle,
            ]);
        } else {
            // If first bonus detail exists in log, replace/overwrite all values in log.
            // Details can be empty in case of missing member_type_id or achivement_type_id
            $details_keys = array_keys($details);
            $bonus_log->close_point = $close_point;            
            if (count($details_keys) > 0 && array_key_exists($details_keys[0], $bonus_log->details)) {
                $bonus_log->total = $close_point ? $total : 0;
                $bonus_log->pass_up = $close_point ? 0 : $pass_up;
                $bonus_log->details = $details;
                $bonus_log->logs = $logs;
                $bonus_log->data = $data;
            } else { 
                // if first bonus detail not exists in log
                // add up or merge log
                $bonus_log->total += $close_point ? $total : 0;
                $bonus_log->pass_up += $close_point ? 0 : $pass_up;
                $bonus_log->details = array_merge($bonus_log->details, $details);
                $bonus_log->logs = array_merge($bonus_log->logs, $logs);
                $bonus_log->data = array_merge($bonus_log->data, $data);
            }
            $bonus_log->save();
        }
        
        return $bonus_log;
    }

    public static function getDirectSponsored($member_id, $process_date)
    {
        $process_date = Carbon::instance($process_date)->toDateTimeString();
        $query = DB::table('members')
            ->leftJoin('bonus_logs', function ($join) use ($process_date) {
                $join
                    ->on('members.id', 'bonus_logs.member_id')
                    ->where('bonus_logs.process_date', $process_date);
            })
            ->where('members.sponsor_id', $member_id)
            ->select(
                'members.id as member_id',
                'bonus_logs.id as bonus_log_id',
                'bonus_logs.close_point',
                'bonus_logs.pass_up'
            )
            ->orderBy('members.id');
        return $query->get();
    }

    public static function getDefaultCurrentBonusDates($company)
    {
        // monthly dates
        $now = new Carbon();
        if ($now->day <= $company->cutoff_day) {
            $now->subMonth();
        }
        $now->day = $company->cutoff_day;
        $date_start = $now->startOfDay();
        $date_end = new Carbon();

        $now = Carbon::now();
        if ($now->day > $company->cutoff_day) {
            $now->addMonth();
        }
        $now->day = $company->cutoff_day;
        $process_date = $now->startOfDay();

        // weekly dates
        $weekly_date_end = $date_end->copy();
        $current_day_name = Company::DAYS[Carbon::now()->dayOfWeek];
        if (!empty($company->weekly_cutoff_day_code) && $current_day_name == strtolower($company->weekly_cutoff_day_code)) {
            // if today is the weekly cutoff day
            // set time to end of day
            $weekly_process_date = Carbon::now()->endOfDay();
        } else {
            // set weekly cutoff day to the next one
            $weekly_process_date = Carbon::createFromTimestamp(strtotime("next {$company->weekly_cutoff_day_code}"));
        }
        $weekly_date_start = $weekly_process_date->copy()->subDays(7);

        return [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'process_date' => $process_date,
            'weekly_date_start' => $weekly_date_start,
            'weekly_date_end' => $weekly_date_end,
            'weekly_process_date' => $weekly_process_date,
        ];
    }

}