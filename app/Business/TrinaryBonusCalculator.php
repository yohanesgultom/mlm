<?php 

namespace App\Business;

use App\Member;
use App\MemberType;
use App\BonusRule;
use App\Registration;
use DB;

class TrinaryBonusCalculator extends BonusCalculator
{
    public $coef;
    public $eligible_member_type_id;
    public $sponsor_bonus_rate;

    public function sortVariables()
    {        
        // do nothing
    }

    public function getMatchedVariables(Member $member)
    {
        // do nothing        
    }

    public function calculate(Member $member, $downlines = null, $bonus_logs = null)
    {
        $logs = collect([]);
        $trinary_bonus = 0;
        $sponsor_bonus = 0;

        try {
            if (!empty($member->member_type) && !empty($member->achievement)) {
                // find coefficient for member
                // with pairing counters higher than threshold
                if (empty($this->coef)) {
                    $res = from_cache(
                        "trinary_coef_{$this->company_id}_{$this->process_date->toDateString()}",
                        now()->addMinutes(60),
                        [$this, 'findCoefficient'], 
                        []
                    );
                    $this->coef = $res['coef'];
                    $this->eligible_member_type_id = $res['eligible_member_type_id']; // eligible downline member_type_id
                    $this->sponsor_bonus_rate = $res['sponsor_bonus_rate']; // sponsor bonus for each eligible downline
                    $logs = $logs->concat($res['logs']);
                }

                // calculate sponsor bonus
                $sponsor_bonus = $this->getSponsorBonus(
                    $this->eligible_member_type_id,
                    $this->sponsor_bonus_rate,
                    $member->id
                );
                $logs->push("sponsor_bonus = {$sponsor_bonus}");                

                // calculate member's trinary bonus
                $pairing_counter = $member->pairingCounters()->where('process_date', $this->process_date)->first();
                if (empty($pairing_counter)) {
                    $logs->push("No pairing counter found for process_date {$this->process_date}");
                } else {
                    if ($pairing_counter->count_eligible <= $this->variables_list['coef_threshold']) {
                        $trinary_bonus = $pairing_counter->count_eligible * $this->variables_list['pairing_bonus'];
                        $logs->push("trinary_bonus = {$pairing_counter->count_eligible} * {$this->variables_list['pairing_bonus']} = {$trinary_bonus}");
                    } else {
                        $trinary_bonus = $pairing_counter->count_eligible * $this->variables_list['pairing_bonus'] * $coef;
                        $logs->push("trinary_bonus = {$pairing_counter->count_eligible} * {$this->variables_list['pairing_bonus']} * {$coef} = {$trinary_bonus}");
                    }
                }
            }

        } catch (\Exception $e) {
            $name = get_class($this);
            \Log::error("{$name} error for member_id {$member->id}: {$e->getMessage()}", $e->getTrace());
        }

        return [            
            'logs' => $logs->toArray(),
            'details' => [
                'trinary_bonus' => $trinary_bonus,
                'sponsor_bonus' => $sponsor_bonus,
            ],
            'pass_up' => 0,
            'data' => [],
            'total' => $trinary_bonus + $sponsor_bonus,
        ];
    }

    /**
     * Find coefficient for member with pairing counters higher than threshold
     */
    public function findCoefficient() {
        $logs = collect([]);

        // get all bonus rule variables
        $vars = $this->variables_list;
        $member_type = MemberType::where('company_id', $this->company_id)
                ->where('name', $vars['member_type_name'])
                ->first();                
        $total_revenue = $this->getRegistrationRevenue($member_type->id);                
        $total_sponsor_bonus = $this->getTotalSponsorBonus($member_type->id, $vars['sponsor_bonus']);
        $total_pairings = $this->getTotalPairings($this->process_date);        
        $max_bonus = $vars['max_bonus_pct'] * 0.01 * $total_revenue - $total_sponsor_bonus;        
        $total_pairing_bonus = $total_pairings * $vars['pairing_bonus'];

        // check if total bonus is more than max bonus limit        
        $reduce_bonus = $total_pairing_bonus > $max_bonus;
        $reduce_bonus_str = $reduce_bonus ? 'true' : 'false';

        // logs
        $logs->push("member_type = {$member_type->name}");
        $logs->push("total_revenue = {$total_revenue}");
        $logs->push("total_sponsor_bonus = {$total_sponsor_bonus}");
        $logs->push("total_pairings = {$total_pairings}");        
        $logs->push("max_bonus = {$vars['max_bonus_pct']} * 0.01 * {$total_revenue} - {$total_sponsor_bonus} = {$max_bonus}");
        $logs->push("total_pairing_bonus = {$total_pairing_bonus}");
        $logs->push("reduce_bonus = {$reduce_bonus_str}");

        // if bonus is bigger than max
        // find coefficient to reduce bonus 
        // for members with eligible pairing > coef_threshold
        $coef = 1;
        if ($reduce_bonus) {
            // count total pairings of members 
            // with eligible pairing > coef_threshold
            $total_pairings_gt_min = $this->getTotalPairings($this->process_date, $vars['coef_threshold']);
            // calculate coefficient
            $total_pairings_le_min = $total_pairings - $total_pairings_gt_min;    
            $coef = $this->calculateCoefficient($total_pairings_gt_min, $total_pairings_le_min, $total_pairings, $vars['pairing_bonus']);

            // logs
            $logs->push("total_pairings_gt_min = {$total_pairings_gt_min}");
            $logs->push("coef = {$coef}");
        }

        return [
            'coef' => $coef,
            'eligible_member_type_id' => $member_type->id,
            'sponsor_bonus_rate' => $vars['sponsor_bonus'],
            'logs' => $logs,
        ];
    }

    /**
     * Get total registration revenue
     * for given <code>$member_type_id</code>
     */
    public function getRegistrationRevenue($member_type_id)
    {
        $company_id = $this->company_id;
        $date_range = [
            $this->date_start->toDateTimeString(),
            $this->date_end->toDateTimeString()
        ];
        $query = Registration::join('packages', 'packages.id', 'registrations.package_id')
            ->where('packages.member_type_id', $member_type_id)
            ->where('registrations.status', Registration::STATUSES['COMPLETED'])
            ->whereBetween('registrations.completed_date', $date_range)
            ->selectRaw('sum(packages.price) as total');
        return $query->first()->total;
    }

    /**
     * Get total sponsor bonus
     */
    public function getTotalSponsorBonus($member_type_id, $sponsor_bonus)
    {
        $company_id = $this->company_id;
        $date_range = [
            $this->date_start->toDateTimeString(),
            $this->date_end->toDateTimeString()
        ];
        $query = Registration::join('packages', 'packages.id', 'registrations.package_id')
            ->where('packages.member_type_id', $member_type_id)
            ->whereNotNull('registrations.sponsor_wallet_id') // sponsored ONLY
            ->where('registrations.status', Registration::STATUSES['COMPLETED'])
            ->whereBetween('registrations.completed_date', $date_range)
            ->selectRaw('count(registrations.id) as count');
        
        return $query->first()->count * $sponsor_bonus;
    }

    /**
     * Get member sponsor bonus
     */
    public function getSponsorBonus($member_type_id, $sponsor_bonus, $member_id)
    {
        $company_id = $this->company_id;
        $date_range = [
            $this->date_start->toDateTimeString(),
            $this->date_end->toDateTimeString()
        ];
        $query = Registration::join('packages', 'packages.id', 'registrations.package_id')
            ->join('wallets', 'wallets.id', 'registrations.sponsor_wallet_id')
            ->where('wallets.member_id', $member_id)
            ->where('packages.member_type_id', $member_type_id)
            ->where('registrations.status', Registration::STATUSES['COMPLETED'])
            ->whereBetween('registrations.completed_date', $date_range)
            ->selectRaw('count(registrations.id) as count');
        
        return $query->first()->count * $sponsor_bonus;
    }    

    /**
     * Get sum of pairing_counters.eligible_count for given $process_date
     * and given $min of $eligible_count
     */
    public function getTotalPairings($process_date, $min = 0)
    {
        return DB::table('pairing_counters')
            ->join('members', 'members.id', 'pairing_counters.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company_id)
            ->where('process_date', $this->process_date)
            ->where('count_eligible', '>', $min)
            ->sum('pairing_counters.count_eligible');
    }

    /**
     * Get coefficient to reduce bonus for member
     * having pairing more than $min
     */
    public function calculateCoefficient($count_gt_min, $count_le_min, $total_pairing_bonus, $pairing_bonus)
    {
        $total_le_min_bonus = $count_le_min * $pairing_bonus;
        return $count_gt_min > 0 ? ($total_pairing_bonus - $total_le_min_bonus) / $count_gt_min : 0;
    }
}