<?php
namespace App\Business;

use App\User;
use App\Member;
use App\Wallet;
use App\Package;
use App\Order;
use App\Transaction;
use App\Registration;
use App\PairingCounter;
use Carbon\Carbon;
use DB;
use Faker\Factory as Faker;

class MemberService
{   
    private $company_id;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * Reset current points and bonus to zero and update from transactions
     */
    public function updateCurrentPointAndBonusValues($date_start, $date_end)
    {
        // reset current points and bonus to zero
        DB::table('members')->update([
            'current_personal_points' => 0,
            'current_personal_bonus' => 0,
        ]);

        // get total points and bonus from transactions for each member
        // update only members with at least one valid transaction
        $transaction_sums = $this->getTransactionsSum($date_start, $date_end);
        foreach ($transaction_sums as $sum) {
            DB::table('members')
                ->where('id', $sum->member_id)
                ->update([
                    'current_personal_points' => $sum->point_sum,
                    'current_personal_bonus' => $sum->bonus_sum,
                ]);
        }
    }

    /**
     * Get summary of transactions per members
     * Member without transactions is NOT included
     */
    public function getTransactionsSum($date_start, $date_end)
    {
        $company_id = $this->company_id;
        $date_start = Carbon::instance($date_start);
        $date_end = Carbon::instance($date_end);
        $query = DB::table('transactions')
            ->join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->join('users', function ($join) use ($company_id) {
                $join->on('members.user_id', 'users.id')
                    ->where('users.company_id', $company_id);
            })
            ->where('transactions.transferred', false)
            ->where('transactions.point_date', '>=', $date_start)
            ->where('transactions.point_date', '<=', $date_end)
            ->groupBy('wallets.member_id')
            ->selectRaw('sum(transactions.points) as point_sum, sum(transactions.bonus) as bonus_sum, wallets.member_id as member_id');
        $transactions = $query->get();
        return $transactions;
    }

    /**
     * Get member's direct downlines (one level below)
     */
    public static function getDirectDownlines($member_id, $field)
    {
        return DB::table('members')
            ->join('users', 'members.user_id', 'users.id')
            ->leftJoin('achievements', 'members.achievement_id', 'achievements.id')
            ->where("members.{$field}", $member_id)
            ->select(
                'members.id',
                'members.member_type_id',
                'members.reg_date',
                'members.current_personal_points',
                'members.current_personal_bonus',
                'members.current_group_points',
                'members.current_group_bonus',
                'members.image_path',
                'achievements.code as achievement_code',
                'achievements.desc as achievement',
                'users.name'
            )
            ->orderBy('members.id')
            ->get()
            ->toArray();
    }

    /**
     * Get new member_id based on number of members
     */
    public static function getNewMemberId($company_id)
    {
        $found = false;
        $count = DB::table('members')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $company_id)
            ->count();
        while (!$found) {
            # prepend company_id to handle multi company
            # max 9,999,999 members
            $member_id = str_pad((string) $company_id, 2, '0', STR_PAD_LEFT).str_pad((string) ++$count, 7, '0', STR_PAD_LEFT);
            $count_member_id = DB::table('members')->where('id', $member_id)->count();
            $found = $count_member_id == 0;
        }
        return $member_id;
    }

    /**
     * Create new member from registration
     * Should be called from transaction
     */
    public static function processCompletedRegistration(&$registration)
    {
        if (empty($registration->completed_date)) {
            throw new \Exception('Missing completed_date');
        }        
        $package = Package::with('package_products.product')->find($registration->package_id);
        if (empty($package)) {
            throw new \Exception('Invalid package_id: '.$registration->package_id);
        }

        // create user
        $faker = Faker::create();
        $random_password = $faker->password;
        $generated_id = static::getNewMemberId($registration->company_id);
        $user = new User;
        $user->company_id = $registration->company_id;
        $user->username = $generated_id;
        $user->name = $registration->name;
        $user->email = $registration->email;
        $user->password = $random_password;
        $user->role = User::ROLES['MEMBER'];
        $user->status = 0; // reserved
        $user->save();

        // create member
        $member = new Member;  
        $member->fill($registration->toArray());
        $member->id = $registration->member_id ?? $generated_id;
        $member->reg_date = $registration->completed_date;
        $member->parent_id = $registration->parent_id;
        $member->member_type_id = $package->member_type_id;        
        $member->name_printed = $user->name;
        $member->user_id = $user->id;        
        $sponsor_wallet = null;
        if (!empty($registration->sponsor_wallet_id)) {
            $sponsor_wallet = Wallet::find($registration->sponsor_wallet_id);    
            if (empty($sponsor_wallet)) {
                throw new \Exception('Invalid sponsor_wallet_id: '.$registration->sponsor_wallet_id);
            }
            if ($sponsor_wallet->balance < $package->price) {
                throw new \Exception('Insufficient sponsor wallet balance: '.$sponsor_wallet->balance);
            }
            // deduct from sponsor wallet's balance
            $sponsor_wallet->balance -= $package->price;
            $sponsor_wallet->save();
            $member->sponsor_id = $sponsor_wallet->member_id;
        }
        $member->save();

        // create wallet
        $wallet = new Wallet;
        $wallet->balance = 0;
        $wallet->member_id = $member->id;
        $wallet->type = Wallet::TYPES['TOPUP'];
        $wallet->save();

        // create order
        $order = new Order;
        $order->wallet_id = !empty($sponsor_wallet) ?  $sponsor_wallet->id : $wallet->id;
        $order->status = Order::STATUSES['COMPLETED'];
        $order->save();
        $reg_bv = 0;
        $transactions = [];
        foreach ($package->package_products as $package_product) {
            $transactions[] = [
                'product_id' => $package_product->product_id,
                'qty' => $package_product->qty,
                'order_id' => $order->id,
                'type' => Transaction::TYPES['ZERO_POINT_BONUS'],
            ];
            $reg_bv += $package_product->product->bonus_value * $package_product->qty;            
        }
        $order->transactions()->createMany($transactions);
        $member->reg_bv = $reg_bv;
        $member->save();        

        // update registration
        $registration->order_id = $order->id;
        $registration->member_id = $member->id;
        $registration->status = Registration::STATUSES['COMPLETED'];
        $registration->save();        

        // return to be sent on email
        return $random_password;
    }

    /**
     * Update members pairing counter
     */
    public function updatePairingCounters($member_type_id, $limit, $date_start, $date_end, $process_date)
    {
        $roots = Member::joinUsers()
            ->byCompany($this->company_id)
            ->whereNull('members.parent_id')
            ->get();
        foreach ($roots as $root) {
            $this->updatePairingCounter($member_type_id, $limit, $date_start, $date_end, $process_date, $root);
        }
    }

    /**
     * Recursively update pairing counter of a member and return total eligible count for his/her group
     */
    public function updatePairingCounter($member_type_id, $limit, $date_start, $date_end, $process_date, $member)
    {
        // init pairing counter object
        $counter = PairingCounter::create([
            'member_id' => $member->id,
            'process_date' => $process_date,
            'count_left' => 0,
            'count_mid' => 0,
            'count_right' => 0,
        ]);

        // get all direct downlines
        $downlines = static::getDirectDownlines($member->id, 'parent_id');
        foreach ($downlines as $i => $downline) {
            // assume max downlines = 3 (left, mid, righ branch)
            // in case of more than 3, count the remaining as right branch
            $downline_count = $this->updatePairingCounter($member_type_id, $limit, $date_start, $date_end, $process_date, $downline);
            $counter->addByIndex($i, $downline_count);                        
        }
        
        // retrieve count_saved previous pairing counter
        // if there is saved count, add it to the least branch
        $last_week_counter = PairingCounter::where('member_id', $member->id)
            ->orderBy('process_date', 'desc')
            ->first();
        if (!empty($last_week_counter) && $last_week_counter->count_saved > 0) {
            $count_arr = [
                $counter->count_left,
                $counter->count_mid,
                $counter->count_right,
            ];
            $min_index = array_search(min($count_arr), $count_arr);
            $counter->addByIndex($min_index, $last_week_counter->count_saved);
        }

        // Pair per member = min(min(kiri, tengah), limit) + min(min(kiri, kanan), limit) + min(min(tengah, kanan), limit)        
        $min_count_array = [
            min(min($counter->count_left, $counter->count_mid), $limit),
            min(min($counter->count_left, $counter->count_right), $limit),
            min(min($counter->count_mid, $counter->count_right), $limit)
        ];
        rsort($min_count_array);
        $counter->count_eligible = array_sum($min_count_array);
        // saved for next week = 1st max - 2nd max
        $counter->count_saved = $min_count_array[0] - $min_count_array[1];
        $counter->save();

        // 1 if this member is eligible
        // or 0 if otherwise
        $self_count = 0;
        $reg_date = Carbon::parse($member->reg_date);
        if ($member->member_type_id == $member_type_id
            && $reg_date->greaterThanOrEqualTo($date_start) 
            && $reg_date->lessThanOrEqualTo($date_end)) {
            $self_count = 1;
        }

        // return total eligible count
        return $self_count + $counter->count_left + $counter->count_mid + $counter->count_right;
    }
}