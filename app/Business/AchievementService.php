<?php 

namespace App\Business;

use App\Member;
use App\Achievement;
use App\AchievementLog;
use App\PairingCounter;
use App\Registration;
use DB;
use Illuminate\Support\Collection;

class AchievementService
{
    public $company_id;
    public $achievements;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
        $this->achievements = Achievement::where('company_id', $company_id)
            ->orderBy('order', 'desc')
            ->get();
        $this->downlines_map = $this->achievements->mapWithKeys(function ($item) {
            return [$item->code => 0];
        })->all();
        $this->downlines_array_map = $this->achievements->mapWithKeys(function ($item) {
            return [$item->code => []];
        })->all();
        $this->achievement_map = $this->achievements
            ->sortBy('order')
            ->mapWithKeys(function ($item) {
                return [$item->code => $item->order];
            })->all();
    }

    public function updateAchievements($process_date)
    {
        $roots = Member::joinUsers()
            ->byCompany($this->company_id)
            ->whereNull('members.parent_id')
            ->get();
        foreach ($roots as $root) {
            $this->updateAchievement($process_date, $root);
        }
    }

    /**
     * Update member achievements, personal/group points, personal/group bonus value (BV)
     */
    public function updateAchievement($process_date, $member)
    {
        $downlines_count = $this->downlines_map;
        $downlines_pv = $this->downlines_array_map;
        $current_group_bonus = 0;
        $current_group_points = 0;
        $highest_downline_achievement = null;

        // $downline_count and $downlines_pv should ONLY
        // contain data from direct downlines 
        $downlines = Member::where('parent_id', $member->id)->get();
        foreach ($downlines as $i => $downline) {
            $res = $this->updateAchievement($process_date, $downline);
            $downlines_count[$res['achievement_code']] += 1;
            // 20181203 downline pv = downline pv + his/her group pv (details in Trello)
            $downlines_pv[$res['achievement_code']][] = $res['pv'] + $res['current_group_points'];
            $current_group_bonus += $res['current_group_bonus'];
            $current_group_points += $res['current_group_points'];
            $downline_achievement = $this->achievements->where('code', $res['achievement_code'])->first();
            // find highest downline achievement
            if (empty($highest_downline_achievement) 
                || $downline_achievement->order > $highest_downline_achievement->order) {
                $highest_downline_achievement = $downline_achievement;
            }
        }

        // find achievement based on performance and rules
        $achievement = $this->findAchievement(
            $downlines_count,
            $downlines_pv,
            $member
        );
        
        // update member
        if (empty($member->achievement) || $achievement->order > $member->achievement->order) {
            $member->highest_achievement_id = $achievement->id;
        }
        $member->achievement_id = $achievement->id;
        $member->current_group_bonus = $current_group_bonus;
        $member->current_group_points = $current_group_points;
        $member->save();

        // include the member itself
        $current_group_bonus += $member->current_personal_bonus;
        $current_group_points += $member->current_personal_points;    
        
        // add or update achievement log
        // to be added to parent
        $achievement_log = AchievementLog::updateOrCreate([
            'process_date' => $process_date,            
            'member_id' => $member->id,
        ],
        [
            'code' => $achievement->code,
            'downlines_count' => $downlines_count,
            'downlines_pv' => $downlines_pv,
        ]);     

        // return highest achievement
        if (!empty($highest_downline_achievement) 
            && $achievement->order < $highest_downline_achievement->order) {
            $achievement = $highest_downline_achievement;
        }
        
        // return to be processed by parent
        return [
            'achievement_code' => $achievement->code,
            'pv' => $member->current_personal_points,
            'downlines_count' => $downlines_count,
            'downlines_pv' => $downlines_pv,
            'current_group_bonus' => $current_group_bonus,
            'current_group_points' => $current_group_points,
        ];
    }

    /**
     * Find achievement based on performance and rules
     */
    public function findAchievement($downlines_count, $downlines_pv, $member)
    {
        $logs = [];
        $achievement = null;
        foreach ($this->achievements as $a) {
            $logs[$a->code] = [];
            if (empty($a->conditions)) {
                $achievement = $a;
                $logs[$a->code]['satisfied'] = 1;
                break;
            } else {
                foreach ($a->conditions as $c) {
                    // no conditions means satisfied
                    $satisfy = true;

                    // check downlines count
                    if ($satisfy && array_key_exists('downlines', $c)) {
                        foreach ($c['downlines'] as $min_code => $min_count) {
                            // count downline with minimal achievement $min_code
                            // eg. if $min_code = 'MD' then 
                            // $min_code_count =  $downline_count['PD'] + $downline_count['MD']
                            // because 'PD' also satisfies minimal 'MD'
                            $min_order = $this->achievement_map[$min_code];
                            $min_code_count = 0;                           
                            foreach ($this->achievement_map as $code => $order) {
                                if ($order >= $min_order) {
                                    $min_code_count += $downlines_count[$code];
                                    if ($min_code_count >= $min_count) break;
                                }
                            } 
                            $satisfy = $satisfy && ($min_code_count >= $min_count);
                        }
                        $logs[$a->code]['downline_counts'] = $satisfy ? 1 : 0;

                        // check downlines above have minimal pv
                        if ($satisfy && array_key_exists('downlines_pv', $c)) {
                            foreach ($c['downlines_pv'] as $min_code => $min_pv) {
                                // check if there are $c['downlines'][$min_code] downlines 
                                // with minimal achievement $min_code that have $min_pv pv
                                $expected_min_pv_count = $c['downlines'][$min_code];
                                $min_pv_count = 0;
                                $min_order = $this->achievement_map[$min_code];
                                foreach ($this->achievement_map as $code => $order) {
                                    if ($order >= $min_order) {
                                        foreach ($downlines_pv[$code] as $dpv) {
                                            if ($dpv >= $min_pv) {
                                                $min_pv_count++;
                                                if ($min_pv_count >= $expected_min_pv_count) break;
                                            }            
                                        }
                                    }
                                }
                                $satisfy = $satisfy && ($min_pv_count >= $expected_min_pv_count);
                            }
                            $logs[$a->code]['downlines_pv'] = $satisfy ? 1 : 0;
                        }
                    }

                    // check own pv
                    // 20180907 Chris: for achievement, pv = pv + gpv
                    // 20181129 Chris: request to revert to pv = pv. This time we put WA request screenshot in trello
                    if ($satisfy && array_key_exists('pv', $c)) {
                        // $total_pv = $member->current_personal_points + $member->current_group_points;
                        $total_pv = $member->current_personal_points;
                        $satisfy = $satisfy && $total_pv  >= $c['pv'];
                        $logs[$a->code]['pv'] = $satisfy ? 1 : 0;
                    }

                    // only 1 condition is needed
                    if ($satisfy) {
                        $logs[$a->code]['satisfied'] = $satisfy ? 1 : 0;
                        $achievement = $a;
                        break;
                    }
                }    
            }
            // return first satisfied achievement
            if (!empty($achievement)) break;
        }
        // \Log::debug([
        //     'member_id' => $member->id,
        //     'achievement_code' => $achievement->code,
        //     'downlines_count' => $downlines_count,
        //     'downlines_pv' => $downlines_pv,
        //     'logs' => $logs,
        // ]);
        return $achievement;
    }

    public function addArrays($base, $data)
    {   
        $data = $data instanceof Collection ? $data->toArray() : $data;
        foreach ($base as $key => $val) {
            if (array_key_exists($key, $data)) {
                if (!is_array($val) && !is_array($data[$key])) {
                    $base[$key] = $val + $data[$key];
                } else {
                    $base[$key] = array_merge($val, $data[$key]);                    
                }                
            }
        }
        return $base;
    }

    public function getAchievementCount()
    {
        $count_map = $this->downlines_map;
        $company_id = $this->company_id;        
        $query = DB::table('members')
            ->join('achievements', function ($join) use ($company_id) {
                $join->on('achievements.id', 'members.achievement_id')
                    ->where('achievements.company_id', $company_id);
            })
            ->where('members.status', '!=', Member::STATUSES['SUSPENDED'])
            ->groupBy('achievements.code')
            ->selectRaw('achievements.code, count(members.id) as count');
        $res = $query->get();
        foreach ($res as $row) {
            $count_map[$row->code] = $row->count;
        }
        return $count_map;
    }

    public function getRegistrationRevenue($member_type_id, $date_start, $date_end)
    {
        $company_id = $this->company_id;
        $query = Registration::join('packages', 'packages.id', 'registrations.package_id')
            ->where('packages.member_type_id', $member_type_id)
            ->where('registrations.status', Registration::STATUSES['COMPLETED'])
            ->whereBetween('registrations.completed_date', [$date_start->toDateTimeString(), $date_end->toDateTimeString()])
            ->selectRaw('sum(packages.price) as total');
        return $query->first()->total;
    }

}