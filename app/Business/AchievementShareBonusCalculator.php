<?php 

namespace App\Business;

use App\Member;
use App\Business\MemberService;
use Carbon\Carbon;
use DB;

class AchievementShareBonusCalculator extends BonusCalculator
{
    public $bonus_map;

    public function sortVariables()
    {        
        // do nothing
    }

    public function getMatchedVariables(Member $member)
    {
        // do nothing
    }

    public function calculate(Member $member, $downlines = null, $bonus_logs = null)
    {       
        $total = 0;
        $logs = collect([]);
        $details = [];

        try {
            if (!empty($member->member_type) && !empty($member->achievement)) {
                // get bonus map
                if (empty($this->bonus_map)) {
                    $this->bonus_map = from_cache(
                        "asb_bonus_map_{$this->company_id}",
                        now()->addMinutes(10),
                        [$this, 'getBonusMap'], 
                        []
                    );
                }

                // calculate member's ASB
                $achievement_code = $member->achievement->code;
                $logs->push("achievement_code = {$achievement_code}");
                if (array_key_exists($achievement_code, $this->bonus_map)) {
                    $res = $this->bonus_map[$achievement_code];
                    $total = $res['total'];
                    $logs = $logs->concat(collect($res['logs']));
                    $details = $res['details'];
                } else {
                    $logs->push("achievement_share_bonus = does not match any rules");
                } 
            }
        } catch (\Exception $e) {
            $name = get_class($this);
            \Log::error("{$name} error for member_id {$member->id}: {$e->getMessage()}", $e->getTrace());
        }

        return [            
            'logs' => $logs->toArray(),
            'details' => $details,
            'data' => [],
            'pass_up' => 0,
            'total' => $total,
        ];
    }

    /**
     * Get global achievement share bonus map
     */
    public function getBonusMap()
    {
        $this->achievementService = $this->achievementService ?? new AchievementService($this->company_id);
        $achievements_count = $this->achievementService->getAchievementCount();
        $total_bv = $this->getTotalBonus($this->company_id);

        // map ASB calculation for each achievements
        $bonus_map = [];
        foreach ($this->variables_list as $vars) {    
            $code = $vars['achievement_type'];
            $logs = collect([]);
            $total = 0;
            foreach ($vars['bonuses'] as $var) {
                $divisor = 0;
                foreach ($var['divisors'] as $div_code) {
                    $divisor += $achievements_count[$div_code];
                }
                $bonus = $divisor > 0 ? round($var['bonus_pct'] / 100 * $total_bv / $divisor) : 0;
                $total += $bonus;
                $divisor_codes = json_encode($var['divisors']);
                $logs->push(
                    "achievement_share_bonus {$divisor_codes} = {$var['bonus_pct']} / 100 * {$total_bv} / {$divisor} = {$bonus}"
                );    
            }            
            $bonus_map[$code] = [
                'total' => $total,
                'details' => ["achievement_share_bonus" => $total],
                'logs' => $logs,
            ];
        }
        // \Log::debug($bonus_map);
        return $bonus_map;
    }

    public function getTotalBonus()
    {
        $company_id = $this->company_id;
        $query = DB::table('members')
            ->join('users', function ($join) use ($company_id) {
                $join->on('members.user_id', 'users.id')
                    ->where('users.company_id', $company_id);
            })
            ->where('members.status', '!=', Member::STATUSES['SUSPENDED'])
            ->selectRaw('sum(members.current_personal_bonus) as bonus_sum');
        return $query->first()->bonus_sum;
    }
}