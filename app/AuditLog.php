<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model
{
    protected $fillable = [
        'ip',
        'path',
        'model',
        'action',
        'userrole',
        'username',
        'before',
        'change',
        'company_id',
    ];

    protected $casts = [
        'before' => 'array',
        'change' => 'array',
    ];

}
