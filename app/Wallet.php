<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Auditable;

class Wallet extends Model
{
    use Auditable;

    const TYPES = [
        'TOPUP' => 0,
        'BONUS' => 1,
        'TRANSFER' => 2,
    ];

    protected $fillable = [
        'type',
        'balance',
        'member_id',
    ];

    protected $appends = ['type_name'];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    function orders(){
        return $this->hasMany('App\Order');
    }
    
    public function scopeOwned($query)
    {
        return $query->where('member_id', \Auth::user()->member->id);
    }

    public function scopeOwnedBy($query, $member_id)
    {
        return $query->where('member_id', $member_id);
    }

    public function scopeSameCompany($query)
    {
        return $query
            ->select('wallets.*')
            ->join('members', 'wallets.member_id', '=', 'members.id')
            ->join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', \Auth::user()->company_id);
    } 
    
    public function getTypeNameAttribute()
    {
        return array_flip(static::TYPES)[$this->type];
    }
}
