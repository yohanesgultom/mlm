<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberStockLog extends Model
{
    protected $fillable = [
        'product_name',
        'expiry_date',
        'qty_before',
        'qty_change',
        'customer_name',
        'transaction_id',
        'member_id',
    ];

    protected $dates = [
        'expiry_date',
    ];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
