<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    public $timestamps = false;
    
    protected $with = ['bank'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_name',
        'account_no',
        'company_id',
        'bank_id',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }
    
    public function scopeSameCompany($query)
    {
        return $query->where('company_id', \Auth::user()->company_id);
    } 
}
