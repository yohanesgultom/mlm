<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    const STATUSES = [
        'UNCONFIRMED' => 0,
        'CONFIRMED' => 1,
        'CANCELLED' => 2,
    ];

    protected $with = ['bank_account'];

    protected $fillable = [
        'amount',
        'desc',
        'deposit_date',
        'status',
        'member_id',
        'bank_account_id',
    ];
    
    protected $dates = [
        'deposit_date',
    ];

    protected $appends = ['status_name'];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
    
    public function bank_account()
    {
        return $this->belongsTo('App\BankAccount');
    }

    public function scopeSameCompany($query)
    {
        return $query
            ->select('deposits.*')
            ->join('bank_accounts', 'bank_account_id', '=', 'bank_accounts.id')
            ->where('bank_accounts.company_id', \Auth::user()->company_id);
    }

    public function getStatusNameAttribute()
    {
        return array_flip(static::STATUSES)[$this->status];
    }
}
