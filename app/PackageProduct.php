<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageProduct extends Model
{
    protected $table = 'package_product';
    public $timestamps = false;

    protected $fillable = [
        'package_id',
        'product_id',
        'qty',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
