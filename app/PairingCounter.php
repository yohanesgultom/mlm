<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PairingCounter extends Model
{
    protected $fillable = [
        'process_date',
        'count_left',
        'count_mid',
        'count_right',
        'count_eligible',
        'count_saved',
        'member_id',
    ];

    protected $dates = [
        'process_date',
    ];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
    
    public function scopeOwned($query)
    {
        return $query->where('member_id', \Auth::user()->member->id);
    }

    public function scopeOwnedBy($query, $member_id)
    {
        return $query->where('member_id', $member_id);
    }

    public function scopeSameCompany($query)
    {
        return $query
            ->select('pairing_counters.*')
            ->join('members', 'member_id', '=', 'members.id')
            ->join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', \Auth::user()->company_id);
    } 

    /**
     * Add counter by index
     */
    public function addByIndex($i, $val)
    {
        if ($i == 0) {
            $this->count_left += $val;
        } else if ($i == 1) {
            $this->count_mid += $val;
        } else {
            $this->count_right += $val;
        }
    }
}
