<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryItem extends Model
{
    protected $with = ['product'];

    protected $fillable = [
        'qty',
        'product_id',
        'delivery_id',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    
    public function delivery()
    {
        return $this->belongsTo('App\Delivery');
    }

}
