<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    const STATUSES = [
        'UNPAID' => 0,
        'COMPLETED' => 1,
    ];

    protected $fillable = [
        'name',
        'email',
        'birth_date',
        'gender',
        'official_id',
        'tax_id',
        'address',
        'address_rt',
        'address_rw',
        'address_zipcode',
        'address_city',
        'address_state',
        'country_id',
        'phone_home',
        'phone_office',
        'phone_mobile',
        'phone_fax',
        'bank_account_no',
        'bank_account_name',
        'bank_account_city',
        'bank_account_branch',        
        'company_id',
        'package_id',
        'sponsor_wallet_id',
        'parent_id',
        'order_id',
        'member_id',
        'status',
        'completed_date',
    ];

    protected $with = [
        'package',
        'sponsor_wallet.member',
    ];

    protected $dates = [
        'completed_date',
    ];

    protected $appends = [
        'status_name',
    ];

    public static function boot()
    {
        parent::boot();
        static::saving(function($model)
        {
            if ((empty($model->getOriginal()) || $model->getOriginal()['status'] == static::STATUSES['UNPAID'])
                && $model->status == static::STATUSES['COMPLETED']) {
                $model->completed_date = new \DateTime();
            }
        });
    }  
    
    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    public function sponsor_wallet()
    {
        return $this->belongsTo('App\Wallet', 'sponsor_wallet_id');
    }

    public function scopeSameCompany($query)
    {
        return $query->where('registrations.company_id', \Auth::user()->company_id);
    }

    public function getStatusNameAttribute()
    {
        return array_flip(static::STATUSES)[$this->attributes['status']];
    }    
}
