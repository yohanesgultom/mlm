<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    const DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

    protected $fillable = [
        'name',
        'cutoff_day',
        'weekly_cutoff_day_code',
        'status'
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }    

    public function productTypes()
    {
        return $this->hasMany('App\ProductType');
    }    

    public function member_types()
    {
        return $this->hasMany('App\MemberType');
    }    

    public function warehouses()
    {
        return $this->hasMany('App\Warehouse');
    }
    
    public function registrations()
    {
        return $this->hasMany('App\Registration');
    }    
    
}
