<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    protected $fillable = [
        'order',
        'code',
        'desc',
        'conditions',
        'maintenance_pv',
        'company_id',
    ];

    protected $casts = [
        'conditions' => 'array',
    ];

    public function scopeSameCompany($query)
    {
        return $query->where('company_id', \Auth::user()->company_id);
    }
}
