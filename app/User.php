<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const ROLES = [
        'SUPERADMIN' => 0,
        'ADMIN' => 1,
        'MEMBER' => 2,
        'IT' => 3,
        'SALESADMIN' => 4,
        'FINANCE' => 5,
        'MARKETING' => 6,
        'LOGISTICS' => 7,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'name', 
        'email', 
        'password', 
        'role', 
        'company_id',
        'status', 
        'verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    protected $appends = ['role_name'];

    protected $casts = [
        'verified' => 'boolean',
    ];

    public function findForPassport($identifier) {
        return $this->where('username', $identifier)->first();
    }

    /**
     * Send a password reset email to the user
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordToken($token));
    }    

    public function member()
    {
        return $this->hasOne('App\Member');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    
    /**
     * Encrypt password
     */
    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt($value);
    }

    public function getRoleNameAttribute()
    {
        return array_flip(static::ROLES)[$this->attributes['role']];
    }
}
