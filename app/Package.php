<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'name',
        'member_type_id',
    ];

    public function member_type()
    {
        return $this->belongsTo('App\MemberType');
    }

    public function package_products()
    {
        return $this->hasMany('App\PackageProduct');
    }
}
