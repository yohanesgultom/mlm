<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CaptureRequestInfo
{
    /**
     * Capture request info for AuditLog
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            session([
                'ip' => $request->ip(),
                'path' => $request->method().' '.$request->path(),
            ]);
        }
        return $next($request);
    }
}