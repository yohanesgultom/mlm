<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\BonusLog;
use App\Business\BonusService;
use App\Jobs\CalculateBonus;
use Auth;
use Carbon\Carbon;

class BonusLogController extends Controller
{
    /**
     * @apiDefine SuccessBonusLog
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {Boolean} close_point
     * @apiSuccess (Success 200) {Double} total Total bonus
     * @apiSuccess (Success 200) {Double} pass_up Total bonus passed up to upline
     * @apiSuccess (Success 200) {Date} process_date
     * @apiSuccess (Success 200) {Array} details Example: {"discount_bonus": 100000, "achievement_share_bonus": 200000, "first_sales_bonus": 300000, "leadership_development_bonus": 400000, "leadership_matching_bonus": 500000}
     * @apiSuccess (Success 200) {Array} logs Bonus calculation logs
     * @apiSuccess (Success 200) {DateTime} transfer_date
     * @apiSuccess (Success 200) {String} bank_account_no
     * @apiSuccess (Success 200) {String} bank_account_name
     * @apiSuccess (Success 200) {String} bank_account_city
     * @apiSuccess (Success 200) {String} bank_account_branch
     * @apiSuccess (Success 200) {String} bank_name
     * @apiSuccess (Success 200) {String} member_id
     * 
     */

    /**
     * 
     * @api {get} /bonusLogs/cycles Cycles
     * @apiName cycles
     * @apiGroup BonusLog
     * @apiPermission public
     * @apiDescription Get list of bonus log cycles
     * 
     * @apiSuccess (Success 200) {Object} _ Cycles
     *
     */ 
    public function cycles(Request $request)
    {        
        return response()->json((object) array_flip(BonusLog::CYCLES));
    }     

    /**
     * 
     * @api {get} /bonusLogs List
     * @apiName list
     * @apiGroup BonusLog
     * @apiPermission admin
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {Integer} [order=process_date] Order by
     * @apiParam {Integer} [sort=desc] Sort direction
     * @apiParam {String} [member_id]
     * @apiParam {String} [cycle]
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of BonusLogs
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */          
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $order = 'bonus_logs.'.$request->input('order', 'process_date');
        $sort = $request->input('sort', 'desc');

        $input = $request->all();
        $query = BonusLog::whereNotNull('member_id');
        $query = $this->queryFilter($query, $input, [
            'member_id' => 'EQ',
            'cycle' => 'EQ',
        ]);
        if (Auth::user()->role == User::ROLES['ADMIN']) {
            $query = $query->sameCompany();
        } else if (Auth::user()->role == User::ROLES['MEMBER']) {
            $query = $query->owned();
        } else if (Auth::user()->role == User::ROLES['SUPERADMIN']) {
            // do nothing
        } else {
            return $this->forbidden('Unknown role: '.Auth::user()->role);
        }
        $list = $query->orderBy($order, $sort)->paginate($limit);
        return response()->json($list);
    }

    /**
     * 
     * @api {get} /bonusLogs/:id Get
     * @apiName get
     * @apiGroup BonusLog
     * @apiPermission admin
     * 
     * @apiUse SuccessBonusLog
     */        
    public function show($id)
    {
        $existing = BonusLog::find($id);
        if (empty($existing)) {
            return $this->notFound('Data not found: '.$id);
        }
        $error = $this->validateMemberCompany($existing->member);
        if ($error != null) return $error;
        $error = $this->validateOwnership($existing->member);
        if ($error != null) return $error;
        return response()->json($existing
            ->makeHidden(['member'])
            ->makeVisible(['details', 'logs']));
    }

    /**
     * 
     * @api {post} /bonusLogs/calculateBonus Calculate Bonus
     * @apiName calculateBonus
     * @apiGroup BonusLog
     * @apiPermission admin
     * @apiDescription Created for TESTING purpose only.
     * Triggers asyncronous/background job to update members current bonus and points, update achievements, calculate bonus and create/replace MONTHLY and WEEKLY bonus logs.
     * This API always delete and create both MONTHLY and WEEKLY <code>bonus_logs</code> despite of given parameters
     * 
     * @apiParam {DateTime} [date_start] Earliest datetime YYYY-MM-DD HH:mm:ss of transaction to calculate monthly bonus
     * @apiParam {DateTime} [date_end] Latest datetime YYYY-MM-DD HH:mm:ss of transaction to calculate monthly bonus
     * @apiParam {DateTime} [process_date] Datetime YYYY-MM-DD HH:mm:ss that indicates calculation period of monthly bonus. All bonus log calculated in the same period will have same <code>process_date</code>
     * @apiParam {DateTime} [weekly_date_start] Earliest datetime YYYY-MM-DD HH:mm:ss of transaction to calculate weekly bonus
     * @apiParam {DateTime} [weekly_date_end] Latest datetime YYYY-MM-DD HH:mm:ss of transaction to calculate weekly bonus
     * @apiParam {DateTime} [weekly_process_date] Datetime YYYY-MM-DD HH:mm:ss that indicates calculation period of weekly bonus. All bonus log calculated in the same period will have same <code>process_date</code>
     * 
     * 
     * @apiSuccess (Success 200) {String} message
     */ 
    public function calculateBonus(Request $request)
    {
        if (Auth::user()->role != User::ROLES['ADMIN']) {
            return $this->forbidden('Only ADMINs are allowed');
        } else {
            $input = $request->all();

            // monthly
            $date_start = array_key_exists('date_start', $input) ? Carbon::parse($input['date_start']) : null;
            $date_end = array_key_exists('date_end', $input) ? Carbon::parse($input['date_end']) : null;
            $process_date = array_key_exists('process_date', $input) ? Carbon::parse($input['process_date']) : null;

            // weekly
            $weekly_date_start = array_key_exists('weekly_date_start', $input) ? Carbon::parse($input['weekly_date_start']) : null;
            $weekly_date_end = array_key_exists('weekly_date_end', $input) ? Carbon::parse($input['weekly_date_end']) : null;
            $weekly_process_date = array_key_exists('weekly_process_date', $input) ? Carbon::parse($input['weekly_process_date']) : null;            
                        
            CalculateBonus::dispatch(
                Auth::user()->company_id,
                true,
                $date_start,
                $date_end,
                $process_date,
                $weekly_date_start,
                $weekly_date_end,
                $weekly_process_date                
            );
            return response()->json(['message' => 'Job is running'], 200);
        }
    }    
}
