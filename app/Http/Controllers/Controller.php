<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\Member;
use App\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function notFound($msg = 'Data not found') {
        return response()->json(['error' => $msg], 404);
    }

    protected function forbidden($msg = 'Forbidden') {
        return response()->json(['error' => $msg], 403);
    }

    protected function notAuthorized($msg = 'Not authorized') {
        return response()->json(['error' => $msg], 401);
    }

    protected function badRequest($msg = 'Bad request') {
        return response()->json(['error' => $msg], 400);
    }

    protected function serverError($msg = 'Server error') {
        return response()->json(['error' => $msg], 500);
    }

    protected function remapQuery($input, $map) {
        foreach ($map as $target => $replace) {
            if (array_key_exists($target, $input)) {
                $input[$replace] = $input[$target];
                unset($input[$target]);
            }
        }
        return $input;
    }

    protected function queryFilter($query, $input, $fields) {        
        foreach ($fields as $field => $op) {
            $op = strtoupper($op);
            if (array_key_exists($field, $input)) {
                if ($op == 'EQ') {
                    $query = $query->where($field, $input[$field]);
                } else if ($op == 'STARTS_WITH') {
                    $query = $query->where($field, 'LIKE', $input[$field].'%');
                } else {
                    $query = $query->where($field, 'LIKE', '%'.$input[$field].'%');
                }                
            }
        }
        return $query;
    }

    protected function validateMemberCompany(Member $member) {
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            // should be from same company 
            if ($member->user->company_id != Auth::user()->company_id) {
                return $this->forbidden();
            }
        }
        return null;
    }

    protected function validateCompany(String $member_id) {
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $member = Member::find($member_id);
            if (empty($member)) {
                return $this->notFound('Member not found: '.$member_id);
            }            
            return $this->validateMemberCompany($member);
        }
        return null;
    }

    protected function validateMembership($member_id) {
        $member = Member::find($member_id);
        if (empty($member)) {
            return $this->notFound();
        } 

        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            // should be from same company
            if ($member->user->company_id != Auth::user()->company_id) {
                return $this->forbidden();
            }
        }
        if (Auth::user()->role == User::ROLES['MEMBER']) {
            // should be accessed by the member
            if ($member->user->id != Auth::id()) {
                return $this->forbidden();
            }
        }
        return null;
    }

    protected function validateOwnership($member)
    {
        if (Auth::user()->role == User::ROLES['MEMBER']) {
            // should be accessed by the member
            if ($member->user->id != Auth::id()) {
                return $this->forbidden();
            }
        }
        return null;
    }

    protected function processPublicInputFiles($dir, $fields, $request, $input)
    {
        foreach ($fields as $field) {
            if ($request->hasFile($field)) {
                $file = $request->file($field);
                $path = $file->store($dir.'/'.$field, 'public');
                $input[$field] = Storage::url($path);
            }
        }
        return $input;
    }
}
