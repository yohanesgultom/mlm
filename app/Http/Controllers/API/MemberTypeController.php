<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MemberType;

class MemberTypeController extends Controller
{
     /**
     * @apiDefine SuccessMemberType
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} name
     * 
     */

     /**
     * @apiDefine ParamMemberType
     *
     * @apiParam {String} name
     * 
     */

    /**
     * 
     * @api {get} /api/companies/:company_id/memberTypes MemberType List
     * @apiName memberType list
     * @apiGroup MemberType
     * @apiPermission public
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {String} [order=order]
     * @apiParam {String} [sort=asc]
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of MemberTypes
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request, $company_id)
    {
        $limit = $request->input('limit', 10);
        $order = $request->input('order', 'order');
        $sort = $request->input('sort', 'asc');
        $query = MemberType::where('company_id', $company_id);
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }

    /**
     * 
     * @api {get} /api/companies/:company_id/memberTypes/:id MemberType Get
     * @apiName memberType get
     * @apiGroup MemberType
     * @apiPermission public
     * 
     * @apiUse SuccessMemberType
     */    
    public function show($company_id, $id)
    {
        return response()->json(MemberType::find($id));
    }

}
