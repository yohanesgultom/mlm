<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\User;
use App\BonusLog;
use App\Business\BonusService;
use Auth;
use Carbon\Carbon;

class BonusLogController extends \App\Http\Controllers\Controller
{
    /**
     * @apiDefine SuccessBonusLog
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {Boolean} close_point
     * @apiSuccess (Success 200) {Double} total Total bonus
     * @apiSuccess (Success 200) {Double} pass_up Total bonus passed up to upline
     * @apiSuccess (Success 200) {Date} process_date
     * @apiSuccess (Success 200) {Array} details Example: {"discount_bonus": 100000, "achievement_share_bonus": 200000, "first_sales_bonus": 300000, "leadership_development_bonus": 400000, "leadership_matching_bonus": 500000}
     * @apiSuccess (Success 200) {Array} logs Bonus calculation logs
     * @apiSuccess (Success 200) {DateTime} transfer_date
     * @apiSuccess (Success 200) {String} bank_account_no
     * @apiSuccess (Success 200) {String} bank_account_name
     * @apiSuccess (Success 200) {String} bank_account_city
     * @apiSuccess (Success 200) {String} bank_account_branch
     * @apiSuccess (Success 200) {String} bank_name
     * @apiSuccess (Success 200) {String} member_id
     * 
     */

    /**
     * 
     * @api {get} /api/bonusLogs List
     * @apiName list
     * @apiGroup BonusLog
     * @apiPermission member
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {Integer} [order=process_date] Order by
     * @apiParam {Integer} [sort=desc] Sort direction
     * @apiParam {Integer} [month] Integer of month, 1-12
     * @apiParam {Integer} [year] Integer of year, eg. 2018
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of BonusLogs
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */          
    public function index(Request $request)
    {
        if (Auth::user()->role != User::ROLES['MEMBER']) {
            return $this->forbidden('Only for member');
        }
        $limit = $request->input('limit', 10);
        $order = 'bonus_logs.'.$request->input('order', 'process_date');
        $sort = $request->input('sort', 'desc');        
        $query = BonusLog::owned();
        if ($request->has('month') || $request->has('year')) {
            $start = Carbon::now();
            $start->startOfMonth();
            $start->month = $request->get('month', $start->month);
            $start->year = $request->get('year', $start->year);
            $end = clone $start;
            $end->month = $start->month + 1; 
            $end->startOfMonth();
            $query->where('process_date', '>=', $start)->where('process_date', '<', $end);
        }
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }

    /**
     * 
     * @api {get} /api/bonusLogs/:id Get
     * @apiName get
     * @apiGroup BonusLog
     * @apiPermission member
     * 
     * @apiUse SuccessBonusLog
     */        
    public function show($id)
    {
        $existing = BonusLog::find($id);
        if (empty($existing)) {
            return $this->notFound('Data not found: '.$id);
        }
        $error = $this->validateOwnership($existing->member);
        if ($error != null) return $error;
        return response()->json($existing
            ->makeHidden(['member'])
            ->makeVisible(['logs']));
    }

}
