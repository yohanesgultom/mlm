<?php

namespace App\Http\Controllers\API;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * @apiDefine SuccessProduct
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} name
     * @apiSuccess (Success 200) {String} desc
     * @apiSuccess (Success 200) {String} desc_short
     * @apiSuccess (Success 200) {String} retail_price
     * @apiSuccess (Success 200) {String} image_path
     * @apiSuccess (Success 200) {String} brochure_file
     * @apiSuccess (Success 200) {Double} base_price
     * @apiSuccess (Success 200) {Double} bonus_value
     * @apiSuccess (Success 200) {Double} point_value
     * @apiSuccess (Success 200) {Double} weight
     * @apiSuccess (Success 200) {ProductType} product_type
     * @apiSuccess (Success 200) {Array} tags
     * @apiSuccess (Success 200) {Integer} created_by
     * @apiSuccess (Success 200) {Integer} updated_by
     * @apiSuccess (Success 200) {DateTime} created_at
     * @apiSuccess (Success 200) {DateTime} updated_at
     * 
     */

    /**
     * 
     * @api {get} /api/productTypes/:product_type_id/products Product List
     * @apiName product list
     * @apiGroup Product
     * @apiPermission member
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {String} [name] Partial name
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Products
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */          
    public function index(Request $request, $product_type_id)
    {
        $limit = $request->input('limit', 10);
        $query = Product::with('tags')->sameCompany()->where('product_type_id', $product_type_id);
        // filter
        $input = $request->all();
        $input = $this->remapQuery($input, [
            'name' => 'products.name'
        ]);
        $query = $this->queryFilter($query, $input, [
            'name' => 'LIKE',
        ]);
        return response()->json($query->paginate($limit));
    }
}
