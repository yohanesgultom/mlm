<?php

namespace App\Http\Controllers\API;

use App\ProductType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductTypeController extends Controller
{
     /**
     * @apiDefine SuccessProductType
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} name
     * 
     */

    /**
     * 
     * @api {get} /api/productTypes ProductType List
     * @apiName list product_type
     * @apiGroup Product
     * @apiPermission member
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of ProductTypes
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */         
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $list = ProductType::sameCompany()->paginate($limit);
        return response()->json($list);
    }
}
