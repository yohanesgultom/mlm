<?php

namespace App\Http\Controllers\API;

use App\Order;
use App\Transaction;
use App\Wallet;
use App\Product;
use App\User;
use App\MemberStock;
use App\Warehouse;
use App\Business\CommerceService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class OrderController extends Controller
{

    /**
     * @apiDefine SuccessOrder
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {Integer} wallet_id
     * @apiSuccess (Success 200) {Array} transactions Array of transaction
     * 
     */

     /**
     * @apiDefine ParamOrder
     *
     * @apiParam {Integer} wallet_id Wallet ID used in this transaction
     * @apiParam {Integer} [warehouse_id] Regular warehouse ID for direct sale
     * @apiParam {Array} transactions Array of transactions
     * 
     */ 

    /**
     * @apiDefine SuccessTransaction
     *
     * @apiSuccess (Transaction) {Integer} id
     * @apiSuccess (Transaction) {Integer} code Transcation code
     * @apiSuccess (Transaction) {String} desc
     * @apiSuccess (Transaction) {Integer} qty Quantity of product
     * @apiSuccess (Transaction) {Double} amount Total amount of money paid (calculcated automatically)
     * @apiSuccess (Transaction) {Double} points Total points earned (calculcated automatically)
     * @apiSuccess (Transaction) {DateTime} process_date DateTime when the transaction processed
     * @apiSuccess (Transaction) {DateTime} point_date DateTime when points will be applied
     * @apiSuccess (Transaction) {Product} product
     * @apiSuccess (Transaction) {Integer} order_id
     * @apiSuccess (Transaction) {Boolean} transferred
     * 
     */

    /**
     * @apiDefine ParamTransaction
     *
     * @apiParam (Transaction) {Integer} [id] Transaction ID. Provide when updating
     * @apiParam (Transaction) {String} [desc]
     * @apiParam (Transaction) {Integer} qty Quantity of product
     * @apiParam (Transaction) {Integer} product_id
     * 
     */     


    /**
     * 
     * @api {get} /api/orders/statuses Statuses
     * @apiName statuses
     * @apiGroup Order
     * @apiPermission public
     * @apiDescription Get list of Statuses
     * 
     * @apiSuccess (Success 200) {Object} _ Statuses 
     *
     */ 

    /**
     * 
     * @api {get} /api/orders List
     * @apiName list
     * @apiGroup Order
     * @apiPermission member
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {Integer} [page] Page index to open
     * @apiParam {Integer} [limit] Limit per page
     * @apiParam {Integer} [order] Order by
     * @apiParam {Integer} [sort] Sort direction
     * @apiParam {Date} [created_date_start] YYYY-MM-DD
     * @apiParam {Date} [created_date_end] YYYY-MM-DD
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Orders
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $order = 'orders.'.$request->input('order', 'created_at');
        $sort = $request->input('sort', 'desc');
        $query = Order::with('transactions')
            ->join('wallets', 'wallets.id', 'orders.wallet_id')
            ->join('members', 'members.id', 'wallets.member_id')
            ->where('members.user_id', Auth::id())
            ->select('orders.*');
        if ($request->has('created_date_start') && $request->has('created_date_end')) {                        
            $range = [$request->input('created_date_start'), $request->input('created_date_end')];
            $query = $query->whereBetween('orders.created_at', $range);
        }
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }


    public function validateTransactionsArray($transactions, $wallet)
    {
        $product_ids = [];
        foreach ($transactions as $id => $t) {
            // get allowed fields
            $transactions[$id] = array_intersect_key($t, array_flip(['id', 'product_id', 'qty']));
            array_push($product_ids, $t['product_id']);            
        }

        // validate total against wallet balance
        if (count($product_ids) > 0) {
            $products = Product::whereIn('id', $product_ids)->get();
            $total = 0;
            foreach ($transactions as $t) {
                $prod = $products->where('id', $t['product_id'])->first();
                $subtotal = $prod->base_price * $t['qty'];
                $total += $subtotal;
            }
            if ($wallet->balance < $total) {
                throw new \Exception('Insufficient wallet balance');
            }
        }
        return $transactions;
    }

    /**
     * 
     * @api {post} /api/orders Create
     * @apiName create
     * @apiGroup Order
     * @apiPermission member
     * 
     * @apiHeader {String} Authorization `Bearer {access_token}`
     * @apiUse ParamOrder
     * @apiUse ParamTransaction
     * 
     * @apiUse SuccessOrder
     *
     */        
    public function store(Request $request)
    {
        $request->validate([
            'wallet_id' => 'required',
            'transactions' => 'required',
        ]);
        $input = $request->only(['wallet_id', 'transactions', 'warehouse_id']);
        $wallet = Wallet::sameCompany()->find($input['wallet_id']);
        if (empty($wallet)) {
            return $this->notFound('Wallet not found: '.$input['wallet_id']);
        }
        if ($wallet->member_id != Auth::user()->member->id) {
            return $this->notAuthorized('Not authorized to use wallet id '.$wallet->id);
        }

        try {
            $input = CommerceService::validateOrderWarehouse($input);
            $input['transactions'] = CommerceService::validateTransactions($input['transactions'], $wallet, $input['warehouse_id'], true);
        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }

        $order = new Order;
        DB::transaction(function () use (&$order, $input) {
            $order->fill($input);        
            $order->save();
            $order->transactions()->createMany($input['transactions']);
        }); 

        return response()->json(Order::with(['transactions'])->find($order->id));
    }

    /**
     * 
     * @api {get} /api/orders/:id Get
     * @apiName get
     * @apiGroup Order
     * @apiPermission member
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiUse SuccessOrder
     * 
     */    
    public function show($id)
    {
        $order = Order::with(['wallet.member', 'transactions'])->find($id);
        if (empty($order)) {
            return $this->notFound();
        }
        if ($order->wallet->member->user_id != Auth::id()) {
            return $this->notAuthorized();
        }
        return response()->json($order);
    }

    /**
     * @api {put} /api/orders/:id Update
     * @apiName update
     * @apiGroup Order
     * @apiPermission member
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} Authorization `Bearer {access_token}`
     * @apiUse ParamOrder
     * @apiUse ParamTransaction
     * 
     * @apiUse SuccessOrder
     */    
    public function update(Request $request, $id)
    {        
        $existing = Order::with(['transactions', 'wallet.member'])->find($id);
        if (empty($existing)) {
            return $this->notFound();
        }

        if ($existing->wallet->member->user_id != Auth::id()) {
            return $this->notAuthorized();
        }
        
        if ($existing->status != Order::STATUSES['UNPAID']) {
            return $this->forbidden('Status does not allow change: '.$existing->status_name);
        }

        $input = $request->only(['wallet_id', 'transactions', 'warehouse_id']);

        try {
            $input = CommerceService::validateOrderWarehouse($input);
            $input['transactions'] = CommerceService::validateTransactions($input['transactions'], $existing->wallet, $input['warehouse_id'], true);
        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }

        // separate new and updated transaction(s)
        $new = [];
        $updated = [];
        foreach ($input['transactions'] as $t) {
            if (array_key_exists('id', $t)) {
                array_push($updated, $t);
            } else {
                array_push($new, $t);
            }
        }

        // separate canceled transaction id(s) 
        $canceledIds = [];
        foreach ($existing->transactions as $te) {
            $found = false;
            foreach ($updated as $tu) {
                if ($tu['id'] == $te->id) {
                    $found = true;
                }
            }
            if (!$found) {
                array_push($canceledIds, $te->id);
            }
        }

        $existing->fill($input);
        DB::transaction(function () use (&$existing, $new, $updated, $canceledIds) {            
            $existing->save();
            $existing->transactions()->createMany($new);
            foreach ($updated as $t) {
                $tx = Transaction::find($t['id']);
                $tx->fill($t);
                $tx->save();
            }
            Transaction::destroy($canceledIds);
        });

        return response()->json(Order::with(['transactions'])->find($existing->id));
    }    

    /**
     * @api {delete} /api/orders/:id Delete
     * @apiName delete
     * @apiGroup Order
     * @apiPermission member
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} Authorization `Bearer {access_token}`
     * 
     * @apiUse SuccessOrder
     */    
    public function destroy($id)
    {
        $existing = Order::with('wallet.member')->find($id);
        if (empty($existing)) {
            return $this->notFound();
        }

        if ($existing->wallet->member->user_id != Auth::id()) {
            return $this->notAuthorized();
        }

        if ($existing->status != Order::STATUSES['UNPAID']) {
            return $this->forbidden('Status does not allow delete: '.$existing->status_name);
        }

        $existing->delete();
        return response()->json($existing);
    }
    
}
