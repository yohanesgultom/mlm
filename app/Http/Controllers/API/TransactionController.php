<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Transaction;
use App\MemberStock;
use Auth;
use DB;
use Carbon\Carbon;

class TransactionController extends \App\Http\Controllers\Controller
{
 	/**
	 * @apiDefine Success200Transaction
	 *
	 * @apiSuccess (Success 200) {Integer} id
	 * @apiSuccess (Success 200) {Integer} code Transcation code
	 * @apiSuccess (Success 200) {String} desc
	 * @apiSuccess (Success 200) {Integer} qty Quantity of product
	 * @apiSuccess (Success 200) {Double} amount Total amount of money paid (calculcated automatically)
	 * @apiSuccess (Success 200) {Double} points Total points earned (calculcated automatically)
	 * @apiSuccess (Success 200) {DateTime} process_date DateTime when the transaction processed
	 * @apiSuccess (Success 200) {DateTime} point_date DateTime when points will be applied
	 * @apiSuccess (Success 200) {Product} product
	 * @apiSuccess (Success 200) {Integer} order_id
	 * @apiSuccess (Success 200) {Boolean} transferred
	 * 
	 */

	/**
	 * @api {post} /api/transactions/:id/point Point
	 * @apiName point
	 * @apiGroup Transaction
	 * @apiPermission member
	 * @apiDescription Point certain quantity of transaction (set <code>point_date</code> to current date time)
	 * 
	 * @apiHeader {String} Accept `application/json`
	 * @apiHeader {String} Authorization `Bearer {access_token}`
	 * 
	 * @apiParam {Number} qty
	 * 
	 * @apiUse Success200Transaction
	 */    
	public function point(Request $request, $id)
	{        
		$existing = Transaction::with('order.wallet.member')->find($id);
		if (empty($existing)) {
			return $this->notFound();
		}
        if ($existing->order->wallet->member->user_id != Auth::id()) {
            return $this->notAuthorized();
        }
        if (!empty($existing->point_date)) {
            return $this->forbidden('Transaction has been pointed');
        }
        if ($existing->order->status != Order::STATUSES['COMPLETED']) {
            return $this->forbidden('Transaction order is not yet completed');
		}
		$request->validate([
			'qty' => 'required|min:1',
		]);
		$qty = $request->input('qty');

		$new_tx = null;
		$member_stock = null;
		$new_member_stock = null;
		if ($qty < $existing->qty) {
			// split transaction
			$new_tx = new Transaction;
			$new_tx->fill($existing->toArray());
			$new_tx->qty = $existing->qty - $qty;						
			$existing->qty = $qty;

			// update member_stock if available
			if (!empty($existing->member_stock_id)) {
				$member_stock = $existing->member_stock;
				$new_member_stock = new MemberStock;
				$new_member_stock->fill($member_stock->toArray());
				$new_member_stock->qty = $new_tx->qty;
				$member_stock->qty = $existing->qty;
			}
		}

		// update point date with current date
		// so it will be included in achivement
		// and bonus calculations
		$existing->point_date = new \DateTime();
		DB::transaction(function () use ($new_tx, $new_member_stock, $existing, $member_stock) {
			if (!empty($new_tx)) {
				$new_tx->save();
			}
			if (!empty($new_member_stock)) {
				$new_member_stock->save();			
			}
			if (!empty($member_stock)) {
				$member_stock->save();
			}			
			$existing->save();
		});
		return response()->json($existing);
	}    


    /**
     * 
     * @api {get} /api/transactions List
     * @apiName list
     * @apiGroup Transaction
     * @apiPermission member
     * 
     * @apiHeader {String} Accept `application/json`
     * 
	 * @apiParam {Number} [limit]
	 * @apiParam {String} [order]
	 * @apiParam {String} [sort] <code>asc</code> or <code>desc</code>
     * 
	 * @apiParam {Boolean} [completed]
	 * @apiParam {Boolean} [pointed]
	 * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Transactions
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request)
    {
		$limit = $request->input('limit', 10);
		$order = $request->input('order', 'id');
		$sort = $request->input('sort', 'desc');

		$query = Transaction::with('product')
			->select('transactions.*')
            ->join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->where('members.user_id', Auth::id());

		if ($request->has('pointed')) {
			if ($request->get('pointed')) {
				$query = $query->whereNotNull('transactions.point_date');
			} else {
				$query = $query->whereNull('transactions.point_date');
			}
        }
        
		if ($request->has('completed')) {
			if ($request->get('completed')) {
				$query = $query->where('orders.status', Order::STATUSES['COMPLETED']);
			} else {
				$query = $query->where('orders.status', '!=', Order::STATUSES['COMPLETED']);
			}
		}

        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }

}
