<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;

class PackageController extends Controller
{
     /**
     * @apiDefine SuccessPackage
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} name
     * @apiSuccess (Success 200) {Array} package_products Array of products and quantity
     * 
     */

    /**
     * 
     * @api {get} /api/companies/:company_id/memberTypes/:member_type_id/packages Package List
     * @apiName package list
     * @apiGroup MemberType
     * @apiPermission admin
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {String} [order=name] Order by
     * @apiParam {String} [sort=asc] Order direction
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Packages
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request, $company_id, $member_type_id)
    {
        $limit = $request->input('limit', 10);
        $order = $request->input('order', 'name');
        $sort = $request->input('sort', 'asc');
        $input = $request->all();
        $query = Package::with('package_products.product')
            ->where('member_type_id', $member_type_id);
        $query = $this->queryFilter($query, $input, [
            'name' => 'LIKE',
        ]);
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }

    /**
     * 
     * @api {get} /api/companies/:company_id/memberTypes/:member_type_id/packages/:id Package Get
     * @apiName package get
     * @apiGroup MemberType
     * @apiPermission admin
     * 
     * @apiUse SuccessPackage
     */    
    public function show($company_id, $member_type_id, $id)
    {
        $package = Package::with('package_products.product')
            ->where('member_type_id', $member_type_id)
            ->find($id);
        if (empty($package)) {
            return $this->notFound();
        }
        return response()->json($package);
    }

}
