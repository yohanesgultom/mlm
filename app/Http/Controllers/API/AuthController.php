<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Member;
use App\Package;
use App\Registration;
use Route;
use Auth;
use DB;

class AuthController extends Controller
{
    /**
     * @apiDefine SuccessUser
     *
     * @apiSuccess (User) {Integer} id
     * @apiSuccess (User) {String} username
     * @apiSuccess (User) {String} name
     * @apiSuccess (User) {String} email
     * @apiSuccess (User) {Integer} role
     * @apiSuccess (User) {String} role_name
     */    

    /**
     * @apiDefine SuccessRegistration
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} name
     * @apiSuccess (Success 200) {String} email
     * @apiSuccess (Success 200) {Package} package
     * @apiSuccess (Success 200) {DateTime} birth_date
     * @apiSuccess (Success 200) {Integer} gender
     * @apiSuccess (Success 200) {String} official_id
     * @apiSuccess (Success 200) {String} tax_id
     * @apiSuccess (Success 200) {String} address
     * @apiSuccess (Success 200) {String} address_rt
     * @apiSuccess (Success 200) {String} address_rw
     * @apiSuccess (Success 200) {String} address_zipcode
     * @apiSuccess (Success 200) {String} address_city
     * @apiSuccess (Success 200) {String} address_state
     * @apiSuccess (Success 200) {Integer} country_id
     * @apiSuccess (Success 200) {String} phone_home
     * @apiSuccess (Success 200) {String} phone_office
     * @apiSuccess (Success 200) {String} phone_mobile
     * @apiSuccess (Success 200) {String} phone_fax
     * @apiSuccess (Success 200) {String} bank_account_no
     * @apiSuccess (Success 200) {String} bank_account_name
     * @apiSuccess (Success 200) {String} bank_account_city
     * @apiSuccess (Success 200) {String} bank_account_branch
     * @apiSuccess (Success 200) {Wallet} sponsor_wallet Sponsor wallet
     * @apiSuccess (Success 200) {DateTime} completed_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {Integer} status
     * @apiSuccess (Success 200) {String} status_name
     * @apiSuccess (Success 200) {String} member_id
     * @apiSuccess (Success 200) {String} parent_id
     * @apiSuccess (Success 200) {Integer} order_id
     * 
     */

     /**
     * @apiDefine ParamRegistration
     *
     * @apiParam {String} name
     * @apiParam {String} email
     * @apiParam {Integer} package_id
     * @apiParam {DateTime} [birth_date]
     * @apiParam {Integer} [gender]
     * @apiParam {String} [official_id]
     * @apiParam {String} [tax_id]
     * @apiParam {String} [address]
     * @apiParam {String} [address_rt]
     * @apiParam {String} [address_rw]
     * @apiParam {String} [address_zipcode]
     * @apiParam {String} [address_city]
     * @apiParam {String} [address_state]
     * @apiParam {Integer} [country_id]
     * @apiParam {String} [phone_home]
     * @apiParam {String} [phone_office]
     * @apiParam {String} [phone_mobile]
     * @apiParam {String} [phone_fax]
     * @apiParam {String} [bank_account_no]
     * @apiParam {String} [bank_account_name]
     * @apiParam {String} [bank_account_city]
     * @apiParam {String} [bank_account_branch]
     * @apiParam {String} [parent_id] Upline member id
     * 
     */ 
     
    /**
     * @api {post} /api/login Login
     * @apiName login
     * @apiGroup Authentication
	 * @apiPermission public
     *
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {String} member_id
     * @apiParam {Integer} company_id
     * @apiParam {String} password
     * 
     * @apiSuccess (Success 200) {Integer} expires_in Number of seconds until the access token expires
     * @apiSuccess (Success 200) {String} access_token 
     * @apiSuccess (Success 200) {String} refresh_token
     * 
     */    	
	public function login(Request $request)
	{
		$client = DB::table('oauth_clients')->where('password_client', 1)->first();

        $member_id = $request->input('member_id');
        $company_id = $request->input('company_id');

        $member = Member::with('user')->where('members.id', $member_id)->first();
        if (empty($member)) {
            return $this->badRequest("Invalid member_id: {$member_id}");
        }

        if ($member->user->company_id != $company_id) {
            return $this->forbidden("Member {$member_id} not belongs to company_id {$company_id}");
        }

		$request->request->add([
			'username' => $member->user->username,
			'grant_type' => 'password',
			'client_id' => $client->id,
			'client_secret' => $client->secret,
			'scope' => '*'
		]);

        $tokenRequest = Request::create('/oauth/token','post');
        return Route::dispatch($tokenRequest);		
	}

    /**
     * @api {post} /api/refresh Refresh
     * @apiName refresh
     * @apiGroup Authentication
	 * @apiPermission member
	 * @apiDescription Refresh or extend `access_token` lifetime
     *
	 * @apiHeader {String} Authorization `Bearer {access_token}`
     * @apiHeader {String} Accept `application/json`
	 * 
     * @apiParam {String} refresh_token
	 * 
     * @apiSuccess (Success 200) {Integer} expires_in Number of seconds until the access token expires
     * @apiSuccess (Success 200) {String} access_token New <code>access_token</code>
     * @apiSuccess (Success 200) {String} refresh_token New <code>refresh_token</code>
     * 
     */     		
	public function refresh(Request $request)
	{
		$client = DB::table('oauth_clients')->where('password_client', 1)->first();

		$request->request->add([
			'grant_type' => 'refresh_token',
			'client_id' => $client->id,
			'client_secret' => $client->secret,
			'scope' => '*'
		]);

        $tokenRequest = Request::create('/oauth/token','post');
        return Route::dispatch($tokenRequest);		
	}
	
    /**
     * @api {post} /api/logout Logout
     * @apiName logout
     * @apiGroup Authentication
	 * @apiPermission member
     *
	 * @apiHeader {String} Authorization `Bearer {access_token}`
     * @apiHeader {String} Accept `application/json`
	 * 
	 * @apiSuccess (Success 204) _
	 * 
     */     	
	public function logout()
	{
		Auth::user()->token()->revoke();
		Auth::user()->token()->delete();
		return response()->json('', 204);
	}


    /**
     * @apiDefine SuccessArticle
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {Integer} type
     * @apiSuccess (Success 200) {String} title
     * @apiSuccess (Success 200) {String} slug
     * @apiSuccess (Success 200) {String} html
     * @apiSuccess (Success 200) {Integer} status
     * @apiSuccess (Success 200) {DateTime} publish_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {Integer} created_by Creator user ID
     * @apiSuccess (Success 200) {Integer} updated_by Modifier user ID
     * 
     */

     /**
     * @apiDefine ParamArticle
     *
     * @apiParam {String} title
     * @apiParam {Integer} type
     * @apiParam {String} html
     * @apiParam {Integer} status
     * @apiParam {DateTime} publish_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * 
     */

    /**
     * 
     * @api {get} /api/articles/types Types
     * @apiName types
     * @apiGroup Article
     * @apiPermission public
     * @apiDescription Get list of Types
     * 
     * @apiSuccess (Success 200) {Object} _ Types 
     *
     */
	
    /**
     * 
     * @api {get} /api/articles/statuses Statuses
     * @apiName statuses
     * @apiGroup Article
     * @apiPermission public
     * @apiDescription Get list of Statuses
     * 
     * @apiSuccess (Success 200) {Object} _ Statuses 
     *
     */ 
    
    /**
     * 
     * @api {get} /api/companies/:company_id/articles Article List
     * @apiName article list
     * @apiGroup Company
     * @apiPermission public
     * 
     * @apiParam {Integer} company_id
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Articles
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     

    /**
     * 
     * @api {get} /api/companies/:company_id/articles/:id Article Get
     * @apiName article get
     * @apiGroup Company
     * @apiPermission public
     * 
     * @apiParam {Integer} company_id
     * @apiParam {Integer} id
     * 
     * @apiUse SuccessArticle
     */    

    /**
     * 
     * @api {get} /api/companies/:id/bankAccounts List
     * @apiName list
     * @apiGroup BankAccount
     * @apiPermission public
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of BankAccounts
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */       
     

    /**
     * 
     * @api {post} /api/register Register
     * @apiName register
     * @apiGroup Authentication
     * @apiPermission public
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiUse ParamRegistration
     * @apiUse SuccessRegistration
     * 
     */     
     public function register(Request $request)
     {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'package_id' => 'required',
        ]);
        $input = $request->except([
            'id',
            'status',
            'member_id',
            'sponsor_wallet_id',
            'order_id',
            'company_id',
            'completed_date',
        ]);
        $package = Package::with('member_type')->find($input['package_id']);
        if (empty($package)) {
            return $this->notFound('package_id not found: '.$input['package_id']);
        }
        if (array_key_exists('parent_id', $input)) {
            $parent = Member::find($input['parent_id']);
            if (empty($parent)) {
                return $this->notFound('parent_id not found: '.$input['parent_id']);
            }
            if ($parent->user->company_id != $package->member_type->company_id) {
                return $this->forbidden('parent_id belongs to other company: '.$input['parent_id']);
            }
        }
        $registration = new Registration;
        $registration->fill($input);
        $registration->company_id = $package->member_type->company_id;
        $registration->status = Registration::STATUSES['UNPAID'];
        $registration->save();        
        return response()->json(Registration::find($registration->id));
     }
}