<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Member;
use App\Wallet;
use App\Registration;
use App\Package;
use Auth;
use DB;

class MemberController extends \App\Http\Controllers\MemberController
{
     /**
     * @apiDefine SuccessMember
     *
     * @apiSuccess (Success 200) {User} user
     * @apiSuccess (Success 200) {String} id Member id
     * @apiSuccess (Success 200) {Integer} parent_id Parent's member id
     * @apiSuccess (Success 200) {Integer} sponsor_id Sponsor's member id
     * @apiSuccess (Success 200) {Integer} member_type_id
     * @apiSuccess (Success 200) {String} name_printed
     * @apiSuccess (Success 200) {String} image_path
     * @apiSuccess (Success 200) {DateTime} reg_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {DateTime} birth_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {DateTime} bonus_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {Achievement} achievement Current achievement
     * @apiSuccess (Success 200) {Achievement} highest_achievement Highest achievement
     * @apiSuccess (Success 200) {Integer} gender
     * @apiSuccess (Success 200) {String} official_id Official/government-released ID number
     * @apiSuccess (Success 200) {String} tax_id Tax Id number
     * @apiSuccess (Success 200) {Double} current_personal_points Current total point value (PV)
     * @apiSuccess (Success 200) {Double} current_personal_bonus Current total bonus value (BV)
     * @apiSuccess (Success 200) {Double} current_group_points
     * @apiSuccess (Success 200) {String} address
     * @apiSuccess (Success 200) {String} address_rt
     * @apiSuccess (Success 200) {String} address_rw
     * @apiSuccess (Success 200) {String} address_zipcode
     * @apiSuccess (Success 200) {String} address_city
     * @apiSuccess (Success 200) {String} address_state
     * @apiSuccess (Success 200) {Integer} country_id
     * @apiSuccess (Success 200) {String} phone_home
     * @apiSuccess (Success 200) {String} phone_office
     * @apiSuccess (Success 200) {String} phone_mobile
     * @apiSuccess (Success 200) {String} phone_fax
     * @apiSuccess (Success 200) {String} beneficiary_name
     * @apiSuccess (Success 200) {String} beneficiary_relation
     * @apiSuccess (Success 200) {String} bank_account_no
     * @apiSuccess (Success 200) {String} bank_account_name
     * @apiSuccess (Success 200) {String} bank_account_city
     * @apiSuccess (Success 200) {String} bank_account_branch
     * @apiSuccess (Success 200) {Integer} bank_id
     * @apiSuccess (Success 200) {Integer} status Member's status
     * 
     */

     /**
     * @apiDefine ParamMember
     *
     * @apiParam {String} name
     * @apiParam {String} email
     * @apiParam {DateTime} birth_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiParam {Integer} gender
     * @apiParam {File} [image_path]
     * @apiParam {String} [name_printed] 
     * @apiParam {String} [official_id] Official/government-released ID number
     * @apiParam {String} [tax_id] Tax Id number
     * @apiParam {String} [address] 
     * @apiParam {String} [address_rt] 
     * @apiParam {String} [address_rw] 
     * @apiParam {String} [address_zipcode] 
     * @apiParam {String} [address_city] 
     * @apiParam {String} [address_state] 
     * @apiParam {Integer} [country_id] 
     * @apiParam {String} [phone_home] 
     * @apiParam {String} [phone_office] 
     * @apiParam {String} [phone_mobile] 
     * @apiParam {String} [phone_fax] 
     * @apiParam {String} [beneficiary_name] 
     * @apiParam {String} [beneficiary_relation] 
     * @apiParam {String} [bank_account_no] 
     * @apiParam {String} [bank_account_name] 
     * @apiParam {String} [bank_account_city] 
     * @apiParam {String} [bank_account_branch] 
     * @apiParam {Integer} [bank_id] 
     * 
     */

    /**
     * @apiDefine SuccessAchievement
     *
     * @apiSuccess (Achievement) {Integer} id
     * @apiSuccess (Achievement) {String} code
     * @apiSuccess (Achievement) {String} desc
     * @apiSuccess (Achievement) {Integer} order
     */
     

    /**
     * @apiDefine SuccessCompany
     *
     * @apiSuccess (Company) {Integer} id
     * @apiSuccess (Company) {String} name
     * @apiSuccess (Company) {Integer} cutoff_day Number of days between cut-off
     * @apiSuccess (Company) {String} weekly_cutoff_day_code Day code of weekly cut-off
     * 
     */     

    /**
     * @apiDefine SuccessSponsorRegistration
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} name
     * @apiSuccess (Success 200) {String} email
     * @apiSuccess (Success 200) {Package} package
     * @apiSuccess (Success 200) {DateTime} birth_date
     * @apiSuccess (Success 200) {Integer} gender
     * @apiSuccess (Success 200) {String} official_id
     * @apiSuccess (Success 200) {String} tax_id
     * @apiSuccess (Success 200) {String} address
     * @apiSuccess (Success 200) {String} address_rt
     * @apiSuccess (Success 200) {String} address_rw
     * @apiSuccess (Success 200) {String} address_zipcode
     * @apiSuccess (Success 200) {String} address_city
     * @apiSuccess (Success 200) {String} address_state
     * @apiSuccess (Success 200) {Integer} country_id
     * @apiSuccess (Success 200) {String} phone_home
     * @apiSuccess (Success 200) {String} phone_office
     * @apiSuccess (Success 200) {String} phone_mobile
     * @apiSuccess (Success 200) {String} phone_fax
     * @apiSuccess (Success 200) {String} bank_account_no
     * @apiSuccess (Success 200) {String} bank_account_name
     * @apiSuccess (Success 200) {String} bank_account_city
     * @apiSuccess (Success 200) {String} bank_account_branch
     * @apiSuccess (Success 200) {Wallet} sponsor_wallet Sponsor wallet
     * @apiSuccess (Success 200) {DateTime} completed_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {Integer} status
     * @apiSuccess (Success 200) {String} status_name
     * @apiSuccess (Success 200) {String} member_id
     * @apiSuccess (Success 200) {String} parent_id
     * @apiSuccess (Success 200) {Integer} order_id
     * 
     */

     /**
     * @apiDefine ParamSponsorRegistration
     *
     * @apiParam {String} name
     * @apiParam {String} email
     * @apiParam {Integer} package_id
     * @apiParam {Integer} sponsor_wallet_id
     * @apiParam {DateTime} [birth_date]
     * @apiParam {Integer} [gender]
     * @apiParam {String} [official_id]
     * @apiParam {String} [tax_id]
     * @apiParam {String} [address]
     * @apiParam {String} [address_rt]
     * @apiParam {String} [address_rw]
     * @apiParam {String} [address_zipcode]
     * @apiParam {String} [address_city]
     * @apiParam {String} [address_state]
     * @apiParam {Integer} [country_id]
     * @apiParam {String} [phone_home]
     * @apiParam {String} [phone_office]
     * @apiParam {String} [phone_mobile]
     * @apiParam {String} [phone_fax]
     * @apiParam {String} [bank_account_no]
     * @apiParam {String} [bank_account_name]
     * @apiParam {String} [bank_account_city]
     * @apiParam {String} [bank_account_branch]
     * @apiParam {String} [parent_id] Upline member id
     * 
     */ 

    /**
     * @api {get} /api/members/me Member Me
     * @apiName me
     * @apiGroup Member
     * @apiPermission member
     * @apiDescription Get details of currently logged-in member
     *
	 * @apiHeader {String} Authorization `Bearer {access_token}`
	 * 
     * @apiUse SuccessMember
     * @apiUse SuccessUser
     * @apiSuccess (User) {Company} company
     * 
     * @apiUse SuccessAchievement
     * @apiUse SuccessCompany
     * 
     */	 

    /**
     * @api {get} /api/downlines Member Downlines
     * @apiName downlines
     * @apiGroup Member
     * @apiPermission member
     *
     * @apiParam {String} by <code>'parent'</code> (default) or <code>'sponsor'</code>
     * 
     * @apiSuccess (Success 200) {Array} _ Nested array (tree) of downlines
     * 
     * @apiSuccessExample {json} Success Response Example
     *  {
     *  	"id": "58499436",
     *  	"name": "Rafael",
     *  	"downlines": [
     *  		{
     *  			"id": "287244482",
     *  			"name": "Kaylee",
     *  			"downlines": [
     *  				{
     *  					"id": "44886691",
     *  					"name": "Nichole",
     *  					"downlines": []
     *  				},
     *  				{
     *  					"id": "970645228",
     *  					"name": "Santina",
     *  					"downlines": []
     *  				}
     *  			]
     *  		},
     *  		{
     *  			"id": "86789748",
     *  			"name": "Elyssa",
     *  			"downlines": [
     *  				{
     *  					"id": "1191394098",
     *  					"name": "Judy",
     *  					"downlines": []
     *  				},
     *  				{
     *  					"id": "704957549",
     *  					"name": "Mariam",
     *  					"downlines": []
     *  				}
     *  			]
     *  		}
     *  	]
     *  }
     * 
     */    	
	public function memberDownlines(Request $request)
    {
        $member = Auth::user()->member;
        $by = strtolower($request->input('by', 'parent'));
        if ($by == 'sponsor') {
            $field = 'sponsor_id';
        } else if ($by == 'parent') {
            $field = 'parent_id';
        } else if (empty($by)) {
            return $this->badRequest('unknown field: '.$field);
        }

        $root = [
            'id' => $member->id, 
            'name' => $member->user->name,
            'downlines' => $this->getDownlines($member->id, $field),
        ];
        return response()->json($root);
    }

    /**
     * @api {post} /api/members/me Member Update
     * @apiName update
     * @apiGroup Member
     * @apiPermission member
     * @apiDescription A multipart PUT request on PHP needs hack by using POST method and adding _method:PUT param https://github.com/laravel/framework/issues/13457#issuecomment-282542694. Original bug https://bugs.php.net/bug.php?id=55815
     * 
     * @apiHeader {String} Authorization `Bearer {access_token}`
     * 
     * @apiParam {String} _method PUT
     * @apiUse ParamMember
     * @apiUse SuccessMember
     */
    public function updateMe(Request $request)
    {
        $request->validate([
            'email' => 'email',
            'image_path'  => 'image|nullable',
        ]);

        $existing = Auth::user()->member;
        $input = $request->only([
            'name',
            'email',
            'name_printed',
            'birth_date',
            'gender',
            'official_id',
            'tax_id',
            'address',
            'address_rt',
            'address_rw',
            'address_zipcode',
            'address_city',
            'address_state',
            'country_id',
            'phone_home',
            'phone_office',
            'phone_mobile',
            'phone_fax',
            'beneficiary_name',
            'beneficiary_relation',
            'bank_account_no',
            'bank_account_name',
            'bank_account_city',
            'bank_account_branch',
            'bank_id',
            'image_path',
        ]);
        $input = $this->processPublicInputFiles('member', ['image_path'], $request, $input);
        $existing->user->fill($input);
        $existing->fill($input);
        DB::transaction(function () use ($existing) {
            $existing->user->save();
            $existing->save();
        });

        return response()->json(Member::find($existing->id));
    }

    /**
     * @api {post} /api/changePassword Member Change Password
     * @apiName changePassword
     * @apiGroup Member
     * @apiPermission member
     * 
     * @apiHeader {String} Authorization `Bearer {access_token}`
     * 
     * @apiParam {String} password
     * @apiParam {String} password_new
     * 
     */    
    public function changePassword(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'password_new'  => 'required|min:6',
        ]);

        $existing = Auth::user();
        $input = $request->only([
            'password',
            'password_new',
        ]);
        
        if (Hash::check($input['password'], $existing->password)) {
            $existing->password = $input['password_new'];
            $existing->save();
        } else {
            return $this->forbidden('Invalid password');
        }
        
        return response()->json([], 204);
    }
    
    /**
     * 
     * @api {post} /api/sponsors Member Sponsors
     * @apiName sponsors
     * @apiGroup Member
     * @apiPermission member
     * @apiDescription Sponsors new member registration
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} Authorization `Bearer {access_token}`
     * 
     * @apiUse ParamSponsorRegistration
     * @apiUse SuccessSponsorRegistration
     * 
     */     
    public function sponsors(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'package_id' => 'required',
            'sponsor_wallet_id' => 'required',
        ]);
        $input = $request->except([
            'id',
            'status',
            'member_id',
            'order_id',
            'company_id',
            'completed_date',
        ]);
        
        // validate package
        $package = Package::with('member_type')->find($input['package_id']);
        if (empty($package)) {
            return $this->notFound('package_id not found: '.$input['package_id']);
        }
        if ($package->member_type->company_id != Auth::user()->company_id) {
            return $this->forbidden('package_id belongs to other company: '.$input['package_id']);
        }

        // validate sponsor
        $sponsor_wallet = Wallet::find($input['sponsor_wallet_id']);
        if (empty($sponsor_wallet)) {
            return $this->notFound('sponsor_wallet_id not found: '.$input['sponsor_wallet_id']);
        }
        if ($sponsor_wallet->member_id != Auth::user()->member->id) {
            return $this->forbidden('sponsor_wallet_id belongs to other member '.$input['sponsor_wallet_id']);
        }

        // validate parent
        if (array_key_exists('parent_id', $input)) {
            $parent = Member::find($input['parent_id']);
            if (empty($parent)) {
                return $this->notFound('parent_id not found: '.$input['parent_id']);
            }
            if ($parent->user->company_id != $package->member_type->company_id) {
                return $this->forbidden('parent_id belongs to other company: '.$input['parent_id']);
            }
        }

        // create registration
        $registration = new Registration;
        $registration->fill($input);
        $registration->company_id = $package->member_type->company_id;
        $registration->status = Registration::STATUSES['UNPAID'];
        $registration->save();        
        return response()->json(Registration::find($registration->id));
    }    
}