<?php

namespace App\Http\Controllers\API;

use App\Wallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class WalletController extends Controller
{
     /**
     * @apiDefine SuccessWallet
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {Integer} type
     * @apiSuccess (Success 200) {String} type_name
     * @apiSuccess (Success 200) {Double} balance
     * @apiSuccess (Success 200) {String} member_id
     * 
     */

    /**
     * 
     * @api {get} /api/wallets/types Types
     * @apiName types
     * @apiGroup Wallet
     * @apiPermission public
     * @apiDescription Get list of Wallet Types
     * 
     * @apiSuccess (Success 200) {Object} _ Wallet Types 
     *
     */ 
    public function types(Request $request)
    {        
        return response()->json((object) array_flip(Wallet::TYPES));
    }
     
    /**
     * 
     * @api {get} /api/wallets List
     * @apiName product list
     * @apiGroup Wallet
     * @apiPermission member
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Wallets
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */          
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $order = $request->input('order', 'wallets.id');
        $sort = $request->input('sort', 'asc');
        $query = Wallet::ownedBy(Auth::user()->member->id);
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }
}
