<?php

namespace App\Http\Controllers\API;

use App\MemberStock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class MemberStockController extends Controller
{
     /**
     * @apiDefine SuccessMemberStock
     *
     * @apiSuccess (MemberStock) {Integer} id
     * @apiSuccess (MemberStock) {Integer} qty Quantity
     * @apiSuccess (MemberStock) {DateTime} exp_date Expiry date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (MemberStock) {Product} product
     * @apiSuccess (MemberStock) {String} member_id
     * 
     */
    
    /**
     * 
     * @api {get} /api/stocks List
     * @apiName list
     * @apiGroup Stock
     * @apiPermission member
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {Integer} [page] Page index to open
     * @apiParam {Integer} [limit] Limit per page
     * @apiParam {Integer} [order] Order by
     * @apiParam {Integer} [sort] Sort direction
     * 
     * @apiParam {String} [name] Product name
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of stocks
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     * 
     * @apiUse SuccessMemberStock
     */ 
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $order = $request->input('order', 'products.name');
        $sort = $request->input('sort', 'asc');

        $query = MemberStock::with('product')
            ->select('member_stocks.*')
            ->join('products', 'products.id', 'member_stocks.product_id')
            ->where('member_id', Auth::user()->member->id)
            ->where('qty', '>', 0);

        // filter
        $input = $request->all();
        $input = $this->remapQuery($input, [
            'name' => 'products.name', 
        ]);
        $query = $this->queryFilter($query, $input, [
            'products.name' => 'LIKE',
        ]);
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }
}
