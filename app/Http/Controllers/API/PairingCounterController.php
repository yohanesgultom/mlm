<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\PairingCounter;
use Auth;
use Carbon\Carbon;

class PairingCounterController extends \App\Http\Controllers\Controller
{
     /**
     * @apiDefine SuccessPairingCounter
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {DateTime} process_date Format YYYY-MM-DD HH:mm:ss
     * @apiSuccess (Success 200) {Integer} count_left Number of left branch downlines
     * @apiSuccess (Success 200) {Integer} count_mid Number of mid branch downlines
     * @apiSuccess (Success 200) {Integer} count_right Number of right branch downlines
     * @apiSuccess (Success 200) {String} member_id
     * 
     */

    /**
     * 
     * @api {get} /api/pairingCounters List
     * @apiName list
     * @apiGroup PairingCounter
     * @apiPermission member
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {Integer} [order=process_date] Order by
     * @apiParam {Integer} [sort=desc] Sort direction
     * @apiParam {Integer} [month] Integer of month, 1-12
     * @apiParam {Integer} [year] Integer of year, eg. 2018
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of PairingCounters
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */          
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $order = 'pairing_counters.'.$request->input('order', 'id');
        $sort = $request->input('sort', 'asc');        
        $query = PairingCounter::owned();
        if ($request->has('month') || $request->has('year')) {
            $start = Carbon::now();
            $start->startOfMonth();
            $start->month = $request->get('month', $start->month);
            $start->year = $request->get('year', $start->year);
            $end = clone $start;
            $end->month = $start->month + 1; 
            $end->startOfMonth();
            $query->whereBetween('process_date', [$start, $end]);
        }
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }

    /**
     * 
     * @api {get} /api/pairingCounters/:id Get
     * @apiName get
     * @apiGroup PairingCounter
     * @apiPermission member
     * 
     * @apiUse SuccessPairingCounter
     */        
    public function show($id)
    {
        $pairing_counter = PairingCounter::find($id);
        if (empty($pairing_counter)) {
            return $this->notFound();
        }
        $error = $this->validateOwnership($pairing_counter->member);
        if ($error != null) return $error;
        return response()->json($pairing_counter);
    }

}
