<?php

namespace App\Http\Controllers\API;

use App\Delivery;
use App\DeliveryItem;
use App\MemberStock;
use App\Business\CommerceService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class DeliveryController extends Controller
{

    /**
     * @apiDefine SuccessDelivery
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {Double} cost
     * @apiSuccess (Success 200) {String} courier Courier name
     * @apiSuccess (Success 200) {String} courier_ref Courier reference code
     * @apiSuccess (Success 200) {Array} items Array of items
     * @apiSuccess (Success 200) {String} address
     * @apiSuccess (Success 200) {String} address_zipcode
     * @apiSuccess (Success 200) {String} address_city
     * @apiSuccess (Success 200) {String} address_state
     * @apiSuccess (Success 200) {Country} country
     * @apiSuccess (Success 200) {String} status_name
     * @apiSuccess (Success 200) {String} member_id
     * @apiSuccess (Success 200) {DateTime} delivery_date 
     * 
     */

     /**
     * @apiDefine ParamDelivery
     *
     * @apiParam {String} [courier] Courier name
     * @apiParam {Array} items Array of items
     * @apiParam {String} [address]
     * @apiParam {String} [address_zipcode]
     * @apiParam {String} [address_city]
     * @apiParam {String} [address_state]
     * @apiParam {Integer} [country_id]
     * 
     */ 

    /**
     * @apiDefine SuccessDeliveryItem
     *
     * @apiSuccess (DeliveryItem) {Integer} id
     * @apiSuccess (DeliveryItem) {Integer} qty Quantity of product
     * @apiSuccess (DeliveryItem) {Product} product
     * @apiSuccess (DeliveryItem) {Integer} delivery_id
     * 
     */

    /**
     * @apiDefine ParamDeliveryItem
     *
     * @apiParam (DeliveryItem) {Integer} [id] Item ID. Provide when updating
     * @apiParam (DeliveryItem) {Integer} qty Quantity of product
     * @apiParam (DeliveryItem) {Integer} product_id
     * 
     */     

    /**
     * 
     * @api {get} /api/deliveries/statuses Statuses
     * @apiName statuses
     * @apiGroup Delivery
     * @apiPermission public
     * @apiDescription Get list of Statuses
     * 
     * @apiSuccess (Success 200) {Object} _ Statuses 
     *
     */ 

    /**
     * 
     * @api {get} /api/deliveries List
     * @apiName list
     * @apiGroup Delivery
     * @apiPermission member
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {Integer} [page] Page index to open
     * @apiParam {Integer} [limit] Limit per page
     * @apiParam {Integer} [order] Order by
     * @apiParam {Integer} [sort] Sort direction
     * @apiParam {Date} [delivery_date_start] YYYY-MM-DD
     * @apiParam {Date} [delivery_date_end] YYYY-MM-DD
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Deliveries
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $order = $request->input('order', 'id');
        $sort = $request->input('sort', 'asc');
        $query = Delivery::with(['country', 'items'])->where('member_id', Auth::user()->member->id);
        if ($request->has('delivery_date_start') && $request->has('delivery_date_end')) {                        
            $range = [$request->input('delivery_date_start'), $request->input('delivery_date_end')];
            $query = $query->whereBetween('delivery_date', $range);
        }
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }

    /**
     * 
     * @api {post} /api/deliveries Create
     * @apiName create
     * @apiGroup Delivery
     * @apiPermission member
     * 
     * @apiHeader {String} Authorization `Bearer {access_token}`
     * @apiUse ParamDelivery
     * @apiUse ParamDeliveryItem
     * 
     * @apiUse SuccessDelivery
     *
     */        
    public function store(Request $request)
    {
        $request->validate([
            'items' => 'required',
            'address' => 'required',            
        ]);
        $input = $request->only([
            'courier',
            'items',
            'address',
            'address_zipcode',
            'address_city',
            'address_state',
            'country_id',
        ]);

        try {
            $input['items'] = CommerceService::validateDeliveryItems($input['items'], Auth::user()->member->id);
        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }
        $delivery = new Delivery;        
        $delivery->fill($input);
        $delivery->member_id = Auth::user()->member->id;
        $delivery->status = Delivery::STATUSES['UNPAID'];
        DB::transaction(function () use (&$delivery, $input) {                    
            $delivery->save();
            $delivery->items()->createMany($input['items']);
        }); 

        return response()->json(Delivery::with(['country', 'items'])->find($delivery->id));
    }

    /**
     * 
     * @api {get} /api/deliveries/:id Get
     * @apiName get
     * @apiGroup Delivery
     * @apiPermission member
     * 
     * @apiParam {Number} id
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiUse SuccessDelivery
     * 
     */    
    public function show($id)
    {
        $existing = Delivery::with(['country', 'items'])->find($id);
        if (empty($existing)) {
            return $this->notFound();
        }
        if ($existing->member_id != Auth::user()->member->id) {
            return $this->notAuthorized();
        }
        return response()->json($existing);
    }

    /**
     * @api {put} /api/deliveries/:id Update
     * @apiName update
     * @apiGroup Delivery
     * @apiPermission member
     * 
     * @apiParam {Number} id
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} Authorization `Bearer {access_token}`
     * @apiUse ParamDelivery
     * @apiUse ParamDeliveryItem
     * 
     * @apiUse SuccessDelivery
     */    
    public function update(Request $request, $id)
    {        
        $existing = Delivery::with('items')->find($id);
        if (empty($existing)) {
            return $this->notFound();
        }
        
        if ($existing->member_id != Auth::user()->member->id) {
            return $this->notAuthorized();
        }

        if ($existing->status != Delivery::STATUSES['UNPAID']) {
            return $this->forbidden('Status does not allow change: '.$existing->status_name);
        }

        $input = $request->only([
            'courier',
            'items',
            'address',
            'address_zipcode',
            'address_city',
            'address_state',
            'country_id',
        ]);

        try {
            $input['items'] = CommerceService::validateDeliveryItems($input['items'], Auth::user()->member->id);
        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }

        // separate new and updated item(s)
        $new = [];
        $updated = [];
        foreach ($input['items'] as $t) {
            if (array_key_exists('id', $t)) {
                array_push($updated, $t);
            } else {
                array_push($new, $t);
            }
        }

        // separate canceled item id(s) 
        $canceledIds = [];
        foreach ($existing->items as $te) {
            $found = false;
            foreach ($updated as $tu) {
                if ($tu['id'] == $te->id) {
                    $found = true;
                }
            }
            if (!$found) {
                array_push($canceledIds, $te->id);
            }
        }

        $existing->fill($input);
        DB::transaction(function () use (&$existing, $new, $updated, $canceledIds) {            
            $existing->save();
            $existing->items()->createMany($new);
            foreach ($updated as $t) {
                DeliveryItem::where('id', $t['id'])->update($t);
            }
            DeliveryItem::destroy($canceledIds);
        });

        return response()->json(Delivery::with(['country', 'items'])->find($existing->id));
    }    

    /**
     * @api {delete} /api/deliveries/:id Delete
     * @apiName delete
     * @apiGroup Delivery
     * @apiPermission member
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} Authorization `Bearer {access_token}`
     * 
     * @apiUse SuccessDelivery
     */    
    public function destroy($id)
    {
        $existing = Delivery::find($id);
        if (empty($existing)) {
            return $this->notFound();
        }

        if ($existing->member_id != Auth::user()->member->id) {
            return $this->notAuthorized();
        }

        if ($existing->status != Delivery::STATUSES['UNPAID']) {
            return $this->forbidden('Status does not allow delete: '.$existing->status_name);
        }

        $existing->delete();
        return response()->json($existing);
    }    
}
