<?php

namespace App\Http\Controllers;

use App\Deposit;
use App\Member;
use App\User;
use Illuminate\Http\Request;
use Auth;

class DepositController extends Controller
{

    /**
     * @apiDefine SuccessDeposit
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} desc
     * @apiSuccess (Success 200) {Double} amount
     * @apiSuccess (Success 200) {DateTime} deposit_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {BankAccount} bank_account
     * @apiSuccess (Success 200) {Integer} status
     * @apiSuccess (Success 200) {String} status_name
     * @apiSuccess (Success 200) {String} member_id     
     * 
     */

     /**
     * @apiDefine ParamDeposit
     *
     * @apiParam {Double} amount
     * @apiParam {String} [desc]
     * @apiParam {DateTime} deposit_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiParam {Integer} bank_account_id
     * @apiParam {Integer} status
     * @apiParam {String} member_id
     * 
     */ 


    /**
     * 
     * @api {get} /deposits/statuses Statuses
     * @apiName statuses
     * @apiGroup Deposit
     * @apiPermission public
     * @apiDescription Get list of Statuses
     * 
     * @apiSuccess (Success 200) {Object} _ Statuses 
     *
     */ 
    public function statuses(Request $request)
    {        
        return response()->json((object) array_flip(Deposit::STATUSES));
    }     

    /**
     * 
     * @api {get} /deposits List
     * @apiName list
     * @apiGroup Deposit
     * @apiPermission admin
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Deposits (without transactions)
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        if (Auth::user()->role == User::ROLES['SUPERADMIN']) {
            $list = Deposit::paginate($limit);
        } else if (Auth::user()->role == User::ROLES['ADMIN']) {
            $list = Deposit::sameCompany()->paginate($limit);
        }
        return response()->json($list);
    }

    /**
     * 
     * @api {post} /deposits Create
     * @apiName create
     * @apiGroup Deposit
     * @apiPermission admin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamDeposit
     * 
     * @apiUse SuccessDeposit
     *
     */        
    public function store(Request $request)
    {
        $input = $request->all();
        $error = $this->validateCompany($input['member_id']);
        if ($error != null) return $error;

        $deposit = new Deposit;
        $deposit->fill($input);        
        $deposit->save();

        return response()->json(Deposit::find($deposit->id));
    }

    /**
     * 
     * @api {get} /deposits/:id Get
     * @apiName get
     * @apiGroup Deposit
     * @apiPermission member
     * 
     * @apiParam {Number} id
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiUse SuccessDeposit
     * 
     */    
    public function show($id)
    {
        $existing = Deposit::find($id);
        if (empty($existing)) {
            return $this->notFound();
        }

        $error = $this->validateCompany($existing->member_id);
        if ($error != null) return $error;
        
        return response()->json($existing);
    }

    /**
     * @api {put} /deposits/:id Update
     * @apiName update
     * @apiGroup Deposit
     * @apiPermission admin
     * 
     * @apiParam {Number} id
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamDeposit
     * 
     * @apiUse SuccessDeposit
     */    
    public function update(Request $request, $id)
    {        
        $existing = Deposit::find($id);
        if (empty($existing)) {
            return $this->notFound();
        }

        $error = $this->validateCompany($existing->member_id);
        if ($error != null) return $error;
        
        $input = $request->all();
        unset($input['id']);
        $existing->fill($input);        
        $existing->save();

        return response()->json(Deposit::find($existing->id));
    }    

    /**
     * @api {delete} /deposits/:id Delete
     * @apiName delete
     * @apiGroup Deposit
     * @apiPermission admin
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * 
     * @apiUse SuccessDeposit
     */    
    public function destroy($id)
    {
        $existing = Deposit::find($id);
        if (empty($existing)) {
            return $this->notFound();
        }

        $error = $this->validateCompany($existing->member_id);
        if ($error != null) return $error;

        $existing->delete();
        return response()->json($existing);
    }
    
}
