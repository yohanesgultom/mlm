<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Tag;

class TagController extends Controller
{

     /**
     * @apiDefine SuccessTag
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {Integer} company_id
     * @apiSuccess (Success 200) {String} name
     * 
     */

     /**
     * @apiDefine ParamTag
     *
     * @apiParam {String} name
     * @apiParam {Integer} [company_id] Optional except for superadmin
     * 
     */

    /**
     * 
     * @api {get} /tags List
     * @apiName list
     * @apiGroup Tag
     * @apiPermission admin
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {String} [name] Partial name
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Tags
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $query = Tag::whereNotNull('id');

        // filter
        $input = $request->all();
        $query = $this->queryFilter($query, $input, [
            'name' => 'LIKE',
        ]);

        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $query = $query->sameCompany();
        }

        return response()->json($query->paginate($limit));
    }

    /**
     * 
     * @api {post} /tags Create
     * @apiName create
     * @apiGroup Tag
     * @apiPermission admin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamTag
     * @apiUse SuccessTag
     *
     */        
    public function store(Request $request)
    {
        $input = $request->all();        
        $model = new Tag;
        $model->fill($input);
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $model->company_id = Auth::user()->company_id;
        }
        $model->save();
        return response()->json($model);
    }

    /**
     * 
     * @api {get} /tags/:id Get
     * @apiName get
     * @apiGroup Tag
     * @apiPermission admin
     * 
     * @apiUse SuccessTag
     */    
    public function show($id)
    {
        $res = Tag::where('id', $id);
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $res = $res->sameCompany();
        }
        $res = $res->first();
        if (empty($res)) {
            return $this->notFound();
        }
        return response()->json($res);
    }

    /**
     * @api {put} /tags/:id Update
     * @apiName update
     * @apiGroup Tag
     * @apiPermission admin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamTag
     * @apiUse SuccessTag
     */    
    public function update(Request $request, $id)
    {
        $res = Tag::where('id', $id);
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $res = $res->sameCompany();
        }
        $existing = $res->first();
        
        if (empty($existing)) {
            return $this->notFound();
        }
        
        $input = $request->all();
        unset($input['id']);
        $existing->fill($input);
        $existing->save();

        return response()->json($existing);
    }

    /**
     * @api {delete} /tags/:id Delete
     * @apiName delete
     * @apiGroup Tag
     * @apiPermission admin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse SuccessTag
     */    
    public function destroy($id)
    {
        $res = Tag::where('id', $id);
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $res = $res->sameCompany();
        }
        $existing = $res->first();        
        if (empty($existing)) {
            return $this->notFound();
        }

        $existing->delete();
        return response()->json($existing);
    }
}
