<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Product;
use App\Warehouse;
use App\Business\ExcelService;
use Auth;
use DB;
use Carbon\Carbon;

class ReportController extends Controller
{

    /**
     * 
     * @apiDefine ProductStockLogSuccess
     * @apiSuccess (Product) {Integer} id
     * @apiSuccess (Product) {String} name
     * @apiSuccess (Product) {String} desc
     * @apiSuccess (Product) {String} desc_short
     * @apiSuccess (Product) {String} image_path
     * @apiSuccess (Product) {String} brochure_file
     * @apiSuccess (Product) {Double} base_price
     * @apiSuccess (Product) {Double} retail_price
     * @apiSuccess (Product) {Double} bonus_value
     * @apiSuccess (Product) {Double} point_value
     * @apiSuccess (Product) {Double} weight
     * 
     */

    /**
     * 
     * @api {get} /reports/orders Orders
     * @apiName orders
     * @apiGroup Report
     * @apiPermission admin
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {Integer} [order=created_at]
     * @apiParam {Integer} [sort=asc]
     * @apiParam {Date} [invoice_date_start] YYYY-MM-DD (default to 1st day of this month)
     * @apiParam {Date} [invoice_date_end] YYYY-MM-DD (default to today)
     * @apiParam {String} [member_id_prefix] Prefix of member id eg. `SC`, `00`
     * 
     * @apiSuccess {Array} _ Array of orders
     * 
     * @apiSuccess (Order) {Integer} id
     * @apiSuccess (Order) {Integer} wallet_id
     * @apiSuccess (Order) {String} invoice_no
     * @apiSuccess (Order) {String} bill_no
     * @apiSuccess (Order) {Array} transactions Array of transaction 
     * 
     */     
    public function orders(Request $request)
    {
        $order = 'orders'.$request->input('order', 'created_at');
        $sort = $request->input('sort', 'asc');
        $invoice_date_start = $request->input('invoice_date_start', Carbon::now()->startOfMonth()->startOfDay()->toDateTimeString());
        $invoice_date_end = $request->input('invoice_date_end', Carbon::now()->endOfDay()->toDateTimeString());
        $query = Order::orderBy($order, $sort);
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $query = $query->sameCompany();
        }
        $input = $request->all();
        $input = $this->remapQuery($input, [
            'member_id_prefix' => 'members.id',
        ]);
        $query = $this->queryFilter($query, $input, [
            'members.id' => 'STARTS_WITH',
        ]);
        $query = $query
            ->with('wallet.member', 'transactions')
            ->where('orders.status', Order::STATUSES['COMPLETED'])
            ->whereBetween('orders.created_at', [$invoice_date_start, $invoice_date_end]);
        return response()->json($query->get());
    }

    /**
     * 
     * @api {get} /reports/warehouses Warehouses
     * @apiName warehouses
     * @apiGroup Report
     * @apiPermission admin
     * @apiDescription Display stocks by warehouses
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {Integer} [order=id]
     * @apiParam {Integer} [sort=asc]
     * @apiParam {String} [id] Warehouse ID
     * 
     * @apiSuccess {Array} _ Array of warehouses and their stocks
     * 
     * @apiSuccess (Warehouse) {Integer} id
     * @apiSuccess (Warehouse) {String} name
     * @apiSuccess (Warehouse) {String} address
     * @apiSuccess (Warehouse) {String} address_city
     * @apiSuccess (Warehouse) {String} address_state
     * @apiSuccess (Warehouse) {String} address_zipcode
     * @apiSuccess (Warehouse) {Integer} country_id
     * @apiSuccess (Warehouse) {Integer} company_id
     * @apiSuccess (Warehouse) {Array} stocks Array of current warehouse stocks per product
     * 
     * 
     */
    public function warehouses(Request $request)
    {
        $order = 'warehouses.'.$request->input('order', 'id');
        $sort = $request->input('sort', 'asc');
        $query = Warehouse::with('stocks.product')->orderBy($order, $sort);
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $query = $query->sameCompany();
        }
        $input = $request->all();
        $input = $this->remapQuery($input, [
            'id' => 'warehouses.id',
        ]);
        $query = $this->queryFilter($query, $input, [
            'warehouses.id' => 'EQ',
        ]);
        return response()->json($query->get());
    }

    /**
     * 
     * @api {get} /reports/productStocks Product Stocks
     * @apiName productStocks
     * @apiGroup Report
     * @apiPermission admin
     * @apiDescription Display stocks by products
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {Integer} [order=id]
     * @apiParam {Integer} [sort=asc]
     * 
     * @apiSuccess {Array} _ Array of products and their stocks
     * 
     * @apiUse ProductStockLogSuccess
     * @apiSuccess (Product) {Array} warehouse_stocks Array of current stocks per warehouse
     * 
     */
    public function productStocks(Request $request)
    {
        $order = 'products.'.$request->input('order', 'name');
        $sort = $request->input('sort', 'asc');
        $query = Product::with('warehouse_stocks');
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $query = $query->sameCompany();
        }
        return response()->json($query->orderBy($order, $sort)->get());
    }    

    /**
     * 
     * @api {get} /reports/productStockLogs Product Stock Logs
     * @apiName productStockLogs
     * @apiGroup Report
     * @apiPermission admin
     * @apiDescription Return products' stock summary per warehouse
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {Integer} [order=id]
     * @apiParam {Integer} [sort=asc]
     * @apiParam {Date} [process_date_start] YYYY-MM-DD (default to 1st day of this month)
     * @apiParam {Date} [process_date_end] YYYY-MM-DD (default to today)
     * 
     * @apiSuccess {Array} _ Array of products and their stock logs
     * 
     * @apiUse ProductStockLogSuccess
     * @apiSuccess (Product) {Array} stock_logs array of stock summaries
     * 
     * @apiSuccess (Stock Summary) {Integer} product_id
     * @apiSuccess (Stock Summary) {Integer} warehouse_id
     * @apiSuccess (Stock Summary) {String} warehouse_name
     * @apiSuccess (Stock Summary) {Integer} qty_before Initial stock quantity
     * @apiSuccess (Stock Summary) {Integer} total_in Total incoming stock quantity
     * @apiSuccess (Stock Summary) {Integer} total_out Total outgoing stock quantity
     * 
     */
    public function productStockLogs(Request $request)
    {
        $order = 'products.'.$request->input('order', 'name');
        $sort = $request->input('sort', 'asc');
        $process_date_start = $request->input('process_date_start', Carbon::now()->startOfMonth()->startOfDay()->toDateTimeString());
        $process_date_end = $request->input('process_date_end', Carbon::now()->endOfDay()->toDateTimeString());
        $query = Product::orderBy($order, $sort);
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $query = $query->sameCompany();
        }
        $products = $query->get();

        // get initial/first stock log id per product per warehouse
        // in order to get initial qty_before
        // within proces_date range
        $subquery1 = DB::table('warehouse_stock_logs')
            ->selectRaw('product_id, warehouse_id, MIN(id) AS id')
            ->whereBetween('process_date', [$process_date_start, $process_date_end])
            ->groupBy('product_id', 'warehouse_id');

        // get sum qty_out (total_out) and qty_in (total_in) per product per warehouse
        // within proces_date range
        $subquery2 = DB::table('warehouse_stock_logs')
            ->selectRaw('product_id, warehouse_id, SUM(qty_in) AS total_in, SUM(qty_out) AS total_out')
            ->whereBetween('process_date', [$process_date_start, $process_date_end])
            ->groupBy('product_id', 'warehouse_id');

        // merge initial stock log's qty_before
        // with total_in and total_out
        $logs = DB::table('warehouse_stock_logs')
            ->select(
                'warehouse_stock_logs.product_id',
                'warehouses.id',
                'warehouses.name',
                'warehouse_stock_logs.qty_before',
                's2.total_in',
                's2.total_out'
            )
            ->joinSub($subquery1, 's1', function ($join) {
                $join->on('s1.id', '=', 'warehouse_stock_logs.id');
            })
            ->joinSub($subquery2, 's2', function ($join) {
                $join->on('s2.product_id', '=', 'warehouse_stock_logs.product_id')->on('s2.warehouse_id', '=', 'warehouse_stock_logs.warehouse_id');
            })
            ->join('warehouses', 'warehouses.id', 'warehouse_stock_logs.warehouse_id')
            ->orderBy('warehouse_stock_logs.product_id', 'warehouses.id')
            ->get();

        // group logs based on product_id
        // then put them in corresponding product
        $results = [];
        foreach ($products as $p) {
            $row = $p->toArray();
            $row['stock_logs'] = $logs->where('product_id', $p->id)->values()->all();
            array_push($results, $row);
        }
        return response()->json($results);
    }

    /**
     * 
     * @api {get} /reports/bonus Bonus
     * @apiName bonus
     * @apiGroup Report
     * @apiPermission admin
     * @apiDescription Return bonus report in Microsoft Excel format (.xlsx)
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {Date} process_date Bonus process date
     * @apiParam {Integer} [company_id] Only required for superadmin
     * 
     * @apiSuccess {Binary} _ Microsoft Excel file (.xlsx)
     * 
     */    
    public function bonus(Request $request)
    {
        $rules = [
            'process_date' => 'required|date',
        ];
        if (Auth::user()->role == User::ROLES['SUPERADMIN']) {
            $rules['company_id'] = 'required';
        }
        $request->validate($rules);
        $company_id = $request->input('company_id', Auth::user()->company_id);
        $process_date = Carbon::parse($request->input('process_date'))->startOfDay();
        $path = ExcelService::getBonusReportPath($company_id, $process_date);
        return response()->download($path)->deleteFileAfterSend(true);
    }
    
    /**
     * 
     * @api {get} /reports/transactions Transactions
     * @apiName transactions
     * @apiGroup Report
     * @apiPermission admin
     * @apiDescription Return transactions report in Microsoft Excel format (.xlsx)
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {Date} start_date
     * @apiParam {Date} end_date
     * @apiParam {Integer} [company_id] Only required for superadmin
     * 
     * @apiSuccess {Binary} _ Microsoft Excel file (.xlsx)
     * 
     */    
    public function transactions(Request $request)
    {
        $rules = [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ];
        if (Auth::user()->role == User::ROLES['SUPERADMIN']) {
            $rules['company_id'] = 'required';
        }
        $request->validate($rules);
        $company_id = $request->input('company_id', Auth::user()->company_id);
        $start_date = Carbon::parse($request->input('start_date'))->startOfDay();
        $end_date = Carbon::parse($request->input('end_date'))->endOfDay();
        // switch date if reversed
        if ($start_date->gt($end_date)) {
            $tmp = $start_date->copy();
            $start_date = $end_date->copy();
            $end_date = $tmp;
        }
        $path = ExcelService::getTransactionsReportPath($company_id, $start_date, $end_date);
        return response()->download($path)->deleteFileAfterSend(true);
    }    
}
