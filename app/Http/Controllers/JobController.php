<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class JobController extends Controller
{
    /**
     * 
     * @api {get} /jobs List
     * @apiName list
     * @apiGroup Jobs
     * @apiPermission admin
     * @apiDescription Display running jobs
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {Integer} [order=id] Order by
     * @apiParam {Integer} [sort=asc] Sort direction
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Jobs
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */
    public function index(Request $request)
    {        
        $limit = $request->input('limit', 10);
        $order = $request->input('order', 'id');
        $sort = $request->input('sort', 'asc');
        $query = DB::table('jobs')->select('id', 'queue', 'attempts', 'created_at');
        return response()->json($query->orderBy($order, $sort)->paginate());
    }
}
