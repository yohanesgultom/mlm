<?php

namespace App\Http\Controllers;

use App\WarehouseStock;
use App\Warehouse;
use Illuminate\Http\Request;
use Auth;
use App\User;

class WarehouseStockController extends Controller
{
     /**
     * @apiDefine SuccessWarehouseStock
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {Integer} qty Quantity
     * @apiSuccess (Success 200) {Date} exp_date Expiry date YYYY-MM-DD
     * @apiSuccess (Success 200) {Product} product
     * @apiSuccess (Success 200) {Integer} warehouse_id
     * 
     */

     /**
     * @apiDefine ParamWarehouseStock
     *
     * @apiParam {Integer} qty Quantity
     * @apiParam {Date} exp_date Expiry date YYYY-MM-DD
     * @apiParam {Integer} product_id
     * 
     */

    /**
     * 
     * @api {get} /warehouses/:warehouse_id/stocks Stock List
     * @apiName list stocks
     * @apiGroup Warehouse
     * @apiPermission admin
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Warehouse Stocks
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request, $warehouse_id)
    {        
        $warehouse = Warehouse::find($warehouse_id);
        if (empty($warehouse)) {
            return $this->notFound();
        }
        $limit = $request->input('limit', 10);
        $list = WarehouseStock::with('product')->where('warehouse_id', $warehouse_id)->paginate($limit);
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {            
            if ($warehouse->company_id != Auth::user()->company_id) {
                return $this->forbidden();
            }
        }
        return response()->json($list);
    }

    // public function store(Request $request, $warehouse_id)
    // {        
    //     $warehouse = Warehouse::find($warehouse_id);
    //     if (empty($warehouse)) {
    //         return $this->notFound();
    //     }
    //     if (Auth::user()->role != User::ROLES['SUPERADMIN']) {            
    //         if ($warehouse->company_id != Auth::user()->company_id) {
    //             return $this->forbidden();
    //         }
    //     }
    //     $input = $request->all();        
    //     $model = new WarehouseStock;
    //     $model->fill($input);
    //     $model->warehouse_id = $warehouse_id;
    //     $model->save();
    //     return response()->json(WarehouseStock::find($model->id));
    // }

    /**
     * 
     * @api {get} /warehouses/:warehouse_id/stocks/:id Stock Get
     * @apiName get stock
     * @apiGroup Warehouse
     * @apiPermission admin
     * 
     * @apiUse SuccessWarehouseStock
     */    
    public function show($warehouse_id, $id)
    {
        $stock = WarehouseStock::with('product')
            ->where('warehouse_id', $warehouse_id)
            ->where('id', $id)
            ->first();
        if (empty($stock)) {
            return $this->notFound();
        }
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            if ($stock->warehouse->company_id != Auth::user()->company_id) {
                return $this->forbidden();
            }
        }        
        return response()->json($stock);
    }

    // public function update(Request $request, $warehouse_id, $id)
    // {       
    //     $stock = WarehouseStock::with('product')
    //         ->where('warehouse_id', $warehouse_id)
    //         ->where('id', $id)
    //         ->first();
    //     if (empty($stock)) {
    //         return $this->notFound();
    //     }        
    //     if (Auth::user()->role != User::ROLES['SUPERADMIN']) {            
    //         if ($stock->warehouse->company_id != Auth::user()->company_id) {
    //             return $this->forbidden();
    //         }            
    //     } 
     
    //     $input = $request->except(['id', 'warehouse_id']);
    //     $stock->fill($input);
    //     $stock->save();

    //     return response()->json(WarehouseStock::find($id));
    // }

    // public function destroy($warehouse_id, $id)
    // {
    //     $stock = WarehouseStock::with('product')
    //         ->where('warehouse_id', $warehouse_id)
    //         ->where('id', $id)
    //         ->first();
    //     if (empty($stock)) {
    //         return $this->notFound();
    //     }        
    //     if (Auth::user()->role != User::ROLES['SUPERADMIN']) {            
    //         if ($stock->warehouse->company_id != Auth::user()->company_id) {
    //             return $this->forbidden();
    //         }            
    //     }

    //     $stock->delete();
    //     return response()->json($stock);
    // }
}
