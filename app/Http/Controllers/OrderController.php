<?php

namespace App\Http\Controllers;

use App\Order;
use App\Transaction;
use App\Wallet;
use App\User;
use App\MemberStock;
use App\Business\CommerceService;
use Illuminate\Http\Request;
use Auth;
use DB;

class OrderController extends Controller
{

    /**
     * @apiDefine SuccessOrder
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {Integer} wallet_id
     * @apiSuccess (Success 200) {Integer} warehouse_id
     * @apiSuccess (Success 200) {String} invoice_no
     * @apiSuccess (Success 200) {String} bill_no
     * @apiSuccess (Success 200) {Array} transactions Array of transaction
     * @apiSuccess (Success 200) {Wallet} wallet
     * @apiSuccess (Success 200) {Integer} status
     * @apiSuccess (Success 200) {String} status_name
     * 
     */

     /**
     * @apiDefine ParamOrder
     *
     * @apiParam {Integer} wallet_id Wallet ID used in this transaction
     * @apiParam {String} invoice_no
     * @apiParam {String} bill_no
     * @apiParam {Array} transactions Array of transactions
     * @apiParam {Integer} [warehouse_id] Regular warehouse ID for direct sale
     * @apiParam {Integer} status
     * 
     */ 

    /**
     * @apiDefine SuccessTransaction
     *
     * @apiSuccess (Transaction) {Integer} id
     * @apiSuccess (Transaction) {Integer} code Transcation code
     * @apiSuccess (Transaction) {String} desc
     * @apiSuccess (Transaction) {Integer} qty Quantity of product
     * @apiSuccess (Transaction) {Double} amount Total amount of money paid (calculcated automatically)
     * @apiSuccess (Transaction) {Double} points Total points earned (calculcated automatically)
     * @apiSuccess (Transaction) {DateTime} process_date DateTime when the transaction processed
     * @apiSuccess (Transaction) {DateTime} point_date DateTime when points will be applied
     * @apiSuccess (Transaction) {Product} product
     * @apiSuccess (Transaction) {Integer} order_id
     * @apiSuccess (Transaction) {Boolean} transferred
     * 
     */

    /**
     * @apiDefine ParamTransaction
     *
     * @apiParam (Transaction) {Integer} [id] Transaction ID. Provide when updating
     * @apiParam (Transaction) {Integer} code Transcation code
     * @apiParam (Transaction) {String} [desc]
     * @apiParam (Transaction) {Integer} qty Quantity of product
     * @apiParam (Transaction) {DateTime} [process_date] DateTime when the transaction processed
     * @apiParam (Transaction) {DateTime} [point_date] DateTime when points will be applied
     * @apiParam (Transaction) {Integer} product_id
     * @apiParam (Transaction) {Boolean} [transferred]
     * 
     */     

    /**
     * 
     * @api {get} /orders/statuses Statuses
     * @apiName statuses
     * @apiGroup Order
     * @apiPermission public
     * @apiDescription Get list of Statuses
     * 
     * @apiSuccess (Success 200) {Object} _ Statuses 
     *
     */ 
    public function statuses(Request $request)
    {        
        return response()->json((object) array_flip(Order::STATUSES));
    }

    /**
     * 
     * @api {get} /orders List
     * @apiName list
     * @apiGroup Order
     * @apiPermission admin
     * 
     * @apiParam {Integer} [order] Order by
     * @apiParam {Integer} [sort] Sort direction
     * @apiParam {Date} [created_date_start] YYYY-MM-DD
     * @apiParam {Date} [created_date_end] YYYY-MM-DD
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Orders (without transactions)
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $order = 'orders.'.$request->input('order', 'created_at');
        $sort = $request->input('sort', 'desc');
        $query = Order::whereNotNull('orders.id');
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $query = $query
                ->sameCompany()
                ->with('wallet.member.user');
        }
        if ($request->has('created_date_start') && $request->has('created_date_end')) {                        
            $range = [$request->input('created_date_start'), $request->input('created_date_end')];
            $query = $query
                ->with('wallet.member.user')
                ->whereBetween('orders.created_at', $range);
        }
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }

    /**
     * 
     * @api {post} /orders Create
     * @apiName create
     * @apiGroup Order
     * @apiPermission admin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamOrder
     * @apiUse ParamTransaction
     * 
     * @apiUse SuccessOrder
     *
     */        
    public function store(Request $request)
    {
        $input = $request->all();
        $wallet = Wallet::sameCompany()->find($input['wallet_id']);
        if (empty($wallet)) {
            return $this->notFound('Wallet not found: '.$input['wallet_id']);
        }

        $error = $this->validateCompany($wallet->member_id);
        if ($error != null) return $error;
        
        try {
            $input = CommerceService::validateOrderWarehouse($input);
            // validate only if the status is not COMPLETED
            // to allow split or transfer transction
            if ($input['status'] != Order::STATUSES['COMPLETED']) {
                $input['transactions'] = CommerceService::validateTransactions($input['transactions'], $wallet, $input['warehouse_id']);
            }
        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }

        $order = new Order;
        try {
            DB::beginTransaction();                        
            $order->fill($input);  
            $order->admin_id = Auth::id();
            $order->save();
            $order->transactions()->createMany($input['transactions']);            
            if ($input['status'] == Order::STATUSES['COMPLETED']) {                
                CommerceService::subtractBalanceAndAddStocks($order, $wallet);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return $this->serverError($e->getMessage());
        }

        return response()->json(Order::with(['transactions'])->find($order->id));
    }

    /**
     * 
     * @api {get} /orders/:id Get
     * @apiName get
     * @apiGroup Order
     * @apiPermission member
     * 
     * @apiParam {Number} id
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiUse SuccessOrder
     * 
     */    
    public function show($id)
    {
        $existing = Order::with(['transactions', 'wallet.member.user'])->find($id);
        if (empty($existing)) {
            return $this->notFound();
        }

        $error = $this->validateCompany($existing->wallet->member_id);
        if ($error != null) return $error;

        return response()->json($existing);
    }

    /**
     * @api {put} /orders/:id Update
     * @apiName update
     * @apiGroup Order
     * @apiPermission admin
     * 
     * @apiParam {Number} id
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamOrder
     * @apiUse ParamTransaction
     * 
     * @apiUse SuccessOrder
     */    
    public function update(Request $request, $id)
    {        
        $existing = Order::with(['transactions'])->find($id);
        if (empty($existing)) {
            return $this->notFound();
        }

        $input = $request->except(['id']);
        
        // validate wallet
        $wallet = null;
        if ($input['wallet_id']) {
            $wallet = Wallet::find($input['wallet_id']);
        } else {
            $wallet = $existing->wallet;
        }
        $error = $this->validateCompany($wallet->member_id);
        if ($error != null) return $error;
                
        try {
            $input = CommerceService::validateOrderWarehouse($input);
            // validate only if the status is not COMPLETED             
            // to allow split or transfer transction
            // and not CANCELED to allow refund 
            if ($input['status'] != Order::STATUSES['COMPLETED'] && 
                $input['status'] != Order::STATUSES['CANCELED']) {
                $input['transactions'] = CommerceService::validateTransactions($input['transactions'], $existing->wallet, $input['warehouse_id']);
            }
        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }

        // separate new and updated transaction(s)
        $new = [];
        $updated = [];
        foreach ($input['transactions'] as $t) {
            if (array_key_exists('id', $t)) {
                array_push($updated, $t);
            } else {
                array_push($new, $t);
            }
        }

        // separate canceled transaction id(s) 
        $canceledIds = [];
        foreach ($existing->transactions as $te) {
            $found = false;
            foreach ($updated as $tu) {
                if ($tu['id'] == $te->id) {
                    $found = true;
                }
            }
            if (!$found) {
                array_push($canceledIds, $te->id);
            }
        }
        
        // save old status
        $existing_status = $existing->status;        
        $existing->fill($input);

        try {
            DB::beginTransaction();
            // update order and transactions            
            $existing->admin_id = Auth::id();
            $existing->save();
            $existing->transactions()->createMany($new);
            foreach ($updated as $t) {
                $tx = Transaction::find($t['id']);
                $tx->fill($t);
                $tx->save();
            }
            Transaction::destroy($canceledIds);
            if ($existing_status != Order::STATUSES['COMPLETED'] 
                && $input['status'] == Order::STATUSES['COMPLETED']) {
                CommerceService::subtractBalanceAndAddStocks($existing, $wallet);
            } else if ($existing_status == Order::STATUSES['COMPLETED'] 
            && $input['status'] == Order::STATUSES['CANCELED']) {
                CommerceService::refundBalanceAndRemoveStocks($existing, $wallet);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->serverError($e->getMessage());
        }

        return response()->json(Order::with(['transactions'])->find($existing->id));
    }    

    /**
     * @api {delete} /orders/:id Delete
     * @apiName delete
     * @apiGroup Order
     * @apiPermission admin
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * 
     * @apiUse SuccessOrder
     */    
    public function destroy($id)
    {
        $existing = Order::find($id);
        if (empty($existing)) {
            return $this->notFound();
        }

        $error = $this->validateCompany($existing->wallet->member_id);
        if ($error != null) return $error;

        $existing->delete();
        return response()->json($existing);
    }
    
}
