<?php

namespace App\Http\Controllers;

use App\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{

     /**
     * @apiDefine SuccessBank
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} name
     * @apiSuccess (Success 200) {String} code
     * 
     */

     /**
     * @apiDefine ParamBank
     *
     * @apiParam {String} name
     * @apiParam {String} code
     * 
     */

    /**
     * 
     * @api {get} /banks List
     * @apiName list
     * @apiGroup Bank
     * @apiPermission member
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {String} [name] Partial name
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Banks
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */         
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $query = Bank::whereNotNull('id');

        // filter
        $input = $request->all();
        $query = $this->queryFilter($query, $input, [
            'name' => 'LIKE',
        ]);

        return response()->json($query->paginate($limit));
    }

    /**
     * @api {post} /banks Create
     * @apiName create
     * @apiGroup Bank
     * @apiPermission superadmin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamBank
     * @apiUse SuccessBank
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $model = new Bank;
        $model->fill($input);
        $model->save();
        return response()->json($model);
    }

    /**
     * 
     * @api {get} /banks/:id Get
     * @apiName get
     * @apiGroup Bank
     * @apiPermission member
     * 
     * @apiUse SuccessBank
     */
    public function show(Bank $bank)
    {
        return response()->json($bank);
    }

    /**
     * @api {put} /banks/:id Update
     * @apiName update
     * @apiGroup Bank
     * @apiPermission superadmin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamBank
     * @apiUse SuccessBank
     */
    public function update(Request $request, Bank $bank)
    {
        if (empty($bank)) {
            return response()->json([]);
        }

        $input = $request->all();
        unset($input['id']);
        $bank->fill($input);
        $bank->save();

        return response()->json($bank);
    }

    /**
     * @api {delete} /banks/:id Delete
     * @apiName delete
     * @apiGroup Bank
     * @apiPermission superadmin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse SuccessBank
     */
    public function destroy(Bank $bank)
    {
        if (empty($bank)) {
            return response()->json([]);
        }

        $bank->delete();
        return response()->json($bank);
    }
}
