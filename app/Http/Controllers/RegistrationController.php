<?php

namespace App\Http\Controllers;

use App\Registration;
use App\Package;
use App\User;
use App\Mail\Generic;
use App\Business\MemberService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;

class RegistrationController extends Controller
{
    /**
     * @apiDefine SuccessRegistration
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} name
     * @apiSuccess (Success 200) {String} email
     * @apiSuccess (Success 200) {Package} package
     * @apiSuccess (Success 200) {DateTime} birth_date
     * @apiSuccess (Success 200) {Integer} gender
     * @apiSuccess (Success 200) {String} official_id
     * @apiSuccess (Success 200) {String} tax_id
     * @apiSuccess (Success 200) {String} address
     * @apiSuccess (Success 200) {String} address_rt
     * @apiSuccess (Success 200) {String} address_rw
     * @apiSuccess (Success 200) {String} address_zipcode
     * @apiSuccess (Success 200) {String} address_city
     * @apiSuccess (Success 200) {String} address_state
     * @apiSuccess (Success 200) {Integer} country_id
     * @apiSuccess (Success 200) {String} phone_home
     * @apiSuccess (Success 200) {String} phone_office
     * @apiSuccess (Success 200) {String} phone_mobile
     * @apiSuccess (Success 200) {String} phone_fax
     * @apiSuccess (Success 200) {String} bank_account_no
     * @apiSuccess (Success 200) {String} bank_account_name
     * @apiSuccess (Success 200) {String} bank_account_city
     * @apiSuccess (Success 200) {String} bank_account_branch
     * @apiSuccess (Success 200) {Wallet} sponsor_wallet Sponsor wallet
     * @apiSuccess (Success 200) {DateTime} completed_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {Integer} status
     * @apiSuccess (Success 200) {String} status_name
     * @apiSuccess (Success 200) {String} member_id
     * @apiSuccess (Success 200) {String} parent_id
     * @apiSuccess (Success 200) {Integer} order_id
     * 
     */

     /**
     * @apiDefine ParamRegistration
     *
     * @apiParam {String} name
     * @apiParam {String} email
     * @apiParam {Integer} package_id
     * @apiParam {DateTime} [birth_date]
     * @apiParam {Integer} [gender]
     * @apiParam {String} [official_id]
     * @apiParam {String} [tax_id]
     * @apiParam {String} [address]
     * @apiParam {String} [address_rt]
     * @apiParam {String} [address_rw]
     * @apiParam {String} [address_zipcode]
     * @apiParam {String} [address_city]
     * @apiParam {String} [address_state]
     * @apiParam {Integer} [country_id]
     * @apiParam {String} [phone_home]
     * @apiParam {String} [phone_office]
     * @apiParam {String} [phone_mobile]
     * @apiParam {String} [phone_fax]
     * @apiParam {String} [bank_account_no]
     * @apiParam {String} [bank_account_name]
     * @apiParam {String} [bank_account_city]
     * @apiParam {String} [bank_account_branch]
     * @apiParam {String} [parent_id] Upline member id
     * @apiParam {Integer} [sponsor_wallet_id] Sponsor's wallet id
     * @apiParam {DateTime} [completed_date] YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiParam {Integer} [status]
     * 
     */ 

     /**
      * Validate package ID
      */
    protected function validatePackage($package_id)
    {
        $package = Package::with('member_type')->find($package_id);
        if (empty($package)) {
            return $this->notFound('package_id not found');
        }
        if (Auth::user()->role == User::ROLES['SUPERADMIN'] 
            || $package->member_type->company_id != Auth::user()->company_id) {
            return $this->badRequest('invalid package_id');
        }
        return null;
    }

    /**
     * Send registration email
     */
    protected function sendMail($registration, $random_password)
    {
        $company_name = Auth::user()->company->name;
        $subject = "Selamat bergabung di {$company_name}";
        $body = "Selamat bergabung di {$company_name}. Silahkan masuk ke aplikasi menggunakan:<br>Member ID: {$registration->member_id}<br>Password: {$random_password}";
        $mail = new Generic(config('mail.from.address'), $subject, $body);
        Mail::to($registration->email)->queue($mail);
    }

    /**
     * 
     * @api {get} /registrations/statuses Statuses
     * @apiName statuses
     * @apiGroup Registration
     * @apiPermission public
     * @apiDescription Get list of Statuses
     * 
     * @apiSuccess (Success 200) {Object} _ Statuses 
     *
     */ 
    public function statuses(Request $request)
    {        
        return response()->json((object) array_flip(Registration::STATUSES));
    }     

    /**
     * 
     * @api {get} /registrations List
     * @apiName list
     * @apiGroup Registration
     * @apiPermission admin
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Registrations
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */     
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $order = $request->input('order', 'id');
        $sort = $request->input('sort', 'asc');
        $query = Registration::whereNotNull('id');
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $query = $query->sameCompany();
        }
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }

    /**
     * 
     * @api {post} /registrations Create
     * @apiName create
     * @apiGroup Registration
     * @apiPermission admin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamRegistration
     * 
     * @apiUse SuccessRegistration
     *
     */        
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'package_id' => 'required',
        ]);
        $input = $request->except(['id', 'order_id', 'member_id']);
        $error = $this->validatePackage($input['package_id']);
        if ($error != null) return $error;
        $registration = new Registration;
        $registration->fill($input);
        // replace if company_id available
        // if not availabe, rely on $input
        if (Auth::user()->company_id) {
            $registration->company_id = Auth::user()->company_id;
        }
        if (empty($registration->company_id)) {
            return $this->badRequest('company_id is required');
        }
        // set default status
        $input['status'] = $input['status'] ?? Registration::STATUSES['UNPAID'];
        if ($input['status'] != Registration::STATUSES['COMPLETED']) {
            $registration->save();
        } else { // completed
            // set default completed date if not available
            $registration->completed_date = $registration->completed_date ?? new \DateTime();
            try {
                DB::beginTransaction();
                $random_password = MemberService::processCompletedRegistration($registration);
                DB::commit();
                $this->sendMail($registration, $random_password);
            } catch (\Exception $e) {
                DB::rollBack();
                \Log::error($e);
                return $this->badRequest($e->getMessage());
            }
        }
        return response()->json(Registration::find($registration->id));
    }

    /**
     * 
     * @api {get} /registrations/:id Get
     * @apiName get
     * @apiGroup Registration
     * @apiPermission member
     * 
     * @apiParam {Number} id
     * 
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiUse SuccessRegistration
     * 
     */    
    public function show(Registration $registration)
    {
        if ($registration->company_id != Auth::user()->company_id) {
            return $this->forbidden();
        }
        return response()->json($registration);
    }

    /**
     * @api {put} /registrations/:id Update
     * @apiName update
     * @apiGroup Registration
     * @apiPermission admin
     * 
     * @apiParam {Number} id
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamRegistration
     * 
     * @apiUse SuccessRegistration
     */    
    public function update(Request $request, Registration $registration)
    {                
        if ($registration->company_id != Auth::user()->company_id) {
            return $this->forbidden();
        }
        if ($registration->status == Registration::STATUSES['COMPLETED']) {
            return $this->forbidden('Registration is completed');
        }
        $input = $request->except(['id', 'order_id', 'member_id', 'company_id']);
        if (array_key_exists('package_id', $input)) {
            $error = $this->validatePackage($input['package_id']);
            if ($error != null) return $error;    
        }        
        $registration->fill($input);
        if ($input['status'] != Registration::STATUSES['COMPLETED']) {
            $registration->save();
        } else { 
            // $registration->status != Registration::STATUSES['COMPLETED']
            // and $input['status'] == Registration::STATUSES['COMPLETED']
            // set default completed date if not available
            $registration->completed_date = $registration->completed_date ?? new \DateTime();
            try {
                DB::beginTransaction();
                $random_password = MemberService::processCompletedRegistration($registration);
                DB::commit();
                $this->sendMail($registration, $random_password);
            } catch (\Exception $e) {
                DB::rollBack();
                \Log::error($e);
                return $this->badRequest($e->getMessage());
            }
        }
        return response()->json(Registration::find($registration->id));
    }    

    /**
     * @api {delete} /registrations/:id Delete
     * @apiName delete
     * @apiGroup Registration
     * @apiPermission admin
     * 
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * 
     * @apiUse SuccessRegistration
     */    
    public function destroy(Registration $registration)
    {
        if ($registration->company_id != Auth::user()->company_id) {
            return $this->forbidden();
        }
        $registration->delete();
        return response()->json($registration);
    }
}
