<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Member;
use App\Business\MemberService;
use Auth;
use DB;

class MemberController extends Controller
{

    /**
     * @apiDefine SuccessUser
     *
     * @apiSuccess (User) {Integer} id
     * @apiSuccess (User) {String} name
     * @apiSuccess (User) {String} username
     * @apiSuccess (User) {String} email
     * @apiSuccess (User) {Integer} role
     * @apiSuccess (User) {String} role_name
     */

     /**
     * @apiDefine SuccessMember
     *
     * @apiSuccess (Success 200) {User} user
     * @apiSuccess (Success 200) {String} id Member id
     * @apiSuccess (Success 200) {String} image_path
     * @apiSuccess (Success 200) {Integer} parent_id Parent's member id
     * @apiSuccess (Success 200) {Integer} sponsor_id Sponsor's member id
     * @apiSuccess (Success 200) {Integer} member_type_id
     * @apiSuccess (Success 200) {String} name_printed
     * @apiSuccess (Success 200) {DateTime} reg_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {DateTime} birth_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {DateTime} bonus_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiSuccess (Success 200) {Achievement} achievement Current achievement
     * @apiSuccess (Success 200) {String} gender_name
     * @apiSuccess (Success 200) {String} official_id Official/government-released ID number
     * @apiSuccess (Success 200) {String} tax_id Tax Id number
     * @apiSuccess (Success 200) {Double} current_personal_points Current total point value (PV)
     * @apiSuccess (Success 200) {Double} current_personal_bonus Current total bonus value (BV)
     * @apiSuccess (Success 200) {Double} current_group_points
     * @apiSuccess (Success 200) {String} address
     * @apiSuccess (Success 200) {String} address_rt
     * @apiSuccess (Success 200) {String} address_rw
     * @apiSuccess (Success 200) {String} address_zipcode
     * @apiSuccess (Success 200) {String} address_city
     * @apiSuccess (Success 200) {String} address_state
     * @apiSuccess (Success 200) {Integer} country_id
     * @apiSuccess (Success 200) {String} phone_home
     * @apiSuccess (Success 200) {String} phone_office
     * @apiSuccess (Success 200) {String} phone_mobile
     * @apiSuccess (Success 200) {String} phone_fax
     * @apiSuccess (Success 200) {String} beneficiary_name
     * @apiSuccess (Success 200) {String} beneficiary_relation
     * @apiSuccess (Success 200) {String} bank_account_no
     * @apiSuccess (Success 200) {String} bank_account_name
     * @apiSuccess (Success 200) {String} bank_account_city
     * @apiSuccess (Success 200) {String} bank_account_branch
     * @apiSuccess (Success 200) {Integer} bank_id
     * @apiSuccess (Success 200) {Integer} status Member's status
     * 
     */

    /**
     * @apiDefine SuccessAchievement
     *
     * @apiSuccess (Achievement) {Integer} id
     * @apiSuccess (Achievement) {String} code
     * @apiSuccess (Achievement) {String} desc
     * @apiSuccess (Achievement) {Integer} order
     */
     

    /**
     * @apiDefine SuccessCompany
     *
     * @apiSuccess (Company) {Integer} id
     * @apiSuccess (Company) {String} name
     * @apiSuccess (Company) {Integer} cutoff_day Number of days between cut-off
     * @apiSuccess (Company) {String} weekly_cutoff_day_code Day code of weekly cut-off
     * 
     */     

     /**
     * @apiDefine ParamMember
     *
     * @apiParam {String} id
     * @apiParam {String} name
     * @apiParam {String} username
     * @apiParam {String} password
     * @apiParam {String} email
     * @apiParam {DateTime} reg_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiParam {DateTime} birth_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiParam {DateTime} bonus_date YYYY-MM-DD HH:mm:ss eg. 2018-01-27 02:46:31
     * @apiParam {Integer} gender
     * @apiParam {File} [image_path]
     * @apiParam {Integer} [achievement_id] 
     * @apiParam {Integer} [parent_id] Upline's member id
     * @apiParam {Integer} [sponsor_id] Sponsor's member id
     * @apiParam {Integer} [member_type_id] 
     * @apiParam {String} [name_printed] 
     * @apiParam {String} [official_id] Official/government-released ID number
     * @apiParam {String} [tax_id] Tax Id number
     * @apiParam {Double} [current_personal_points] 
     * @apiParam {Double} [current_group_points] 
     * @apiParam {String} [address] 
     * @apiParam {String} [address_rt] 
     * @apiParam {String} [address_rw] 
     * @apiParam {String} [address_zipcode] 
     * @apiParam {String} [address_city] 
     * @apiParam {String} [address_state] 
     * @apiParam {Integer} [country_id] 
     * @apiParam {String} [phone_home] 
     * @apiParam {String} [phone_office] 
     * @apiParam {String} [phone_mobile] 
     * @apiParam {String} [phone_fax] 
     * @apiParam {String} [beneficiary_name] 
     * @apiParam {String} [beneficiary_relation] 
     * @apiParam {String} [bank_account_no] 
     * @apiParam {String} [bank_account_name] 
     * @apiParam {String} [bank_account_city] 
     * @apiParam {String} [bank_account_branch] 
     * @apiParam {Integer} [bank_id] 
     * 
     */
         

    /**
     * 
     * @api {get} /members/genders Member Genders
     * @apiName genders
     * @apiGroup Member
     * @apiPermission public
     * @apiDescription Get list of Genders
     * 
     * @apiSuccess (Success 200) {Object} _ Genders
     *
     */ 
    public function genders(Request $request)
    {        
        return response()->json((object) array_flip(Member::GENDERS));
    }

    /**
     * 
     * @api {get} /members/statuses Member Statuses
     * @apiName statuses
     * @apiGroup Member
     * @apiPermission public
     * @apiDescription Get list of Statuses
     * 
     * @apiSuccess (Success 200) {Object} _ Statuses
     *
     */ 
    public function statuses(Request $request)
    {        
        return response()->json((object) array_flip(Member::STATUSES));
    }

    /**
     * 
     * @api {get} /members Member List
     * @apiName list
     * @apiGroup Member
     * @apiPermission admin
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {String} [id] Partial ID
     * @apiParam {String} [name] Partial name
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Members
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */
    public function index(Request $request)
    {                  
        $limit = $request->input('limit', 10);
        $query = Member::joinUsers();

        // filter
        $input = $request->all();
        $input = $this->remapQuery($input, [
            'id' => 'members.id'
        ]);
        $query = $this->queryFilter($query, $input, [
            'members.id' => 'LIKE',
            'name' => 'LIKE',
        ]);

        if (Auth::user()->role == User::ROLES['ADMIN']) {
            $query = $query->sameCompany();
        } else if (Auth::user()->role == User::ROLES['MEMBER']) {
            $query = $query->sameCompany()->public();
        } else if (Auth::user()->role == User::ROLES['SUPERADMIN']) {
            // do nothing
        } else {
            return $this->forbidden('Unknown role: '.Auth::user()->role);
        }
        return response()->json($query->paginate($limit));
    }

    
    /**
     * 
     * @api {post} /members Member Create
     * @apiName create
     * @apiGroup Member
     * @apiPermission admin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamMember
     * @apiUse SuccessMember
     * @apiUse SuccessUser
     * @apiUse SuccessAchievement
     */    
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required',
            'password' => 'required',
            'email' => 'required|email',
            'image_path'  => 'image',
        ]);

        $input = $request->all();
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            $input['company_id'] = Auth::user()->company_id;
        }

        $input = $this->processPublicInputFiles('member', ['image_path'], $request, $input);
        $input['role'] = User::ROLES['MEMBER'];
        $user = new User;
        $model = new Member;
        $model->fill($input);
        $user->fill($input);
        
        DB::transaction(function () use ($user, $model) {
            $user->save();
            $user->member()->save($model);
        });        
        return response()->json(Member::find($input['id']));
    }

    /**
     * 
     * @api {get} /members/:id Member Get
     * @apiName get
     * @apiGroup Member
     * @apiPermission admin
     * 
     * @apiUse SuccessMember
     * @apiUse SuccessUser
     * @apiUse SuccessAchievement
     */
    public function show($id)
    {
        $existing = Member::find($id);
        if (empty($existing)) {
            return $this->notFound('Member not found: '.$id);
        }

        $error = $this->validateMemberCompany($existing);
        if ($error != null) return $error;

        if (Auth::user()->role == User::ROLES['MEMBER']) {
            $existing = Member::public()->find($id);
        }

        return response()->json($existing);
    }

    /**
     * @api {post} /members/:id Member Update
     * @apiName update
     * @apiGroup Member
     * @apiPermission admin
     * @apiDescription A multipart PUT request on PHP needs hack by using POST method and adding _method:PUT param https://github.com/laravel/framework/issues/13457#issuecomment-282542694. Original bug https://bugs.php.net/bug.php?id=55815
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * 
     * @apiParam {String} _method PUT
     * @apiUse ParamMember
     * @apiUse SuccessMember
     * @apiUse SuccessUser
     * @apiUse SuccessAchievement
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'email',
            'image_path'  => 'image|nullable',
        ]);

        $existing = Member::find($id);
        if (empty($existing)) {
            return $this->notFound('Member not found: '.$id);
        }

        $error = $this->validateMemberCompany($existing);
        if ($error != null) return $error;   

        $input = $request->all();
        $input = $this->processPublicInputFiles('member', ['image_path'], $request, $input);
        unset($input['id']);
        $existing->user->fill($input);
        $existing->fill($input);
        DB::transaction(function () use ($existing) {
            $existing->user->save();
            $existing->save();
        });

        return response()->json(Member::find($existing->id));
    }

    /**
     * @api {delete} /members/:id Member Delete
     * @apiName delete
     * @apiGroup Member
     * @apiPermission admin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse SuccessMember
     * @apiUse SuccessUser
     * @apiUse SuccessAchievement
     */
    public function destroy($id)
    {
        $existing = Member::find($id);
        if (empty($existing)) {
            return $this->notFound('Member not found: '.$id);
        }

        $error = $this->validateMemberCompany($existing);
        if ($error != null) return $error;

        DB::transaction(function () use ($existing) {
            $existing->user->delete();
            $existing->delete();
        });

        return response()->json($existing);
    }

    /**
     * @api {get} /members/me Member Me
     * @apiName me
     * @apiGroup Member
     * @apiPermission member
     * @apiDescription Get details of currently logged-in member
     *
     * @apiUse SuccessMember
     * @apiUse SuccessUser
     * @apiSuccess (User) {Company} company
     * 
     * @apiUse SuccessAchievement
     * @apiUse SuccessCompany
     * 
     */    
    public function me()
    {
        if (Auth::user()->role == User::ROLES['SUPERADMIN']) {
            return $this->notFound();
        }
        $member = Member::with('user.company')->where('user_id', Auth::id())->first();
        if (empty($member)) {
            return $this->forbidden();
        }
        return response()->json($member);
    }

    /**
     * @api {get} /members/:id/downlines Member Downlines
     * @apiName downlines
     * @apiGroup Member
     * @apiPermission admin
     *
     * @apiParam {String} by <code>'parent'</code> (default) or <code>'sponsor'</code>
     * 
     * @apiSuccess (Success 200) {Array} _ Nested array (tree) of downlines
     * 
     * @apiSuccessExample {json} Success Response Example
     *  {
     *  	"id": "58499436",
     *  	"name": "Rafael",
     *  	"downlines": [
     *  		{
     *  			"id": "287244482",
     *  			"name": "Kaylee",
     *  			"downlines": [
     *  				{
     *  					"id": "44886691",
     *  					"name": "Nichole",
     *  					"downlines": []
     *  				},
     *  				{
     *  					"id": "970645228",
     *  					"name": "Santina",
     *  					"downlines": []
     *  				}
     *  			]
     *  		},
     *  		{
     *  			"id": "86789748",
     *  			"name": "Elyssa",
     *  			"downlines": [
     *  				{
     *  					"id": "1191394098",
     *  					"name": "Judy",
     *  					"downlines": []
     *  				},
     *  				{
     *  					"id": "704957549",
     *  					"name": "Mariam",
     *  					"downlines": []
     *  				}
     *  			]
     *  		}
     *  	]
     *  }
     * 
     */    
    public function downlines(Request $request, $id)
    {
        $member = Member::with('user')->find($id);
        if (empty($member)) {
            return $this->notFound('Member not found: '.$id);
        }
        
        if (Auth::user()->role != User::ROLES['SUPERADMIN']) {
            if (Auth::user()->company_id != $member->user->company_id) {
                return $this->forbidden('Member belongs to another company');
            }
        }

        $by = strtolower($request->input('by', 'parent'));
        if ($by == 'sponsor') {
            $field = 'sponsor_id';
        } else if ($by == 'parent') {
            $field = 'parent_id';
        } else if (empty($by)) {
            return $this->badRequest('unknown field: '.$field);
        }

        $root = [
            'id' => $member->id, 
            'name' => $member->user->name,
            'downlines' => $this->getDownlines($member->id, $field),
        ];
        return response()->json($root);
    }


    protected function getDownlines($member_id, $field)
    {
        $downlines = MemberService::getDirectDownlines($member_id, $field);
        if (empty($downlines)) {
            return [];
        } else {
            for ($i = 0; $i < count($downlines); $i++) {
                $member = (array)$downlines[$i];
                $member['downlines'] = $this->getDownlines($member['id'], $field);
                $downlines[$i] = $member;
            }
            return $downlines;
        }
    }
}
