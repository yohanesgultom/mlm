<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
     /**
     * @apiDefine SuccessCountry
     *
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} name
     * 
     */

     /**
     * @apiDefine ParamCountry
     *
     * @apiParam {String} name
     * 
     */

    /**
     * 
     * @api {get} /countries List
     * @apiName list
     * @apiGroup Country
     * @apiPermission member
     * 
     * @apiParam {Integer} [page=1] Page index to open
     * @apiParam {Integer} [limit=10] Limit per page
     * @apiParam {String} [name] Partial name
     * 
     * @apiSuccess {Integer} current_page
     * @apiSuccess {Array} data Array of Countries
     * @apiSuccess {String} first_page_url
     * @apiSuccess {Integer} from Start index
     * @apiSuccess {Integer} last_page
     * @apiSuccess {String} last_page_url
     * @apiSuccess {String} next_page_url
     * @apiSuccess {String} path
     * @apiSuccess {Integer} per_page
     * @apiSuccess {String} prev_page_url
     * @apiSuccess {Integer} to Last index
     * @apiSuccess {Integer} total Total records
     */         
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $query = Country::whereNotNull('id');
        
        // filter
        $input = $request->all();
        $query = $this->queryFilter($query, $input, [
            'name' => 'LIKE',
        ]);
        
        return response()->json($query->paginate($limit));
    }

    /**
     * @api {post} /countries Create
     * @apiName create
     * @apiGroup Country
     * @apiPermission superadmin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamCountry
     * @apiUse SuccessCountry
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $model = new Country;
        $model->fill($input);
        $model->save();
        return response()->json($model);
    }

    /**
     * 
     * @api {get} /countries/:id Get
     * @apiName get
     * @apiGroup Country
     * @apiPermission member
     * 
     * @apiUse SuccessCountry
     */
    public function show(Country $country)
    {
        return response()->json($country);
    }

    /**
     * @api {put} /countries/:id Update
     * @apiName update
     * @apiGroup Country
     * @apiPermission superadmin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse ParamCountry
     * @apiUse SuccessCountry
     */
    public function update(Request $request, Country $country)
    {
        if (empty($country)) {
            return response()->json([]);
        }

        $input = $request->all();
        unset($input['id']);
        $country->fill($input);
        $country->save();

        return response()->json($country);
    }

    /**
     * @api {delete} /countries/:id Delete
     * @apiName delete
     * @apiGroup Country
     * @apiPermission superadmin
     * 
     * @apiHeader {String} X-CSRF-TOKEN Laravel CSRF token generated by `csrf_token()`
     * @apiUse SuccessCountry
     */
    public function destroy(Country $country)
    {
        if (empty($country)) {
            return response()->json([]);
        }

        $country->delete();
        return response()->json($country);
    }
}
