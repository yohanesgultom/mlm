<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\WarehouseStock;

class WarehouseStockLog extends Model
{
    protected $fillable = [
        'qty_before',
        'qty_in',
        'qty_out',
        'available_before',
        'available_in',
        'available_out',
        'process_date',
        'exp_date',
        'warehouse_id',
        'product_id',
        'company_id',
        'warehouse_stock_log_id', // linked logs in case of transfer between warehouse
        'member_stock_id', // linked member stocks in case of indirect transfer to member
        'note', // multipurpose note
    ];    

    protected $dates = [
        'process_date',
        'exp_date',
    ];

    public static function boot()
    {
        parent::boot();

        // modify warehouse qty and availability stock on creation
        static::creating(function ($model) {
            $stock = WarehouseStock::firstOrNew([
                'warehouse_id' => $model->warehouse_id, 
                'product_id' => $model->product_id,
                'exp_date' => $model->exp_date,
            ]);
            // if not new, replace data in model
            if (!empty($stock->id)) {
                $model->qty_before = $stock->qty;
                $model->available_before = $stock->available;
            }
            $stock->qty = $stock->qty + $model->qty_in - $model->qty_out;
            $stock->available = $stock->available + $model->available_in - $model->available_out;
            if ($stock->qty < 0) {
                throw new \Exception('Negative qty');
            }
            if ($stock->available < 0) {
                throw new \Exception('Negative available');
            }
            if ($stock->qty < $stock->available) {
                throw new \Exception('Qty is less than available');
            }
            $stock->save();            
        });
        
    }   

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Warehouse');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }    
}
