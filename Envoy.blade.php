@servers(['web' => 'deploy@188.166.216.151'])

@setup
    $repository = 'https://gitlab.com/yohanesgultom/mlm';
    $releases_dir = '$HOME/dev/releases';
    $app_dir = '/var/www/dev';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    set_permissions
    update_symlinks
    update_database
    clear_cache
    clean_old_releases
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir -p {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    {{-- composer install --prefer-dist --no-scripts -q -o --no-dev --}}
    composer install --prefer-dist --no-scripts -q -o    
@endtask

@task('build_frontend')
    echo "Building frontend"
    export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"    
    cd {{ $new_release_dir }}
    yarn install
    node_modules/.bin/bower install
    yarn run production    
@endtask

@task('set_permissions')
    echo "Setting permissions"
    chmod -R 755 {{ $new_release_dir }}/apidoc    
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs $HOME/storage {{ $new_release_dir }}/storage

    echo "Linking public storage directory"
    rm -rf {{ $new_release_dir }}/public/storage
    ln -nfs $HOME/storage/app/public {{ $new_release_dir }}/public/storage

    echo 'Linking .env file'    
    ln -nfs $HOME/.env {{ $new_release_dir }}/.env

    echo 'Linking tmp dir'    
    rm -Rf {{ $new_release_dir }}/tmp
    ln -nfs $HOME/tmp {{ $new_release_dir }}/tmp

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('clear_cache')    
    echo "Clearing cache"
    cd {{ $new_release_dir }}
    sudo -u www-data php artisan route:clear
    sudo -u www-data php artisan view:clear
    sudo -u www-data php artisan cache:clear
    sudo -u www-data php artisan queue:restart
@endtask

@task('update_database')
    echo "Updating database"
    cd {{ $new_release_dir }}
    php artisan migrate
@endtask

@task('refresh_database')
    {{-- Refresh database. Only for development --}}
    echo "Refreshing database"
    cd {{ $new_release_dir }}
    php artisan migrate:fresh --seed
    php artisan passport:install
@endtask

@task('clean_old_releases')
    {{-- This will list our releases by modification time and delete all but the 3 most recent. --}}
    purging=$(ls -dt {{ $releases_dir }}/* | tail -n +3);

    if [ "$purging" != "" ]; then
        echo Purging old releases: $purging;
        rm -rf $purging;
    else
        echo "No releases found for purging at this time";
    fi
@endtask
