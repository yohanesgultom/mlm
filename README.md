# Multi Level Marketing Information System

Multi Level Marketing Information System

## Dev dependencies

To develop this system you need:

* PHP CLI 7.x
* Composer 1.5.x
* MySQL 14.14 Distrib 5.7.20
* Node.js 8.9.x
* Yarn 1.3.x
* SQLite3 for PHP (for testing)
* apidoc (optional) http://apidocjs.com (API documentation generation)

## Run app

Follow these steps to run app in development mode:

1. Copy `.env.example` to `.env` and edit it to match your configuration (esp. MySQL server details)
1. Install Composer dependencies from terminal/command prompt: `composer install` (this will take awhile depending on your internet connection)
1. Migrate database by running: `php artisan migrate:fresh --seed`
1. Setup OAuth2: `php artisan passport:install`
1. Install front-end dependencies: `yarn install`
1. Install assets: `node_modules/.bin/bower install` 
1. Build front-end: `yarn run development`
1. Run server: `php artisan serve`

> On Ubuntu 16 amd64 or later, there is an [issue](https://github.com/imagemin/mozjpeg-bin/issues/18) with node's `mozjpeg` that require us to install (older) `libpng12` from http://ppa.launchpad.net/hiberis/ppa/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.27-1ubuntu1~ppa1_amd64.deb

## Test app

Run `phpunit` testing by calling: `vendor/bin/phpunit` (requires `sqlite3` for corresponding PHP version)

## Generate API docs

To generate API docs (static HTML) from controllers:

1. Install node module globaly: `npm i -g apidoc`
1. Generate from inside project's root directory: 
	1. Session-based API (for web-client) `apidoc -i app/Http/Controllers/ -e API/ -o apidoc/web`
	1. OAuth2 API (for mobile-client) `apidoc -i app/Http/Controllers/API -o apidoc/mobile`
1. Access the doc from `apidoc/web/index.html` and `apidoc/mobile/index.html`


## Seed Data

Running seeder will populate these data:

* Superadmin and admin are defined in `database/seeds/UsersTableSeeder.php`.
* Members are imported from `database/seeds/members.sql` with same password: `secret`

## Setup queue worker in server

To setup queue worker in server (tested on Ubuntu 16.04 LTS):

1. Install `supervisor`: `sudo apt-get install supervisor`
1. Modify `queue-worker.conf` based on your server configuration (eg. deploy path, processors .etc)
1. Copy `queue-worker.conf` to `/etc/supervisor/conf.d/queue-worker.conf`
1. Start `supervisor`: 
	```
	sudo supervisorctl reread
	sudo supervisorctl update
	sudo supervisorctl start queue-worker:*
	```
