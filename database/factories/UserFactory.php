<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $name = $faker->name();
    $username = uniqid();
    $email = $username.'@email.com';

    return [
        'name' => $name,
        'username' => $username,
        'email' => $email,
        'password' => 'secret',
        'role' => App\User::ROLES['MEMBER'],
        'verified' => true,
    ];
});

$factory->state(App\User::class, 'superadmin', [    
    'role' => App\User::ROLES['SUPERADMIN'],
]);

$factory->state(App\User::class, 'admin', [
    'role' => App\User::ROLES['ADMIN'],
]);

$factory->state(App\User::class, 'member', [
    'role' => App\User::ROLES['MEMBER'],
]);