<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    $cutoff_day = 30;
    $date = $faker->dateTime();
    $date->add(new \DateInterval('P'.$cutoff_day.'D'));    
    return [
        'name' => $faker->company,
        'cutoff_day' => $cutoff_day,
        'weekly_cutoff_day_code' => $faker->randomElement(['MON', 'TUE', 'WEN', 'THU', 'FRI', 'SAT', 'SUN']),
    ];
});
