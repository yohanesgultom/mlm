<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'invoice_no' => (string) $faker->unixTime(),
        'invoice_date' => $faker->dateTimeThisYear(),
        'bill_no' => (string) $faker->unixTime(),
        'status' => App\Order::STATUSES['COMPLETED'],
    ];
});
