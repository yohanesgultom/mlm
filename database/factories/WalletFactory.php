<?php

use Faker\Generator as Faker;

$factory->define(App\Wallet::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(array_values(App\Wallet::TYPES)),
        'balance' => $faker->randomFloat(2, 100000.0, 1000000.0),
    ];
});
