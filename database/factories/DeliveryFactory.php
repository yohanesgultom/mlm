<?php

use Faker\Generator as Faker;

$factory->define(App\Delivery::class, function (Faker $faker) {
    static $cities = [
        ['Medan', 'Sumatera Utara'],
        ['Jakarta Timur', 'DKI Jakarta'],
        ['Surabaya', 'Jawa Timur'],
        ['Denpasar', 'Bali'],
        ['Pontianak', 'Kalimantan Barat'],
        ['Manado', 'Sulawesi Utara'],
        ['Jayapura', 'Papua'],
        ['Ambon', 'Maluku'],
    ];    
    $city = $faker->randomElement($cities);
    return [
        'cost' => $faker->randomNumber(6),
        'delivery_date' => $faker->dateTime(),
        'courier' => $faker->sentence(3),
        'courier_ref' => (string) $faker->unixTime(),
        'address' => $faker->streetAddress,
        'address_zipcode' => $faker->postcode,
        'address_city' => $city[0],
        'address_state' => $city[1],
        'status' => $faker->randomElement(array_values(App\Delivery::STATUSES)),
    ];
});
