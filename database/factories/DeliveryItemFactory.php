<?php

use Faker\Generator as Faker;

$factory->define(App\DeliveryItem::class, function (Faker $faker) {
    return [
        'qty' => $faker->numberBetween(1, 10),
    ];
});
