<?php

use Faker\Generator as Faker;

$factory->define(App\Registration::class, function (Faker $faker) {
    $name = $faker->name();
    $username = uniqid();
    $email = $username.'@email.com';
    return [
        'name' => $name,
        'email' => $email,
    ];
});
