<?php

use Faker\Generator as Faker;

$cities = [
    ['Medan', 'Sumatera Utara'],
    ['Jakarta Timur', 'DKI Jakarta'],
    ['Surabaya', 'Jawa Timur'],
    ['Denpasar', 'Bali'],
    ['Pontianak', 'Kalimantan Barat'],
    ['Manado', 'Sulawesi Utara'],
    ['Jayapura', 'Papua'],
    ['Ambon', 'Maluku'],
];

$factory->define(App\Warehouse::class, function (Faker $faker) use ($cities) {
    static $id;
    $id = $id ? $id + 1 : 1;
    return [
        'id' => str_pad((string) $id, 7, '0', STR_PAD_LEFT),
        'name' => 'Warehouse '.$id,
        'type' => $id == 1 ? App\Warehouse::TYPES['CENTRAL'] : App\Warehouse::TYPES['REGULAR'],
        'address' => $faker->streetAddress,
        'address_zipcode' => $faker->postcode,
        'address_city' => $faker->randomElement($cities)[0],
        'address_state' => $faker->randomElement($cities)[1],
    ];
});
