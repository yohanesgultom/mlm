<?php

use Faker\Generator as Faker;

$factory->define(App\Transaction::class, function (Faker $faker) {
    return [
        'code' => (string) $faker->unixTime(),
        'desc' => $faker->sentence(),
        'qty' => 0,
        'amount' => 0,
        'points'=> 0,
        'process_date' => $faker->dateTime(),
        'point_date'=> $faker->randomElement([null, $faker->dateTimeBetween('-1 months', '-1 weeks')]),
    ];
});
