<?php

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'type' => $faker->randomElement(array_values(App\Article::TYPES)),
        'status' => $faker->randomElement(array_values(App\Article::STATUSES)),
        'html' => $faker->paragraph(5),
        'publish_date' => $faker->dateTime(),
    ];
});
