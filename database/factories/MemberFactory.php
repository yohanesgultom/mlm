<?php

use Faker\Generator as Faker;

$factory->define(App\Member::class, function (Faker $faker) {
    static $id;

    $regDate = $faker->dateTime();
    $bonusDate = clone $regDate;
    $bonusDate->add(new DateInterval('P30D'));
    $id = $id ? $id + 1 : 1;    
    return [
        'id' => str_pad((string) $id, 7, '0', STR_PAD_LEFT),
        'reg_date' => $regDate,
        'birth_date' => $faker->dateTime('-17 years'),
        'bonus_date' => $bonusDate,
        'gender' => $faker->randomElement(array_values(App\Member::GENDERS)),
        'official_id' => $faker->creditCardNumber,
        'tax_id' => $faker->creditCardNumber,
        'address' => $faker->streetAddress,
        'address_zipcode' => $faker->postcode,
        // 'address_city',
        // 'address_state',
        // 'country_id',
        // 'phone_home',
        // 'phone_office',
        // 'phone_mobile',
        // 'phone_fax',
        // 'beneficiary_name',
        // 'beneficiary_relation',
        // 'bank_account_no',
        // 'bank_account_name',
        // 'bank_account_city',
        // 'bank_account_branch',
        // 'bank_id',
        'status' => App\Member::STATUSES['ACTIVE'],
    ];
});
