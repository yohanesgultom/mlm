<?php

use Faker\Generator as Faker;

// setlocale (LC_MONETARY, 'id_ID');
$factory->define(App\Product::class, function (Faker $faker) {
    $base_price = $faker->numberBetween(100000, 500000);
    return [
        'name' => $faker->sentence(),
        'desc' => $faker->paragraph(),
        'desc_short' => $faker->sentence(),
        'retail_price' => money_format('%.2n', $base_price),
        'base_price' => $base_price,
        'bonus_value' => $base_price,
        'point_value' => round($base_price / 1000),
        'weight' => $faker->numberBetween(1, 10),
    ];
});
