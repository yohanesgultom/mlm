<?php

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            ['name' => 'AMERICAN EXPRESS BANK LTD', 'code' => '30'],
            ['name' => 'ANGLOMAS INTERNASIONAL BANK', 'code' => '531'],
            ['name' => 'ANZ PANIN BANK', 'code' => '61'],
            ['name' => 'BANK ABN AMRO', 'code' => '52'],
            ['name' => 'BANK AGRO NIAGA', 'code' => '494'],
            ['name' => 'BANK AKITA', 'code' => '525'],
            ['name' => 'BANK ALFINDO', 'code' => '503'],
            ['name' => 'BANK ANTARDAERAH', 'code' => '88'],
            ['name' => 'BANK ARTA NIAGA KENCANA', 'code' => '20'],
            ['name' => 'BANK ARTHA GRAHA', 'code' => '37'],
            ['name' => 'BANK ARTOS IND', 'code' => '542'],
            ['name' => 'BANK BCA', 'code' => '14'],
            ['name' => 'BANK BCA SYARIAH', 'code' => '536'],
            ['name' => 'BANK BENGKULU', 'code' => '133'],
            ['name' => 'BANK BII MAYBANK', 'code' => '16'],
            ['name' => 'BANK BINTANG MANUNGGAL', 'code' => '484'],
            ['name' => 'BANK BISNIS INTERNASIONAL', 'code' => '459'],
            ['name' => 'BANK BNI', 'code' => '9'],
            ['name' => 'BANK BNI SYARIAH', 'code' => '9'],
            ['name' => 'BANK BNP PARIBAS INDONESIA', 'code' => '57'],
            ['name' => 'BANK BRI', 'code' => '2'],
            ['name' => 'BANK BRI SYARIAH', 'code' => '422'],
            ['name' => 'BANK BUANA IND', 'code' => '23'],
            ['name' => 'BANK BUKOPIN', 'code' => '441'],
            ['name' => 'BANK BUMI ARTA', 'code' => '76'],
            ['name' => 'BANK CAPITAL INDONESIA', 'code' => '54'],
            ['name' => 'BANK CENTURY', 'code' => '95'],
            ['name' => 'BANK CHINA TRUST INDONESIA', 'code' => '949'],
            ['name' => 'BANK CIMB NIAGA', 'code' => '22'],
            ['name' => 'BANK CIMB NIAGA SYARIAH', 'code' => '22'],
            ['name' => 'BANK COMMONWEALTH', 'code' => '950'],
            ['name' => 'BANK CREDIT AGRICOLE INDOSUEZ', 'code' => '39'],
            ['name' => 'BANK DANAMON', 'code' => '11'],
            ['name' => 'BANK DBS INDONESIA', 'code' => '46'],
            ['name' => 'BANK DIPO INTERNATIONAL', 'code' => '523'],
            ['name' => 'BANK DKI', 'code' => '111'],
            ['name' => 'BANK EKONOMI', 'code' => '87'],
            ['name' => 'BANK EKSEKUTIF', 'code' => '558'],
            ['name' => 'BANK EKSPOR INDONESIA', 'code' => '3'],
            ['name' => 'BANK FAMA INTERNASIONAL', 'code' => '562'],
            ['name' => 'BANK FINCONESIA', 'code' => '945'],
            ['name' => 'BANK GANESHA', 'code' => '161'],
            ['name' => 'BANK HAGA', 'code' => '89'],
            ['name' => 'BANK HAGAKITA', 'code' => '159'],
            ['name' => 'BANK HARDA', 'code' => '567'],
            ['name' => 'BANK HARFA', 'code' => '517'],
            ['name' => 'BANK HARMONI INTERNATIONAL', 'code' => '166'],
            ['name' => 'BANK HIMPUNAN SAUDARA 1906', 'code' => '212'],
            ['name' => 'BANK IFI', 'code' => '93'],
            ['name' => 'BANK INA PERDANA', 'code' => '513'],
            ['name' => 'BANK INDEX SELINDO', 'code' => '555'],
            ['name' => 'BANK INDOMONEX', 'code' => '498'],
            ['name' => 'BANK JABAR', 'code' => '110'],
            ['name' => 'BANK JASA JAKARTA', 'code' => '427'],
            ['name' => 'BANK JASA JAKARTA', 'code' => '472'],
            ['name' => 'BANK JATENG', 'code' => '113'],
            ['name' => 'BANK JATIM', 'code' => '114'],
            ['name' => 'BANK KEPPEL TATLEE BUANA', 'code' => '53'],
            ['name' => 'BANK KESEJAHTERAAN EKONOMI', 'code' => '535'],
            ['name' => 'BANK LAMPUNG', 'code' => '121'],
            ['name' => 'BANK LIPPO', 'code' => '26'],
            ['name' => 'BANK MALUKU', 'code' => '131'],
            ['name' => 'BANK MANDIRI', 'code' => '8'],
            ['name' => 'BANK MASPION', 'code' => '157'],
            ['name' => 'BANK MAYAPADA', 'code' => '97'],
            ['name' => 'BANK MAYBANK INDOCORP', 'code' => '947'],
            ['name' => 'BANK MAYORA', 'code' => '553'],
            ['name' => 'BANK MEGA', 'code' => '426'],
            ['name' => 'BANK MERINCORP', 'code' => '946'],
            ['name' => 'BANK MESTIKA', 'code' => '151'],
            ['name' => 'BANK METRO EXPRESS', 'code' => '152'],
            ['name' => 'BANK MITRANIAGA', 'code' => '491'],
            ['name' => 'BANK MIZUHO INDONESIA', 'code' => '48'],
            ['name' => 'BANK MNC/BANK BUMIPUTERA', 'code' => '485'],
            ['name' => 'BANK MUAMALAT', 'code' => '147'],
            ['name' => 'BANK MULTI ARTA SENTOSA', 'code' => '548'],
            ['name' => 'BANK MULTICOR', 'code' => '36'],
            ['name' => 'BANK NAGARI', 'code' => '118'],
            ['name' => 'BANK NTT', 'code' => '130'],
            ['name' => 'BANK NUSANTARA PARAHYANGAN', 'code' => '145'],
            ['name' => 'BANK OCBC INDONESIA', 'code' => '948'],
            ['name' => 'BANK OCBC NISP', 'code' => '28'],
            ['name' => 'BANK OF AMERICA, N.A', 'code' => '33'],
            ['name' => 'BANK OF CHINA LIMITED', 'code' => '69'],
            ['name' => 'BANK PANIN', 'code' => '19'],
            ['name' => 'BANK PERMATA', 'code' => '13'],
            ['name' => 'BANK PERSYARIKATAN INDONESIA', 'code' => '521'],
            ['name' => 'BANK PURBA DANARTA', 'code' => '547'],
            ['name' => 'BANK QNB KESAWAN', 'code' => '167'],
            ['name' => 'BANK RESONA PERDANIA', 'code' => '47'],
            ['name' => 'BANK RIAU', 'code' => '119'],
            ['name' => 'BANK ROYAL INDONESIA', 'code' => '501'],
            ['name' => 'BANK SINAR HARAPAN BALI', 'code' => '564'],
            ['name' => 'BANK SINARMAS', 'code' => '153'],
            ['name' => 'BANK SRI PARTHA', 'code' => '466'],
            ['name' => 'BANK SULTRA', 'code' => '135'],
            ['name' => 'BANK SULUT', 'code' => '127'],
            ['name' => 'BANK SUMITOMO MITSUI INDONESIA', 'code' => '45'],
            ['name' => 'BANK SUMSEL', 'code' => '120'],
            ['name' => 'BANK SUMUT', 'code' => '117'],
            ['name' => 'BANK SWADESI', 'code' => '146'],
            ['name' => 'BANK SWAGUNA', 'code' => '405'],
            ['name' => 'BANK SYARIAH MANDIRI', 'code' => '451'],
            ['name' => 'BANK SYARIAH MEGA', 'code' => '506'],
            ['name' => 'BANK TABUNGAN NEGARA (BTN)', 'code' => '200'],
            ['name' => 'BANK UOB INDONESIA', 'code' => '23'],
            ['name' => 'BANK VICTORIA INTERNATIONAL', 'code' => '566'],
            ['name' => 'BANK WINDU KENTJANA', 'code' => '162'],
            ['name' => 'BANK WOORI INDONESIA', 'code' => '68'],
            ['name' => 'BANK YUDHA BHAKTI', 'code' => '490'],
            ['name' => 'BPD ACEH', 'code' => '116'],
            ['name' => 'BPD BALI', 'code' => '129'],
            ['name' => 'BPD DIY', 'code' => '112'],
            ['name' => 'BPD JAMBI', 'code' => '115'],
            ['name' => 'BPD KALIMANTAN BARAT', 'code' => '123'],
            ['name' => 'BPD KALSEL', 'code' => '122'],
            ['name' => 'BPD KALTENG', 'code' => '125'],
            ['name' => 'BPD KALTIM', 'code' => '124'],
            ['name' => 'BPD NTB', 'code' => '128'],
            ['name' => 'BPD PAPUA', 'code' => '132'],
            ['name' => 'BPD SULAWESI TENGAH', 'code' => '134'],
            ['name' => 'BPD SULSEL', 'code' => '126'],
            ['name' => 'BTPN', 'code' => '213'],
            ['name' => 'CENTRATAMA NASIONAL BANK', 'code' => '559'],
            ['name' => 'CITIBANK', 'code' => '31'],
            ['name' => 'CITIBANK N.A.', 'code' => '31'],
            ['name' => 'DEUTSCHE BANK AG.', 'code' => '67'],
            ['name' => 'HALIM INDONESIA BANK', 'code' => '164'],
            ['name' => 'INDOSAT DOMPETKU', 'code' => '789'],
            ['name' => 'ING INDONESIA BANK', 'code' => '34'],
            ['name' => 'JENIUS', 'code' => '213'],
            ['name' => 'JP. MORGAN CHASE BANK, N.A.', 'code' => '32'],
            ['name' => 'KOREA EXCHANGE BANK DANAMON', 'code' => '59'],
            ['name' => 'LIMAN INTERNATIONAL BANK', 'code' => '526'],            
            ['name' => 'PRIMA MASTER BANK', 'code' => '520'],
            ['name' => 'RABOBANK INTERNASIONAL INDONESIA', 'code' => '60'],
            ['name' => 'STANDARD CHARTERED BANK', 'code' => '50'],
            ['name' => 'TELKOMSEL TCASH', 'code' => '911'],
            ['name' => 'THE BANGKOK BANK COMP. LTD', 'code' => '40'],
            ['name' => 'THE BANK OF TOKYO MITSUBISHI UFJ LTD', 'code' => '42'],
            ['name' => 'THE HONGKONG & SHANGHAI B.C. (BANK HSBC)', 'code' => '41'],
        ]);
    }
}
