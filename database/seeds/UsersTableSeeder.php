<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        // superadmin doesn't have company
        factory(App\User::class, 1)->states('superadmin')->create([
            'username' => 'superadmin',
            'email' => 'superadmin@email.com',
            'password' => 'superadmin'
        ]);
        
        // we only have 1 company for now
        $company = App\Company::all()->first();

        // admin must have company        
        factory(App\User::class, 1)->states('admin')->create([
            'username' => 'admin',
            'email' => 'admin@greenvit.co.id',
            'password' => 'admin123',
            'company_id' => $company->id,
        ]);

        factory(App\User::class, 1)->create([
            'username' => 'owner',
            'email' => 'owner@greenvit.co.id',
            'password' => 'owner123',
            'company_id' => $company->id,
            'role' => App\User::ROLES['ADMIN'],
        ]);

        factory(App\User::class, 1)->create([
            'username' => 'it',
            'email' => 'it@greenvit.co.id',
            'password' => 'it123',
            'company_id' => $company->id,
            'role' => App\User::ROLES['IT'],
        ]);
        
        factory(App\User::class, 1)->create([
            'username' => 'salesadmin',
            'email' => 'salesadmin@greenvit.co.id',
            'password' => 'salesadmin123',
            'company_id' => $company->id,
            'role' => App\User::ROLES['SALESADMIN'],
        ]);

        factory(App\User::class, 1)->create([
            'username' => 'finance',
            'email' => 'finance@greenvit.co.id',
            'password' => 'finance123',
            'company_id' => $company->id,
            'role' => App\User::ROLES['FINANCE'],
        ]);

        factory(App\User::class, 1)->create([
            'username' => 'marketing',
            'email' => 'marketing@greenvit.co.id',
            'password' => 'marketing123',
            'company_id' => $company->id,
            'role' => App\User::ROLES['MARKETING'],
        ]);

        factory(App\User::class, 1)->create([
            'username' => 'logistics',
            'email' => 'logistics@greenvit.co.id',
            'password' => 'logistics123',
            'company_id' => $company->id,
            'role' => App\User::ROLES['LOGISTICS'],
        ]);
        
        // trinary member tree
        $numLevel = 3;
        $numBranch = 3;        
        $companyId = $company->id;

        $achievements = App\Achievement::where('company_id', $company->id)        
            ->orderBy('order', 'asc')
            ->take($numLevel)
            ->get();

        $silverMemberType = App\MemberType::where('company_id', $company->id)
            ->where('name', 'SILVER')
            ->first();
            
        $members = collect([]);
        $lastId = 0;
        $regDate = new \DateTime();
        $regDate->sub(new \DateInterval('P'.$numLevel.'D'));
        $sponsorId = null;
        for ($i = 0; $i < $numLevel; $i++) {
            $regDate->add(new \DateInterval('P'.$i.'D'));
            $achievement = $achievements->pop();
            if ($i == 0) {
                $members = $this->createMembers(
                    $regDate,
                    $companyId,
                    null,
                    null,
                    $achievement->id,
                    $silverMemberType->id,
                    1,
                    $lastId
                );
                // use root member as sponsor for all
                // to differentiate sponsor and parent tree
                $sponsorId = $members->first()->id;
            } else {
                $newMembers = collect([]);
                foreach ($members as $m) {                    
                    $parentId = $m->id;
                    $sponsorId = $m->id;
                    $newMembers = $newMembers->concat(
                        $this->createMembers(
                            $regDate,
                            $companyId,
                            $parentId,
                            $sponsorId,
                            $achievement->id,
                            $silverMemberType->id,
                            $numBranch,
                            $lastId                            
                        )
                    );
                }
                $members = $newMembers;
            }
        }

    }

    function createMembers(
        $regDate,
        $companyId,
        $parentId,
        $sponsorId, 
        $achievementId,
        $memberTypeId,
        $count,
        &$lastId
    )
    {                
        $members = factory(App\Member::class, $count)
            ->make()
            ->each(function ($m) use (
                $regDate,
                $companyId,
                $sponsorId,
                $parentId,
                $achievementId,
                $memberTypeId,
                &$lastId) 
            {
                $lastId++;
                $m->id = str_pad((string) $lastId, 7, '0', STR_PAD_LEFT);
                $u = factory(App\User::class, 1)
                    ->states('member')
                    ->create([
                        'username' => 'member'.$lastId,
                        'email' => 'member'.$lastId.'@greenvit.co.id',
                        'company_id' => $companyId
                    ])->first();                
                $m->user_id = $u->id;
                $m->name_printed = $u->name;
                $m->reg_date = $regDate;
                $m->reg_bv = 300000;
                $m->achievement_id = $achievementId;
                $m->current_personal_points = 60; // TODO: calculate from transaction
                $m->current_personal_bonus = 100000; // TODO: calculate from transaction
                $m->parent_id = $parentId;
                $m->sponsor_id = $sponsorId;
                $m->member_type_id = $memberTypeId;
                $m->save();
                // create wallets
                factory(App\Wallet::class, 1)->create(['member_id' => $m->id, 'type' => App\Wallet::TYPES['TOPUP']]);
                factory(App\Wallet::class, 1)->create(['member_id' => $m->id, 'type' => App\Wallet::TYPES['BONUS']]);                
            });
        return $members;
    }    
    
}
