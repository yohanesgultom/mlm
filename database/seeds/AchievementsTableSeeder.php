<?php

use Illuminate\Database\Seeder;

class AchievementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        DB::table('achievements')->insert([
            ['company_id' => $companyId, 'order' => 10, 'code' => 'D', 'desc' => 'Distributor', 'maintenance_pv' => 5, 'conditions' => null],
            ['company_id' => $companyId, 'order' => 20, 'code' => 'SPV', 'desc' => 'Supervisor', 'maintenance_pv' => 20, 'conditions' => json_encode([
                ['downlines' => ['D' => 3], 'downlines_pv' => ['D' => 60]],
                ['pv' => 200],
            ])],
            ['company_id' => $companyId, 'order' => 30, 'code' => 'QM', 'desc' => 'Qualified Manager', 'maintenance_pv' => 30, 'conditions' => json_encode([
                ['downlines' => ['SPV' => 3]]
            ])],
            ['company_id' => $companyId, 'order' => 40, 'code' => 'M', 'desc' => 'Manager', 'maintenance_pv' => 30, 'conditions' => json_encode([
                ['downlines' => ['QM' => 1, 'SPV' => 2]],
            ])],
            ['company_id' => $companyId, 'order' => 50, 'code' => 'SM', 'desc' => 'Senior Manager', 'maintenance_pv' => 30, 'conditions' => json_encode([
                ['downlines' => ['QM' => 2, 'SPV' => 1]],
            ])],
            ['company_id' => $companyId, 'order' => 60, 'code' => 'GM', 'desc' => 'General Manager', 'maintenance_pv' => 30, 'conditions' => json_encode([
                ['downlines' => ['QM' => 3]],
            ])],
            ['company_id' => $companyId, 'order' => 70, 'code' => 'DI', 'desc' => 'Director', 'maintenance_pv' => 45, 'conditions' => json_encode([
                ['downlines' => ['GM' => 3]],
            ])],
            ['company_id' => $companyId, 'order' => 80, 'code' => 'MD', 'desc' => 'Managing Director', 'maintenance_pv' => 45, 'conditions' => json_encode([
                ['downlines' => ['DI' => 2, 'GM' => 1]],
            ])],
            ['company_id' => $companyId, 'order' => 90, 'code' => 'PD', 'desc' => 'President Director', 'maintenance_pv' => 45, 'conditions' => json_encode([
                ['downlines' => ['DI' => 3]],
            ])],
        ]);

        // DB::table('achievements')->insert([
        //     ['company_id' => $companyId, 'order' => 10, 'code' => 'D', 'desc' => 'Distributor', 'conditions' => null],
        //     ['company_id' => $companyId, 'order' => 20, 'code' => 'SPV', 'desc' => 'Supervisor', 'conditions' => '(( >= 3 && avg_pv >= 60) || current_personal_points >= 200'],
        //     ['company_id' => $companyId, 'order' => 20, 'code' => 'QM', 'desc' => 'Qualified Manager', 'conditions' => 'downlines["SPV"].length >= 3'],
        //     ['company_id' => $companyId, 'order' => 30, 'code' => 'M', 'desc' => 'Manager', 'conditions' => 'downlines["QM"].length >= 1 && downlines["SPV"].length >= 2'],
        //     ['company_id' => $companyId, 'order' => 40, 'code' => 'SM', 'desc' => 'Senior Manager', 'conditions' => 'downlines["QM"].length >= 2 && downlines["SPV"].length >= 1'],
        //     ['company_id' => $companyId, 'order' => 50, 'code' => 'GM', 'desc' => 'General Manager', 'conditions' => 'downlines["QM"].length >= 3'],
        //     ['company_id' => $companyId, 'order' => 60, 'code' => 'DI', 'desc' => 'Director', 'conditions' => 'downlines["GM"].length >= 3'],
        //     ['company_id' => $companyId, 'order' => 70, 'code' => 'MD', 'desc' => 'Managing Director', 'conditions' => 'downlines["DI"].length >= 2 && downlines["GM"].length >= 1'],
        //     ['company_id' => $companyId, 'order' => 80, 'code' => 'PD', 'desc' => 'President Director', 'conditions' => 'downlines["DI"].length >= 3'],
        // ]);
    }
}
