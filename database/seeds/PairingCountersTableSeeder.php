<?php

use Illuminate\Database\Seeder;
use App\Member;
use Carbon\Carbon;
use Faker\Factory as Faker;

class PairingCountersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = collect([]);
        $members = Member::all();
        $dates = [
            Carbon::now()->subMonths(2)->toDateTimeString(),
            Carbon::now()->subMonths(1)->toDateTimeString(),
            Carbon::now(),
        ];
        foreach ($members as $m) {
            $left = 0;
            $mid = 0;
            $right = 0;
            foreach ($dates as $d) {
                $left += $faker->numberBetween(0, 100);
                $mid += $faker->numberBetween(0, 100);
                $right += $faker->numberBetween(0, 100);
                $data->push([
                    'process_date' => $d,
                    'count_left' => $left,
                    'count_mid' => $mid,
                    'count_right' => $right,
                    'member_id' => $m->id,            
                ]);
            }
        }
        DB::table('pairing_counters')->insert($data->toArray());
    }
}
