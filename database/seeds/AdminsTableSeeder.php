<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // superadmin doesn't have company
        factory(App\User::class, 1)->states('superadmin')->create([
            'username' => 'superadmin',
            'email' => 'superadmin@greenvit.co.id',
            'password' => 'superadmin'
        ]);
        
        // we only have 1 company for now
        $company = App\Company::all()->first();

        // admin must have company        
        factory(App\User::class, 1)->states('admin')->create([
            'username' => 'admin',
            'email' => 'admin@greenvit.co.id',
            'password' => 'admin123',
            'company_id' => $company->id,
        ]);
    }
}
