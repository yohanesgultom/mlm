<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductRelatedTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $faker = Faker::create();

        // tags and types
        $tags = [];
        $types = [];
        for ($i = 0; $i < 10; $i++) array_push($tags, ['name' => $faker->word, 'company_id' => $companyId]);
        for ($i = 0; $i < 10; $i++) array_push($types, ['name' => $faker->word, 'company_id' => $companyId]);
        DB::table('tags')->insert($tags);
        DB::table('product_types')->insert($types);

        // create products
        $tagIds = DB::table('tags')->select('id')->pluck('id')->toArray();
        $typeIds = DB::table('product_types')->select('id')->pluck('id')->toArray();
        foreach ($typeIds as $typeId) {
            factory(App\Product::class, 5)
                ->make()
                ->each(function ($p) use ($typeId, $tagIds, $faker) {
                    $p->bonus_value = 50000;
                    $p->point_value = 15;
                    $p->product_type_id = $typeId;
                    $p->save();
                    $p->tags()->attach($faker->randomElements($tagIds, 3));
                });
        }        
    }
}
