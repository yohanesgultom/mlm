<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cutoff_day = 30;
        DB::table('companies')->insert([
            'name' => 'GreenVit International',
            'cutoff_day' => $cutoff_day,
            'weekly_cutoff_day_code' => 'wednesday',
        ]);
    }
}
