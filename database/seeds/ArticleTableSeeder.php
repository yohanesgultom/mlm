<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = \App\Company::where('name', 'GreenVit International')->first();
        $admin = \App\User::where('company_id', $company->id)->where('role', \App\User::ROLES['ADMIN'])->first();
        $this->articles = factory(App\Article::class, 10)->create([
            'created_by' => $admin->id,
            'company_id' => $company->id,
        ]);
    }
}
