<?php

use Illuminate\Database\Seeder;

class ReligionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('religions')->insert([
            ['name' => 'Islam'],
            ['name' => 'Katholik'],
            ['name' => 'Budha'],
            ['name' => 'Kristen Protestan'],
            ['name' => 'Hindu'],
            ['name' => 'Kong Hu Chu'],
        ]);
    }
}
