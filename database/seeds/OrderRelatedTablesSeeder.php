<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class OrderRelatedTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $walletIds = DB::table('wallets')->join('members', 'wallets.member_id', '=', 'members.id')->join('users', 'members.user_id', '=', 'users.id')->select('wallets.id')->where('users.company_id', $companyId)->pluck('id');
        $products = App\Product::joinProductTypes()
            ->where('product_types.company_id', $companyId)
            ->select('products.*')
            ->get();
        $admin_ids = App\User::where('company_id', $companyId)->where('role', App\User::ROLES['ADMIN'])->pluck('id')->all();
        $transactionCount = 3;
        foreach($walletIds as $walletId) {
            $admin_id = $faker->randomElement($admin_ids);
            $order = factory(App\Order::class, 1)
                ->create(['wallet_id' => $walletId, 'admin_id' => $admin_id])
                ->each(function ($o) use ($faker, $products, $transactionCount) {    
                    $i = 0;
                    factory(App\Transaction::class, $transactionCount)
                        ->make(['order_id' => $o->id])
                        ->each(function ($t) use ($faker, $products, &$i) {
                            $p = $products[$i];
                            $t->product_id = $p['id'];
                            $t->qty = $faker->numberBetween(2, 11);
                            $t->save();
                            $i++;
                        });
                });
        }
    }
}
