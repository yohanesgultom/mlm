<?php

use Illuminate\Database\Seeder;

class BankAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $accountName = 'PT GreenVit International';
        DB::table('bank_accounts')->insert([
            [
                'company_id' => $companyId,
                'account_name' => $accountName,
                'bank_id' => DB::table('banks')->where('name', 'BANK BCA')->value('id'),
                'account_no' => '372-345-6785',
            ],
            [
                'company_id' => $companyId,
                'account_name' => $accountName,
                'bank_id' => DB::table('banks')->where('name', 'BANK DANAMON')->value('id'),
                'account_no' => '000-587-4754',
            ],
            [
                'company_id' => $companyId,
                'account_name' => $accountName,
                'bank_id' => DB::table('banks')->where('name', 'BANK PERMATA')->value('id'),
                'account_no' => '070-162-6362',
            ],
            [
                'company_id' => $companyId,
                'bank_id' => DB::table('banks')->where('name', 'BANK MANDIRI')->value('id'),
                'account_no' => '117-000-532-8174',
                'account_name' => $accountName,
            ],
        ]);
    }
}
