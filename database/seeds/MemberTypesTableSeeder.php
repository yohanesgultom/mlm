<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class MemberTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        DB::table('member_types')->insert([
            [
                'company_id' => $companyId,
                'name' => 'BRONZE',
                'order' => 10,
            ],
            [
                'company_id' => $companyId,
                'name' => 'SILVER',
                'order' => 20,
            ],
            [
                'company_id' => $companyId,
                'name' => 'GOLD',
                'order' => 30,
            ],
            [
                'company_id' => $companyId,
                'name' => 'PLATINUM',
                'order' => 40,
            ],            
        ]);
    }
}
