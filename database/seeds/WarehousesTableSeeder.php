<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class WarehousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $countryId = DB::table('countries')->where('name', 'Indonesia')->value('id');
        factory(App\Warehouse::class, 10)->create([
            'company_id' => $companyId,
            'country_id' => $countryId,
        ]);
    }
}
