<?php

use Illuminate\Database\Seeder;

class BonusRulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        DB::table('bonus_rules')->insert([
            [
                'company_id' => $companyId,
                'name' => 'Discount Bonus',
                'type' => '\App\Business\DiscountBonusCalculator',
                'tree_field' => 'sponsor_id',
                'cycle' => 'MONTHLY',
                'variables' => json_encode([
                    [
                        'min_member_type' => 'BRONZE',
                        'min_achievement' => 'D',
                        'personal_bv_pct' => 20,
                    ],
                    [
                        'min_member_type' => 'BRONZE',
                        'min_achievement' => 'SPV',    
                        'personal_bv_pct' => 25,
                    ],
                    [
                        'min_member_type' => 'SILVER',
                        'min_achievement' => 'D',
                        'personal_bv_pct' => 20,
                        'sponsored_bv_pct' => 5,
                    ],
                    [
                        'min_member_type' => 'SILVER',
                        'min_achievement' => 'SPV',
                        'personal_bv_pct' => 25,
                        'sponsored_bv_pct' => 10,
                    ],
                ]),    
            ],
            [
                'company_id' => $companyId,
                'name' => 'First Sales Bonus',
                'type' => '\App\Business\FirstSalesBonusCalculator',
                'tree_field' => 'sponsor_id',
                'cycle' => 'MONTHLY',
                'variables' => json_encode([
                    [
                        'min_member_type' => 'SILVER',
                        'sponsored_member_type' => 'SILVER',
                        'base_bv' => 55000,
                        'bv_pct' => 30,
                        'join_factor' => 0.8,
                        'max_bv' => 1000000,
                    ],
                ]),    
            ],
            [
                'company_id' => $companyId,
                'name' => 'Achievement Share Bonus',
                'type' => '\App\Business\AchievementShareBonusCalculator',
                'tree_field' => 'parent_id',
                'cycle' => 'MONTHLY',
                'variables' => json_encode([
                    ['achievement_type' => 'QM', 'bonuses' => [['bonus_pct' => 7, 'divisors' => ['QM', 'M']], ]],
                    ['achievement_type' => 'M',  'bonuses' => [['bonus_pct' => 7, 'divisors' => ['QM', 'M']], ['bonus_pct' => 7, 'divisors' => ['M', 'SM', 'GM']], ]],
                    ['achievement_type' => 'SM', 'bonuses' => [['bonus_pct' => 7, 'divisors' => ['M', 'SM', 'GM']], ['bonus_pct' => 2, 'divisors' => ['SM', 'GM']], ]],
                    ['achievement_type' => 'GM', 'bonuses' => [['bonus_pct' => 7, 'divisors' => ['M', 'SM', 'GM']], ['bonus_pct' => 2, 'divisors' => ['SM', 'GM']], ['bonus_pct' => 1, 'divisors' => ['GM']], ]],
                    ['achievement_type' => 'DI', 'bonuses' => [['bonus_pct' => 3, 'divisors' => ['DI', 'MD', 'PD']], ]],
                    ['achievement_type' => 'MD', 'bonuses' => [['bonus_pct' => 3, 'divisors' => ['DI', 'MD', 'PD']], ['bonus_pct' => 1, 'divisors' => ['MD', 'PD']], ]],
                    ['achievement_type' => 'PD', 'bonuses' => [['bonus_pct' => 3, 'divisors' => ['DI', 'MD', 'PD']], ['bonus_pct' => 1, 'divisors' => ['MD', 'PD']], ['bonus_pct' => 1, 'divisors' => ['PD']], ]],
                ]),
            ],
            [
                'company_id' => $companyId,
                'name' => 'Leadership Bonus',
                'type' => '\App\Business\LeadershipBonusCalculator',
                'tree_field' => 'parent_id',
                'cycle' => 'MONTHLY',
                'variables' => json_encode([
                    'development' => [
                        ['achievement_type' => 'D', 'bonus_pct' => 0, 'royalty_pct' => 0],
                        ['achievement_type' => 'SPV', 'bonus_pct' => 7, 'royalty_pct' => 2],
                        ['achievement_type' => 'QM', 'bonus_pct' => 12, 'royalty_pct' => 2],
                        ['achievement_type' => 'M', 'bonus_pct' => 18, 'royalty_pct' => 2],
                        ['achievement_type' => 'SM', 'bonus_pct' => 24, 'royalty_pct' => 2],
                        ['achievement_type' => 'GM', 'bonus_pct' => 30, 'royalty_pct' => 2],
                        ['achievement_type' => 'DI', 'bonus_pct' => 37, 'royalty_pct' => 2],
                        ['achievement_type' => 'MD', 'bonus_pct' => 44, 'royalty_pct' => 2],
                        ['achievement_type' => 'PD', 'bonus_pct' => 52, 'royalty_pct' => 2],
                    ],
                    'matching' => [
                        ['achievement_type' => 'D', 'bonus_pct' => []],
                        ['achievement_type' => 'SPV', 'bonus_pct' => []],
                        ['achievement_type' => 'QM', 'bonus_pct' => []],
                        ['achievement_type' => 'M', 'bonus_pct' => [40]],
                        ['achievement_type' => 'SM', 'bonus_pct' => [40, 30]],
                        ['achievement_type' => 'GM', 'bonus_pct' => [40, 30, 20]],
                        ['achievement_type' => 'DI', 'bonus_pct' => [40, 30, 20, 5]],
                        ['achievement_type' => 'MD', 'bonus_pct' => [40, 30, 20, 5, 5]],
                        ['achievement_type' => 'PD', 'bonus_pct' => [40, 30, 20, 5, 5, 5]],
                    ],
                ]),    
            ],
            [
                'company_id' => $companyId,
                'name' => 'Trinary Bonus',
                'type' => '\App\Business\TrinaryBonusCalculator',
                'tree_field' => 'parent_id',
                'cycle' => 'WEEKLY',
                'variables' => json_encode([
                    'member_type_name' => 'GOLD',
                    'max_bonus_pct' => 40,
                    'max_pair_count' => 250,
                    'coef_threshold' => 12,
                    'pairing_bonus' => 50000,
                    'sponsor_bonus' => 100000,
                ]),    
            ],

        ]);
    }
}
