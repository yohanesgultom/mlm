<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Payment;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $memberIds = DB::table('members')->join('users', 'members.user_id', '=', 'users.id')->select('members.id')->where('users.company_id', $companyId)->pluck('id');
        
        $payments = [];
        foreach($memberIds as $memberId) {            
            for ($i = 0; $i < $faker->numberBetween(1, 3); $i++) {
                $status = $faker->randomElement(array_values(Payment::STATUSES));
                if ($status == Payment::STATUSES['PAID']) {
                    $paymentDate = $faker->dateTime();
                } else if ($status == Payment::STATUSES['UNPAID']) {
                    $paymentDate = null;
                }
                array_push($payments, [
                    'type' => $faker->randomElement(array_values(Payment::TYPES)),
                    'desc' => $faker->sentence(3),
                    'amount' => $faker->randomNumber(6),
                    'payment_date' => $paymentDate,
                    'status' => $status,
                    'member_id' => $memberId,
                ]);
            }
        }

        DB::table('payments')->insert($payments);
    }
}
