<?php

use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_id = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $member_types = DB::table('member_types')->where('company_id', $company_id)->get();
        
        // packages
        $packages = [];
        $packages_count = 3;
        foreach ($member_types as $member_type) {
            for ($i = 1; $i <= $packages_count; $i++) {
                $packages[] = [
                    'name' => "{$member_type->name} {$i}",
                    'member_type_id' => $member_type->id,
                    'created_at' => new \DateTime(),
                ];                
            }
        }
        DB::table('packages')->insert($packages);

        // package_product
        $package_ids = DB::table('packages')
            ->join('member_types', 'member_types.id', 'packages.member_type_id')
            ->where('member_types.company_id', $company_id)
            ->pluck('packages.id');
        $products = DB::table('products')
            ->join('product_types', 'product_types.id', 'products.product_type_id')
            ->where('product_types.company_id', $company_id)
            ->select('products.id', 'products.base_price', 'products.point_value', 'products.bonus_value')
            ->get();
        $package_product_array = [];
        $product_count = 2;        
        foreach ($package_ids as $package_id) {
            $total = ['price' => 0, 'point_value' => 0, 'bonus_value' => 0];
            foreach ($products->random($product_count) as $product) {
                $qty = 3; // make it predictable
                // $qty = mt_rand(1, 3);
                $package_product_array[] = [
                    'product_id' => $product->id,
                    'package_id' => $package_id,
                    'qty' => $qty,
                ];
                $total['price'] += $qty * $product->base_price;
                $total['point_value'] += $qty * $product->point_value;
                $total['bonus_value'] += $qty * $product->bonus_value;
            } 
            DB::table('packages')->where('id', $package_id)->update($total);
        }
        DB::table('package_product')->insert($package_product_array);
    }
}
