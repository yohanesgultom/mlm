<?php

use Illuminate\Database\Seeder;

class RegistrationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_id = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $package_ids = DB::table('packages')->inRandomOrder()->pluck('id'); 
        factory(App\Registration::class, 10)
            ->make([
                'company_id' => $company_id,
            ])
            ->each (function ($r) use ($package_ids) {
                $r->package_id = $package_ids->random();
                $r->save();
            });
    }
}
