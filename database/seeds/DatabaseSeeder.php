<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ReligionsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(AchievementsTableSeeder::class);
        $this->call(BankAccountsTableSeeder::class);
        $this->call(MemberTypesTableSeeder::class);
        $this->call(BonusRulesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
