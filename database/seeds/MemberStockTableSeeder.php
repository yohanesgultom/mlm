<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class MemberStockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $memberIds = DB::table('members')->join('users', 'members.user_id', '=', 'users.id')->select('members.id')->where('users.company_id', $companyId)->pluck('id');
        $products = App\Product::joinProductTypes()
            ->where('product_types.company_id', $companyId)
            ->select('products.*')
            ->get();
        
        $stocks = [];
        foreach($memberIds as $memberId) {            
            for ($i = 0; $i < 10; $i++) {
                $p = $products->get($i);
                array_push($stocks, [
                    'exp_date' => $faker->dateTimeBetween('+1 years', '+2 years'),
                    'qty' => $faker->numberBetween(10, 100),
                    'product_id' => $p->id,
                    'member_id' => $memberId,
                ]);
            }
        }

        DB::table('member_stocks')->insert($stocks);
    }
}
