<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Faker\Factory as Faker;

class AdditionalUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $company = App\Company::all()->first();

        // binary member tree
        $numLevel = 3;
        $numBranch = 3;        
        $companyId = $company->id;

        $achievements = App\Achievement::where('company_id', $company->id)        
            ->orderBy('order', 'asc')
            ->take($numLevel)
            ->get();

        $silverMemberType = App\MemberType::where('company_id', $company->id)
            ->where('name', 'SILVER')
            ->first();
            
        $members = new Collection;
        $lastId = 0;
        $regDate = new \DateTime();
        $regDate->sub(new \DateInterval('P'.$numLevel.'D'));
        for ($i = 0; $i < $numLevel; $i++) {
            $regDate->add(new \DateInterval('P'.$i.'D'));
            $achievement = $achievements->pop();
            if ($i == 0) {
                $members = $this->createMembers(
                    $regDate,
                    $companyId,
                    null,
                    $achievement->id,
                    $silverMemberType->id,
                    1,
                    $lastId
                );
            } else {
                $newMembers = new Collection;
                foreach ($members as $m) {                    
                    $parentId = $m->id;
                    $newMembers = $newMembers->concat(
                        $this->createMembers(
                            $regDate,
                            $companyId,
                            $parentId,
                            $achievement->id,
                            $silverMemberType->id,
                            $numBranch,
                            $lastId
                        )
                    );
                }
                $members = $newMembers;
            }
        }

    }

    function createMembers($regDate, $companyId, $parentId, $achievementId, $memberTypeId, $count, &$lastId)
    {                
        $members = factory(App\Member::class, $count)
            ->make()
            ->each(function ($m) use ($regDate, $companyId, $parentId, $achievementId, $memberTypeId, &$lastId) {
                $lastId++;
                $m->id = 'SC'.str_pad((string) $lastId, 5, '0', STR_PAD_LEFT);
                $u = factory(App\User::class, 1)
                    ->states('member')
                    ->create([
                        'username' => 'member'.$m->id,
                        'email' => 'member'.$m->id.'@greenvit.co.id',
                        'company_id' => $companyId
                    ])->first();                
                $m->user_id = $u->id;
                $m->name_printed = $u->name;
                $m->reg_date = $regDate;
                $m->reg_bv = 300000;
                $m->achievement_id = $achievementId;
                $m->current_personal_points = 60; // TODO: calculate from transaction
                $m->current_personal_bonus = 100000; // TODO: calculate from transaction
                if ($parentId != null) {
                    $m->parent_id = $parentId;
                    $m->sponsor_id = $parentId;
                }
                $m->member_type_id = $memberTypeId;
                $m->save();
                // create wallets
                factory(App\Wallet::class, 1)->create(['member_id' => $m->id, 'type' => App\Wallet::TYPES['TOPUP']]);
                factory(App\Wallet::class, 1)->create(['member_id' => $m->id, 'type' => App\Wallet::TYPES['BONUS']]);                
            });
        return $members;
    }    
    
}
