<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\WarehouseStockLog;

class WarehouseStocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $warehouseIds = DB::table('warehouses')->where('company_id', $companyId)->pluck('id');
        $products = App\Product::joinProductTypes()
            ->where('product_types.company_id', $companyId)
            ->select('products.*')
            ->get();
        
        foreach ($products as $p) {
            $exp_date = Carbon::instance($faker->dateTimeBetween('+1 year', '+3 year'))->toDateString();
            foreach($warehouseIds as $warehouseId) {            
                $qty = $faker->numberBetween(500, 1000);
                $available = round($qty - $faker->numberBetween(5, 8) / 10.0);

                // create logs that will automatically create/update stock
                WarehouseStockLog::create([
                    'warehouse_id' => $warehouseId,
                    'product_id' => $p->id,
                    'process_date' => Carbon::now()->subDays(3),
                    'qty_before' => 0,
                    'qty_in' => $qty * 2,
                    'qty_out' => 0,
                    'available_before' => 0,
                    'available_in' => $available * 2,
                    'available_out' => 0,
                    'exp_date' => $exp_date,
                ]);

                WarehouseStockLog::create([
                    'warehouse_id' => $warehouseId,
                    'product_id' => $p->id,
                    'process_date' => Carbon::now()->subDays(1),
                    'qty_before' => $qty * 2,
                    'qty_in' => 0,
                    'qty_out' => $qty,
                    'available_before' => $available * 2,
                    'available_in' => 0,
                    'available_out' => $available,
                    'exp_date' => $exp_date,
                ]);
            }            

        }                    
    }
}
