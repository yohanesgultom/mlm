<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class NetworkCenterTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $data = [];
        for ($i = 0; $i < 10; $i++) {
            array_push($data, [
                'company_id' => $companyId,
                'name' => $faker->sentence(2),
                'fee_percentage' => $faker->randomFloat(1, 1.0, 10.0),
            ]);
        }
        DB::table('network_center_types')->insert($data);
    }
}
