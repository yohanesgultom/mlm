<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DeliveryRelatedTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $countryId = DB::table('countries')->where('name', 'Indonesia')->value('id');

        $memberIds = DB::table('members')->join('users', 'members.user_id', '=', 'users.id')->select('members.id')->where('users.company_id', $companyId)->pluck('id');
        $products = App\Product::joinProductTypes()
            ->where('product_types.company_id', $companyId)
            ->select('products.*')
            ->get();

        foreach($memberIds as $memberId) {
            factory(App\Delivery::class, $faker->numberBetween(1, 5))->create([
                'member_id' => $memberId,
                'country_id' => $countryId,
            ])->each(function ($d) use ($products, $faker) {
                $numItems = $faker->numberBetween(1, 5);
                $selected_products = $faker->randomElements($products->toArray(), $numItems);
                $i = 0;
                factory(App\DeliveryItem::class, $numItems)
                    ->make([ 'delivery_id' => $d->id ])
                    ->each(function ($it) use ($selected_products, &$i) {
                        $it->product_id = $selected_products[$i]['id'];
                        $it->save();
                        $i++;
                    });
            });
        }
    }
}
