<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Deposit;

class DepositTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $companyId = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $bankAccountIds = DB::table('bank_accounts')->select('bank_accounts.id')->where('company_id', $companyId)->pluck('id');
        $memberIds = DB::table('members')->join('users', 'members.user_id', '=', 'users.id')->select('members.id')->where('users.company_id', $companyId)->pluck('id');
        
        $deposits = [];
        foreach($memberIds as $memberId) {            
            for ($i = 0; $i < $faker->numberBetween(1, 3); $i++) {
                array_push($deposits, [
                    'desc' => $faker->sentence(3),
                    'amount' => $faker->randomNumber(6),
                    'deposit_date' => $faker->dateTime(),
                    'status' => $faker->randomElement(array_values(Deposit::STATUSES)),
                    'member_id' => $memberId,
                    'bank_account_id' => $bankAccountIds->random(),
                ]);
            }
        }

        DB::table('deposits')->insert($deposits);
    }
}
