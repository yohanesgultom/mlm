<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function($table) {            
            $table->double('price', 11, 2)->default(0);
            $table->double('point_value', 11, 2)->default(0);
            $table->double('bonus_value', 11, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function($table) {
            $table->dropColumn('price');
            $table->dropColumn('point_value');
            $table->dropColumn('bonus_value');
        });
    }
}
