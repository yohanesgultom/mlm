<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberStockLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_stock_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_name');
            $table->date('expiry_date');
            $table->integer('qty_before');
            $table->integer('qty_change');
            $table->string('customer_name')->nullable();
            $table->integer('transaction_id')->unsigned()->nullable();
            $table->string('member_id', 30);
            $table->foreign('member_id')->references('id')->on('members');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_stock_logs');
    }
}
