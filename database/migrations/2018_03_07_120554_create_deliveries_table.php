<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->double('cost', 11, 2)->nullable();
            $table->date('delivery_date')->nullable();
            $table->string('courier')->nullable();
            $table->string('courier_ref')->nullable();
            $table->string('address')->nullable();
            $table->string('address_zipcode')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_state')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->string('member_id', 30);
            $table->foreign('member_id')->references('id')->on('members');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
