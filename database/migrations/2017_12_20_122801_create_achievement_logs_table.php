<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievementLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievement_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->json('downlines_count')->nullable();
            $table->json('downlines_pv')->nullable();
            $table->date('process_date');
            $table->string('member_id', 30);
            $table->foreign('member_id')->references('id')->on('members');
            $table->timestamps();
            $table->index(['process_date', 'member_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievement_logs');
    }
}
