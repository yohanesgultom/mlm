<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairingCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pairing_counters', function (Blueprint $table) {
            $table->increments('id');
            $table->date('process_date');
            $table->integer('count_left')->unsigned()->default(0);
            $table->integer('count_mid')->unsigned()->default(0);
            $table->integer('count_right')->unsigned()->default(0);
            $table->string('member_id', 30);
            $table->foreign('member_id')->references('id')->on('members');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pairing_counters');
    }
}
