<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransferFromIdInTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('transactions', function (Blueprint $table) {
            $table->string('transfer_from_member_id')->nullable();
            $table->unsignedInteger('transfer_from_id')->nullable();
            $table->foreign('transfer_from_id')->references('id')->on('transactions');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign(['transfer_from_id']);
            $table->dropColumn('transfer_from_member_id');
            $table->dropColumn('transfer_from_id');
        });
        Schema::enableForeignKeyConstraints();
    }
}
