<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip')->nullable();
            $table->string('path')->nullable();
            $table->string('model')->nullable();
            $table->string('action')->nullable();
            $table->string('userrole')->nullable();
            $table->string('username')->nullable();
            $table->json('before')->nullable();
            $table->json('change')->nullable();
            $table->integer('company_id')->unsigned()->nullable();
            $table->timestamps();
            $table->index('username');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_logs');
    }
}
