<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function($table) {
            $table->string('member_id', 30)->nullable()->change();
            $table->json('data')->nullable();
            $table->unsignedInteger('wallet_id')->nullable();
            $table->foreign('wallet_id')->references('id')->on('wallets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function($table) {
            $table->dropForeign('wallet_id');
            $table->dropColumn('wallet_id');
            $table->dropColumn('data');
        });
    }
}
