<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');            
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('package_id');
            $table->unsignedInteger('sponsor_wallet_id')->nullable();
            $table->string('parent_id')->nullable();
            $table->unsignedInteger('order_id')->nullable();
            $table->string('member_id', 30)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->date('completed_date')->nullable();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('package_id')->references('id')->on('packages');
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('sponsor_wallet_id')->references('id')->on('wallets');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
