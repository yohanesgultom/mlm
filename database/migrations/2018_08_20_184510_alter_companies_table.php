<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function($table) {
            $table->dropColumn('next_cutoff_date');
        });

        Schema::table('companies', function($table) {
            $table->string('weekly_cutoff_day_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function($table) {
            $table->dropColumn('weekly_cutoff_day_code');
        });

        Schema::table('companies', function($table) {
            $table->date('next_cutoff_date')->nullable();
        });
    }
}
