<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->string('id', 30)->unique();
            $table->primary('id');
            $table->integer('user_id')->unsigned();
            $table->string('parent_id', 30)->nullable();
            $table->string('sponsor_id', 30)->nullable();
            $table->integer('member_type_id')->unsigned()->nullable();
            $table->integer('network_center_type_id')->unsigned()->nullable();
            $table->string('name_printed')->nullable();
            $table->string('birth_place')->nullable();
            $table->date('birth_date')->nullable();
            $table->date('reg_date')->nullable();
            $table->double('reg_bv', 11, 2)->default(0);
            $table->date('bonus_date')->nullable(); // TODO: removal candidate
            $table->integer('achievement_id')->unsigned()->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->string('official_id', 100)->nullable();
            $table->string('tax_id', 100)->nullable();
            $table->double('current_personal_points', 11, 2)->default(0);
            $table->double('current_personal_bonus', 11, 2)->default(0);
            $table->double('current_group_points', 11, 2)->default(0);
            $table->double('current_group_bonus', 11, 2)->default(0);
            $table->string('address')->nullable();
            $table->string('address_rt', 4)->nullable();
            $table->string('address_rw', 4)->nullable();
            $table->string('address_zipcode', 12)->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_state')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->string('phone_home', 100)->nullable();
            $table->string('phone_office', 100)->nullable();
            $table->string('phone_mobile', 100)->nullable();
            $table->string('phone_fax', 100)->nullable();
            $table->string('beneficiary_name')->nullable();
            $table->string('beneficiary_relation')->nullable();
            $table->string('bank_account_no', 100)->nullable();
            $table->string('bank_account_name', 100)->nullable();
            $table->string('bank_account_city', 100)->nullable();
            $table->string('bank_account_branch', 100)->nullable();
            $table->string('image_path')->nullable();
            $table->integer('bank_id')->unsigned()->nullable();
            $table->tinyInteger('status')->unsigned()->default(0);
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('member_type_id')->references('id')->on('member_types');
            $table->foreign('bank_id')->references('id')->on('banks');
            $table->foreign('achievement_id')->references('id')->on('achievements');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('members')->onDelete('set null');
            $table->foreign('sponsor_id')->references('id')->on('members')->onDelete('set null');
            $table->foreign('network_center_type_id')->references('id')->on('network_center_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
