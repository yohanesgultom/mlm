<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('desc')->nullable();
            $table->unsignedInteger('qty');
            $table->double('amount', 11, 2);
            $table->double('bonus', 11, 2);
            $table->double('points', 11, 2);
            $table->date('process_date')->nullable();
            $table->date('point_date')->nullable();
            $table->date('exp_date')->nullable();
            $table->boolean('transferred')->default(false);
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
