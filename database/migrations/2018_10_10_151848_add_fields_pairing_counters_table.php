<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsPairingCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pairing_counters', function($table) {
            $table->integer('count_eligible')->nullable();
            $table->integer('count_saved')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pairing_counters', function($table) {
            $table->dropColumn('count_eligible');
            $table->dropColumn('count_saved');
        });
    }
}
