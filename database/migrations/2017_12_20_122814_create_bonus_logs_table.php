<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('close_point')->default(true);
            $table->double('pass_up', 11, 2)->default(0);
            $table->double('total', 11, 2)->default(0);
            $table->date('process_date');
            $table->json('details')->nullable();
            $table->json('logs')->nullable();
            $table->dateTime('transfer_date')->nullable();
            $table->string('bank_account_no', 100)->nullable();
            $table->string('bank_account_name', 100)->nullable();
            $table->string('bank_account_city', 100)->nullable();
            $table->string('bank_account_branch', 100)->nullable();
            $table->string('bank_name')->nullable();            
            $table->string('member_id', 30);
            $table->foreign('member_id')->references('id')->on('members');
            $table->timestamps();
            $table->index(['process_date', 'member_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus_logs');
    }
}
