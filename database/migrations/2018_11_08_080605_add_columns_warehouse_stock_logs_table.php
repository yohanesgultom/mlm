<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsWarehouseStockLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouse_stock_logs', function (Blueprint $table) {
            $table->dropColumn('transaction_id');
        });

        Schema::table('warehouse_stock_logs', function (Blueprint $table) {
            $table->integer('available_before')->nullable()->default(0);
            $table->integer('available_in')->nullable()->default(0);
            $table->integer('available_out')->nullable()->default(0);
            $table->unsignedInteger('warehouse_stock_log_id')->nullable();
            $table->unsignedInteger('member_stock_id')->nullable();
            $table->string('note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouse_stock_logs', function (Blueprint $table) {
            $table->unsignedInteger('transaction_id')->nullable();
        });

        Schema::table('warehouse_stock_logs', function (Blueprint $table) {
            $table->dropColumn([
                'available_before',
                'available_in',
                'available_out',
                'warehouse_stock_log_id',
                'member_stock_id',
                'note',
            ]);
        });
    }
}
