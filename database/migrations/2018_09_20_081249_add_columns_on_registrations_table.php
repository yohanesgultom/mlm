<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registrations', function($table) {
            $table->date('birth_date')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->string('official_id', 100)->nullable();
            $table->string('tax_id', 100)->nullable();
            $table->string('address')->nullable();
            $table->string('address_rt', 4)->nullable();
            $table->string('address_rw', 4)->nullable();
            $table->string('address_zipcode', 12)->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_state')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->string('phone_home', 100)->nullable();
            $table->string('phone_office', 100)->nullable();
            $table->string('phone_mobile', 100)->nullable();
            $table->string('phone_fax', 100)->nullable();
            $table->string('bank_account_no', 100)->nullable();
            $table->string('bank_account_name', 100)->nullable();
            $table->string('bank_account_city', 100)->nullable();
            $table->string('bank_account_branch', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registrations', function($table) {
            $table->dropColumn('birth_date');
            $table->dropColumn('gender');
            $table->dropColumn('official_id');
            $table->dropColumn('tax_id');
            $table->dropColumn('address');
            $table->dropColumn('address_rt');
            $table->dropColumn('address_rw');
            $table->dropColumn('address_zipcode');
            $table->dropColumn('address_city');
            $table->dropColumn('address_state');
            $table->dropColumn('country_id');
            $table->dropColumn('phone_home');
            $table->dropColumn('phone_office');
            $table->dropColumn('phone_mobile');
            $table->dropColumn('phone_fax');
            $table->dropColumn('bank_account_no');
            $table->dropColumn('bank_account_name');
            $table->dropColumn('bank_account_city');
            $table->dropColumn('bank_account_branch');
        });
    }
}
