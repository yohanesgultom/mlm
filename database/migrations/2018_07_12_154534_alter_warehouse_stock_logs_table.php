<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWarehouseStockLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouse_stock_logs', function($table) {
            $table->dropColumn([
                'product_name',
                'expiry_date',
                'qty_change',
                'from_warehouse_id',
                'to_warehouse_id',
                'order_id',
            ]);
        });

        Schema::table('warehouse_stock_logs', function($table) {
            $table->dateTime('exp_date')->nullable();
            $table->dateTime('process_date')->nullable();
            $table->integer('qty_in')->default(0);
            $table->integer('qty_out')->default(0);
            $table->unsignedInteger('product_id')->nullable();
            $table->unsignedInteger('warehouse_id')->nullable();
            $table->unsignedInteger('transaction_id')->nullable();
            $table->index('product_id');
            $table->index('warehouse_id');
            $table->index('transaction_id');
        });        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouse_stock_logs', function($table) {
            $table->dropColumn([
                'exp_date',
                'process_date',
                'qty_in',
                'qty_out',
                'product_id',
                'warehouse_id',
            ]);
            $table->string('product_name');
            $table->date('expiry_date');
            $table->integer('qty_change');
            $table->integer('from_warehouse_id')->unsigned()->nullable();
            $table->integer('to_warehouse_id')->unsigned()->nullable();
            $table->integer('order_id')->unsigned()->nullable();
        });        
    }
}
