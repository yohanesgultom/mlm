let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//====jQuery autoloading=====
mix.autoload({
    'jquery': ['$', 'window.jQuery', 'jQuery'],
})
//====set alias for isotope
mix.webpackConfig({
    resolve: {
        alias: {
            'masonry': 'masonry-layout',
            'isotope': 'isotope-layout'
        }
    }
});

mix.js('resources/assets/main.js', 'public/js');
mix.webpackConfig({
    output: {
        chunkFilename: 'js/[name].js',
    },
    resolve: {
        alias: {
	      '@': path.resolve(__dirname, 'resources/assets')
	    }
    }

});

// mix.setResourceRoot('/mlm/public/js');