<?php

namespace Tests;

use Tests\TestCase;
use App\User;
use App\Country;
use App\Company;
use App\Warehouse;
use App\Product;
use App\ProductType;
use App\Tag;
use DB;

abstract class WarehouseTestCase extends TestCase
{
    protected $companies;
    protected $country;
    protected $admin;
    protected $member;
    protected $warehouses;
    protected $otherWarehouses;

    protected function prepare($count = 5, $otherCount = 2)
    {
        $country = Country::create(['name' => 'Indonesia']);
        $companies = factory(Company::class, 2)->create();        
        $this->country = $country;
        $this->companies = $companies;        

        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();

        $this->warehouses = factory(Warehouse::class, $count)->make()->each(function ($w) use ($country, $companies) {
            $w->id = (String) $this->faker->unixTime();
            $w->name = 'Warehouse '.$w->id;
            $w->country_id = $country->id;
            $w->company_id = $companies->get(0)->id;
            $w->save();
        });
        $this->otherWarehouses = factory(Warehouse::class, $otherCount)->make()->each(function ($w) use ($country, $companies) {
            $w->id = (String) $this->faker->unixTime();
            $w->country_id = $country->id;
            $w->company_id = $companies->get(1)->id;
            $w->save();
        });

        // products
        $faker = $this->faker;
        $companyId0 = $this->companies->get(0)->id;
        $companyId1 = $this->companies->get(1)->id;
        $this->tags = factory(Tag::class, 10)->create(['company_id' => $companyId0]);
        $this->types = factory(ProductType::class, 10)->create(['company_id' => $companyId0]);
        $this->otherTags = factory(Tag::class, 10)->create(['company_id' => $companyId1]);
        $this->otherTypes = factory(ProductType::class, 10)->create(['company_id' => $companyId1]);

        $tagIds = $this->tags->pluck('id')->toArray();
        $typeIds = $this->types->pluck('id')->toArray();
        $this->products = factory(Product::class, $count)
            ->make()
            ->each(function ($p) use ($typeIds, $tagIds, $faker) {
                $p->product_type_id = $faker->randomElement($typeIds);
                $p->save();
                $p->tags()->attach($faker->randomElements($tagIds, 3));
            });        

        $tagIds = $this->otherTags->pluck('id')->toArray();
        $typeIds = $this->otherTypes->pluck('id')->toArray();
        $this->otherProducts = factory(Product::class, $otherCount)
            ->make()
            ->each(function ($p) use ($typeIds, $tagIds, $faker) {
                $p->product_type_id = $faker->randomElement($typeIds);
                $p->save();
                $p->tags()->attach($faker->randomElements($tagIds, 3));
            });        

        $stocks = [];
        foreach ($this->warehouses as $warehouse) {
            for ($i = 0; $i < $faker->numberBetween(1, 3); $i++) {
                $product = $this->products->random();
                array_push($stocks, [
                    'qty' => $faker->numberBetween(10, 100),
                    'exp_date' => $faker->dateTime('+3 year')->format('Y-m-d H:i:s'),
                    'product_id' => $product->id,
                    'warehouse_id' => $warehouse->id, 
                ]);
            }
        }
        DB::table('warehouse_stocks')->insert($stocks);        
    }

}
