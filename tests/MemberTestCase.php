<?php

namespace Tests;

use Tests\TestCase;
use App\User;
use App\Member;
use App\Wallet;
use App\Company;
use App\Achievement;
use App\Attachment;
use App\Tag;
use App\Product;
use App\ProductType;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use DB;

abstract class MemberTestCase extends TestCase
{
    protected $companies;
    protected $products;
    protected $otherProducts;
    protected $achievements;
    protected $members;
    protected $otherMembers;
    protected $users;
    protected $otherUsers;
    protected $admin;
    protected $resourceDir;

    protected function prepare($count = 5, $otherCount = 2)
    {
        $faker = $this->faker;
        $this->companies = factory(Company::class, 2)->create();        
        $companyId0 = $this->companies->get(0)->id;
        $companyId1 = $this->companies->get(1)->id;

        $this->companies->get(0)->name = 'GreenVit International';
        $this->companies->get(0)->save();

        // company 0 admin
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $companyId0
        ])->first();
        
        Achievement::insert([
            ['company_id' => $companyId0, 'order' => 10, 'code' => 'D', 'desc' => 'Distributor'],
            ['company_id' => $companyId0, 'order' => 20, 'code' => 'SPV', 'desc' => 'Supervisor'],
            ['company_id' => $companyId0, 'order' => 20, 'code' => 'S', 'desc' => 'Supervisor'],
            ['company_id' => $companyId0, 'order' => 20, 'code' => 'AM', 'desc' => 'Supervisor'],
            ['company_id' => $companyId0, 'order' => 20, 'code' => 'QM', 'desc' => 'Qualified Manager'],
            ['company_id' => $companyId0, 'order' => 30, 'code' => 'M', 'desc' => 'Manager'],
            ['company_id' => $companyId1, 'order' => 40, 'code' => 'SM', 'desc' => 'Senior Manager'],
            ['company_id' => $companyId1, 'order' => 50, 'code' => 'GM', 'desc' => 'General Manager'],
            ['company_id' => $companyId1, 'order' => 60, 'code' => 'DI', 'desc' => 'Director'],
            ['company_id' => $companyId1, 'order' => 70, 'code' => 'MD', 'desc' => 'Managing Director'],
            ['company_id' => $companyId1, 'order' => 80, 'code' => 'PD', 'desc' => 'President Director'],
        ]);

        $achievements = Achievement::where('company_id', $companyId0)->get();
        $this->createMemberBinaryTree($companyId0, $achievements, 3);
        $this->achievements = $achievements;
        $this->members = Member::joinUsers()->where('users.company_id', $companyId0)->get();
        $this->users = User::where('role', User::ROLES['MEMBER'])->where('company_id', $companyId0)->get();

        $otherAchievements = Achievement::where('company_id', $companyId1)->get();
        $this->createMemberBinaryTree($companyId1, $otherAchievements, 3);
        $this->otherAchievements = $otherAchievements;        
        $this->otherMembers = Member::joinUsers()->where('users.company_id', $companyId1)->get();
        $this->otherUsers = User::where('role', User::ROLES['MEMBER'])->where('company_id', $companyId1)->get();
        
        // // company 0 members
        // $users = factory(User::class, $count)->states('member')->create([
        //     'company_id' => $companyId0
        // ]);
        // $i = 0;
        // $achievements = Achievement::where('company_id', $companyId0)->get();
        // $this->users = $users; 
        // $this->achievements = $achievements;
        // $this->members = factory(Member::class, $count)->make()->each(function ($m) use ($users, $achievements, &$i) {
        //     $u = $users->get($i);
        //     $m->user_id = $u->id;
        //     $m->name_printed = $u->name;
        //     $m->achievement_id = $achievements->random()->id;
        //     $m->save();
        //     // create wallets
        //     factory(Wallet::class, 1)->create(['member_id' => $m->id, 'type' => Wallet::TYPES['TOPUP']]);
        //     factory(Wallet::class, 1)->create(['member_id' => $m->id, 'type' => Wallet::TYPES['BONUS']]);
        //     $i++;            
        // });

        // // company 1 members
        // $otherUsers = factory(User::class, $otherCount)->states('member')->create([
        //     'company_id' => $companyId1
        // ]);
        // $i = 0;        
        // $otherAchievements = Achievement::where('company_id', $companyId1)->get();
        // $this->otherUsers = $otherUsers;
        // $this->otherAchievements = $otherAchievements;
        // $this->otherMembers = factory(Member::class, $otherCount)->make()->each(function ($m) use ($otherUsers, $otherAchievements, &$i) {
        //     $u = $otherUsers->get($i);
        //     $m->user_id = $u->id;
        //     $m->name_printed = $u->name;
        //     $m->achievement_id = $otherAchievements->random()->id;
        //     $m->save();
        //     // create wallets
        //     factory(Wallet::class, 1)->create(['member_id' => $m->id, 'type' => Wallet::TYPES['TOPUP']]);
        //     factory(Wallet::class, 1)->create(['member_id' => $m->id, 'type' => Wallet::TYPES['BONUS']]);            
        //     $i++;
        // });

        // attachments
        Storage::fake('test');
        $files = [
            'sim_blangko.png',
            'ktp_blangko.jpg',
            'npwp_blangko.jpg',
        ];
        $resourceDir = base_path('tests'.DIRECTORY_SEPARATOR.'_Resources');
        $this->resourceDir = $resourceDir;
        $attachments = [];
        foreach ($this->members->concat($this->otherMembers) as $m) {
            for ($i = 0; $i < $faker->numberBetween(1, 3); $i++) {
                $type = $faker->randomElement(array_keys(Attachment::TYPES));
                $type_id = Attachment::TYPES[$type];
                $file = new File($resourceDir.DIRECTORY_SEPARATOR.$files[$type_id]);
                $file_ext = $file->getExtension();
                $now = new \DateTime();
                $timestamp = $now->format('YmdHisu');
                $filename = $type.'_'.$m->id.'_'.$timestamp.'.'.$file_ext;
                array_push($attachments, [
                    'member_id' => $m->id,
                    'type' => $type_id,
                    'filename' => $file->getFilename(),
                    'path' => Storage::disk()->putFileAs(Attachment::storage_dir, $file, $filename),
                    'mime' => $file->getMimeType(),
                    'desc' => $type.' '.$faker->sentence(3),
                ]);
            }        
        }
        DB::table('attachments')->insert($attachments);

        // Pairing counter
        $pairingCounters = [];
        foreach ($this->members->concat($this->otherMembers) as $m) {
            for ($i = 0; $i < $faker->numberBetween(1, 3); $i++) {                
                array_push($pairingCounters, [
                    'member_id' => $m->id,
                    'process_date' => $faker->dateTime(),
                    'count_left' => $faker->numberBetween(0, 10),
                    'count_mid' => $faker->numberBetween(0, 10),
                    'count_right' => $faker->numberBetween(0, 10),
                ]);
            }        
        }
        DB::table('pairing_counters')->insert($pairingCounters);

        // products

        $this->tags = factory(Tag::class, 10)->create(['company_id' => $companyId0]);
        $this->types = factory(ProductType::class, 10)->create(['company_id' => $companyId0]);
        $this->otherTags = factory(Tag::class, 10)->create(['company_id' => $companyId1]);
        $this->otherTypes = factory(ProductType::class, 10)->create(['company_id' => $companyId1]);

        $tagIds = $this->tags->pluck('id')->toArray();
        $typeIds = $this->types->pluck('id')->toArray();
        $this->products = factory(Product::class, $count)
            ->make()
            ->each(function ($p) use ($typeIds, $tagIds, $faker) {
                $p->product_type_id = $faker->randomElement($typeIds);
                $p->save();
                $p->tags()->attach($faker->randomElements($tagIds, 3));
            });        

        $tagIds = $this->otherTags->pluck('id')->toArray();
        $typeIds = $this->otherTypes->pluck('id')->toArray();
        $this->otherProducts = factory(Product::class, $otherCount)
            ->make()
            ->each(function ($p) use ($typeIds, $tagIds, $faker) {
                $p->product_type_id = $faker->randomElement($typeIds);
                $p->save();
                $p->tags()->attach($faker->randomElements($tagIds, 3));
            });        

        $stocks = [];
        foreach ($this->members as $member) {
            for ($i = 0; $i < $faker->numberBetween(1, 3); $i++) {
                $product = $this->products->random();
                array_push($stocks, [
                    'qty' => $faker->numberBetween(10, 100),
                    'exp_date' => $faker->dateTime('+3 year')->format('Y-m-d H:i:s'),
                    'product_id' => $product->id,
                    'member_id' => $member->id, 
                ]);
            }
        }
        DB::table('member_stocks')->insert($stocks);
    }

    function createMemberBinaryTree($companyId, $achievements, $numLevel = 3)
    {
        $members = new Collection;
        for ($i = 0; $i < $numLevel; $i++) {            
            if ($i == 0) {
                $members = $this->createMembers($companyId, null, $achievements, 1);
            } else {
                $newMembers = new Collection;
                foreach ($members as $m) {                    
                    $parentId = $m->id;
                    $newMembers = $newMembers->concat($this->createMembers($companyId, $parentId, $achievements, 2));
                }
                $members = $newMembers;
            }
        }        
    }

    function createMembers($companyId, $parentId, $achievements, $count)
    {                
        $faker = $this->faker;
        $members = factory(Member::class, $count)
            ->make()
            ->each(function ($m) use ($faker, $companyId, $parentId, $achievements) {
                $m->id = (string)$faker->unixTime();
                $name = $faker->firstName;
                $username = str_slug($name).'_'.$m->id;
                $email = $username.'@email.com';
                $u = factory(User::class, 1)
                    ->states('member')
                    ->create([
                        'username' => $username,
                        'email' => $email,
                        'company_id' => $companyId
                    ])->first();                
                $m->user_id = $u->id;
                $m->name_printed = $u->name;
                $m->achievement_id = $achievements->random()->id;
                $m->highest_achievement_id = $m->achievement_id;
                if ($parentId != null) {
                    $m->parent_id = $parentId;
                    $m->sponsor_id = $parentId;
                }
                $m->save();
                // create wallets
                factory(Wallet::class, 1)->create(['member_id' => $m->id, 'type' => Wallet::TYPES['TOPUP']]);
                factory(Wallet::class, 1)->create(['member_id' => $m->id, 'type' => Wallet::TYPES['BONUS']]);                
            });
        return $members;
    }
}
