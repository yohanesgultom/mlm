<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;

abstract class APITestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;
    use WithFaker;

    const API_PREFIX = '/api';

    protected function setUp()
    {
        parent::setUp();
        Storage::fake('test');
    }

    public function assertResponse($action, $url, $status, $user = null)
    {
        if (!starts_with(strtolower($url), static::API_PREFIX)) {
            $url = static::API_PREFIX.$url;
        }

        if ($user == null) {
            $response = $this->json($action, $url);
        } else {
            $response = $this->actingAs($user)->json($action, $url);
        }

        if ($response->status() != $status) {            
            $json = $response->json();
            if (array_key_exists('message', $json)) {
                print_r($json['message']."\n");
                print_r($json['trace'][0]['file'].":".$json['trace'][0]['line']."\n");
            } else if (array_key_exists('error', $json)) {
                print_r($json['error']."\n");
            }
        }

        $response->assertStatus($status);

        // return for further testing
        return $response;
    }

}
