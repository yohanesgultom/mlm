<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\BonusRule;
use App\Achievement;
use App\Member;
use App\MemberType;
use App\Transaction;
use App\Business\AchievementShareBonusCalculator;
use DB;

class AchievementShareBonusCalculatorTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        $company_id = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $bonus_rule = BonusRule::where('company_id', $company_id)
            ->where('type', '\App\Business\AchievementShareBonusCalculator')
            ->first();
        $member_types = MemberType::where('company_id', $company_id)->get();
        $achievements = Achievement::where('company_id', $company_id)->get();
        
        $this->variables_list = $bonus_rule->variables;
        $this->member_type_orders = $member_types->mapWithKeys(function ($item) {
            return [$item->name => $item->order];
        });
        $this->achievement_orders = $achievements->mapWithKeys(function ($item) {
            return [$item->code => $item->order];
        });
        $this->date_start = Transaction::orderBy('point_date', 'asc')->pluck('point_date')->first();
        $this->date_end = Transaction::orderBy('point_date', 'desc')->pluck('point_date')->first();
        $this->calculator = new AchievementShareBonusCalculator(
            $company_id,
            $this->variables_list,
            $this->member_type_orders,
            $this->achievement_orders,
            $this->date_start,
            $this->date_end,
            $this->date_start
        );
    }

    public function testGetBonusMap()
    {
        $actual = $this->calculator->getBonusMap();
        $this->assertEquals([
            'total' => 91000.0,
            'details' => ['achievement_share_bonus' => 91000.0],
            'logs' => collect(['achievement_share_bonus ["QM","M"] = 7 / 100 * 1300000.0 / 1 = 91000']),
        ], $actual['QM']);
    }

    public function testGetTotalBonus()
    {
        $total = $this->calculator->getTotalBonus();
        $this->assertEquals(1300000.0, $total);
    }

    public function testCalculate()
    {        
        $member = Member::find('0000001');
        $bonus = $this->calculator->calculate($member);
        $this->assertEquals($bonus['total'], 91000);

        $member = Member::find('0000002');
        $bonus = $this->calculator->calculate($member);
        $this->assertEquals($bonus['total'], 0);
    }
}