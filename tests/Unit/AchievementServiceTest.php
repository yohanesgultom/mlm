<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Business\MemberService;
use App\Business\AchievementService;
use App\Company;
use App\Member;
use App\Transaction;
use App\Achievement;
use App\AchievementLog;

class AchievementServiceTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed', ['--class' => 'CompaniesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'AchievementsTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'MemberTypesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'UsersTableSeeder']);
        $this->company = Company::where('name', 'GreenVit International')->first();
        $this->service = new AchievementService($this->company->id);
    }

    public function testAddArrays()
    {
        $this->assertEquals(
            $this->service->addArrays(['D' => 0, 'SPV' => 0], ['D' => 1, 'SPV' => 1]),
            ['D' => 1, 'SPV' => 1]
        );

        $this->assertEquals(
            $this->service->addArrays(['D' => 1, 'SPV' => 0], ['D' => 2, 'SPV' => 0]),
            ['D' => 3, 'SPV' => 0]
        );

        // should handle array of array
        $this->assertEquals(
            $this->service->addArrays(['D' => [1, 2], 'SPV' => [4]], ['D' => [3], 'SPV' => [5, 6]]),
            ['D' => [1, 2, 3], 'SPV' => [4, 5, 6]]
        );
    }

    public function testFindAchievement()
    {
        $member = Member::find('0000001');

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['D'] = 3;
        $downlines_pv['D'] = [60, 60, 60];
        $member->current_personal_points = 100;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('SPV', $achievement->code);
        
        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $member->current_personal_points = 200;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('SPV', $achievement->code);        

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['M'] = 1;
        $member->current_personal_points = 30;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('D', $achievement->code);

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['SPV'] = 3;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('QM', $achievement->code);

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['SPV'] = 2;
        $downlines_count['QM'] = 1;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('M', $achievement->code);

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['SPV'] = 2;
        $downlines_count['GM'] = 1;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('M', $achievement->code);

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['SPV'] = 1;
        $downlines_count['QM'] = 2;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('SM', $achievement->code);

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['QM'] = 3;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('GM', $achievement->code);

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['GM'] = 3;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('DI', $achievement->code);

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['DI'] = 2;
        $downlines_count['GM'] = 1;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('MD', $achievement->code);

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['DI'] = 3;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('PD', $achievement->code);

        $downlines_count = $this->service->downlines_map;
        $downlines_pv = $this->service->downlines_array_map;
        $downlines_count['GM'] = 1;
        $downlines_pv['GM'] = [30];
        $member->current_personal_points = 30;
        $achievement = $this->service->findAchievement($downlines_count, $downlines_pv, $member);
        $this->assertEquals('D', $achievement->code);         
    }

    public function testUpdateAchievement()
    {
        $process_date = $this->faker->dateTime();                
        
        // level 3 (leaf)
        $member = Member::find('0000005');
        $res = $this->service->updateAchievement($process_date, $member);        
        $downlines_count = $this->service->downlines_map;
        $this->assertEquals($downlines_count, $res['downlines_count']);
        $member->fresh();
        $this->assertEquals(100000, $member->current_personal_bonus);
        $this->assertEquals(60, $member->current_personal_points);
        $this->assertEquals(0, $member->current_group_bonus);
        $this->assertEquals(0, $member->current_group_points);
        $log_count = AchievementLog::where('process_date', $process_date)->count();
        $this->assertEquals(1, $log_count);

        // level 2
        $member = Member::find('0000002');
        $res = $this->service->updateAchievement($process_date, $member);
        $downlines_count = $this->service->downlines_map;
        $downlines_count['D'] = 3;
        $this->assertEquals($downlines_count, $res['downlines_count']);
        $member->fresh();
        $this->assertEquals(100000, $member->current_personal_bonus);
        $this->assertEquals(60, $member->current_personal_points);
        $this->assertEquals(300000, $member->current_group_bonus);
        $this->assertEquals(180, $member->current_group_points);
        $log_count = AchievementLog::where('process_date', $process_date)->count();
        $this->assertEquals(4, $log_count);

        // level 1 (root)
        $member = Member::find('0000001');
        $res = $this->service->updateAchievement($process_date, $member);
        $downlines_count = $this->service->downlines_map;
        $downlines_count['SPV'] = 3;
        $this->assertEquals($downlines_count, $res['downlines_count']);
        $member->fresh();
        $this->assertEquals(100000, $member->current_personal_bonus);
        $this->assertEquals(60, $member->current_personal_points);
        $this->assertEquals(1200000, $member->current_group_bonus);
        $this->assertEquals(720, $member->current_group_points);
        $log_count = AchievementLog::where('process_date', $process_date)->count();
        $this->assertEquals(13, $log_count);

    }

    public function testUpdateAchievements()
    {
        $process_date = $this->faker->dateTime();
        $this->service->updateAchievements($process_date);

        $members = Member::joinUsers()->byCompany($this->company->id)->get();
        $logs = AchievementLog::where('process_date', $process_date)->get();
        $this->assertEquals(count($logs), count($members));

        $log = AchievementLog::where('process_date', $process_date)
            ->where('member_id', '0000001')
            ->first();
        $this->assertEquals($log->code, 'QM');
        $log = AchievementLog::where('process_date', $process_date)
            ->where('member_id', '0000002')
            ->first();
        $this->assertEquals($log->code, 'SPV');
    }

    public function testGetAchievementCount()
    {
        $count_map = $this->service->getAchievementCount();
        $this->assertEquals($count_map, [
            'PD' => 0,
            'MD' => 0,
            'DI' => 0,
            'GM' => 0,
            'SM' => 0,
            'M' => 0,
            'QM' => 1,
            'SPV' => 3,
            'D' => 9,
        ]);
    }
}