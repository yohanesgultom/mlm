<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Company;
use App\Member;
use App\BonusLog;
use App\AchievementLog;
use App\Business\BonusService;
use App\Jobs\CalculateBonus;
use Carbon\Carbon;

class CalculateBonusTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);
        $this->company = Company::where('name', 'GreenVit International')->first();
        $this->date_end = Carbon::now();
        $this->date_start = Carbon::now();
        $this->date_start->subMonth();        
    }

    public function testHandle()
    {
        // do not delete logs as they start empty
        // and delete join does not work in sqlite3
        $job = new CalculateBonus($this->company->id, false, $this->date_start, $this->date_end);
        $job->handle();
        $this->assertCalculateBonusResult($this->company, $job->process_date);
    }

    public function testHandleCompanies()
    {
        // do not delete logs as they start empty
        // and delete join does not work in sqlite3
        $job = new CalculateBonus(null, false, $this->date_start, $this->date_end);
        $job->handle();
        $this->assertCalculateBonusResult($this->company, $job->process_date);
    }

    private function assertCalculateBonusResult($company, $process_date)
    {
        $member = Member::find('0000001');
        $bonus_log = BonusLog::where('member_id', $member->id)
            ->where('process_date', $process_date)
            ->first();
        // TODO: watch for intermittent error
        $this->assertEquals($bonus_log->member_id, $member->id);
        $this->assertTrue($bonus_log->total > 0);
        $sponsored = BonusService::getDirectSponsored($member->id, $process_date);

        $this->assertEquals(3, count($sponsored));
        foreach ($sponsored as $s) {
            $this->assertNotNull($s->bonus_log_id);    
        }
    }
}