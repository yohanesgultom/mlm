<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\BonusRule;
use App\Achievement;
use App\User;
use App\Member;
use App\MemberType;
use App\Transaction;
use App\Business\LeadershipBonusCalculator;
use DB;

class LeadershipBonusCalculatorTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed', ['--class' => 'ReligionsTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'CountriesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'BanksTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'CompaniesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'AchievementsTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'MemberTypesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'BonusRulesTableSeeder']);        
        $company_id = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $bonus_rule = BonusRule::where('company_id', $company_id)
            ->where('type', '\App\Business\LeadershipBonusCalculator')
            ->first();
        $member_types = MemberType::where('company_id', $company_id)->get();
        $achievements = Achievement::where('company_id', $company_id)->get();
        
        $this->achievements = $achievements;
        $this->variables_list = $bonus_rule->variables;
        $this->member_type_orders = $member_types->mapWithKeys(function ($item) {
            return [$item->name => $item->order];
        });
        $this->achievement_orders = $achievements->mapWithKeys(function ($item) {
            return [$item->code => $item->order];
        });
        $this->date_start = Transaction::orderBy('point_date', 'asc')->pluck('point_date')->first();
        $this->date_end = Transaction::orderBy('point_date', 'desc')->pluck('point_date')->first();
        $this->calculator = new LeadershipBonusCalculator(
            $company_id,
            $this->variables_list,
            $this->member_type_orders,
            $this->achievement_orders,
            $this->date_start,
            $this->date_end,
            $this->date_start
        );
    }
    
    private function createMember($id, $current_personal_bonus, $current_group_bonus, $achievement_id, $parent_id = null)
    {
        $user = factory(User::class, 1)->states('member')->create()->first();
        return factory(Member::class, 1)->create([
            'id' => $id,
            'user_id' => $user->id,
            'current_personal_bonus' => $current_personal_bonus,
            'current_group_bonus' => $current_group_bonus,
            'achievement_id' => $achievement_id,
            'parent_id' => $parent_id,
        ])->first();
    }

    public function testCalculateLDB1()
    {        
        $achievement_index = $this->achievements->mapWithKeys(function ($item) {
            return [$item->code => $item->id];
        });

        // higher title
        $member_m1  = $this->createMember('0000001', 20000, 1050000, $achievement_index['DI'], null);
        $member_m2 = $this->createMember('0000002', 50000, 1000000, $achievement_index['GM'], $member_m1->id);

        $downlines = $member_m1->downlines;
        $bonus_logs = collect([]);
        foreach ($downlines as $d) {
            $bonus_log = $this->calculator->calculate($d, null, null);
            $bonus_log['data']['ldb_claimables'][30] = 70000;
            $bonus_logs->push($bonus_log);
        }

        // verify ldb_claimables
        $bonus = $this->calculator->calculate($member_m1, $downlines, $bonus_logs);        
        $this->assertEquals(23400, $bonus['details']['leadership_development_bonus']);
        $this->assertEquals(120000, $bonus['data']['ldb_claimables'][37]);
    }

    public function testCalculateLDB2()
    {        
        $achievement_index = $this->achievements->mapWithKeys(function ($item) {
            return [$item->code => $item->id];
        });

        // equal title
        $member_m1  = $this->createMember('0000001', 20000, 1050000, $achievement_index['M'], null);
        $member_m2 = $this->createMember('0000002', 50000, 1000000, $achievement_index['M'], $member_m1->id);

        $downlines = $member_m1->downlines;
        $bonus_logs = collect([]);
        foreach ($downlines as $d) {
            $bonus_log = $this->calculator->calculate($d, null, null);
            $bonus_log['data']['ldb_claimables'][18] = 70000;
            $bonus_logs->push($bonus_log);
        }

        // verify ldb_claimables
        $bonus = $this->calculator->calculate($member_m1, $downlines, $bonus_logs);        
        $this->assertEquals(10400, $bonus['details']['leadership_development_bonus']);
        $this->assertEquals(120000, $bonus['data']['ldb_claimables'][18]);
    }

    public function testCalculateLDB3()
    {        
        $achievement_index = $this->achievements->mapWithKeys(function ($item) {
            return [$item->code => $item->id];
        });

        // lower title
        $member_m1  = $this->createMember('0000001', 0, 0, $achievement_index['M'], null);
        $member_sm1 = $this->createMember('0000002', 30000, 1000000, $achievement_index['SM'], $member_m1->id);

        $downlines = $member_m1->downlines;
        $bonus_logs = collect([]);
        foreach ($downlines as $d) {
            $bonus_log = $this->calculator->calculate($d, null, null);
            $bonus_log['data']['ldb_claimables'][24] = 50000;
            $bonus_logs->push($bonus_log);
        }

        // verify ldb_claimables
        $bonus = $this->calculator->calculate($member_m1, $downlines, $bonus_logs);
        $this->assertEquals(30000, $bonus['data']['ldb_claimables'][18]);
    }    

    public function testCalculateLMB()
    {        
        $achievement_index = $this->achievements->mapWithKeys(function ($item) {
            return [$item->code => $item->id];
        });

        $member_gm  = $this->createMember('0000001', 0, 0, $achievement_index['GM'], null);
        $member_sm1 = $this->createMember('0000011', 0, 0, $achievement_index['SM'], $member_gm->id);
        $member_sm2 = $this->createMember('0000012', 0, 0, $achievement_index['SM'], $member_gm->id);
        $member_sm3 = $this->createMember('0000013', 0, 0, $achievement_index['SM'], $member_gm->id);
        $member_m1 = $this->createMember('0000021', 0, 0, $achievement_index['M'], $member_sm1->id);
        $member_m2 = $this->createMember('0000022', 0, 0, $achievement_index['M'], $member_sm2->id);
        $member_m3 = $this->createMember('0000023', 0, 0, $achievement_index['M'], $member_m1->id);

        $bonus_logs = collect([
            [
                'details' => ['leadership_development_bonus' => 10000000],
                'data' => ['matching_ldb' => [6000000, 5000000]]],
            [
                'details' => ['leadership_development_bonus' => 8000000],
                'data' => ['matching_ldb' => [4000000]]
            ],
            [
                'details' => ['leadership_development_bonus' => 15000000],
                'data' => ['matching_ldb' => []]
            ],
        ]);

        $downlines = $member_gm->downlines;
        $bonus = $this->calculator->calculate($member_gm, $downlines, $bonus_logs);
        $this->assertEquals(17200000, $bonus['details']['leadership_matching_bonus']);
        $expected_logs = [
            'leadership_matching_bonus G1 = 40 * 0.01 * 33000000 = 13200000',
            'leadership_matching_bonus G2 = 30 * 0.01 * 10000000 = 3000000',
            'leadership_matching_bonus G3 = 20 * 0.01 * 5000000 = 1000000',
        ];
        $this->assertEquals($expected_logs, array_splice($bonus['logs'], 3));
    }
}