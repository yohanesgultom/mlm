<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\BonusRule;
use App\Achievement;
use App\Member;
use App\MemberType;
use App\Transaction;
use App\Business\DiscountBonusCalculator;
use DB;

class DiscountBonusCalculatorTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        $company_id = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $bonus_rule = BonusRule::where('company_id', $company_id)
            ->where('type', '\App\Business\DiscountBonusCalculator')
            ->first();
        $member_types = MemberType::where('company_id', $company_id)->get();
        $achievements = Achievement::where('company_id', $company_id)->get();
        
        $this->variables_list = $bonus_rule->variables;
        $this->member_type_orders = $member_types->mapWithKeys(function ($item) {
            return [$item->name => $item->order];
        });
        $this->achievement_orders = $achievements->mapWithKeys(function ($item) {
            return [$item->code => $item->order];
        });
        $this->date_start = Transaction::orderBy('point_date', 'asc')->pluck('point_date')->first();
        $this->date_end = Transaction::orderBy('point_date', 'desc')->pluck('point_date')->first();
        $this->calculator = new DiscountBonusCalculator(
            $company_id,
            $this->variables_list,
            $this->member_type_orders,
            $this->achievement_orders,
            $this->date_start,
            $this->date_end,
            $this->date_start
        );
    }

    public function testSortVariables()
    {
        $expected = [
            [
                'min_member_type' => 'SILVER',
                'min_achievement' => 'SPV',
                'personal_bv_pct' => 25,
                'sponsored_bv_pct' => 10,
                'sponsored_bv_passup' => true,
            ],
            [
                'min_member_type' => 'SILVER',
                'min_achievement' => 'D',
                'personal_bv_pct' => 20,
                'sponsored_bv_pct' => 5,
                'sponsored_bv_passup' => true,
            ],
            [
                'min_member_type' => 'BRONZE',
                'min_achievement' => 'SPV',    
                'personal_bv_pct' => 25,
            ],
            [
                'min_member_type' => 'BRONZE',
                'min_achievement' => 'D',
                'personal_bv_pct' => 20,
            ],
        ];

        $this->calculator->sortVariables();
        $sorted_list = $this->calculator->variables_list;
        for ($i = 0; $i < count($sorted_list); $i++) {
            $this->assertEquals($sorted_list[$i]['min_member_type'], $expected[$i]['min_member_type']);
            $this->assertEquals($sorted_list[$i]['min_achievement'], $expected[$i]['min_achievement']);
            $this->assertEquals($sorted_list[$i]['personal_bv_pct'], $expected[$i]['personal_bv_pct']);
        }        
    }

    public function testGetMatchedVariables()
    {        
        $member = Member::find('0000001');
        $variables = $this->calculator->getMatchedVariables($member);
        $this->assertEquals($variables['min_member_type'], 'SILVER');
        $this->assertEquals($variables['min_achievement'], 'SPV');
    }

    public function testCalculate()
    {        
        $member = Member::find('0000001');
        $bonus = $this->calculator->calculate($member);
        $this->assertEquals(55000, $bonus['total']);
        $this->assertEquals(55000, $bonus['details']['discount_bonus']);
        $expected_logs = [
            "discount_bonus_personal = 100000.0 * 25 / 100 = 25000",
            "discount_bonus_group = 300000 * 10 / 100 = 30000",
        ];
        $this->assertEquals($expected_logs, $bonus['logs']);

        $member = Member::find('0000005');
        $bonus = $this->calculator->calculate($member);
        $this->assertEquals(20000, $bonus['total']);
        $this->assertEquals(20000, $bonus['details']['discount_bonus']);        
        $expected_logs = [
            "discount_bonus_personal = 100000.0 * 20 / 100 = 20000",
        ];
        $this->assertEquals($expected_logs, $bonus['logs']);
    }
}