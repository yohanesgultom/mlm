<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Business\CommerceService;
use App\Product;
use App\Company;
use App\Warehouse;
use App\WarehouseStock;

class CommerceServiceTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        $this->company = Company::first();
    } 

    public function testGetCentralStockAvailability()
    {
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);
        $product_ids = Product::ofCompany($this->company->id)
            ->inRandomOrder()
            ->take($this->faker->numberBetween(2, 5))
            ->pluck('id')
            ->all();
        sort($product_ids);
        $stock_product_ids = CommerceService::getCentralStockAvailability($product_ids, $this->company->id)
            ->sortBy('product_id')
            ->pluck('product_id')
            ->all();        
        $this->assertEquals(count($product_ids), count($stock_product_ids));
        $this->assertEquals($product_ids, $stock_product_ids);
    }
}
