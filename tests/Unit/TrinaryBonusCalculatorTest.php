<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\BonusRule;
use App\Achievement;
use App\User;
use App\Member;
use App\Wallet;
use App\MemberType;
use App\Transaction;
use App\Registration;
use App\PairingCounter;
use App\Business\MemberService;
use App\Business\TrinaryBonusCalculator;
use DB;
use Carbon\Carbon;

class TrinaryBonusCalculatorTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        // set product base price to fixed price
        // to make test result predictable
        DB::table('products')->update([
            'base_price' => 300000,
            'bonus_value' => 300000,
            'point_value' => 300,
        ]);
        \Artisan::call('db:seed', ['--class' => 'PackagesTableSeeder']);        

        $company_id = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $bonus_rule = BonusRule::where('company_id', $company_id)
            ->where('type', '\App\Business\TrinaryBonusCalculator')
            ->first();
        $member_types = MemberType::where('company_id', $company_id)->get();
        $achievements = Achievement::where('company_id', $company_id)->get();
        
        $this->variables_list = $bonus_rule->variables;
        $this->member_type_orders = $member_types->mapWithKeys(function ($item) {
            return [$item->name => $item->order];
        });
        $this->achievement_orders = $achievements->mapWithKeys(function ($item) {
            return [$item->code => $item->order];
        });

        $this->date_end = Carbon::now()->endOfDay();
        $this->date_start = Carbon::now()->subDays(7)->startOfDay();
        $this->process_date = $this->date_start->copy();

        $member_type = MemberType::with('packages')
            ->where('company_id', $company_id)
            ->where('name', $bonus_rule->variables['member_type_name'])
            ->first();
        $package = $member_type->packages->first();
        $wallets = Wallet::get();
        
        // create registrations members who don't have any downline (new members)
        $members = DB::table('members')->whereNotExists(function ($query) {
            $query->select('m.id')->fromRaw('members m')->whereColumn('m.parent_id', 'members.id');
        })
            ->join('users', 'users.id', 'members.user_id')
            ->select(
                'members.id', 
                'users.name', 
                'users.email', 
                'users.company_id', 
                'members.parent_id',
                'members.sponsor_id'
            )
            ->get();

        foreach ($members as $m) {            
            $wallet_id = $wallets->where('member_id', $m->sponsor_id)->first()['id'];
            Registration::create([
                'company_id' => $m->company_id,
                'name' => $m->name,
                'email' => $m->email,
                'member_id' => $m->id,
                'parent_id' => $m->parent_id,
                'package_id' => $package->id,
                'sponsor_wallet_id' => $wallet_id,
                'completed_date' => $this->date_start,
                'status' => Registration::STATUSES['COMPLETED'],
            ]);
        }

        // update reg date of all leaf members
        DB::table('members')->whereNotExists(function ($query) {
            $query->select('m.id')->fromRaw('members m')->whereColumn('m.parent_id', 'members.id');
        })->update([
            'reg_date' => $this->date_start,
            'member_type_id' => $member_type->id,
        ]);

        // update pairing counters
        $member_service = new MemberService($company_id);
        $member_service->updatePairingCounters(
            $member_type->id,
            $bonus_rule->variables['max_pair_count'],
            $this->date_start,
            $this->date_end,
            $this->process_date
        );

        $this->calculator = new TrinaryBonusCalculator(
            $company_id,
            $this->variables_list,
            $this->member_type_orders,
            $this->achievement_orders,
            $this->date_start,
            $this->date_end,
            $this->process_date
        );
    }
    
    public function testCalculate()
    {
        // empty member
        $user = factory(User::class, 1)->create()->first();
        $member = factory(Member::class, 1)->create([
            'user_id' => $user->id,
            'id' => 'TEST001'
        ])->first();
        $res = $this->calculator->calculate($member);
        $this->assertEquals([
            'logs' => [],    
            'details' => ['trinary_bonus' => 0, 'sponsor_bonus' => 0],
            'pass_up' => 0,
            'data' => [],
            'total' => 0,
        ], $res);

        // member having new sponsored member
        $registration = Registration::where('status', Registration::STATUSES['COMPLETED'])->first();
        $res = $this->calculator->calculate($registration->sponsor_wallet->member);
        $this->assertEquals([
            'member_type = GOLD',
            'total_revenue = 16200000.0',
            'total_sponsor_bonus = 900000',
            'total_pairings = 18',
            'max_bonus = 40 * 0.01 * 16200000.0 - 900000 = 5580000',
            'total_pairing_bonus = 900000',
            'reduce_bonus = false',
            'sponsor_bonus = 300000',
            'trinary_bonus = 3 * 50000 = 150000',
        ], $res['logs']);
        $this->assertEquals(150000, $res['details']['trinary_bonus']);
        $this->assertEquals(300000, $res['details']['sponsor_bonus']);
        $this->assertEquals(450000, $res['total']);        
    }

}