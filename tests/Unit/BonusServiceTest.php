<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Business\BonusService;
use App\BonusRule;
use App\BonusLog;
use App\Member;
use App\Company;
use DB;
use Carbon\Carbon;

class BonusServiceTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        $company = Company::where('name', 'GreenVit International')->first();
        $this->company_id = $company->id;
        // calculate bonus for members
        $date_end = Carbon::now();
        $date_start = Carbon::now();
        $date_start->subMonth();        
        $bonus_rules = BonusRule::where('company_id', $this->company_id)->get();        
        $this->bonus_service = new BonusService($bonus_rules, $date_start, $date_end, $date_start);
    }

    public function testGetDirectSponsored()
    {
        $member = Member::find('0000001');
        $sponsored = BonusService::getDirectSponsored($member->id, $this->bonus_service->process_date);
        $this->assertEquals(3, count($sponsored));

        // check if each sponsor has NO bonus_log generated
        foreach ($sponsored as $s) {
            $this->assertNull($s->bonus_log_id);    
        }
    }

    public function testCalculate()
    {
        $member = Member::find('0000001');
        $bonus_log = $this->bonus_service->calculateBySponsorTree($member);
        $bonus_log = $this->bonus_service->calculateByMemberTree($member);
        $this->assertEquals($bonus_log->member_id, $member->id);
        $this->assertEquals(1112000, $bonus_log->total);
        $sponsored = BonusService::getDirectSponsored($member->id, $this->bonus_service->process_date);

        // check if each sponsor has bonus_log generated
        $this->assertEquals(3, count($sponsored));
        foreach ($sponsored as $s) {
            $this->assertNotNull($s->bonus_log_id);    
        }
    }

    public function testCalculateAll()
    {
        $this->bonus_service->calculateAll();
        $members = Member::joinUsers()->byCompany($this->company_id)->get();
        $logs = BonusLog::byCompany($this->company_id)
            ->where('bonus_logs.process_date', $this->bonus_service->process_date)
            ->get();
        $this->assertEquals(count($logs), count($members));
    }
}