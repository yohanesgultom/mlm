<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\BonusRule;
use App\Achievement;
use App\Member;
use App\MemberType;
use App\Transaction;
use App\Business\MemberService;
use App\Business\FirstSalesBonusCalculator;
use DB;

class FirstSalesBonusCalculatorTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed', ['--class' => 'CompaniesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'AchievementsTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'MemberTypesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'UsersTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'BonusRulesTableSeeder']);

        $company_id = DB::table('companies')->where('name', 'GreenVit International')->value('id');
        $bonus_rule = BonusRule::where('company_id', $company_id)
            ->where('type', '\App\Business\FirstSalesBonusCalculator')
            ->first();
        $member_types = MemberType::where('company_id', $company_id)->get();
        $achievements = Achievement::where('company_id', $company_id)->get();
        
        $this->variables_list = $bonus_rule->variables;
        $this->member_type_orders = $member_types->mapWithKeys(function ($item) {
            return [$item->name => $item->order];
        });
        $this->achievement_orders = $achievements->mapWithKeys(function ($item) {
            return [$item->code => $item->order];
        });
        $this->date_end = new \DateTime();
        $this->date_start = clone $this->date_end;
        $this->date_start->sub(new \DateInterval('P3D'));
        $this->calculator = new FirstSalesBonusCalculator(
            $company_id,
            $this->variables_list,
            $this->member_type_orders,
            $this->achievement_orders,
            $this->date_start,
            $this->date_end,
            $this->date_start
        );
    }

    public function testGetFirstSales()
    {
        $sponsor = Member::find('0000001');
        $sponsoreds = MemberService::getDirectDownlines($sponsor->id, 'sponsor_id');
        $silver_member_type = MemberType::where('name', 'SILVER')->first();
        $silver_count = 0;
        foreach ($sponsoreds as $s) {
            if ($s->member_type_id == $silver_member_type->id) $silver_count++;
        }
        $first_sales = $this->calculator->getFirstSales($sponsor->id, $silver_member_type->name, $this->date_start, $this->date_end);
        $this->assertEquals($silver_count, count($first_sales));
    }
    
    public function testCalculate()
    {
        $member = Member::find('0000001');
        $res = $this->calculator->calculate($member);
        $this->assertEquals(885000, $res['total']);
        $this->assertEquals(['first_sales_bonus' => 885000], $res['details']);
        $expected_logs = [
            "first_sales_bonus 0000002 = 300000.0 * 0.8 + 55000 = 295000",
            "first_sales_bonus 0000003 = 300000.0 * 0.8 + 55000 = 295000",
            "first_sales_bonus 0000004 = 300000.0 * 0.8 + 55000 = 295000",
        ];
        $this->assertEquals($expected_logs, $res['logs']);

        $member = Member::find('0000002');
        $res = $this->calculator->calculate($member);
        $this->assertEquals(885000, $res['total']);
        $this->assertEquals(['first_sales_bonus' => 885000], $res['details']);
        $expected_logs = [
            "first_sales_bonus 0000005 = 300000.0 * 0.8 + 55000 = 295000",
            "first_sales_bonus 0000006 = 300000.0 * 0.8 + 55000 = 295000",
            "first_sales_bonus 0000007 = 300000.0 * 0.8 + 55000 = 295000",
        ];
        $this->assertEquals($expected_logs, $res['logs']);
    }
}