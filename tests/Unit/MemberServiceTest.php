<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Business\MemberService;
use App\Company;
use App\Member;
use App\MemberType;
use App\Transaction;
use App\BonusRule;
use App\PairingCounter;
use DB;

class MemberServiceTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');        
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);
        
        $this->company = Company::where('name', 'GreenVit International')->first();
        $this->service = new MemberService($this->company->id);
        $this->date_start = Transaction::whereNotNull('point_date')->orderBy('point_date', 'asc')->pluck('point_date')->first();
        $this->date_end = Transaction::whereNotNull('point_date')->orderBy('point_date', 'desc')->pluck('point_date')->first();
    }

    public function testUpdateCurrentPointAndBonusValues()
    {
        $expected_sums = $this->service->getTransactionsSum($this->date_start, $this->date_end);
        $this->service->updateCurrentPointAndBonusValues($this->date_start, $this->date_end);
        foreach ($expected_sums as $expected) {
            $actual = DB::table('members')
                ->where('id', $expected->member_id)
                ->select('current_personal_points', 'current_personal_bonus')
                ->first();
            $this->assertEquals($actual->current_personal_bonus, $expected->bonus_sum);
            $this->assertEquals($actual->current_personal_points, $expected->point_sum);            
        }
    }

    public function testUpdatePairingCounters()
    {
        $member_type = MemberType::where('name', 'GOLD')->first();        
        $bonus_rule = BonusRule::where('type', '\App\Business\TrinaryBonusCalculator')->first();
        // change every member to GOLD 
        Member::whereNotNull('member_type_id')->update(['member_type_id' => $member_type->id]);
        $date_start = Member::orderBy('reg_date', 'asc')->select('reg_date')->first()->reg_date->startOfDay();
        $date_end = Member::orderBy('reg_date', 'desc')->select('reg_date')->first()->reg_date->endOfDay();
        $this->service->updatePairingCounters(
            $member_type->id, 
            $bonus_rule->variables['max_pair_count'],
            $date_start, 
            $date_end,
            $date_end
        );
        $pairing_counters = PairingCounter::where('process_date', $date_end)->get();
        $member_count = Member::count();
        $this->assertEquals($member_count, count($pairing_counters));
        $pc = $pairing_counters->where('member_id', '0000001')->first();
        $this->assertEquals($pc->count_left, 4);
        $this->assertEquals($pc->count_mid, 4);
        $this->assertEquals($pc->count_right, 4);
        $this->assertEquals($pc->count_eligible, 12);
        $pc = $pairing_counters->where('member_id', '0000003')->first();
        $this->assertEquals($pc->count_left, 1);
        $this->assertEquals($pc->count_mid, 1);
        $this->assertEquals($pc->count_right, 1);
        $this->assertEquals($pc->count_eligible, 3);
    }

    public function testGetDirectDownlines()
    {
        $downlines = MemberService::getDirectDownlines('0000001', 'parent_id');
        $ids = array_map(function ($m) { return $m->id; }, $downlines);
        $this->assertEquals(['0000002', '0000003', '0000004'], $ids);
        $this->assertTrue(array_key_exists('reg_date', $downlines[0]));
        $this->assertTrue(array_key_exists('current_personal_points', $downlines[0]));
        $this->assertTrue(array_key_exists('current_personal_bonus', $downlines[0]));
        $this->assertTrue(array_key_exists('current_group_points', $downlines[0]));
        $this->assertTrue(array_key_exists('current_group_bonus', $downlines[0]));
        $this->assertTrue(array_key_exists('achievement_code', $downlines[0]));
        $this->assertTrue(array_key_exists('achievement', $downlines[0]));
        $this->assertTrue(array_key_exists('image_path', $downlines[0]));
        $this->assertTrue(array_key_exists('name', $downlines[0]));
    }
}