<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Delivery;
use App\DeliveryItem;
use App\Member;
use App\Product;
use App\ProductType;
use App\Company;
use App\Country;
use App\Warehouse;
use App\WarehouseStockLog;
use DB;
use Carbon\Carbon;

class DeliveryTest extends TestCase
{   
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'MemberStockTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'DeliveryRelatedTablesSeeder']);
        $this->company = Company::first();
        $this->admin = $this->company->users()->where('role', User::ROLES['ADMIN'])->first();
        $this->user = $this->company->users()->where('role', User::ROLES['MEMBER'])->first();        
        $this->countryId = Country::where('name', 'Indonesia')->first()->id;
    } 

    public function testAdminIndex()
    {
        $deliveries = Delivery::join('members', 'members.id', 'deliveries.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->select('deliveries.*')
            ->get();
        $actual = $this->assertResponse('GET', '/deliveries', 200, $this->admin)->json();
        $this->assertEquals(count($deliveries), $actual['total']);

        // test filter by delivery_date
        $delivery = Delivery::where('member_id', $this->user->member->id)
            ->inRandomOrder()
            ->orderBy('delivery_date', 'asc')
            ->select('delivery_date')
            ->first();            
        $delivery_date_start = Carbon::instance($delivery->delivery_date)->toDateString();
        $delivery_date_end = Carbon::instance($delivery->delivery_date)->addYears(5)->toDateString();
        $data = [
            'delivery_date_start' => $delivery_date_start,
            'delivery_date_end' => $delivery_date_end,
        ];        
        $deliveries = Delivery::join('members', 'members.id', 'deliveries.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->whereBetween('deliveries.delivery_date', [$delivery_date_start, $delivery_date_end])
            ->select('deliveries.*')
            ->get();
        $response = $this->actingAs($this->admin)->json('GET', '/deliveries', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($deliveries), $actual['total']);
    }

    public function testAdminStore()
    {
        $faker = $this->faker;
        $m = $this->user->member;
        $stocks = $m->stocks;

        // create new delivery object
        $delivery = factory(Delivery::class, 1)
            ->make([
                'member_id' => $m->id,
                'country_id' => $this->countryId,
                'status' => Delivery::STATUSES['DELIVERED'],
            ])
            ->first();
        
        $central_warehouse = Warehouse::ofCompany($this->company->id)->central()->first();
        $data = $delivery->toArray();
        $data['items'] = [];
        foreach ($stocks as $stock) {
            $item = DeliveryItem::make([
                'delivery_id' => $delivery->id,
                'product_id' => $stock->product_id,
                'qty' => $stock->qty,
            ]);
            $data['items'][] = $item->toArray();
            // create corresponding warehouse stock
            $available = $stock->qty; // make sure there is half available left
            WarehouseStockLog::create([
                'warehouse_id' => $central_warehouse->id,
                'product_id' => $item->product_id,
                'process_date' => Carbon::now(),
                'qty_before' => 0,
                'qty_in' => $stock->qty + $available,
                'qty_out' => 0,
                'available_before' => 0,
                'available_in' => $available,
                'available_out' => 0,
                'exp_date' => $stock->exp_date,
            ]);
        }

        // get initial stock
        $init_warehouse_stocks = $central_warehouse->stocks;

        $response = $this->actingAs($this->admin)->json('POST', '/deliveries', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['courier'], $actual['courier']);
        $this->assertEquals($data['courier_ref'], $actual['courier_ref']);
        $this->assertEquals($data['delivery_date'], $actual['delivery_date']);
        $this->assertEquals($data['cost'], $actual['cost']);
        $this->assertEquals($data['address'], $actual['address']);
        $this->assertEquals($data['address_zipcode'], $actual['address_zipcode']);
        $this->assertEquals($data['address_city'], $actual['address_city']);
        $this->assertEquals($data['address_state'], $actual['address_state']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Delivery::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals($data['country_id'], $actual['country']['id']);
        $this->assertEquals($m->id, $actual['member_id']);
        $this->assertEquals(count($data['items']), count($actual['items']));
        foreach ($data['items'] as $ite) {
            $found = false;
            foreach ($actual['items'] as $ita) {
                if ($ite['product_id'] == $ita['product_id']) {
                    $this->assertEquals($ite['qty'], $ita['qty']);
                    $this->assertEquals($ite['product_id'], $ita['product_id']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }

        // validate warehouse stocks
        $central_warehouse->refresh();
        $latest_warehouse_stocks = $central_warehouse->stocks;
        foreach ($actual['items'] as $item) {
            $initial_stock = $init_warehouse_stocks->where('product_id', $item['product_id'])->first();
            $latest_stock = $latest_warehouse_stocks->where('id', $initial_stock->id)->first();
            $this->assertEquals($initial_stock->qty, $item['qty'] + $latest_stock->qty);           
        }        

    }

    public function testAdminShow()
    {
        $delivery = Delivery::join('members', 'members.id', 'deliveries.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->select('deliveries.*')
            ->first();
        $actual = $this->assertResponse('GET', '/deliveries/'.$delivery->id, 200, $this->admin)->json();
        $this->assertEquals($delivery->id, $actual['id']);
        $this->assertEquals($delivery->courier, $actual['courier']);
        $this->assertEquals($delivery->courier_ref, $actual['courier_ref']);
        $this->assertEquals($delivery->delivery_date->format('Y-m-d H:i:s'), $actual['delivery_date']);
        $this->assertEquals($delivery->address, $actual['address']);
        $this->assertEquals($delivery->address_zipcode, $actual['address_zipcode']);
        $this->assertEquals($delivery->address_city, $actual['address_city']);
        $this->assertEquals($delivery->address_state, $actual['address_state']);
        $this->assertEquals($delivery->country_id, $actual['country']['id']);
        $this->assertEquals($delivery->member_id, $actual['member_id']);
        $this->assertEquals($delivery->status, $actual['status']);
        $this->assertEquals($delivery->status_name, $actual['status_name']);
        $this->assertEquals(count($delivery->items), count($actual['items']));
    }
 
    public function testAdminUpdate()
    {
        $faker = $this->faker;
        $m = $this->user->member;

        // create valid delivery & items in database
        $delivery = factory(Delivery::class, 1)->create([
            'member_id' => $m->id,
            'country_id' => $this->countryId,
            'status' => Delivery::STATUSES['UNPAID'],
        ])->first();
        $stocks = $m->stocks;
        $stock_for_update = $stocks->pop();
        foreach ($stocks as $id => $stock) {
            $item = DeliveryItem::create([
                'delivery_id' => $delivery->id,
                'product_id' => $stock->product_id,
                'qty' => $stock->qty,
            ])->first();
        }

        // update details
        $delivery->address = $faker->streetAddress;
        $delivery->address_zipcode = $faker->postcode;

        // add new item
        $items = factory(DeliveryItem::class, 1)
            ->make([
                'delivery_id' => $delivery->id,
                'product_id' => $stock_for_update->product_id,
                'qty' => $stock_for_update->qty,
            ]);

        // update 1 transaction (cancel the rest)
        $it = $delivery->items()->get()->random();
        $it->qty = $it->qty - 1;
        $items->push($it);        

        $data = $delivery->toArray();
        $data['items'] = $items->toArray();

        for ($i = 0; $i < count($data['items']); $i++) {
            unset($data['items'][$i]['product']);
        }

        $response = $this->actingAs($this->admin)->json('PUT', '/deliveries/'.$delivery->id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($delivery->id, $actual['id']);
        $this->assertEquals($data['courier'], $actual['courier']);
        $this->assertEquals($data['courier_ref'], $actual['courier_ref']);
        $this->assertEquals($data['delivery_date'], $actual['delivery_date']);
        $this->assertEquals($data['address'], $actual['address']);
        $this->assertEquals($data['address_zipcode'], $actual['address_zipcode']);
        $this->assertEquals($data['address_city'], $actual['address_city']);
        $this->assertEquals($data['address_state'], $actual['address_state']);
        $this->assertEquals($data['country_id'], $actual['country']['id']);
        $this->assertEquals($data['member_id'], $actual['member_id']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Delivery::STATUSES)[$data['status']], $actual['status_name']);

        $this->assertEquals(count($data['items']), count($actual['items']));
        foreach ($data['items'] as $ite) {
            $found = false;
            foreach ($actual['items'] as $ita) {
                if ($ite['product_id'] == $ita['product_id']) {
                    $this->assertEquals($ite['qty'], $ita['qty']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }
    }

    public function testAdminDelete()
    {
        $delivery = Delivery::join('members', 'members.id', 'deliveries.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->select('deliveries.*')
            ->first();
        $actual = $this->assertResponse('DELETE', '/deliveries/'.$delivery->id, 200, $this->admin)->json();
        $this->assertEquals($delivery->id, $actual['id']);
        $this->assertEquals($delivery->courier, $actual['courier']);
        $this->assertEquals($delivery->courier_ref, $actual['courier_ref']);
        $this->assertEquals($delivery->delivery_date->format('Y-m-d H:i:s'), $actual['delivery_date']);
        $this->assertEquals($delivery->address, $actual['address']);
        $this->assertEquals($delivery->address_zipcode, $actual['address_zipcode']);
        $this->assertEquals($delivery->address_city, $actual['address_city']);
        $this->assertEquals($delivery->address_state, $actual['address_state']);
        $this->assertEquals($delivery->country_id, $actual['country']['id']);
        $this->assertEquals($delivery->member_id, $actual['member_id']);
        $this->assertEquals($delivery->status, $actual['status']);
        $this->assertEquals($delivery->status_name, $actual['status_name']);
   
        $actual = $this->assertResponse('GET', '/deliveries/'.$delivery->id, 404, $this->admin)->json();
    }    

    // public function testDeliveryCompletion()
    // {
    //     \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
    //     \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);


    // }
}
