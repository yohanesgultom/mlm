<?php

namespace Tests\Feature;

use Tests\WarehouseTestCase;
use App\Warehouse;
use App\WarehouseStock;
use App\Company;
use App\User;
use Carbon\Carbon;

class WarehouseTest extends WarehouseTestCase
{    
    
    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/warehouses', 200, $this->admin)->json();
        $this->assertEquals(count($this->warehouses), count($actual['data']));
    }

    public function testStatuses()
    {
        $actual = $this->assertResponse('GET', '/warehouses/types', 200)->json();
        $this->assertEquals(array_flip(Warehouse::TYPES), $actual);
    }

    public function testAdminIndexSearch()
    {
        $this->prepare();
        $searchName = 'e';
        $expected = Warehouse::where('company_id', $this->admin->company_id)
            ->where('name', 'LIKE', '%'.$searchName.'%')
            ->get();
        $data = ['name' => $searchName];
        $response = $this->actingAs($this->admin)->json('GET', '/warehouses', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);
    }

    public function testAdminStore()
    {
        $this->prepare();
        $data = factory(Warehouse::class, 1)->make([
            'id' => (String) $this->faker->unixTime(),
            'name' => $this->faker->sentence(),
            'country_id' => $this->country->id,
        ])->first()->toArray();
        $response = $this->actingAs($this->admin)->json('POST', '/warehouses', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['id'], $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['address'], $actual['address']);
        $this->assertEquals($data['address_city'], $actual['address_city']);
        $this->assertEquals($data['address_state'], $actual['address_state']);
        $this->assertEquals($data['address_zipcode'], $actual['address_zipcode']);
        $this->assertEquals($data['country_id'], $actual['country_id']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }


    public function testAdminShow()
    {
        $this->prepare();
        $data = $this->warehouses->first();
        $actual = $this->assertResponse('GET', '/warehouses/'.$data->id, 200, $this->admin)->json();
        $this->assertEquals($data->id, $actual['id']);
        $this->assertEquals($data->name, $actual['name']);
        $this->assertEquals($data->address, $actual['address']);
        $this->assertEquals($data->address_city, $actual['address_city']);
        $this->assertEquals($data->address_state, $actual['address_state']);
        $this->assertEquals($data->address_zipcode, $actual['address_zipcode']);
        $this->assertEquals($data->country_id, $actual['country_id']);
        $this->assertEquals($data->company_id, $actual['company_id']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $existing = $this->warehouses->first();
        $id = $existing->id;

        // update data
        $data = factory(Warehouse::class, 1)->make([
            'name' => $this->faker->sentence(),
            'country_id' => $this->country->id,
        ])->first()->toArray();
        $response = $this->actingAs($this->admin)->json('PUT', 'warehouses/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['address'], $actual['address']);
        $this->assertEquals($data['address_city'], $actual['address_city']);
        $this->assertEquals($data['address_state'], $actual['address_state']);
        $this->assertEquals($data['address_zipcode'], $actual['address_zipcode']);
        $this->assertEquals($data['country_id'], $actual['country_id']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $existing = $this->warehouses->first();
        $id = $existing->id;
        $actual = $this->assertResponse('DELETE', '/warehouses/'.$id, 200, $this->admin)->json();
        $this->assertEquals($existing->name, $actual['name']);
        $this->assertEquals($existing->address, $actual['address']);
        $this->assertEquals($existing->address_city, $actual['address_city']);
        $this->assertEquals($existing->address_state, $actual['address_state']);
        $this->assertEquals($existing->address_zipcode, $actual['address_zipcode']);
        $this->assertEquals($existing->country_id, $actual['country_id']);
        $this->assertEquals($existing->company_id, $actual['company_id']);
        $actual = $this->assertResponse('GET', '/warehouses/'.$id, 404, $this->admin)->json();
    }
    
    /**
     * As a member I want to see warehouses of my company
     */    
    public function testMemberIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/warehouses', 200, $this->member)->json();
        $this->assertEquals(count($this->warehouses), count($actual['data']));
    }

    public function testTransfer()
    {
        // TODO: move to setUp()
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);
        $this->company = Company::first();
        $this->admin = $this->company->users()->where('role', User::ROLES['ADMIN'])->first();

        $warehouse =  $this->company->warehouses()->inRandomOrder()->first();
        $stock = $warehouse->stocks()->inRandomOrder()->first();    

        $dest = $this->company->warehouses()->where('warehouses.id', '!=', $warehouse->id)->inRandomOrder()->first();
        $dest_stock = $dest->stocks()
            ->where('product_id', $stock->product_id)
            ->where('exp_date', $stock->exp_date)
            ->first();            

        $data = [
            'warehouse_id' => $dest->id,
            'product_id' => $stock->product_id,
            'exp_date' => $stock->exp_date->toDateTimeString(),
            'qty' => $stock->available, // take all available
        ];
        $response = $this->actingAs($this->admin)->json('POST', "/warehouses/{$warehouse->id}/transfer", $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();

        // check updated stock
        $updated_stock =$warehouse->stocks()
            ->where('product_id', $data['product_id'])
            ->where('exp_date', Carbon::parse($data['exp_date']))
            ->first();
        $this->assertEquals(0, $updated_stock->available);
        $this->assertEquals($stock->qty - $data['qty'], $updated_stock->qty);

        $updated_dest_stock = $dest->stocks()
            ->where('product_id', $data['product_id'])
            ->where('exp_date', Carbon::parse($data['exp_date']))
            ->first();
        $this->assertEquals($dest_stock->available + $data['qty'], $updated_dest_stock->available);
        $this->assertEquals($dest_stock->qty + $data['qty'], $updated_dest_stock->qty);
    }    
}
