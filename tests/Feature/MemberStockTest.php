<?php

namespace Tests\Feature;

use Tests\MemberTestCase;
use App\MemberStock;

class MemberStockTest extends MemberTestCase
{

    public function testAdminMemberStocks()
    {
        $this->prepare();
        $member = $this->members->random();
        $stocks = $member->stocks;
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/stocks', 200, $this->admin)->json();
        $this->assertEquals(count($stocks), count($actual['data']));
        for($i = 0; $i < count($stocks); $i++) {
            $this->assertEquals($stocks->get($i)->id, $actual['data'][$i]['id']);
            $this->assertEquals($stocks->get($i)->qty, $actual['data'][$i]['qty']);
            $this->assertEquals($stocks->get($i)->exp_date->format('Y-m-d H:i:s'), $actual['data'][$i]['exp_date']);
            $this->assertEquals($stocks->get($i)->product_id, $actual['data'][$i]['product']['id']);
            $this->assertEquals($stocks->get($i)->member_id, $actual['data'][$i]['member_id']);
        }
    }

    public function testMemberMemberStocks()
    {
        $this->prepare();
        $user = $this->users->get(0);
        $member = $user->member;
        $stocks = $member->stocks;
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/stocks', 200, $user)->json();
        $this->assertEquals(count($stocks), count($actual['data']));
        for($i = 0; $i < count($stocks); $i++) {
            $this->assertEquals($stocks->get($i)->id, $actual['data'][$i]['id']);
            $this->assertEquals($stocks->get($i)->qty, $actual['data'][$i]['qty']);
            $this->assertEquals($stocks->get($i)->exp_date->format('Y-m-d H:i:s'), $actual['data'][$i]['exp_date']);
            $this->assertEquals($stocks->get($i)->product_id, $actual['data'][$i]['product']['id']);
            $this->assertEquals($stocks->get($i)->member_id, $actual['data'][$i]['member_id']);
        }

        // must not be able to see other member's stocks
        $otherUser = $this->users->get(1);
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/stocks', 403, $otherUser)->json();
    }

    public function testAdminMemberStockShow()
    {
        $this->prepare();
        $member = $this->members->random();
        $stock = $member->stocks->random();
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/stocks/'.$stock->id, 200, $this->admin)->json();
        $this->assertEquals($stock->id, $actual['id']);
        $this->assertEquals($stock->qty, $actual['qty']);
        $this->assertEquals($stock->exp_date->format('Y-m-d H:i:s'), $actual['exp_date']);
        $this->assertEquals($stock->product_id, $actual['product']['id']);
        $this->assertEquals($stock->member_id, $actual['member_id']);
    }

    public function testMemberMemberStockShow()
    {
        $this->prepare();
        $member = $this->members->random();
        $user = $member->user;
        $stock = $member->stocks->random();
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/stocks/'.$stock->id, 200, $user)->json();
        $this->assertEquals($stock->id, $actual['id']);
        $this->assertEquals($stock->qty, $actual['qty']);
        $this->assertEquals($stock->exp_date->format('Y-m-d H:i:s'), $actual['exp_date']);
        $this->assertEquals($stock->product_id, $actual['product']['id']);
        $this->assertEquals($stock->member_id, $actual['member_id']);
    }

    public function testAdminMemberStockStore()
    {
        $this->prepare();
        $member = $this->members->random();
        $product = $this->products->random();
        $faker = $this->faker;
        $data = [
            'qty' => $faker->numberBetween(10, 100),
            'exp_date' => $faker->dateTime('+3 year')->format('Y-m-d H:i:s'),
            'product_id' => $product->id,
            'member_id' => $member->id,            
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/members/'.$member->id.'/stocks', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['qty'], $actual['qty']);
        $this->assertEquals($data['exp_date'], $actual['exp_date']);
        $this->assertEquals($data['product_id'], $actual['product_id']);
        $this->assertEquals($data['member_id'], $actual['member_id']);

        // member should not be able to create stock
        $data = [
            'qty' => $faker->numberBetween(10, 100),
            'exp_date' => $faker->dateTime('+3 year')->format('Y-m-d H:i:s'),
            'product_id' => $product->id,
            'member_id' => $member->id,  
        ];
        $response = $this->actingAs($member->user)->json('POST', '/members/'.$member->id.'/stocks', $data);
        $response->assertStatus(403);
    }
    
    public function testAdminMemberStockUpdate()
    {
        $this->prepare();
        $member = $this->members->random();
        $stock = $member->stocks->random();

        // update data
        $product = $this->products->random();
        $faker = $this->faker;
        $data = [
            'qty' => $faker->numberBetween(10, 100),
            'exp_date' => $faker->dateTime('+3 year')->format('Y-m-d H:i:s'),
            'product_id' => $product->id,
            'member_id' => $member->id,            
        ];
        $response = $this->actingAs($this->admin)->json('PUT', '/members/'.$member->id.'/stocks/'.$stock->id, $data);        
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($stock->id, $actual['id']);
        $this->assertEquals($data['qty'], $actual['qty']);
        $this->assertEquals($data['exp_date'], $actual['exp_date']);
        $this->assertEquals($data['product_id'], $actual['product_id']);
        $this->assertEquals($data['member_id'], $actual['member_id']);
        
    }    
    
    public function testAdminMemberStockDelete()
    {
        $this->prepare();
        $member = $this->members->random();
        $stock = $member->stocks->random();

        $actual = $this->assertResponse('DELETE', '/members/'.$member->id.'/stocks/'.$stock->id, 200, $this->admin)->json();
        $this->assertEquals($stock->id, $actual['id']);
        $this->assertEquals($stock->qty, $actual['qty']);
        $this->assertEquals($stock->exp_date->format('Y-m-d H:i:s'), $actual['exp_date']);
        $this->assertEquals($stock->product_id, $actual['product_id']);
        $this->assertEquals($stock->member_id, $actual['member_id']);

        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/stocks/'.$stock->id, 404, $this->admin)->json();
    }

}
