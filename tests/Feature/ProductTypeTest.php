<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use App\User;
use App\Company;
use App\ProductType;

class ProductTypeTest extends TestCase
{
    private $companies;
    private $admin;
    private $member;
    private $data;
    private $otherData;

    public function prepare($count = 5, $otherCount = 2)
    {
        Storage::fake('public');
        $this->companies = factory(Company::class, 2)->create();
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        
        $this->data = factory(ProductType::class, 10)->create(['company_id' => $this->companies->get(0)->id]);
        $this->otherData = factory(ProductType::class, 10)->create(['company_id' => $this->companies->get(1)->id]);        
    }
    
    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/productTypes', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }


    public function testAdminStore()
    {
        $this->prepare();
        $data = [
            'name' => $this->faker->word,
            'image_path' => UploadedFile::fake()->image('random.jpg'),
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/productTypes', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
        $this->assertTrue(starts_with($actual['image_path'], '/storage/productType/image_path/'));
        Storage::disk('public')->assertExists(str_after($actual['image_path'], '/storage'));
    }


    public function testAdminShow()
    {
        $this->prepare();
        $data = $this->data->first();
        $actual = $this->assertResponse('GET', '/productTypes/'.$data->id, 200, $this->admin)->json();
        $this->assertEquals($data->name, $actual['name']);
        $this->assertEquals($data->company_id, $actual['company_id']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $existing = $this->data->first();
        $id = $existing->id;

        // update data
        $data = [
            'name' => $this->faker->sentence(),
            'image_path' => UploadedFile::fake()->image('random.jpg'),
        ];
        $response = $this->actingAs($this->admin)->json('PUT', 'productTypes/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
        $this->assertTrue(starts_with($actual['image_path'], '/storage/productType/image_path/'));
        Storage::disk('public')->assertExists(str_after($actual['image_path'], '/storage'));

        // update data remove image
        $data = [
            'name' => $this->faker->sentence(),
            'image_path' => null,
        ];
        $response = $this->actingAs($this->admin)->json('PUT', 'productTypes/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertNull($actual['image_path']);        
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $existing = $this->data->first();
        $id = $existing->id;
        $actual = $this->assertResponse('DELETE', '/productTypes/'.$id, 200, $this->admin)->json();
        $this->assertEquals($existing->name, $actual['name']);
        $this->assertEquals($existing->company_id, $actual['company_id']);
        $actual = $this->assertResponse('GET', '/productTypes/'.$id, 404, $this->admin)->json();
    }
    
    /**
     * As a member I want to see products of my company
     */    
    public function testMemberIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/productTypes', 200, $this->member)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }
    
}
