<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Company;
use App\NetworkCenterType;

class NetworkCenterTypeTest extends TestCase
{
    private $companies;
    private $admin;
    private $member;
    private $data;
    private $otherData;

    public function prepare($count = 5, $otherCount = 2)
    {
        $this->companies = factory(Company::class, 2)->create();
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();

        $data = [];
        for ($i = 0; $i < $count; $i++) {
            array_push($data, [
                'company_id' => $this->companies->get(0)->id,
                'name' => $this->faker->sentence(3),
                'fee_percentage' => $this->faker->randomFloat(1, 1.0, 10.0),
            ]);
        }
        for ($i = 0; $i < $otherCount; $i++) {
            array_push($data, [
                'company_id' => $this->companies->get(1)->id,
                'name' => $this->faker->sentence(3),
                'fee_percentage' => $this->faker->randomFloat(1, 1.0, 10.0),
            ]);
        }

        NetworkCenterType::insert($data);
        $this->data = NetworkCenterType::where('company_id', $this->companies->get(0)->id)->get();
        $this->otherData = NetworkCenterType::where('company_id', $this->companies->get(1)->id)->get();        
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/networkCenterTypes', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }


    public function testAdminStore()
    {
        $this->prepare();
        $data = [
            'name' => $this->faker->word,
            'fee_percentage' => $this->faker->randomFloat(1, 1.0, 10.0),
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/networkCenterTypes', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['fee_percentage'], $actual['fee_percentage']);
        $this->assertEquals($this->admin->id, $actual['created_by']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }


    public function testAdminShow()
    {
        $this->prepare();
        $data = $this->data->random();
        $actual = $this->assertResponse('GET', '/networkCenterTypes/'.$data->id, 200, $this->admin)->json();        
        $this->assertEquals($data->name, $actual['name']);
        $this->assertEquals($data->fee_percentage, $actual['fee_percentage']);
        $this->assertEquals($data->company_id, $actual['company_id']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $existing = $this->data->random();
        $id = $existing->id;

        // update data
        $data = [
            'name' => $this->faker->sentence(3),
            'fee_percentage' => $this->faker->randomFloat(1, 1.0, 10.0),
        ];
        $response = $this->actingAs($this->admin)->json('PUT', 'networkCenterTypes/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['fee_percentage'], $actual['fee_percentage']);
        $this->assertEquals($this->admin->id, $actual['updated_by']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $existing = $this->data->random();
        $id = $existing->id;
        $actual = $this->assertResponse('DELETE', '/networkCenterTypes/'.$id, 200, $this->admin)->json();
        $this->assertEquals($existing->name, $actual['name']);
        $this->assertEquals($existing->fee_percentage, $actual['fee_percentage']);
        $this->assertEquals($existing->company_id, $actual['company_id']);
        $actual = $this->assertResponse('GET', '/networkCenterTypes/'.$id, 404, $this->admin)->json();
    }
    
}
