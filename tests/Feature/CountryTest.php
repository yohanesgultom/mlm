<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Country;
use DB;

class CountryTest extends TestCase
{
    private $admin;
    private $data;

    protected function prepare()
    {
        $this->admin = factory(User::class, 1)->states('superadmin')->create()->first();
        $this->data = [
            ['name' => 'Iceland'],
            ['name' => 'India'],
            ['name' => 'Indonesia'],
            ['name' => 'Iran'],
            ['name' => 'Iraq'],
            ['name' => 'Ireland'],
        ];
        DB::table('countries')->insert($this->data);
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/countries', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }

    public function testAdminIndexSearch()
    {
        $this->prepare();
        $searchName = $this->faker->randomLetter;
        $expected = Country::where('name', 'LIKE', '%'.$searchName.'%')->get();
        $data = ['name' => $searchName];
        $response = $this->actingAs($this->admin)->json('GET', '/countries', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);
    }

    public function testAdminStore()
    {
        $this->prepare();
        $data = ['name' => $this->faker->word];
        $response = $this->actingAs($this->admin)->json('POST', '/countries', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        
    }


    public function testAdminShow()
    {
        $this->prepare();
        $id = DB::table('countries')->where('name', $this->data[0]['name'])->value('id');
        $actual = $this->assertResponse('GET', '/countries/'.$id, 200, $this->admin)->json();
        $this->assertEquals($this->data[0]['name'], $actual['name']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $id = DB::table('countries')->where('name', $this->data[0]['name'])->value('id');
        $data = ['id' => $id, 'name' => $this->faker->word()];
        $response = $this->actingAs($this->admin)->json('PUT', 'countries/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $data = ['id' => null, 'name' => $this->data[0]['name']];
        $data['id'] = DB::table('countries')->where('name', $data['name'])->value('id');
        $actual = $this->assertResponse('DELETE', '/countries/'.$data['id'], 200, $this->admin)->json();
        $this->assertEquals($data['name'], $actual['name']); 
        $actual = $this->assertResponse('GET', '/countries/'.$data['id'], 404, $this->admin)->json();
    }    

}
