<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Deposit;
use App\Member;
use App\Company;
use App\Bank;
use App\BankAccount;
use DB;

class DepositTest extends TestCase
{   
    private $admins;
    private $memberGroups;
    private $depositGroups;

    private function prepare()
    {
        // const
        $faker = $this->faker;
        $companiesCount = 2;
        $memberCount = 10;

        Bank::insert([
            ['name' => 'BANK ARTHA GRAHA', 'code' => '37'],
            ['name' => 'BANK ARTOS IND', 'code' => '542'],
            ['name' => 'BANK BCA', 'code' => '14'],
            ['name' => 'BANK BCA SYARIAH', 'code' => '536'],
            ['name' => 'BANK BENGKULU', 'code' => '133'],
            ['name' => 'BANK BII MAYBANK', 'code' => '16'],            
        ]);
        $banks = Bank::all();


        // create data
        $companies = factory(Company::class, $companiesCount)->create();
        $admins = [];
        $memberGroups = [];
        $depositGroups = [];
        $bankAccountGroups = [];

        foreach ($companies as $c) {
            // admin
            $admin = factory(User::class, 1)->states('admin')->create(['company_id' => $c->id])->first();

            // members
            $i = 0;
            $users = factory(User::class, $memberCount)->states('member')->create(['company_id' => $c->id]);
            $members = factory(Member::class, $memberCount)->make()->each(function ($m) use ($users, &$i) {
                $u = $users->get($i);
                $m->user_id = $u->id;
                $m->name_printed = $u->name;
                $m->save();            
                $i++;            
            });            

            $bankAccounts = [];
            for ($i = 0; $i < $faker->numberBetween(3, 5); $i++) {
                array_push($bankAccounts, [
                    'company_id' => $c->id,
                    'account_name' => $c->name,
                    'bank_id' => $banks->random()->id,
                    'account_no' => $faker->creditCardNumber,
                ]);                
            }
            BankAccount::insert($bankAccounts);
            $bankAccounts = BankAccount::where('company_id', $c->id)->get();

            // deposits
            $deposits = [];            
            foreach ($members as $m) {
                for ($j = 0; $j < $faker->numberBetween(1, 5); $j++) {                    array_push($deposits, [
                        'desc' => $faker->sentence(3),
                        'amount' => $faker->randomNumber(6),
                        'deposit_date' => $faker->dateTime(),
                        'status' => $faker->randomElement(array_values(Deposit::STATUSES)),
                        'member_id' => $m->id,
                        'bank_account_id' => $bankAccounts->random()->id,
                    ]);
                }
            }
            Deposit::insert($deposits);

            $this->admins[$c->id] = $admin;
            $this->memberGroups[$c->id] = $members;
            $this->bankAccountGroups[$c->id] = $bankAccounts;
            $this->depositGroups[$c->id] = Deposit::join('bank_accounts', 'deposits.bank_account_id', '=', 'bank_accounts.id')
                ->where('bank_accounts.company_id', '=', $c->id)
                ->select('deposits.*')
                ->get();
        }

        $this->companies = $companies;
        $this->$banks = $banks;
    }
    
    public function testStatuses()
    {
        $actual = $this->assertResponse('GET', '/deposits/statuses', 200)->json();
        $this->assertEquals(array_flip(Deposit::STATUSES), $actual);
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $companyId = $this->companies->get(0)->id;
        $actual = $this->assertResponse('GET', '/deposits', 200, $this->admins[$companyId])->json();
        $this->assertEquals(count($this->depositGroups[$companyId]), $actual['total']);
    }

    public function testAdminStore()
    {
        $this->prepare();
        $faker = $this->faker;
        $companyId = $this->companies->get(0)->id;
        $member = $this->memberGroups[$companyId]->random();
        $bankAccount = $this->bankAccountGroups[$companyId]->random();

        $data = [
            'desc' => $faker->sentence(3),
            'amount' => $faker->randomNumber(6),
            'deposit_date' => $faker->dateTime()->format('Y-m-d H:i:s'),
            'status' => $faker->randomElement(array_values(Deposit::STATUSES)),
            'member_id' => $member->id,
            'bank_account_id' => $bankAccount->id,
        ];

        $response = $this->actingAs($this->admins[$companyId])->json('POST', '/deposits', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['desc'], $actual['desc']);
        $this->assertEquals($data['amount'], $actual['amount']);
        $this->assertEquals($data['deposit_date'], $actual['deposit_date']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Deposit::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals($data['member_id'], $actual['member_id']);
        $this->assertEquals($data['bank_account_id'], $actual['bank_account']['id']);
    }

    public function testAdminShow()
    {
        $this->prepare();
        $companyId = $this->companies->get(0)->id;
        $deposit = $this->depositGroups[$companyId]->random();
        $actual = $this->assertResponse('GET', '/deposits/'.$deposit->id, 200, $this->admins[$companyId])->json();
        $this->assertEquals($deposit->desc, $actual['desc']);
        $this->assertEquals($deposit->amount, $actual['amount']);
        $this->assertEquals($deposit->deposit_date->format('Y-m-d H:i:s'), $actual['deposit_date']);
        $this->assertEquals($deposit->status, $actual['status']);
        $this->assertEquals($deposit->status_name, $actual['status_name']);
        $this->assertEquals($deposit->member_id, $actual['member_id']);
        $this->assertEquals($deposit->bank_account->toArray(), $actual['bank_account']);
    }
 
    public function testAdminUpdate()
    {
        $this->prepare();
        $faker = $this->faker;
        $companyId = $this->companies->get(0)->id;
        $deposit = $this->depositGroups[$companyId]->random();   
        $member = $this->memberGroups[$companyId]->random();
        $bankAccount = $this->bankAccountGroups[$companyId]->random();
        
        $data = [
            'desc' => $faker->sentence(3),
            'amount' => $faker->randomNumber(6),
            'deposit_date' => $faker->dateTime()->format('Y-m-d H:i:s'),
            'status' => $faker->randomElement(array_values(Deposit::STATUSES)),
            'bank_account_id' => $bankAccount->id,
            'member_id' => $member->id,
        ];

        $response = $this->actingAs($this->admins[$companyId])->json('PUT', '/deposits/'.$deposit->id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($deposit->id, $actual['id']);
        $this->assertEquals($data['desc'], $actual['desc']);
        $this->assertEquals($data['amount'], $actual['amount']);
        $this->assertEquals($data['deposit_date'], $actual['deposit_date']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Deposit::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals($member->id, $actual['member_id']);
        $this->assertEquals($bankAccount->toArray(), $actual['bank_account']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $companyId = $this->companies->get(0)->id;
        $deposit = $this->depositGroups[$companyId]->random();
        $actual = $this->assertResponse('DELETE', '/deposits/'.$deposit->id, 200, $this->admins[$companyId])->json();
        $this->assertEquals($deposit->id, $actual['id']);
        $this->assertEquals($deposit->desc, $actual['desc']);
        $this->assertEquals($deposit->amount, $actual['amount']);
        $this->assertEquals($deposit->deposit_date->format('Y-m-d H:i:s'), $actual['deposit_date']);
        $this->assertEquals($deposit->status, $actual['status']);
        $this->assertEquals($deposit->status_name, $actual['status_name']);
        $this->assertEquals($deposit->member_id, $actual['member_id']);
        $this->assertEquals($deposit->bank_account->toArray(), $actual['bank_account']);
   
        $actual = $this->assertResponse('GET', '/deposits/'.$deposit->id, 404, $this->admins[$companyId])->json();
    }    
}
