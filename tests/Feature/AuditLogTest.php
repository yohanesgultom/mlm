<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\AuditLog;
use App\Company;
use App\User;
use App\Wallet;
use App\Order;
use App\Transaction;
use App\Product;
use App\Business\CommerceService;

class AuditLogTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);
        $this->company = Company::first();
        $this->admin = $this->company->users()->where('role', User::ROLES['ADMIN'])->first();
    }

    public function assertArrayEquals($data, $actual)
    {
        $this->assertEquals($data['model'], $actual['model']);
        $this->assertEquals($data['action'], $actual['action']);
        $this->assertEquals($data['ip'], $actual['ip']);
        $this->assertEquals($data['path'], $actual['path']);        
        $this->assertEquals($data['userrole'], $actual['userrole']);        
        $this->assertEquals($data['username'], $actual['username']);        
        $this->assertEquals($data['before'], $actual['before']);        
        $this->assertEquals($data['change'], $actual['change']);        
        $this->assertEquals($data['company_id'], $actual['company_id']);        
    }

    public function testIndexShow()
    {
        // trigger creation        
        $products = Product::ofCompany($this->admin->company_id)->take(5)->get();
        $stocks = CommerceService::getCentralStockAvailability($products->pluck('id')->all(), $this->admin->company_id);
        $wallet = Wallet::join('members', 'members.id', 'wallets.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->admin->company_id)
            ->select('wallets.*')
            ->first();        
        $order = factory(Order::class, 1)
            ->make(['wallet_id' => $wallet->id, 'status' => Order::STATUSES['COMPLETED']])
            ->first();
        $faker = $this->faker;
        $transactions = factory(Transaction::class, 3)
            ->make(['order_id' => $order->id])
            ->each(function ($t) use ($products, $stocks) {
                $p = $products->pop();
                $stock = CommerceService::getStockToSubtract($stocks, $p->id, 1);
                $qty = $stock->available;
                $t->product_id = $p->id;
                $t->qty = $qty;
                $t->amount = $p->base_price * $qty;
                $t->points = $p->point_value * $qty;                
                $t->bonus = $p->bonus_value * $qty;
            });

        // make sure member has sufficient balance
        $total = $transactions->sum('amount');
        $wallet->balance = $total;
        $wallet->save();

        $data = $order->toArray();
        $data['transactions'] = $transactions->toArray();
        $response = $this->actingAs($this->admin)->json('POST', '/orders', $data);
        $this->assertStatus(200, $response);

        // index
        $this->data = AuditLog::where('company_id', $this->admin->company_id)->get();
        $actual = $this->assertResponse('GET', '/auditLogs', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), $actual['total']);

        // show
        $data = AuditLog::where('company_id', $this->admin->company_id)->inRandomOrder()->first()->toArray();
        $actual = $this->assertResponse('GET', '/auditLogs/'.$data['id'], 200, $this->admin)->json();
        $this->assertArrayEquals($data, $actual);
    }

}
