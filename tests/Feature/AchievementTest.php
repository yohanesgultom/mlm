<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Company;
use App\Achievement;

class AchievementTest extends TestCase
{
    private $companies;
    private $admin;
    private $member;
    private $data;
    private $otherData;

    public function prepare($count = 5, $otherCount = 2)
    {
        $this->companies = factory(Company::class, 2)->create();
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        
        Achievement::insert([
            ['company_id' => $this->companies->get(0)->id, 'order' => 10, 'code' => 'D', 'desc' => 'Distributor'],
            ['company_id' => $this->companies->get(0)->id, 'order' => 20, 'code' => 'SPV', 'desc' => 'Supervisor'],
            ['company_id' => $this->companies->get(0)->id, 'order' => 20, 'code' => 'S', 'desc' => 'Supervisor'],
            ['company_id' => $this->companies->get(0)->id, 'order' => 20, 'code' => 'AM', 'desc' => 'Supervisor'],
            ['company_id' => $this->companies->get(1)->id, 'order' => 20, 'code' => 'QM', 'desc' => 'Qualified Manager'],
            ['company_id' => $this->companies->get(1)->id, 'order' => 30, 'code' => 'M', 'desc' => 'Manager'],
        ]);

        $this->data = Achievement::where('company_id', $this->companies->get(0)->id)->get();
        $this->otherData = Achievement::where('company_id', $this->companies->get(1)->id)->get();
    }
    
    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/achievements', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }


    public function testAdminStore()
    {
        $this->prepare();
        $data = [
            'order' => $this->faker->randomDigitNotNull,
            'code' => $this->faker->word,
            'desc' => $this->faker->word,
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/achievements', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['order'], $actual['order']);
        $this->assertEquals($data['code'], $actual['code']);
        $this->assertEquals($data['desc'], $actual['desc']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }


    public function testAdminShow()
    {
        $this->prepare();
        $data = $this->data->first();
        $actual = $this->assertResponse('GET', '/achievements/'.$data->id, 200, $this->admin)->json();
        $this->assertEquals($data->order, $actual['order']);
        $this->assertEquals($data->code, $actual['code']);
        $this->assertEquals($data->desc, $actual['desc']);
        $this->assertEquals($data->company_id, $actual['company_id']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $existing = $this->data->first();
        $id = $existing->id;

        // update data
        $data = [
            'order' => $this->faker->randomDigitNotNull,
            'code' => $this->faker->word,
            'desc' => $this->faker->word,
        ];
        $response = $this->actingAs($this->admin)->json('PUT', 'achievements/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['order'], $actual['order']);
        $this->assertEquals($data['code'], $actual['code']);
        $this->assertEquals($data['desc'], $actual['desc']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $existing = $this->data->first();
        $id = $existing->id;
        $actual = $this->assertResponse('DELETE', '/achievements/'.$id, 200, $this->admin)->json();
        $this->assertEquals($existing->order, $actual['order']);
        $this->assertEquals($existing->code, $actual['code']);
        $this->assertEquals($existing->code, $actual['code']);
        $this->assertEquals($existing->desc, $actual['desc']);
        $this->assertEquals($existing->company_id, $actual['company_id']);
        $actual = $this->assertResponse('GET', '/achievements/'.$id, 404, $this->admin)->json();
    }
    
    /**
     * As a member I want to see products of my company
     */    
    public function testMemberIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/achievements', 200, $this->member)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }
    
}
