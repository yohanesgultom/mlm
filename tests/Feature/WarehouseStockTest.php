<?php

namespace Tests\Feature;

use Tests\WarehouseTestCase;
use App\WarehouseStock;

class WarehouseStockTest  extends WarehouseTestCase
{

    public function testAdminWarehouseStocks()
    {
        $this->prepare();
        $warehouse = $this->warehouses->random();
        $stocks = $warehouse->stocks;
        $actual = $this->assertResponse('GET', '/warehouses/'.$warehouse->id.'/stocks', 200, $this->admin)->json();
        $this->assertEquals(count($stocks), count($actual['data']));
        for($i = 0; $i < count($stocks); $i++) {
            $this->assertEquals($stocks->get($i)->id, $actual['data'][$i]['id']);
            $this->assertEquals($stocks->get($i)->qty, $actual['data'][$i]['qty']);
            $this->assertEquals($stocks->get($i)->exp_date->format('Y-m-d H:i:s'), $actual['data'][$i]['exp_date']);
            $this->assertEquals($stocks->get($i)->product_id, $actual['data'][$i]['product_id']);
            $this->assertEquals($stocks->get($i)->warehouse_id, $actual['data'][$i]['warehouse_id']);
        }
    }
    
    public function testAdminWarehouseStockShow()
    {
        $this->prepare();
        $warehouse = $this->warehouses->random();
        $stock = $warehouse->stocks->random();
        $actual = $this->assertResponse('GET', '/warehouses/'.$warehouse->id.'/stocks/'.$stock->id, 200, $this->admin)->json();
        $this->assertEquals($stock->id, $actual['id']);
        $this->assertEquals($stock->qty, $actual['qty']);
        $this->assertEquals($stock->exp_date->format('Y-m-d H:i:s'), $actual['exp_date']);
        $this->assertEquals($stock->product_id, $actual['product_id']);
        $this->assertEquals($stock->warehouse_id, $actual['warehouse_id']);
    }

    // public function testAdminWarehouseStockStore()
    // {
    //     $this->prepare();
    //     $warehouse = $this->warehouses->random();
    //     $product = $this->products->random();
    //     $faker = $this->faker;
    //     $data = [
    //         'qty' => $faker->numberBetween(10, 100),
    //         'exp_date' => $faker->dateTime('+3 year')->format('Y-m-d H:i:s'),
    //         'product_id' => $product->id,
    //         'warehouse_id' => $warehouse->id,            
    //     ];
    //     $response = $this->actingAs($this->admin)->json('POST', '/warehouses/'.$warehouse->id.'/stocks', $data);
    //     $this->assertStatus(200, $response);

    //     $actual = $response->json();
    //     $this->assertEquals($data['qty'], $actual['qty']);
    //     $this->assertEquals($data['exp_date'], $actual['exp_date']);
    //     $this->assertEquals($data['product_id'], $actual['product_id']);
    //     $this->assertEquals($data['warehouse_id'], $actual['warehouse_id']);
    // }
    
    // public function testAdminWarehouseStockUpdate()
    // {
    //     $this->prepare();
    //     $warehouse = $this->warehouses->random();
    //     $stock = $warehouse->stocks->random();

    //     // update data
    //     $product = $this->products->random();
    //     $faker = $this->faker;
    //     $data = [
    //         'qty' => $faker->numberBetween(10, 100),
    //         'exp_date' => $faker->dateTime('+3 year')->format('Y-m-d H:i:s'),
    //         'product_id' => $product->id,
    //         'warehouse_id' => $warehouse->id,            
    //     ];
    //     $response = $this->actingAs($this->admin)->json('PUT', '/warehouses/'.$warehouse->id.'/stocks/'.$stock->id, $data);        
    //     $this->assertStatus(200, $response);

    //     $actual = $response->json();
    //     $this->assertEquals($stock->id, $actual['id']);
    //     $this->assertEquals($data['qty'], $actual['qty']);
    //     $this->assertEquals($data['exp_date'], $actual['exp_date']);
    //     $this->assertEquals($data['product_id'], $actual['product_id']);
    //     $this->assertEquals($data['warehouse_id'], $actual['warehouse_id']);
        
    // }    
    
    // public function testAdminWarehouseStockDelete()
    // {
    //     $this->prepare();
    //     $warehouse = $this->warehouses->random();
    //     $stock = $warehouse->stocks->random();

    //     $actual = $this->assertResponse('DELETE', '/warehouses/'.$warehouse->id.'/stocks/'.$stock->id, 200, $this->admin)->json();
    //     $this->assertEquals($stock->id, $actual['id']);
    //     $this->assertEquals($stock->qty, $actual['qty']);
    //     $this->assertEquals($stock->exp_date->format('Y-m-d H:i:s'), $actual['exp_date']);
    //     $this->assertEquals($stock->product_id, $actual['product_id']);
    //     $this->assertEquals($stock->warehouse_id, $actual['warehouse_id']);

    //     $actual = $this->assertResponse('GET', '/warehouses/'.$warehouse->id.'/stocks/'.$stock->id, 404, $this->admin)->json();
    // }

}
