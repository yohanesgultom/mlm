<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Payment;
use App\Member;
use App\Company;
use DB;

class PaymentTest extends TestCase
{   
    private $admins;
    private $memberGroups;
    private $paymentGroups;

    private function prepare()
    {
        // const
        $faker = $this->faker;
        $companiesCount = 2;
        $memberCount = 10;

        // create data
        $companies = factory(Company::class, $companiesCount)->create();
        $admins = [];
        $memberGroups = [];
        $paymentGroups = [];

        foreach ($companies as $c) {
            // admin
            $admin = factory(User::class, 1)->states('admin')->create(['company_id' => $c->id])->first();

            // members
            $i = 0;
            $users = factory(User::class, $memberCount)->states('member')->create(['company_id' => $c->id]);
            $members = factory(Member::class, $memberCount)->make()->each(function ($m) use ($users, &$i) {
                $u = $users->get($i);
                $m->user_id = $u->id;
                $m->name_printed = $u->name;
                $m->save();            
                $i++;            
            });

            // payments
            $payments = [];            
            foreach ($members as $m) {
                for ($j = 0; $j < $faker->numberBetween(1, 5); $j++) {                    array_push($payments, [
                        'type' => $faker->randomElement(array_values(Payment::TYPES)),
                        'desc' => $faker->sentence(3),
                        'amount' => $faker->randomNumber(6),
                        'payment_date' => $faker->dateTime(),
                        'status' => $faker->randomElement(array_values(Payment::STATUSES)),
                        'member_id' => $m->id,
                    ]);
                }
            }
            DB::table('payments')->insert($payments);

            $this->admins[$c->id] = $admin;
            $this->memberGroups[$c->id] = $members;
            $this->paymentGroups[$c->id] = Payment::join('members', 'payments.member_id', '=', 'members.id')
                ->join('users', 'members.user_id', '=', 'users.id')
                ->where('users.company_id', '=', $c->id)
                ->select('payments.*')
                ->get();            
        }

        $this->companies = $companies;
    }
    
    public function testTypes()
    {
        $actual = $this->assertResponse('GET', '/payments/types', 200)->json();
        $this->assertEquals(array_flip(Payment::TYPES), $actual);
    }

    public function testStatuses()
    {
        $actual = $this->assertResponse('GET', '/payments/statuses', 200)->json();
        $this->assertEquals(array_flip(Payment::STATUSES), $actual);
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $companyId = $this->companies->get(0)->id;
        $actual = $this->assertResponse('GET', '/payments', 200, $this->admins[$companyId])->json();
        $this->assertEquals(count($this->paymentGroups[$companyId]), $actual['total']);
    }

    public function testAdminStore()
    {
        $this->prepare();
        $faker = $this->faker;
        $companyId = $this->companies->get(0)->id;
        $member = $this->memberGroups[$companyId]->random();

        $data = [
            'type' => $faker->randomElement(array_values(Payment::TYPES)),
            'desc' => $faker->sentence(3),
            'amount' => $faker->randomNumber(6),
            'payment_date' => $faker->dateTime()->format('Y-m-d H:i:s'),
            'status' => $faker->randomElement(array_values(Payment::STATUSES)),
            'member_id' => $member->id,
        ];

        $response = $this->actingAs($this->admins[$companyId])->json('POST', '/payments', $data);
        if ($response->status() != 200) {
            print_r($response->json());
        }

        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['type'], $actual['type']);
        $this->assertEquals(array_flip(Payment::TYPES)[$data['type']], $actual['type_name']);
        $this->assertEquals($data['desc'], $actual['desc']);
        $this->assertEquals($data['amount'], $actual['amount']);
        $this->assertEquals($data['payment_date'], $actual['payment_date']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Payment::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals($data['member_id'], $actual['member_id']);
    }

    public function testAdminShow()
    {
        $this->prepare();
        $companyId = $this->companies->get(0)->id;
        $payment = $this->paymentGroups[$companyId]->random();
        $actual = $this->assertResponse('GET', '/payments/'.$payment->id, 200, $this->admins[$companyId])->json();
        $this->assertEquals($payment->type, $actual['type']);
        $this->assertEquals($payment->type_name, $actual['type_name']);
        $this->assertEquals($payment->desc, $actual['desc']);
        $this->assertEquals($payment->amount, $actual['amount']);
        $this->assertEquals($payment->payment_date->format('Y-m-d H:i:s'), $actual['payment_date']);
        $this->assertEquals($payment->status, $actual['status']);
        $this->assertEquals($payment->status_name, $actual['status_name']);
        $this->assertEquals($payment->member_id, $actual['member_id']);
    }
 
    public function testAdminUpdate()
    {
        $this->prepare();
        $faker = $this->faker;
        $companyId = $this->companies->get(0)->id;
        $payment = $this->paymentGroups[$companyId]->random();                
        
        $data = [
            'type' => $faker->randomElement(array_values(Payment::TYPES)),
            'desc' => $faker->sentence(3),
            'amount' => $faker->randomNumber(6),
            'payment_date' => $faker->dateTime()->format('Y-m-d H:i:s'),
            'status' => $faker->randomElement(array_values(Payment::STATUSES)),
        ];

        $response = $this->actingAs($this->admins[$companyId])->json('PUT', '/payments/'.$payment->id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($payment->id, $actual['id']);
        $this->assertEquals($data['type'], $actual['type']);
        $this->assertEquals(array_flip(Payment::TYPES)[$data['type']], $actual['type_name']);
        $this->assertEquals($data['desc'], $actual['desc']);
        $this->assertEquals($data['amount'], $actual['amount']);
        $this->assertEquals($data['payment_date'], $actual['payment_date']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Payment::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals($payment->member_id, $actual['member_id']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $companyId = $this->companies->get(0)->id;
        $payment = $this->paymentGroups[$companyId]->random();
        $actual = $this->assertResponse('DELETE', '/payments/'.$payment->id, 200, $this->admins[$companyId])->json();
        $this->assertEquals($payment->id, $actual['id']);
        $this->assertEquals($payment->type, $actual['type']);
        $this->assertEquals($payment->type_name, $actual['type_name']);
        $this->assertEquals($payment->desc, $actual['desc']);
        $this->assertEquals($payment->amount, $actual['amount']);
        $this->assertEquals($payment->payment_date->format('Y-m-d H:i:s'), $actual['payment_date']);
        $this->assertEquals($payment->status, $actual['status']);
        $this->assertEquals($payment->status_name, $actual['status_name']);
        $this->assertEquals($payment->member_id, $actual['member_id']);
   
        $actual = $this->assertResponse('GET', '/payments/'.$payment->id, 404, $this->admins[$companyId])->json();
    }    
}
