<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use App\User;
use App\Company;
use App\Product;
use App\ProductType;
use App\Tag;

class ProductTest extends TestCase
{
    protected $companies;
    protected $admin;
    protected $member;
    protected $tags;
    protected $types;
    protected $products;
    protected $otherProducts;

    public function prepare($count = 5, $otherCount = 2)
    {
        Storage::fake('public');
        $this->companies = factory(Company::class, 2)->create();
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        
        // tags and types
        $this->tags = factory(Tag::class, 10)->create(['company_id' => $this->companies->get(0)->id]);
        $this->types = factory(ProductType::class, 10)->create(['company_id' => $this->companies->get(0)->id]);
        $this->otherTags = factory(Tag::class, 10)->create(['company_id' => $this->companies->get(1)->id]);
        $this->otherTypes = factory(ProductType::class, 10)->create(['company_id' => $this->companies->get(1)->id]);

        // create products
        $faker = $this->faker;
        $tagIds = $this->tags->pluck('id')->toArray();
        $typeIds = $this->types->pluck('id')->toArray();
        $this->products = factory(Product::class, $count)
            ->make()
            ->each(function ($p) use ($typeIds, $tagIds, $faker) {
                $p->product_type_id = $faker->randomElement($typeIds);
                $p->save();
                $p->tags()->attach($faker->randomElements($tagIds, 3));
            });        
        $this->tagIds = $tagIds;
        $this->typeIds = $typeIds;

        // create other products
        $otherTagIds = $this->otherTags->pluck('id')->toArray();
        $otherTypeIds = $this->otherTypes->pluck('id')->toArray();
        $this->otherProducts = factory(Product::class, $otherCount)
            ->make()
            ->each(function ($p) use ($otherTypeIds, $otherTagIds, $faker) {
                $p->product_type_id = $faker->randomElement($otherTypeIds);
                $p->save();
                $p->tags()->attach($faker->randomElements($otherTagIds, 3));
            });        
        
    }
    
    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/products', 200, $this->admin)->json();
        $this->assertEquals(count($this->products), count($actual['data']));
    }

    public function testAdminIndexSearch()
    {
        $this->prepare();
        $searchName = 'e';
        $expected = Product::joinProductTypes()
            ->where('product_types.company_id', $this->admin->company_id)
            ->where('products.name', 'LIKE', '%'.$searchName.'%')
            ->get();
        $data = [
            'name' => $searchName,
        ];
        $response = $this->actingAs($this->admin)->json('GET', '/products', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);
    }

    public function testAdminStore()
    {
        $this->prepare();

        $expectedTagIds = $this->faker->randomElements($this->tagIds, 3);
        sort($expectedTagIds);
        $data = factory(Product::class, 1)->make([
            'product_type_id' => $this->faker->randomElement($this->typeIds),
            'tag_ids' => $expectedTagIds,            
        ])->first()->toArray();

        // test invalid file
        $data['image_path'] = UploadedFile::fake()->create('document.pdf', 100);
        $response = $this->actingAs($this->admin)->json('POST', '/products', $data);
        $this->assertStatus(422, $response);        

        // test valid
        $data['image_path'] = UploadedFile::fake()->image('random.jpg');
        $response = $this->actingAs($this->admin)->json('POST', '/products', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $actualTagIds = collect($actual['tags'])->pluck('id')->toArray();
        sort($actualTagIds);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['base_price'], $actual['base_price']);
        $this->assertEquals($data['bonus_value'], $actual['bonus_value']);
        $this->assertEquals($data['point_value'], $actual['point_value']);
        $this->assertEquals($data['weight'], $actual['weight']);
        $this->assertEquals($data['product_type_id'], $actual['product_type_id']);
        $this->assertEquals($data['tag_ids'], $actualTagIds);
        $this->assertEquals($this->admin->id, $actual['created_by']);
        $this->assertTrue(starts_with($actual['image_path'], '/storage/product/image_path/'));
        Storage::disk('public')->assertExists(str_after($actual['image_path'], '/storage'));
    }


    public function testAdminShow()
    {
        $this->prepare();
        $product = $this->products->first();
        $actual = $this->assertResponse('GET', '/products/'.$product->id, 200, $this->admin)->json();
        $this->assertEquals($product->name, $actual['name']);
        $this->assertEquals($product->base_price, $actual['base_price']);
        $this->assertEquals($product->bonus_value, $actual['bonus_value']);
        $this->assertEquals($product->point_value, $actual['point_value']);
        $this->assertEquals($product->weight, $actual['weight']);
        $this->assertEquals($product->product_type_id, $actual['product_type_id']);
        $this->assertEquals($product->tags->toArray(), $actual['tags']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $product = $this->products->first();

        // update data
        $expectedTagIds = $this->faker->randomElements($this->tagIds, 3);
        sort($expectedTagIds);        
        $data = $product->toArray();
        $data['name'] = $this->faker->name;
        $data['product_type_id'] = $this->faker->randomElement($this->typeIds);
        $data['tag_ids'] = $expectedTagIds;
        $data['image_path'] = UploadedFile::fake()->image('random.jpg');
        $response = $this->actingAs($this->admin)->json('PUT', 'products/'.$product->id, $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $actualTagIds = collect($actual['tags'])->pluck('id')->toArray();
        sort($actualTagIds);
        $this->assertEquals($product->id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['base_price'], $actual['base_price']);
        $this->assertEquals($data['bonus_value'], $actual['bonus_value']);
        $this->assertEquals($data['point_value'], $actual['point_value']);
        $this->assertEquals($data['weight'], $actual['weight']);
        $this->assertEquals($data['product_type_id'], $actual['product_type_id']);
        $this->assertEquals($data['tag_ids'], $actualTagIds);
        $this->assertEquals($this->admin->id, $actual['updated_by']);
        $this->assertTrue(starts_with($actual['image_path'], '/storage/product/image_path/'));
        $this->assertNotEquals($product->image_path, $actual['image_path']);
        Storage::disk('public')->assertExists(str_after($actual['image_path'], '/storage'));

        // update data remove image
        $data['name'] = $this->faker->name;
        $data['image_path'] = null;
        $response = $this->actingAs($this->admin)->json('PUT', 'products/'.$product->id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($product->id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertNull($actual['image_path']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $product = $this->products->first();
        $actual = $this->assertResponse('DELETE', '/products/'.$product->id, 200, $this->admin)->json();
        $this->assertEquals($product->name, $actual['name']);
        $this->assertEquals($product->base_price, $actual['base_price']);
        $this->assertEquals($product->bonus_value, $actual['bonus_value']);
        $this->assertEquals($product->point_value, $actual['point_value']);
        $this->assertEquals($product->weight, $actual['weight']);
        $actual = $this->assertResponse('GET', '/products/'.$product->id, 404, $this->admin)->json();
    }
    
    /**
     * As a member I want to see products of my company
     */    
    public function testMemberIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/products', 200, $this->member)->json();
        $this->assertEquals(count($this->products), count($actual['data']));
    }
    
}
