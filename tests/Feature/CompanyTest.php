<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Member;
use App\Company;
use DB;
use DateInterval;

class CompanyTest extends TestCase
{
    private $superadmin;
    private $admin;
    private $member;
    private $data;

    protected function prepare()
    {
        $count = 2;
        $cutoff_day = 30;
        $data = [];
        for ($i = 0; $i < $count; $i++) {
            array_push($data, [
                'name' => $this->faker->company,
                'cutoff_day' => $cutoff_day,
                'weekly_cutoff_day_code' => 'MON',
            ]);
        }        
        DB::table('companies')->insert($data);
        $this->data = Company::all();
        
        // superadmin
        $this->superadmin = factory(User::class, 1)->states('superadmin')->create()->first();

        // admin
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->data->get(0)->id
        ])->first();

        // member
        $this->member = factory(User::class, 1)
            ->states('member')
            ->create([
                'company_id' => $this->data->get(0)->id
            ])
            ->each(function ($user) {
                factory(Member::class, 1)->create([
                    'user_id' => $user->id,
                    'name_printed' => $user->name,
                ]);
            });
        
    }

    public function testSuperadminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/companies', 200, $this->superadmin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }


    public function testSuperadminStore()
    {
        $this->prepare();
        $cutoff_day = 30;
        $data = [
            'name' => $this->faker->company,
            'cutoff_day' => $cutoff_day,
            'weekly_cutoff_day_code' => 'MON',
        ];
        $response = $this->actingAs($this->superadmin)->json('POST', '/companies', $data);
        $this->assertStatus(200, $response);        
        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['cutoff_day'], $actual['cutoff_day']);
        $this->assertEquals($data['weekly_cutoff_day_code'], $actual['weekly_cutoff_day_code']);
        
    }


    public function testSuperadminShow()
    {
        $this->prepare();
        $data = $this->data->get(0);
        $actual = $this->assertResponse('GET', '/companies/'.$data->id, 200, $this->superadmin)->json();
        $this->assertEquals($data->id, $actual['id']);
        $this->assertEquals($data->name, $actual['name']);
        $this->assertEquals($data->cutoff_day, $actual['cutoff_day']);
        $this->assertEquals($data->weekly_cutoff_day_code, $actual['weekly_cutoff_day_code']);
    }

    public function testSuperadminUpdate()
    {
        $this->prepare();
        $id = $this->data->get(0)->id;
        $cutoff_day = 30;
        $data = [
            'id' => $id,
            'name' => $this->faker->company,
            'cutoff_day' => $cutoff_day,
            'weekly_cutoff_day_code' => 'MON',
        ];
        $response = $this->actingAs($this->superadmin)->json('PUT', 'companies/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['cutoff_day'], $actual['cutoff_day']);
        $this->assertEquals($data['weekly_cutoff_day_code'], $actual['weekly_cutoff_day_code']);        
    }    

    public function testSuperadminDelete()
    {
        $this->prepare();
        $data = $this->data->get(0);
        $actual = $this->assertResponse('DELETE', '/companies/'.$data->id, 200, $this->superadmin)->json();
        $this->assertEquals($data->id, $actual['id']);
        $this->assertEquals($data->name, $actual['name']);
        $this->assertEquals($data->cutoff_day, $actual['cutoff_day']);
        $this->assertEquals($data->weekly_cutoff_day_code, $actual['weekly_cutoff_day_code']);
        $actual = $this->assertResponse('GET', '/companies/'.$data->id, 404, $this->superadmin)->json();
    }


    public function testAdminShow()
    {
        $this->prepare();
        // other company
        $data = $this->data->get(1);
        $actual = $this->assertResponse('GET', '/companies/'.$data->id, 404, $this->admin)->json();

        // own company
        $data = $this->data->get(0);        
        $actual = $this->assertResponse('GET', '/companies/'.$data->id, 200, $this->admin)->json();
        $this->assertEquals($data->id, $actual['id']);
        $this->assertEquals($data->name, $actual['name']);
        $this->assertEquals($data->cutoff_day, $actual['cutoff_day']);
        $this->assertEquals($data->weekly_cutoff_day_code, $actual['weekly_cutoff_day_code']);
    }
    
}
