<?php

namespace Tests\Feature;

use Tests\MemberTestCase;
use App\Wallet;

class WalletTest extends MemberTestCase
{
    public function testWalletTypes()
    {
        $actual = $this->assertResponse('GET', '/wallets/types', 200)->json();
        $this->assertEquals(array_flip(Wallet::TYPES), $actual);
    }

    public function testAdminWallets()
    {
        $this->prepare();
        $member = $this->members->random();
        $wallets = $member->wallets;
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/wallets', 200, $this->admin)->json();
        $this->assertEquals(count($wallets), count($actual['data']));
        for($i = 0; $i < count($wallets); $i++) {
            $this->assertEquals($wallets->get($i)->id, $actual['data'][$i]['id']);
            $this->assertEquals($wallets->get($i)->balance, $actual['data'][$i]['balance']);
            $this->assertEquals($wallets->get($i)->type, $actual['data'][$i]['type']);
            $this->assertEquals(array_flip(Wallet::TYPES)[$wallets->get($i)->type], $actual['data'][$i]['type_name']);
        }
    }

    public function testMemberWallets()
    {
        $this->prepare();
        $user = $this->users->get(0);
        $member = $user->member;
        $wallets = $member->wallets;
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/wallets', 200, $user)->json();
        $this->assertEquals(count($wallets), count($actual['data']));
        for($i = 0; $i < count($wallets); $i++) {
            $this->assertEquals($wallets->get($i)->id, $actual['data'][$i]['id']);
            $this->assertEquals($wallets->get($i)->balance, $actual['data'][$i]['balance']);
            $this->assertEquals($wallets->get($i)->type, $actual['data'][$i]['type']);
            $this->assertEquals(array_flip(Wallet::TYPES)[$wallets->get($i)->type], $actual['data'][$i]['type_name']);
        }

        // must not be able to see other member's wallet
        $otherUser = $this->users->get(1);
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/wallets', 403, $otherUser)->json();
    }

    public function testAdminWalletShow()
    {
        $this->prepare();
        $member = $this->members->random();
        $wallet = $member->wallets->random();
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/wallets/'.$wallet->id, 200, $this->admin)->json();
        $this->assertEquals($wallet->id, $actual['id']);
        $this->assertEquals($wallet->balance, $actual['balance']);
        $this->assertEquals($wallet->type, $actual['type']);
    }

    public function testMemberWalletShow()
    {
        $this->prepare();
        $member = $this->members->random();
        $user = $member->user;
        $wallet = $member->wallets->random();
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/wallets/'.$wallet->id, 200, $user)->json();
        $this->assertEquals($wallet->id, $actual['id']);
        $this->assertEquals($wallet->balance, $actual['balance']);
        $this->assertEquals($wallet->type, $actual['type']);
    }

    public function testAdminWalletStore()
    {
        $this->prepare();
        $member = $this->members->random();
        $data = [
            'type' => $this->faker->randomElement(array_values(Wallet::TYPES)),
            'balance' => $this->faker->randomFloat(2, 100000.0, 1000000.0),
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/members/'.$member->id.'/wallets', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['type'], $actual['type']);
        $this->assertEquals(array_flip(Wallet::TYPES)[$data['type']], $actual['type_name']);
        $this->assertEquals($data['balance'], $actual['balance']);
        $this->assertEquals($member->id, $actual['member_id']);        

        // member should not be able to create wallet
        $data = [
            'type' => $this->faker->randomElement(array_values(Wallet::TYPES)),
            'balance' => $this->faker->randomFloat(2, 100000.0, 1000000.0),
        ];
        $response = $this->actingAs($member->user)->json('POST', '/members/'.$member->id.'/wallets', $data);
        $this->assertStatus(403, $response);
    }
    
    public function testAdminWalletUpdate()
    {
        $this->prepare();
        $member = $this->members->random();
        $wallet = $member->wallets->random();

        // update data
        $data = [
            'type' => $this->faker->randomElement(array_values(Wallet::TYPES)),
            'balance' => $this->faker->randomFloat(2, 100000.0, 1000000.0),
        ];
        $response = $this->actingAs($this->admin)->json('PUT', '/members/'.$member->id.'/wallets/'.$wallet->id, $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($wallet->id, $actual['id']);
        $this->assertEquals($member->id, $actual['member_id']);
        $this->assertEquals($data['balance'], $actual['balance']);
        $this->assertEquals($data['type'], $actual['type']);
        $this->assertEquals(array_flip(Wallet::TYPES)[$data['type']], $actual['type_name']);
        
    }    
    
    public function testAdminWalletDelete()
    {
        $this->prepare();
        $member = $this->members->random();
        $wallet = $member->wallets->random();

        $actual = $this->assertResponse('DELETE', '/members/'.$member->id.'/wallets/'.$wallet->id, 200, $this->admin)->json();
        $this->assertEquals($wallet->id, $actual['id']);
        $this->assertEquals($wallet->balance, $actual['balance']);
        $this->assertEquals($wallet->type, $actual['type']);
        $this->assertEquals(array_flip(Wallet::TYPES)[$wallet->type], $actual['type_name']);

        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/wallets/'.$wallet->id, 404, $this->admin)->json();
    }

}
