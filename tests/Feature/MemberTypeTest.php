<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Company;
use App\MemberType;

class MemberTypeTest extends TestCase
{
    private $companies;
    private $admin;
    private $member;
    private $data;
    private $otherData;

    public function prepare($count = 5, $otherCount = 2)
    {
        $this->companies = factory(Company::class, 2)->create();
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();

        $data = [];
        for ($i = 0; $i < $count; $i++) {
            array_push($data, [
                'company_id' => $this->companies->get(0)->id,
                'name' => $this->faker->sentence(3),
            ]);
        }
        for ($i = 0; $i < $otherCount; $i++) {
            array_push($data, [
                'company_id' => $this->companies->get(1)->id,
                'name' => $this->faker->sentence(3),
            ]);
        }

        MemberType::insert($data);
        $this->data = MemberType::where('company_id', $this->companies->get(0)->id)->get();
        $this->otherData = MemberType::where('company_id', $this->companies->get(1)->id)->get();        
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/memberTypes', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }


    public function testAdminStore()
    {
        $this->prepare();
        $data = [
            'name' => $this->faker->word,
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/memberTypes', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }


    public function testAdminShow()
    {
        $this->prepare();
        $data = $this->data->random();
        $actual = $this->assertResponse('GET', '/memberTypes/'.$data->id, 200, $this->admin)->json();
        $this->assertEquals($data->name, $actual['name']);
        $this->assertEquals($data->company_id, $actual['company_id']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $existing = $this->data->random();
        $id = $existing->id;

        // update data
        $data = [
            'name' => $this->faker->sentence(3),
        ];
        $response = $this->actingAs($this->admin)->json('PUT', 'memberTypes/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $existing = $this->data->random();
        $id = $existing->id;
        $actual = $this->assertResponse('DELETE', '/memberTypes/'.$id, 200, $this->admin)->json();
        $this->assertEquals($existing->name, $actual['name']);
        $this->assertEquals($existing->company_id, $actual['company_id']);
        $actual = $this->assertResponse('GET', '/memberTypes/'.$id, 404, $this->admin)->json();
    }
    
    /**
     * As a member I want to see memberTypes of my company
     */    
    public function testMemberIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/memberTypes', 200, $this->member)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }

    
    public function testMemberShow()
    {
        $this->prepare();
        $data = $this->data->random();
        $actual = $this->assertResponse('GET', '/memberTypes/'.$data->id, 200, $this->member)->json();
        $this->assertEquals($data->name, $actual['name']);
        $this->assertEquals($data->company_id, $actual['company_id']);

        $data = $this->otherData->random();
        $actual = $this->assertResponse('GET', '/memberTypes/'.$data->id, 404, $this->member)->json();
    }
}
