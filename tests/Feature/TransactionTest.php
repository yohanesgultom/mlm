<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Company;
use App\Member;
use App\Order;
use App\Transaction;

class TransactionTest extends TestCase
{   
    protected function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);
        $this->company = Company::first();
        $this->admin = $this->company->users()->where('role', User::ROLES['ADMIN'])->first();
        $this->user = $this->company->users()->where('role', User::ROLES['MEMBER'])->first();
    }

    public function testAdminIndex()
    {
        $count = Transaction::join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->join('users', 'members.user_id', 'users.id')
            ->where('users.company_id', $this->company->id)
            ->count();
        $actual = $this->assertResponse('GET', '/transactions', 200, $this->admin)->json();
        $this->assertEquals($count, $actual['total']);

        $count = Transaction::join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->join('users', 'members.user_id', 'users.id')
            ->where('transactions.transferred', true)     
            ->whereNull('transactions.point_date')       
            ->where('users.company_id', $this->company->id)
            ->count();
        $data = ['transferred' => true, 'pointed' => false];
        $response = $this->actingAs($this->admin)->json('GET', '/transactions', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($count, $actual['total']);

        $count = Transaction::join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->join('users', 'members.user_id', 'users.id')
            ->where(function ($q) {
                $q->where('transactions.transferred', false)
                    ->orWhereNull('transactions.transferred'); 
            })       
            ->whereNotNull('transactions.point_date')
            ->where('users.company_id', $this->company->id)
            ->where('wallets.member_id', $this->user->member->id)
            ->count();
        $data = ['transferred' => false, 'pointed' => true, 'member_id' => $this->user->member->id];
        $response = $this->actingAs($this->admin)->json('GET', '/transactions', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($count, $actual['total']);        
    }

    public function testAdminTransfer()
    {
        $order = Order::select('orders.*')
            ->join('wallets', 'wallets.id', 'orders.wallet_id')
            ->join('members', 'members.id', 'wallets.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->inRandomOrder()
            ->first();;
        if ($order->status != Order::STATUSES['COMPLETED']) {
            $order->status = Order::STATUSES['COMPLETED'];
            $order->save();
        }
        $transaction = $order->transactions->random();
        $member = $order->wallet->member;

        $this->assertEquals(false, $transaction->transferred);

        // find target member
        $target = Member::with('wallets')
            ->joinUsers()
            ->byCompany($this->company->id)
            ->where('members.id', '!=', $member->id)
            ->inRandomOrder()
            ->first();
        $wallet = $target->wallets->random();
        $data = [
            'wallet_id' => $wallet->id,
        ];

        $response = $this->actingAs($this->admin)->json('POST', '/transactions/'.$transaction->id.'/transfer', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $actualTx = $actual['transactions'][0];

        $this->assertEquals($wallet->id, $actual['wallet_id']);
        $this->assertEquals(1, count($actual['transactions']));
        $this->assertEquals($actualTx['code'], $transaction->code);
        $this->assertEquals($actualTx['qty'], $transaction->qty);
        $this->assertEquals($actualTx['amount'], $transaction->amount);
        $this->assertEquals($actualTx['points'], $transaction->points);
        $this->assertEquals($actualTx['bonus'], $transaction->bonus);
        $this->assertEquals($actualTx['product']['id'], $transaction->product_id);
        $this->assertEquals($actualTx['transfer_from_id'], $transaction->id);
        $this->assertEquals($actualTx['transfer_from_member_id'], $member->id);

        // check original transaction
        $actual = $this->assertResponse('GET', '/transactions/'.$transaction->id, 200, $this->admin)->json();
        $this->assertEquals($transaction->id, $actual['id']);
        $this->assertEquals(true, $actual['transferred']);
    }
}
