<?php

namespace Tests\Feature;

use Tests\TestCase;
use Carbon\Carbon;
use App\User;
use App\Order;
use App\Company;
use App\Warehouse;
use App\Product;
use App\Business\BonusService;

class ReportTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed', ['--class' => 'ReligionsTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'CountriesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'BanksTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'CompaniesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'AchievementsTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'BankAccountsTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'MemberTypesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'BonusRulesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'UsersTableSeeder']);
        $company = Company::where('name', 'GreenVit International')->first();
        $this->company = $company;
        $this->company_id = $company->id;
        $this->admin = User::where('username', 'admin')->where('company_id', $this->company_id)->first();
    }

    public function _testAdminOrders($range = [], $member_id_prefix = null)
    {        
        $invoice_date_start = count($range) > 1 ? $range[0] : Carbon::now()->startOfMonth()->startOfDay();
        $invoice_date_end = count($range) > 1 ? $range[1] : Carbon::now()->endOfDay();
        $data = [];
        if (count($range) > 1) {
            $data = [
                'invoice_date_start' => $invoice_date_start->toDateTimeString(),
                'invoice_date_end' => $invoice_date_end->toDateTimeString(),
            ];
        }
        $query = Order::ofCompany($this->company_id);
        if (!empty($member_id_prefix)) {
            $data['member_id_prefix'] = $member_id_prefix;
            $query = $query->where('members.id', 'LIKE', $member_id_prefix.'%');
        }
        $orders = $query
            ->whereBetween('orders.created_at', [
                $invoice_date_start->toDateTimeString(),
                $invoice_date_end->toDateTimeString(),
            ])
            ->where('orders.status', Order::STATUSES['COMPLETED'])
            ->orderBy('orders.created_at', 'asc')
            ->get();
        $response = $this->actingAs($this->admin)->json('GET', '/reports/orders', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        if (count($orders) != count($actual)) {
            print_r(array_map(function ($o) { return $o['id']; }, $orders->toArray()));
            print_r(array_map(function ($o) { return $o['id']; }, $actual));
        }
        $this->assertEquals(count($orders), count($actual));        
    }

    public function testAdminOrders()
    {
        \Artisan::call('db:seed', ['--class' => 'AdditionalUsersTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);

        $this->_testAdminOrders();
        $this->_testAdminOrders([], 'NC');
        $this->_testAdminOrders([], '00');
        $start = Carbon::now()->startOfMonth()->subMonths(2);
        $end = Carbon::now();
        $this->_testAdminOrders([$start, $end]);
        $start = Carbon::now()->startOfMonth()->subMonths(4);
        $this->_testAdminOrders([$start, $end]);
    }

    public function testAdminWarehouses()
    {        
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);

        $warehouses = Warehouse::where('company_id', $this->company_id)->get();
        $response = $this->actingAs($this->admin)->json('GET', '/reports/warehouses', []);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($warehouses), count($actual));
        foreach ($actual as $warehouse) {
            $this->assertFalse(empty($warehouse['stocks']));
        }

        $warehouse = Warehouse::where('company_id', $this->company_id)->inRandomOrder()->first();
        $response = $this->actingAs($this->admin)->json('GET', '/reports/warehouses', ['id' => $warehouse->id]);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(1, count($actual));
        foreach ($actual as $warehouse) {
            $this->assertFalse(empty($warehouse['stocks']));
        }
    }

    public function testAdminProductStocks()
    {        
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);

        $products = Product::ofCompany($this->company_id)->orderBy('name', 'asc')->get();
        $response = $this->actingAs($this->admin)->json('GET', '/reports/productStocks', []);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($products), count($actual));
        foreach ($actual as $product) {
            $this->assertFalse(empty($product['warehouse_stocks']));
        }
    }
    
    public function testAdminProductStockLogs()
    {
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);

        $start = Carbon::now()->subDays(3)->toDateString();
        $end = Carbon::now()->toDateString();
        $data = [
            'process_date_start' => $start,
            'process_date_end' => $end,
        ];
        $products = Product::ofCompany($this->company_id)->orderBy('name', 'asc')->get();        
        $response = $this->actingAs($this->admin)->json('GET', '/reports/productStockLogs', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($products), count($actual));
        foreach ($actual as $product) {
            $this->assertFalse(empty($product['stock_logs']));
            // assumption based on seeder logic
            foreach ($product['stock_logs'] as $log) {
                $this->assertEquals($log['qty_before'], 0);
                $this->assertEquals($log['total_in'], $log['total_out'] * 2);
            }
        }

        $start = Carbon::now()->subDays(1)->toDateString();
        $end = Carbon::now()->toDateString();
        $data = [
            'process_date_start' => $start,
            'process_date_end' => $end,
        ];
        $response = $this->actingAs($this->admin)->json('GET', '/reports/productStockLogs', $data);
        $actual = $response->json();
        $this->assertEquals(count($products), count($actual));
        foreach ($actual as $product) {
            $this->assertFalse(empty($product['stock_logs']));
            foreach ($product['stock_logs'] as $log) {
                // assumption based on seeder logic
                $this->assertEquals($log['qty_before'], $log['total_out'] * 2);
                $this->assertEquals($log['total_in'], 0);
            }
        }
    }

    public function testAdminBonus()
    {
        \Artisan::call('db:seed', ['--class' => 'AdditionalUsersTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);

        $defaultDate = BonusService::getDefaultCurrentBonusDates($this->company);
        $response = $this->actingAs($this->admin)
            ->json('GET', '/reports/bonus', ['process_date' => $defaultDate['process_date']->toDateString()]);
        if (method_exists($response, 'status')) {
            $this->assertStatus(200, $response); // check the error
            $this->assertTrue(false);
        } else {
            $file = $response->getFile();
            $this->assertTrue($response->isOk());
            $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', $file->getMimeType());    
        }
    }

    public function testAdminTransactions()
    {
        \Artisan::call('db:seed', ['--class' => 'AdditionalUsersTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);

        $start_date = Order::where('status', Order::STATUSES['COMPLETED'])
            ->orderBy('created_at', 'asc')
            ->first()
            ->created_at->startOfDay();
        $end_date = $start_date->copy()->addMonth()->subDay()->endOfDay();
        $response = $this->actingAs($this->admin)
            ->json('GET', '/reports/transactions', ['start_date' => $start_date->toDateString(), 'end_date' => $end_date->toDateString()]);
        if (method_exists($response, 'status')) {
            $this->assertStatus(200, $response); // check the error
            $this->assertTrue(false);
        } else {
            $file = $response->getFile();
            $this->assertTrue($response->isOk());
            $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', $file->getMimeType());    
        }
    }
}
