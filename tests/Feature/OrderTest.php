<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Order;
use App\Member;
use App\Wallet;
use App\Product;
use App\ProductType;
use App\Transaction;
use App\Company;
use App\Warehouse;
use App\Business\CommerceService;
use DB;
use Carbon\Carbon;

class OrderTest extends TestCase
{   
    protected function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);
        $this->company = Company::first();
        $this->admin = $this->company->users()->where('role', User::ROLES['ADMIN'])->first();
        $this->user = $this->company->users()->where('role', User::ROLES['MEMBER'])->first();
    }

    public function testStatuses()
    {
        $actual = $this->assertResponse('GET', '/orders/statuses', 200)->json();
        $this->assertEquals(array_flip(Order::STATUSES), $actual);
    }

    public function testAdminIndex()
    {
        $orders = Order::select('orders.*')
            ->join('wallets', 'wallets.id', 'orders.wallet_id')
            ->join('members', 'members.id', 'wallets.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->get();
        $actual = $this->assertResponse('GET', '/orders', 200, $this->admin)->json();
        $this->assertEquals(count($orders), $actual['total']);
        $this->assertNotNull($actual['data'][0]['wallet']['member']['user']['name']);

        // test range filter
        $order = $orders->first();
        $created_date_start = Carbon::instance($order->created_at)->toDateString();
        $created_date_end = Carbon::instance($order->created_at)->addYears(5)->toDateString();
        $orders = Order::join('wallets', 'wallets.id', 'orders.wallet_id')
            ->join('members', 'members.id', 'wallets.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->whereBetween('orders.created_at', [$created_date_start, $created_date_end])
            ->select('orders.*')
            ->get();
        $data = [
            'created_date_start' => $created_date_start,
            'created_date_end' => $created_date_end,
        ];
        $response = $this->actingAs($this->admin)->json('GET', '/orders', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($orders), $actual['total']);        
    }

    public function testAdminStore()
    {
        $faker = $this->faker;
        $products = Product::ofCompany($this->company->id)->take(10)->get();       
        $w = $this->user->member->wallets->random();
        $order = factory(Order::class, 1)
            ->make(['wallet_id' => $w->id, 'status' => Order::STATUSES['COMPLETED']])
            ->first();

        $stocks = CommerceService::getCentralStockAvailability($products->pluck('id')->all(), $this->company->id);        
        $transactions = factory(Transaction::class, $faker->numberBetween(1, 3))
            ->make(['order_id' => $order->id, 'process_date' => new \DateTime])
            ->each(function ($t) use ($products, $stocks) {
                $p = $products->pop();                
                $stock = CommerceService::getStockToSubtract($stocks, $p->id, 1);
                $qty = $stock->available;
                $t->product_id = $p->id;
                $t->qty = $qty;
                $t->amount = $p->base_price * $qty;
                $t->points = $p->point_value * $qty;                
                $t->bonus = $p->bonus_value * $qty;
            });

        $data = $order->toArray();
        $data['transactions'] = $transactions->toArray();

        // make sure member has sufficient balance
        $total = $transactions->sum('amount');
        $w->balance = $total;
        $w->save();

        $response = $this->actingAs($this->admin)->json('POST', '/orders', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['invoice_no'], $actual['invoice_no']);
        $this->assertEquals($data['bill_no'], $actual['bill_no']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Order::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals($w->id, $actual['wallet_id']);
        $this->assertEquals(count($transactions), count($actual['transactions']));
        $this->assertNull($actual['warehouse_id']);
        $this->assertEquals($this->admin->id, $actual['admin_id']);
        foreach ($data['transactions'] as $te) {
            $found = false;
            foreach ($actual['transactions'] as $ta) {
                if ($te['code'] == $ta['code']) {
                    $this->assertEquals($te['qty'], $ta['qty']);
                    $this->assertEquals($te['amount'], $ta['amount']);
                    $this->assertEquals($te['points'], $ta['points']);
                    $this->assertEquals($te['bonus'], $ta['bonus']);
                    $this->assertEquals($te['point_date'], $ta['point_date']);
                    $this->assertEquals($te['process_date'], $ta['process_date']);
                    $this->assertEquals($te['product_id'], $ta['product_id']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }

        // check balance subtracted
        $w = Wallet::find($w->id);
        $this->assertEquals(0, $w->balance);
    }

    public function testAdminShow()
    {
        $companyId = $this->company->id;
        $order = Order::select('orders.*')
            ->join('wallets', 'wallets.id', 'orders.wallet_id')
            ->join('members', 'members.id', 'wallets.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->inRandomOrder()
            ->first();
        $actual = $this->assertResponse('GET', '/orders/'.$order->id, 200, $this->admin)->json();
        $this->assertEquals($order->invoice_no, $actual['invoice_no']);
        $this->assertEquals($order->bill_no, $actual['bill_no']);
        $this->assertEquals($order->wallet_id, $actual['wallet_id']);
        $this->assertEquals($order->status, $actual['status']);
        $this->assertEquals($order->status_name, $actual['status_name']);
        $this->assertNotNull($actual['wallet']['member']['user']['name']);
        $this->assertEquals(count($order->transactions), count($actual['transactions']));
    }
 
    public function testAdminUpdate()
    {
        $faker = $this->faker;
        $order = Order::select('orders.*')
            ->join('wallets', 'wallets.id', 'orders.wallet_id')
            ->join('members', 'members.id', 'wallets.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->inRandomOrder()
            ->first();

        // update details
        $order->address = $faker->streetAddress;
        $order->address_zipcode = $faker->postcode;
        $order->status = Order::STATUSES['COMPLETED'];
        
        $p = Product::ofCompany($this->company->id)
            ->whereNotIn('products.id', $order->transactions->pluck('product_id')->all())
            ->first();

        $product_ids = $order->transactions->pluck('product_id');
        $product_ids->push($p->id);
        
        // make sure there is sufficient stock
        $stocks = CommerceService::getCentralStockAvailability($product_ids->all(), $this->company->id);
        $stock = CommerceService::getStockToSubtract($stocks, $p->id, 3);
        $qty = $stock->available;

        // new transactions        
        $transactions = factory(Transaction::class, 1)
            ->make(['order_id' => $order->id, 'process_date' => new \DateTime])
            ->each(function ($t) use ($p, $qty) {
                $t->product_id = $p->id;
                $t->qty = $qty;
                $t->amount = $p->base_price * $qty;
                $t->points = $p->point_value * $qty;
                $t->bonus = $p->bonus_value * $qty;
            });

        // update 1 transaction (cancel the rest)
        $t = $order->transactions->random();
        $p = Product::find($t->product_id);
        $stock = CommerceService::getStockToSubtract($stocks, $p->id, 3);
        $t->qty = $stock->available;
        $t->amount = $p->base_price * $t->qty;
        $t->points = $p->point_value * $t->qty;
        $t->bonus = $p->bonus_value * $t->qty;
        $transactions->push($t);                        

        $data = $order->toArray();
        $data['transactions'] = $transactions->toArray();

        for ($i = 0; $i < count($data['transactions']); $i++) {
            unset($data['transactions'][$i]['product']);
        }

        // make sure member has sufficient balance
        $total = $transactions->sum('amount');
        $w = $order->wallet;
        $w->balance = $total;
        $w->save();        
        
        $response = $this->actingAs($this->admin)->json('PUT', '/orders/'.$order->id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();        
        $this->assertEquals($data['invoice_no'], $actual['invoice_no']);
        $this->assertEquals($data['bill_no'], $actual['bill_no']);
        $this->assertEquals($data['wallet_id'], $actual['wallet_id']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Order::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals(count($data['transactions']), count($actual['transactions']));
        $this->assertEquals($this->admin->id, $actual['admin_id']);
        foreach ($data['transactions'] as $te) {
            $found = false;
            foreach ($actual['transactions'] as $ta) {
                if ($te['code'] == $ta['code']) {
                    $this->assertEquals($te['qty'], $ta['qty']);
                    $this->assertEquals($te['amount'], $ta['amount']);
                    $this->assertEquals($te['points'], $ta['points']);
                    $this->assertEquals($te['bonus'], $ta['bonus']);
                    $this->assertEquals($te['product_id'], $ta['product_id']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }
    }    

    public function testAdminSplitTransaction()
    {
        $faker = $this->faker;
        $order = Order::select('orders.*')
            ->join('wallets', 'wallets.id', 'orders.wallet_id')
            ->join('members', 'members.id', 'wallets.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->inRandomOrder()
            ->first();

        // make sure it has been completed
        $order->status = Order::STATUSES['COMPLETED'];
        $order->save();

        $data = $order->toArray();        
        $data['transactions'] = $order->transactions->toArray();

        // split one transaction
        $t = $faker->randomElement($data['transactions']);
        array_push($data['transactions'], [
            'order_id' => $t['order_id'],
            'product_id' => $t['product']['id'],
            'code' => (string) $faker->unixTime(),
            'amount' => $t['product']['base_price'],
            'points' => $t['product']['point_value'],
            'bonus' => $t['product']['bonus_value'],
            'qty' => 1,
            'process_date' => $faker->dateTime()->format('Y-m-d H:i:s'),
            'point_date'=> $faker->dateTimeBetween('+1 weeks', '+1 months')->format('Y-m-d H:i:s'),
            'transferred' => false,
        ]);

        for ($i = 0; $i < count($data['transactions']); $i++) {            
            // reduce one
            if ($data['transactions'][$i]['code'] == $t['code']) {
                $data['transactions'][$i]['qty'] = $data['transactions'][$i]['qty'] - 1;
                $data['transactions'][$i]['amount'] = $data['transactions'][$i]['qty'] * $data['transactions'][$i]['product']['base_price'];
                $data['transactions'][$i]['points'] = $data['transactions'][$i]['qty'] * $data['transactions'][$i]['product']['point_value'];
                $data['transactions'][$i]['bonus'] = $data['transactions'][$i]['qty'] * $data['transactions'][$i]['product']['bonus_value'];
            }
            unset($data['transactions'][$i]['product']);
        }
        
        $response = $this->actingAs($this->admin)->json('PUT', '/orders/'.$order->id, $data);        
        $this->assertStatus(200, $response);
        $actual = $response->json();        
        $this->assertEquals($data['invoice_no'], $actual['invoice_no']);
        $this->assertEquals($data['bill_no'], $actual['bill_no']);
        $this->assertEquals($data['wallet_id'], $actual['wallet_id']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Order::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals(count($data['transactions']), count($actual['transactions']));
        foreach ($data['transactions'] as $te) {
            $found = false;
            foreach ($actual['transactions'] as $ta) {
                if ($te['code'] == $ta['code']) {
                    $this->assertEquals($te['qty'], $ta['qty']);
                    $this->assertEquals($te['amount'], $ta['amount']);
                    $this->assertEquals($te['points'], $ta['points']);
                    $this->assertEquals($te['bonus'], $ta['bonus']);
                    $this->assertEquals($te['transferred'], $ta['transferred']);
                    $this->assertEquals($te['product_id'], $ta['product_id']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }
    }
    
    public function testAdminDelete()
    {
        $order = Order::select('orders.*')
            ->join('wallets', 'wallets.id', 'orders.wallet_id')
            ->join('members', 'members.id', 'wallets.member_id')
            ->join('users', 'users.id', 'members.user_id')
            ->where('users.company_id', $this->company->id)
            ->inRandomOrder()
            ->first();
        $actual = $this->assertResponse('DELETE', '/orders/'.$order->id, 200, $this->admin)->json();
        $this->assertEquals($order->invoice_no, $actual['invoice_no']);
        $this->assertEquals($order->bill_no, $actual['bill_no']);
        $this->assertEquals($order->wallet_id, $actual['wallet_id']);
        $this->assertEquals($order->status, $actual['status']);
        $this->assertEquals($order->status_name, $actual['status_name']);
   
        $actual = $this->assertResponse('GET', '/orders/'.$order->id, 404, $this->admin)->json();
    }

    public function testAdminTransactionIndex()
    {
        // // pointed false
        $expected = Transaction::select('transactions.*')
            ->join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->join('users', 'members.user_id', 'users.id')
            ->where('users.company_id', $this->company->id)
            ->whereNull('transactions.point_date')
            ->get();
        $data = ['pointed' => false];
        $response = $this->actingAs($this->admin)->json('GET', '/transactions', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);
        if ($actual['total'] > 0) {
            $this->assertNotNull($actual['data'][0]['order']['wallet']['member_id']);
        }        

        // pointed true
        $expected = Transaction::select('transactions.*')
            ->join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->join('users', 'members.user_id', 'users.id')
            ->where('users.company_id', $this->company->id)
            ->whereNotNull('transactions.point_date')
            ->get();
        $data = ['pointed' => true];
        $response = $this->actingAs($this->admin)->json('GET', '/transactions', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);

        // member_id filter
        $member_id = $expected->random()->order->wallet->member_id;
        $expected = Transaction::select('transactions.*')
            ->join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->where('wallets.member_id', $member_id)
            ->get();
        $data = ['member_id' => $member_id];
        $response = $this->actingAs($this->admin)->json('GET', '/transactions', $data);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);
    }

    public function testAdminCompletingIndirectOrder()
    {
        $faker = $this->faker;
        $w = $this->user->member->wallets->random();
        $order = factory(Order::class, 1)
            ->make(['wallet_id' => $w->id, 'status' => Order::STATUSES['UNPAID']])
            ->first();

        $products = Product::ofCompany($this->company->id)->take(10)->get();
        $stocks = CommerceService::getCentralStockAvailability($products->pluck('id')->all(), $this->company->id);

        $transactions = factory(Transaction::class, 3)
            ->make(['order_id' => $order->id, 'process_date' => new \DateTime])
            ->each(function ($t) use ($products, $stocks) {
                $p = $products->pop();
                $stock = CommerceService::getStockToSubtract($stocks, $p->id, 3);
                $qty = $stock->available;
                $t->product_id = $p->id;
                $t->qty = $qty;
                $t->amount = $p->base_price * $qty;
                $t->points = $p->point_value * $qty;                
                $t->bonus = $p->bonus_value * $qty;                
            });

        // needed to check stocks after refund
        $stocks = CommerceService::getCentralStockAvailability($transactions->pluck('product_id')->all(), $this->company->id);
        $initial_stocks_sum = array_sum($stocks->pluck('available')->all());

        $data = $order->toArray();
        $data['transactions'] = $transactions->toArray();
        // make sure member has sufficient balance
        $total = $transactions->sum('amount');
        $w->balance = $total;
        $w->save();
        $response = $this->actingAs($this->admin)->json('POST', '/orders', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($actual['status'], Order::STATUSES['UNPAID']);
        
        // check balance NOT subtracted
        $w = Wallet::find($w->id);
        $this->assertEquals($total, $w->balance);

        // update status to completed
        $data['status'] = Order::STATUSES['COMPLETED'];
        $response = $this->actingAs($this->admin)->json('PUT', '/orders/'.$actual['id'], $data);
        $this->assertStatus(200, $response);        

        // check balance subtracted
        $w = Wallet::find($w->id);
        $this->assertEquals(0, $w->balance);

        // check stocks subtracted to zero
        // because transaction quantities were equals to availablities
        $order = Order::with('transactions')->find($actual['id']);
        $stocks = CommerceService::getCentralStockAvailability($order->transactions->pluck('product_id')->all(), $this->company->id);        
        foreach ($order->transactions as $t) {
            $this->assertNotNull($t->exp_date);
            $stock = $stocks->where('product_id', $t->product_id)->where('exp_date', $t->exp_date)->first();
            $this->assertEquals(0, $stock->available);
        }        

        // update status to completed
        $data['status'] = Order::STATUSES['CANCELED'];
        $response = $this->actingAs($this->admin)->json('PUT', '/orders/'.$actual['id'], $data);
        $this->assertStatus(200, $response);     
        
        // check balance refunded
        $w = Wallet::find($w->id);
        $this->assertEquals($total, $w->balance);       
        
        // check stocks restored
        $stocks = CommerceService::getCentralStockAvailability($transactions->pluck('product_id')->all(), $this->company->id);
        $this->assertEquals($initial_stocks_sum, array_sum($stocks->pluck('available')->all()));
    }

    public function testAdminCompletingDirectOrder()
    {
        $faker = $this->faker;
        $w = $this->user->member->wallets->random();
        $warehouse = Warehouse::ofCompany($this->company->id)->regular()->inRandomOrder()->first();
        $order = factory(Order::class, 1)
            ->make([
                'wallet_id' => $w->id,
                'status' => Order::STATUSES['UNPAID'],
                'warehouse_id' => $warehouse->id
            ])
            ->first();

        $products = Product::ofCompany($this->company->id)->take(10)->get();
        $stocks = $warehouse->stocks()->whereIn('product_id', $products->pluck('id')->all())->get();

        $transactions = factory(Transaction::class, 3)
            ->make(['order_id' => $order->id, 'process_date' => new \DateTime])
            ->each(function ($t) use ($products, $stocks) {
                $p = $products->pop();
                $stock = CommerceService::getStockToSubtract($stocks, $p->id, 3);
                $qty = $stock->available;
                $t->product_id = $p->id;
                $t->qty = $qty;
                $t->amount = $p->base_price * $qty;
                $t->points = $p->point_value * $qty;                
                $t->bonus = $p->bonus_value * $qty;                
            });

        // needed to check stocks after refund
        $stocks = $warehouse->stocks()->whereIn('product_id', $transactions->pluck('product_id')->all())->get();
        $initial_stocks_sum = array_sum($stocks->pluck('available')->all());

        $data = $order->toArray();
        $data['transactions'] = $transactions->toArray();
        // make sure member has sufficient balance
        $total = $transactions->sum('amount');
        $w->balance = $total;
        $w->save();
        $response = $this->actingAs($this->admin)->json('POST', '/orders', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($warehouse->id, $actual['warehouse_id']);
        $this->assertEquals(Order::STATUSES['UNPAID'], $actual['status']);
        
        // check balance NOT subtracted
        $w = Wallet::find($w->id);
        $this->assertEquals($total, $w->balance);

        // update status to completed
        $data['status'] = Order::STATUSES['COMPLETED'];
        $response = $this->actingAs($this->admin)->json('PUT', '/orders/'.$actual['id'], $data);
        $this->assertStatus(200, $response);        

        // check balance subtracted
        $w = Wallet::find($w->id);
        $this->assertEquals(0, $w->balance);

        // check stocks subtracted to zero
        // because transaction quantities were equals to availablities
        $order = Order::with('transactions')->find($actual['id']);
        $stocks = $warehouse->stocks()->whereIn('product_id', $order->transactions->pluck('product_id')->all())->get();
        foreach ($order->transactions as $t) {
            $this->assertNotNull($t->exp_date);
            $stock = $stocks->where('product_id', $t->product_id)->where('exp_date', $t->exp_date)->first();
            $this->assertEquals(0, $stock->available);
        }        

        // refund
        $data['status'] = Order::STATUSES['CANCELED'];
        $response = $this->actingAs($this->admin)->json('PUT', '/orders/'.$actual['id'], $data);
        $this->assertStatus(200, $response);     
        
        // check balance refunded
        $w = Wallet::find($w->id);
        $this->assertEquals($total, $w->balance);       
        
        // check stocks restored
        $stocks = $warehouse->stocks()->whereIn('product_id', $transactions->pluck('product_id')->all())->get();
        $this->assertEquals($initial_stocks_sum, array_sum($stocks->pluck('available')->all()));
    }    
}
