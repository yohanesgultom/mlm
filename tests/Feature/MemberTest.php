<?php

namespace Tests\Feature;

use Tests\MemberTestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use App\Member;

class MemberTest extends MemberTestCase
{

    public function testGenders()
    {
        $actual = $this->assertResponse('GET', '/members/genders', 200)->json();
        $this->assertEquals(array_flip(Member::GENDERS), $actual);
    }

    public function testStatuses()
    {
        $actual = $this->assertResponse('GET', '/members/statuses', 200)->json();
        $this->assertEquals(array_flip(Member::STATUSES), $actual);
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/members', 200, $this->admin)->json();
        $this->assertEquals(count($this->members), $actual['total']);
    }

    public function testAdminIndexSearch()
    {
        $this->prepare();

        // search by user name
        $searchName = 'e';
        $expected = Member::join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', $this->admin->company_id)
            ->where('users.name', 'LIKE', '%'.$searchName.'%')
            ->get();
        $data = [
            'name' => $searchName,
        ];
        $response = $this->actingAs($this->admin)->json('GET', '/members', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);

        // search by id
        $searchID = '1';
        $expected = Member::join('users', 'members.user_id', '=', 'users.id')
            ->where('users.company_id', $this->admin->company_id)
            ->where('members.id', 'LIKE', '%'.$searchID.'%')
            ->get();
        $data = [
            'id' => $searchID,
        ];
        $response = $this->actingAs($this->admin)->json('GET', '/members', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);
    }

    public function testAdminStore()
    {
        $this->prepare();
        Storage::fake('public');

        $name = $this->faker->firstName();
        $regDate = $this->faker->dateTime();
        $bonusDate = clone $regDate;
        $bonusDate->add(new \DateInterval('P30D'));
        
        $data = [
            'id' => (string) $this->faker->randomNumber(7),
            'name' => $name,
            'username' => str_slug($name),
            'email' => str_slug($name).'@email.com',
            'password' => 'test',
            'image_path' => UploadedFile::fake()->image('random.jpg'),
            'reg_date' => $regDate->format('Y-m-d H:i:s'),
            'birth_date' => $this->faker->dateTime('-17 years')->format('Y-m-d H:i:s'),
            'bonus_date' => $bonusDate->format('Y-m-d H:i:s'),
            'gender' => $this->faker->randomElement(array_values(Member::GENDERS)),
            'official_id' => $this->faker->creditCardNumber,
            'achievement_id' => $this->achievements->random()->id,
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/members', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['id'], $actual['id']);
        $this->assertEquals($data['name'], $actual['user']['name']);
        $this->assertEquals($data['email'], $actual['user']['email']);
        $this->assertEquals($data['reg_date'], $actual['reg_date']);
        $this->assertEquals($data['birth_date'], $actual['birth_date']);
        $this->assertEquals($data['bonus_date'], $actual['bonus_date']);
        $this->assertEquals($data['achievement_id'], $actual['achievement']['id']);
        $this->assertEquals($this->companies->get(0)->id, $actual['user']['company_id']);
        $this->assertTrue(starts_with($actual['image_path'], '/storage/member/image_path/'));
        Storage::disk('public')->assertExists(str_after($actual['image_path'], '/storage'));   
    }

    public function testAdminShow()
    {
        $this->prepare();
        $member = $this->members->first();
        $user = $member->user;
        $actual = $this->assertResponse('GET', '/members/'.$member->id, 200, $this->admin)->json();
        $this->assertEquals($user->name, $actual['user']['name']);
        $this->assertEquals($user->username, $actual['user']['username']);
        $this->assertEquals($user->email, $actual['user']['email']);
        $this->assertEquals($member->official_id, $actual['official_id']);
        $this->assertEquals($member->achievement_id, $actual['achievement']['id']);
        $this->assertEquals($member->reg_date->format('Y-m-d H:i:s'), $actual['reg_date']);
        $this->assertEquals($member->birth_date->format('Y-m-d H:i:s'), $actual['birth_date']);
        $this->assertEquals($member->bonus_date->format('Y-m-d H:i:s'), $actual['bonus_date']);
    }

    /**
     * As admin I want to update details of member from my company
     */
    public function testAdminUpdate()
    {
        $this->prepare();
        Storage::fake('public');
        $member = $this->members->first();

        // update data
        $name = $this->faker->firstName();
        $regDate = $this->faker->dateTime();
        $bonusDate = clone $regDate;
        $bonusDate->add(new \DateInterval('P30D'));
        $data = [
            'id' => $member->id,
            'name' => $name,
            'username' => str_slug($name),
            'image_path' => UploadedFile::fake()->image('random.jpg'),
            'email' => str_slug($name).'@email.com',
            'password' => 'test',
            'reg_date' => $regDate->format('Y-m-d H:i:s'),
            'birth_date' => $this->faker->dateTime('-17 years')->format('Y-m-d H:i:s'),
            'bonus_date' => $bonusDate->format('Y-m-d H:i:s'),
            'gender' => $this->faker->randomElement(array_values(Member::GENDERS)),
            'official_id' => $this->faker->creditCardNumber,
            'achievement_id' => $this->achievements->random()->id,
        ];
        $response = $this->actingAs($this->admin)->json('PUT', '/members/'.$member->id, $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($member->id, $actual['id']);
        $this->assertEquals($data['username'], $actual['user']['username']);
        $this->assertEquals($data['name'], $actual['user']['name']);
        $this->assertEquals($data['email'], $actual['user']['email']);
        $this->assertEquals($data['reg_date'], $actual['reg_date']);
        $this->assertEquals($data['birth_date'], $actual['birth_date']);
        $this->assertEquals($data['bonus_date'], $actual['bonus_date']);
        $this->assertEquals($data['achievement_id'], $actual['achievement']['id']);
        $this->assertEquals($this->companies->get(0)->id, $actual['user']['company_id']);
        $this->assertTrue(starts_with($actual['image_path'], '/storage/member/image_path/'));
        Storage::disk('public')->assertExists(str_after($actual['image_path'], '/storage'));   
    }    

    /**
     * As admin I want to receive 404 if I'm trying to update invalid member id
     */
    public function testAdminInvalidUpdate()
    {
        $this->prepare();
        $member = $this->otherMembers->first();
        $response = $this->actingAs($this->admin)->json('PUT', '/members/'.$member->id, []);
        $response->assertStatus(403);        
    }    


    /**
     * As admin I want to delete member from my company
     */
    public function testAdminDelete()
    {
        $this->prepare();
        $member = $this->members->first();
        $user = $member->user;
        $actual = $this->assertResponse('DELETE', '/members/'.$member->id, 200, $this->admin)->json();
        $this->assertEquals($user->username, $actual['user']['username']);
        $this->assertEquals($user->name, $actual['user']['name']);
        $this->assertEquals($user->email, $actual['user']['email']);
        $this->assertEquals($member->official_id, $actual['official_id']);
   
        $actual = $this->assertResponse('GET', '/members/'.$member->id, 404, $this->admin)->json();
    }

    /**
     * As admin I want to receive 404 if I'm trying to delete invalid member id
     */
    public function testAdminInvalidDelete()
    {
        $this->prepare();
        $member = $this->otherMembers->first();
        $response = $this->actingAs($this->admin)->json('DELETE', '/members/'.$member->id, []);
        $response->assertStatus(403);
    }        

    /**
     * As a member I want to see members of my company
     */
    public function testMemberIndex()
    {
        $this->prepare();
        
        $actual = $this->assertResponse('GET', '/members', 200, $this->users->first())->json();
        $this->assertEquals(count($this->members), count($actual['data']));
        
        // test a random sample
        $sample = $this->faker->randomElement($actual['data']);
        $this->assertTrue(array_key_exists('user', $sample));
        $this->assertTrue(array_key_exists('username', $sample['user']));
        $this->assertTrue(array_key_exists('name', $sample['user']));
        $this->assertTrue(array_key_exists('email', $sample['user']));

        $this->assertFalse(array_key_exists('official_id', $sample));
        $this->assertFalse(array_key_exists('tax_id', $sample));
        $this->assertFalse(array_key_exists('address', $sample));
        $this->assertFalse(array_key_exists('address_rt', $sample));
        $this->assertFalse(array_key_exists('address_rw', $sample));
        $this->assertFalse(array_key_exists('address_zipcode', $sample));
        $this->assertFalse(array_key_exists('address_city', $sample));
        $this->assertFalse(array_key_exists('address_state', $sample));
        $this->assertFalse(array_key_exists('country_id', $sample));
        $this->assertFalse(array_key_exists('phone_home', $sample));
        $this->assertFalse(array_key_exists('phone_office', $sample));
        $this->assertFalse(array_key_exists('phone_mobile', $sample));
        $this->assertFalse(array_key_exists('phone_fax', $sample));
        $this->assertFalse(array_key_exists('beneficiary_name', $sample));
        $this->assertFalse(array_key_exists('beneficiary_relation', $sample));
        $this->assertFalse(array_key_exists('bank_account_no', $sample));
        $this->assertFalse(array_key_exists('bank_account_name', $sample));
        $this->assertFalse(array_key_exists('bank_account_city', $sample));
        $this->assertFalse(array_key_exists('bank_account_branch', $sample));
        $this->assertFalse(array_key_exists('bank_id', $sample));
        
    }  
      
    /**
     * As a member I want to see details of member from my company
     */
    public function testMemberShow()
    {
        $this->prepare();
        $user = $this->users->get(0);
        $member = $this->members->get(1);
        $actual = $this->assertResponse('GET', '/members/'.$member->id, 200, $user)->json();
        $this->assertEquals($member->user->username, $actual['user']['username']);
        $this->assertEquals($member->user->name, $actual['user']['name']);
        $this->assertEquals($member->user->email, $actual['user']['email']);        
        $this->assertEquals($member->achievement_id, $actual['achievement']['id']);
        $this->assertEquals($member->reg_date->format('Y-m-d H:i:s'), $actual['reg_date']);

        // must not showing private details
        $this->assertFalse(array_key_exists('official_id', $actual));
        $this->assertFalse(array_key_exists('tax_id', $actual));
        $this->assertFalse(array_key_exists('address', $actual));
        $this->assertFalse(array_key_exists('address_rt', $actual));
        $this->assertFalse(array_key_exists('address_rw', $actual));
        $this->assertFalse(array_key_exists('address_zipcode', $actual));
        $this->assertFalse(array_key_exists('address_city', $actual));
        $this->assertFalse(array_key_exists('address_state', $actual));
        $this->assertFalse(array_key_exists('country_id', $actual));
        $this->assertFalse(array_key_exists('phone_home', $actual));
        $this->assertFalse(array_key_exists('phone_office', $actual));
        $this->assertFalse(array_key_exists('phone_mobile', $actual));
        $this->assertFalse(array_key_exists('phone_fax', $actual));
        $this->assertFalse(array_key_exists('beneficiary_name', $actual));
        $this->assertFalse(array_key_exists('beneficiary_relation', $actual));
        $this->assertFalse(array_key_exists('bank_account_no', $actual));
        $this->assertFalse(array_key_exists('bank_account_name', $actual));
        $this->assertFalse(array_key_exists('bank_account_city', $actual));
        $this->assertFalse(array_key_exists('bank_account_branch', $actual));
        $this->assertFalse(array_key_exists('bank_id', $actual));
        
    }
    
    public function testMemberMe()
    {
        $this->prepare();
        $user = $this->users->get(0);
        $member = $user->member;
        $actual = $this->assertResponse('GET', '/members/me', 200, $user)->json();
        $this->assertEquals($user->username, $actual['user']['username']);
        $this->assertEquals($user->name, $actual['user']['name']);
        $this->assertEquals($user->email, $actual['user']['email']);
        $this->assertEquals($user->company_id, $actual['user']['company']['id']);
        $this->assertEquals($member->official_id, $actual['official_id']);
        $this->assertEquals($member->achievement_id, $actual['achievement']['id']);
        $this->assertEquals($member->reg_date->format('Y-m-d H:i:s'), $actual['reg_date']);
        $this->assertEquals($member->birth_date->format('Y-m-d H:i:s'), $actual['birth_date']);
        $this->assertEquals($member->bonus_date->format('Y-m-d H:i:s'), $actual['bonus_date']);
    }

    public function testAdminDownlines()
    {
        $this->prepare();
        $member = Member::joinUsers()
            ->whereNull('members.parent_id')
            ->where('company_id', $this->admin->company_id)
            ->first();
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/downlines', 200, $this->admin)->json();
        $this->assertEquals(2, count($actual['downlines']));
        $this->assertEquals(2, count($actual['downlines'][0]['downlines']));
    }    
}
