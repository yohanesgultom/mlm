<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Company;
use App\Bank;
use App\BankAccount;

class BankAccountTest extends TestCase
{
    private $companies;
    private $admin;
    private $member;
    private $banks;
    private $data;
    private $otherData;

    public function prepare($count = 5, $otherCount = 2)
    {
        $this->companies = factory(Company::class, 2)->create();
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();

        Bank::insert([
            ['name' => 'BANK ARTHA GRAHA', 'code' => '37'],
            ['name' => 'BANK ARTOS IND', 'code' => '542'],
            ['name' => 'BANK BCA', 'code' => '14'],
            ['name' => 'BANK BCA SYARIAH', 'code' => '536'],
            ['name' => 'BANK BENGKULU', 'code' => '133'],
            ['name' => 'BANK BII MAYBANK', 'code' => '16'],            
        ]);
        $this->banks = Bank::all();

        BankAccount::insert([
            [
                'company_id' => $this->companies->get(0)->id,
                'account_name' => $this->companies->get(0)->name,
                'bank_id' => $this->banks->random()->id,
                'account_no' => $this->faker->creditCardNumber,
            ],
            [
                'company_id' => $this->companies->get(0)->id,
                'account_name' => $this->companies->get(0)->name,
                'bank_id' => $this->banks->random()->id,
                'account_no' => $this->faker->creditCardNumber,
            ],
        ]);
        $this->data = BankAccount::where('company_id', $this->companies->get(0)->id)->get();

        BankAccount::insert([
            [
                'company_id' => $this->companies->get(1)->id,
                'account_name' => $this->companies->get(1)->name,
                'bank_id' => $this->banks->random()->id,
                'account_no' => $this->faker->creditCardNumber,
            ],
        ]);                
        $this->otherData = BankAccount::where('company_id', $this->companies->get(1)->id)->get();        
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/bankAccounts', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }


    public function testAdminStore()
    {
        $this->prepare();
        $data = [
            'account_name' => $this->faker->word,
            'account_no' => $this->faker->creditCardNumber,
            'bank_id' => $this->banks->random()->id,
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/bankAccounts', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['account_name'], $actual['account_name']);
        $this->assertEquals($data['account_no'], $actual['account_no']);
        $this->assertEquals($data['bank_id'], $actual['bank']['id']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }


    public function testAdminShow()
    {
        $this->prepare();
        $data = $this->data->random();
        $actual = $this->assertResponse('GET', '/bankAccounts/'.$data->id, 200, $this->admin)->json();
        $this->assertEquals($data->account_name, $actual['account_name']);
        $this->assertEquals($data->account_no, $actual['account_no']);
        $this->assertEquals($data->bank_id, $actual['bank']['id']);
        $this->assertEquals($data->company_id, $actual['company_id']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $existing = $this->data->random();
        $id = $existing->id;

        // update data
        $data = [
            'account_name' => $this->faker->sentence(),
            'account_no' => $this->faker->creditCardNumber,
            'bank_id' => $this->banks->random()->id,
        ];
        $response = $this->actingAs($this->admin)->json('PUT', 'bankAccounts/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['account_name'], $actual['account_name']);
        $this->assertEquals($data['account_no'], $actual['account_no']);
        $this->assertEquals($data['bank_id'], $actual['bank']['id']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $existing = $this->data->random();
        $id = $existing->id;
        $actual = $this->assertResponse('DELETE', '/bankAccounts/'.$id, 200, $this->admin)->json();
        $this->assertEquals($existing->account_name, $actual['account_name']);
        $this->assertEquals($existing->account_no, $actual['account_no']);
        $this->assertEquals($existing->bank_id, $actual['bank']['id']);
        $this->assertEquals($existing->company_id, $actual['company_id']);
        $actual = $this->assertResponse('GET', '/bankAccounts/'.$id, 404, $this->admin)->json();
    }
    
    /**
     * As a member I want to see bank accounts of my company
     */    
    public function testMemberIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/bankAccounts', 200, $this->member)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }
    
    public function testMemberShow()
    {
        $this->prepare();
        $data = $this->data->random();
        $actual = $this->assertResponse('GET', '/bankAccounts/'.$data->id, 200, $this->member)->json();
        $this->assertEquals($data['account_name'], $actual['account_name']);
        $this->assertEquals($data['account_no'], $actual['account_no']);
        $this->assertEquals($data['bank_id'], $actual['bank']['id']);
        $this->assertEquals($data->company_id, $actual['company_id']);

        $data = $this->otherData->random();
        $actual = $this->assertResponse('GET', '/bankAccounts/'.$data->id, 404, $this->member)->json();
    }    

    public function testIndexByCompany()
    {
        $this->prepare();
        $company_id = $this->companies->random()->id;
        $bankAccounts = BankAccount::where('company_id', $company_id)->orderBy('id', 'asc')->get()->toArray();
        $actuals = $this->assertResponse('GET', '/companies/'.$company_id.'/bankAccounts', 200)->json();
        $this->assertEquals(count($bankAccounts), $actuals['total']);
        for ($i = 0; $i < count($bankAccounts); $i++) {
            $data = $bankAccounts[$i];
            $actual = $actuals['data'][$i];
            $this->assertEquals($data['account_name'], $actual['account_name']);
            $this->assertEquals($data['account_no'], $actual['account_no']);
            $this->assertEquals($data['bank_id'], $actual['bank']['id']);
        }
    }
}
