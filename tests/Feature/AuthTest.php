<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Session;
use App\User;

class AuthTest extends TestCase
{    
    private $company;
    private $superadmin;
    private $admin;
    private $member;

    public function prepare()
    {
        $this->company = factory(\App\Company::class, 1)->create()->first();

        $this->superadmin = factory(User::class, 1)->states('superadmin')->create([
            'username' => 'superadmin',
            'email' => 'superadmin@email.com',
            'password' => 'superadmin'
        ])->first();
        
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'username' => 'admin',
            'email' => 'admin@email.com',
            'password' => 'admin',
            'company_id' => $this->company->id,
        ])->first();

        $this->member = factory(User::class, 1)
            ->states('member')
            ->create([
                'username' => 'member',
                'email' => 'member@email.com',
                'password' => 'member',
                'company_id' => $this->company->id,
            ])
            ->each(function ($u) {
                factory(\App\Member::class, 1)->create([
                    'user_id' => $u->id,
                    'name_printed' => $u->name,
                ]);
            })->first();
    }

    public function testLogin()
    {
        $this->prepare();
        Session::start();

        // login
        $data = [
            'username' => $this->superadmin->username,
            'password' => 'superadmin',
        ];
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/login', $data);

        $this->assertStatus(200, $response);
        $actual = $response->json();

        $this->assertEquals($this->superadmin->email, $actual['email']);
        $this->assertEquals($this->superadmin->role, $actual['role']);
        $this->assertEquals('SUPERADMIN', $actual['role_name']);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/logout', $data);        
        $this->assertStatus(200, $response);

        // admin
        $data = [
            'username' => $this->admin->username,
            'password' => 'admin',
        ];
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/login', $data);

        $this->assertStatus(200, $response);
        $actual = $response->json();

        $this->assertEquals($this->admin->username, $actual['username']);
        $this->assertEquals($this->admin->email, $actual['email']);
        $this->assertEquals($this->admin->role, $actual['role']);
        $this->assertEquals('ADMIN', $actual['role_name']);        

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/logout', $data);        
        $this->assertStatus(200, $response);

        // member
        $data = [
            'username' => $this->member->username,
            'password' => 'member',
        ];
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/login', $data);

        $this->assertStatus(200, $response);
        $actual = $response->json();

        $this->assertEquals($this->member->username, $actual['username']);
        $this->assertEquals($this->member->email, $actual['email']);
        $this->assertEquals($this->member->role, $actual['role']);
        $this->assertEquals('MEMBER', $actual['role_name']);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'X-CSRF-TOKEN' => csrf_token(),
        ])->json('POST', '/logout', $data);        
        $this->assertStatus(200, $response);        
    }

    public function testUnsecuredMemberAccess()
    {
        $this->assertResponse('GET', '/members/me', 401);
    }

    public function testSecuredMemberAccess()
    {
        $company = factory(\App\Company::class, 1)->create()->first();

        $user = factory(User::class, 1)
            ->states('member')
            ->create()
            ->each(function ($u) {
                factory(\App\Member::class, 1)->create([
                    'user_id' => $u->id,
                    'name_printed' => $u->name,
                ]);
            })->first();
        
        $response = $this->assertResponse('GET', '/members/me', 200, $user);

        $actual = $response->json();
        $this->assertEquals($actual['id'], $user->member->id);
        $this->assertEquals($actual['user']['username'], $user->username);
        $this->assertEquals($actual['user']['name'], $user->name);
        $this->assertEquals($actual['user']['email'], $user->email);
    }

    public function testUnauthorizedMemberAccess()
    {

        // member should not be able to access admin functions
        $company = factory(\App\Company::class, 1)->create()->first();
        $member = factory(User::class, 1)
            ->states('member')
            ->create()
            ->each(function ($u) {
                factory(\App\Member::class, 1)->create([
                    'user_id' => $u->id,
                    'name_printed' => $u->name,
                ]);
            })->first();
        $this->assertResponse('POST', '/religions', 403, $member);
        
        // superadmin doesn't have company nor member
        $superadmin = factory(User::class, 1)->states('superadmin')->create([
            'username' => 'superadmin',
            'email' => 'superadmin@email.com',
            'password' => 'superadmin'
        ])->first();
        $this->assertResponse('GET', '/members/me', 404, $superadmin);
    }
    
   
}
