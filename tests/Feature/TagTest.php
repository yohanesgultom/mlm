<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Company;
use App\Tag;

class TagTest extends TestCase
{
    private $companies;
    private $admin;
    private $member;
    private $data;
    private $otherData;

    public function prepare($count = 5, $otherCount = 2)
    {
        $this->companies = factory(Company::class, 2)->create();
        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        
        $this->data = factory(Tag::class, 10)->create(['company_id' => $this->companies->get(0)->id]);
        $this->otherData = factory(Tag::class, 10)->create(['company_id' => $this->companies->get(1)->id]);        
    }
    
    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/tags', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }

    public function testAdminIndexSearch()
    {
        $this->prepare();
        $searchName = 'e';
        $expected = Tag::where('company_id', $this->admin->company_id)
            ->where('name', 'LIKE', '%'.$searchName.'%')
            ->get();
        $data = ['name' => $searchName];
        $response = $this->actingAs($this->admin)->json('GET', '/tags', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);
    }

    public function testAdminStore()
    {
        $this->prepare();
        $data = [
            'name' => $this->faker->word,
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/tags', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }


    public function testAdminShow()
    {
        $this->prepare();
        $data = $this->data->first();
        $actual = $this->assertResponse('GET', '/tags/'.$data->id, 200, $this->admin)->json();
        $this->assertEquals($data->name, $actual['name']);
        $this->assertEquals($data->company_id, $actual['company_id']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $existing = $this->data->first();
        $id = $existing->id;

        // update data
        $data = [
            'name' => $this->faker->sentence(),
        ];
        $response = $this->actingAs($this->admin)->json('PUT', 'tags/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $existing = $this->data->first();
        $id = $existing->id;
        $actual = $this->assertResponse('DELETE', '/tags/'.$id, 200, $this->admin)->json();
        $this->assertEquals($existing->name, $actual['name']);
        $this->assertEquals($existing->company_id, $actual['company_id']);
        $actual = $this->assertResponse('GET', '/tags/'.$id, 404, $this->admin)->json();
    }
    
    /**
     * As a member I want to see products of my company
     */    
    public function testMemberIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/tags', 200, $this->member)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }
    
}
