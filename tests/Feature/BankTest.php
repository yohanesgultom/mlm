<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Bank;
use DB;

class BankTest extends TestCase
{
    private $admin;
    private $data;

    protected function prepare()
    {
        $this->admin = factory(User::class, 1)->states('superadmin')->create()->first();
        $this->data = [
            ['name' => 'BANK ARTHA GRAHA', 'code' => '37'],
            ['name' => 'BANK ARTOS IND', 'code' => '542'],
            ['name' => 'BANK BCA', 'code' => '14'],
            ['name' => 'BANK BCA SYARIAH', 'code' => '536'],
            ['name' => 'BANK BENGKULU', 'code' => '133'],
            ['name' => 'BANK BII MAYBANK', 'code' => '16'],
        ];
        DB::table('banks')->insert($this->data);
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/banks', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }

    public function testAdminIndexSearch()
    {
        $this->prepare();
        $searchName = $this->faker->randomLetter;
        $expected = Bank::where('name', 'LIKE', '%'.$searchName.'%')->get();
        $data = ['name' => $searchName];
        $response = $this->actingAs($this->admin)->json('GET', '/banks', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($expected), $actual['total']);
    }

    public function testAdminStore()
    {
        $this->prepare();
        $data = ['name' => $this->faker->word, 'code' => $this->faker->randomNumber(3)];
        $response = $this->actingAs($this->admin)->json('POST', '/banks', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['code'], $actual['code']);
        
    }


    public function testAdminShow()
    {
        $this->prepare();
        $id = DB::table('banks')->where('name', $this->data[0]['name'])->value('id');
        $actual = $this->assertResponse('GET', '/banks/'.$id, 200, $this->admin)->json();
        $this->assertEquals($this->data[0]['name'], $actual['name']);
        $this->assertEquals($this->data[0]['code'], $actual['code']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $id = DB::table('banks')->where('name', $this->data[0]['name'])->value('id');
        $data = ['id' => $id, 'name' => $this->faker->word(), 'code' => $this->faker->randomNumber(3)];
        $response = $this->actingAs($this->admin)->json('PUT', 'banks/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['code'], $actual['code']);        
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $data = ['id' => null, 'name' => $this->data[0]['name'], 'code' => $this->data[0]['code']];
        $data['id'] = DB::table('banks')->where('name', $data['name'])->value('id');
        $actual = $this->assertResponse('DELETE', '/banks/'.$data['id'], 200, $this->admin)->json();
        $this->assertEquals($data['name'], $actual['name']); 
        $this->assertEquals($data['code'], $actual['code']); 
        $actual = $this->assertResponse('GET', '/banks/'.$data['id'], 404, $this->admin)->json();
    }    

}
