<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Company;
use App\User;
use App\Member;
use App\Wallet;
use App\Registration;
use App\Order;
use DB;

class RegistrationTest extends TestCase
{   
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'PackagesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'RegistrationsTableSeeder']);
        $this->company = Company::first();
        $this->admin = $this->company->users()->where('role', User::ROLES['ADMIN'])->first();
        $this->user = $this->company->users()->where('role', User::ROLES['MEMBER'])->first();
        $this->member_type = $this->company->member_types->random();
        $this->packages = $this->member_type->packages;
    } 

    public function testStatuses()
    {
        $actual = $this->assertResponse('GET', "/registrations/statuses", 200)->json();
        $expected = array_flip(Registration::STATUSES);
        $this->assertEquals($expected, $actual);
    }

    public function testAdminIndex()
    {
        $expected = $this->company->registrations;
        $actual = $this->assertResponse('GET', "/registrations", 200, $this->admin)->json();
        $this->assertEquals(count($expected), $actual['total']);
    }

    public function testAdminStore()
    {
        $package = $this->packages->random();
        $sponsor = Member::with('wallets')->joinUsers()
            ->where('users.company_id', $this->company->id)
            ->first();
        $sponsor_wallet = $sponsor->wallets->random();
        // make sure there is enough balance
        $balance_before = $sponsor_wallet->balance;
        $sponsor_wallet->balance += $package->price;
        $sponsor_wallet->save();
        $data = factory(Registration::class, 1)
            ->make([
                'birth_date' => $this->faker->date(),
                'gender' => $this->faker->numberBetween(0, 1),
                'bank_account_no' => $this->faker->creditCardNumber,
                'bank_account_name' => $this->faker->randomElement(['BCA', 'Bank Mandiri', 'BNI', 'BRI', 'CIMB', 'HSBC']),
                'company_id' => $this->company->id,
                'sponsor_wallet_id' => $sponsor_wallet->id,
                'package_id' => $package->id,
                'status' => Registration::STATUSES['COMPLETED'],
            ])
            ->first()
            ->toArray();
        $response = $this->actingAs($this->admin)->json('POST', "/registrations", $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $member = Member::find($actual['member_id']);
        $this->assertEquals($data['name'], $member->user->name);
        $this->assertEquals($data['email'], $member->user->email);
        $this->assertEquals($data['birth_date'], $member->birth_date->toDateString());
        $this->assertEquals($data['gender'], $member->gender);
        $this->assertEquals($data['bank_account_no'], $member->bank_account_no);
        $this->assertEquals($data['bank_account_name'], $member->bank_account_name);
        $this->assertEquals($data['company_id'], $member->user->company_id);
        $this->assertEquals($sponsor->id, $member->sponsor_id);
        $this->assertEquals($package->member_type_id, $member->member_type_id);
        $this->assertEquals($package->bonus_value, $member->reg_bv);
        $wallet = $member->wallets->first();
        $this->assertNotNull($wallet);
        $order = Order::find($actual['order_id']);
        $this->assertNotNull($order);
        $transactions = $order->transactions;
        $package_products = $package->package_products;
        $this->assertEquals(count($package_products), count($transactions));
        $this->assertEquals($package_products->pluck('product_id', 'qty')->all(), $transactions->pluck('product_id', 'qty')->all());
        $this->assertEquals(0, $transactions->sum('bonus'));
        $this->assertEquals(0, $transactions->sum('points'));
        $sponsor_wallet = Wallet::find($actual['sponsor_wallet_id']);
        $this->assertEquals($balance_before, $sponsor_wallet->balance);
    }

    public function testAdminShow()
    {
        $registration = Registration::where('company_id', $this->company->id)
            ->inRandomOrder()->first();
        $actual = $this->assertResponse('GET', "/registrations/{$registration->id}", 200, $this->admin)->json();
        $this->assertEquals($registration->toArray(), $actual);
    }
 
    public function testAdminUpdate()
    {
        $registration = Registration::where('company_id', $this->company->id)
        ->inRandomOrder()->first();
        $package = $this->packages->random();
        $sponsor = Member::with('wallets')->joinUsers()
            ->where('users.company_id', $this->company->id)
            ->first();
        $sponsor_wallet = $sponsor->wallets->random();
        // make sure there is enough balance
        $balance_before = $sponsor_wallet->balance;
        $sponsor_wallet->balance += $package->price;
        $sponsor_wallet->save();
        $data = [
            'birth_date' => $this->faker->date(),
            'gender' => $this->faker->numberBetween(0, 1),
            'bank_account_no' => $this->faker->creditCardNumber,
            'bank_account_name' => $this->faker->randomElement(['BCA', 'Bank Mandiri', 'BNI', 'BRI', 'CIMB', 'HSBC']),
            'package_id' => $package->id,
            'sponsor_wallet_id' => $sponsor_wallet->id,
            'status' => Registration::STATUSES['COMPLETED'],
        ];
        $response = $this->actingAs($this->admin)->json('PUT', "/registrations/{$registration->id}", $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $member = Member::find($actual['member_id']);
        $this->assertEquals($sponsor->id, $member->sponsor_id);
        $this->assertEquals($data['package_id'], $actual['package_id']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals($data['birth_date'], $member->birth_date->toDateString());
        $this->assertEquals($data['gender'], $member->gender);
        $this->assertEquals($data['bank_account_no'], $member->bank_account_no);
        $this->assertEquals($data['bank_account_name'], $member->bank_account_name);
        $this->assertEquals($package->member_type_id, $member->member_type_id);
        $this->assertEquals($package->bonus_value, $member->reg_bv);        
        $wallet = $member->wallets->first();
        $this->assertNotNull($wallet);
        $order = Order::find($actual['order_id']);
        $this->assertNotNull($order);
        $transactions = $order->transactions;
        $package_products = $package->package_products;
        $this->assertEquals(count($package_products), count($transactions));
        $this->assertEquals($package_products->pluck('product_id', 'qty')->all(), $transactions->pluck('product_id', 'qty')->all());
        $this->assertEquals(0, $transactions->sum('bonus'));
        $this->assertEquals(0, $transactions->sum('points'));        
        $sponsor_wallet = Wallet::find($actual['sponsor_wallet_id']);
        $this->assertEquals($balance_before, $sponsor_wallet->balance);
    }

    public function testAdminDelete()
    {
        $registration = Registration::where('company_id', $this->company->id)
            ->inRandomOrder()->first();
        $actual = $this->assertResponse('DELETE', "/registrations/{$registration->id}", 200, $this->admin)->json();
        $this->assertEquals($registration->toArray(), $actual);
        $this->assertResponse('GET', "/registrations/{$registration->id}", 404, $this->admin)->json();
    }
}
