<?php

namespace Tests\Feature;

use Tests\MemberTestCase;
use App\Attachment;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

class AttachmentTest extends MemberTestCase
{
    public function testTypes()
    {
        $actual = $this->assertResponse('GET', '/attachments/types', 200)->json();
        $this->assertEquals(array_flip(Attachment::TYPES), $actual);
    }

    public function testAdminAttachments()
    {
        $this->prepare();
        $member = $this->members->random();
        $attachments = $member->attachments;
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/attachments', 200, $this->admin)->json();
        $this->assertEquals(count($attachments), count($actual['data']));
        for($i = 0; $i < count($attachments); $i++) {
            $this->assertEquals($attachments->get($i)->id, $actual['data'][$i]['id']);
            $this->assertEquals($attachments->get($i)->filename, $actual['data'][$i]['filename']);
            $this->assertEquals($attachments->get($i)->type, $actual['data'][$i]['type']);
        }
    }

    public function testAdminAttachmentDownload()
    {
        $this->prepare();
        $member = $this->members->random();
        $user = $member->user;
        $attachment = $member->attachments->random();
        $fileExpected = new File($this->resourceDir.DIRECTORY_SEPARATOR.$attachment->filename);
        $actual = $this->actingAs($this->admin)->json('GET', '/members/'.$member->id.'/attachments/'.$attachment->id.'/download');
        $fileActual = $actual->getFile();
        $this->assertTrue($actual->isOk());
        $this->assertEquals($fileExpected->getMimeType(), $fileActual->getMimeType());
        $this->assertEquals($fileExpected->getSize(), $fileActual->getSize());
    }

    public function testAdminAttachmentStore()
    {
        $this->prepare();
        $member = $this->members->random();
        $file = new File($this->resourceDir.DIRECTORY_SEPARATOR.'ktp_blangko.jpg');
        $files = [
            'attachment' => new UploadedFile(
                $file->getRealPath(),
                $file->getFilename()
            ),
        ];
        $data = [            
            'type' => $this->faker->randomElement(array_values(Attachment::TYPES)),
            'desc' => $this->faker->sentence(),
        ];
        $response = $this->actingAs($this->admin)->call('POST', '/members/'.$member->id.'/attachments', $data, [], $files);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['type'], $actual['type']);
        $this->assertEquals($data['desc'], $actual['desc']);
        $this->assertEquals($file->getFilename(), $actual['filename']);
        $this->assertEquals($member->id, $actual['member_id']);
        
        $attachment = Attachment::find($actual['id']);
        Storage::disk('test')->assertExists($attachment->path);
    }

    public function testAdminAttachmentShow()
    {
        $this->prepare();
        $member = $this->members->random();
        $attachment = $member->attachments->random();
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/attachments/'.$attachment->id, 200, $this->admin)->json();
        $this->assertEquals($attachment->id, $actual['id']);
        $this->assertEquals($attachment->type, $actual['type']);
        $this->assertEquals($attachment->desc, $actual['desc']);
        $this->assertEquals($attachment->filename, $actual['filename']);
        $this->assertEquals($attachment->mime, $actual['mime']);
    }

    public function testAdminAttachmentDelete()
    {
        $this->prepare();
        $member = $this->members->random();
        $attachment = $member->attachments->random();

        $actual = $this->assertResponse('DELETE', '/members/'.$member->id.'/attachments/'.$attachment->id, 200, $this->admin)->json();
        $this->assertEquals($attachment->id, $actual['id']);
        $this->assertEquals($attachment->desc, $actual['desc']);
        $this->assertEquals($attachment->type, $actual['type']);
        $this->assertEquals($attachment->filename, $actual['filename']);
        $this->assertEquals($attachment->mime, $actual['mime']);

        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/attachments/'.$attachment->id, 404, $this->admin)->json();

        Storage::disk('test')->assertMissing($attachment->path);
    }

}
