<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Company;
use App\User;
use App\Warehouse;
use App\WarehouseStockLog;
use Carbon\Carbon;

class WarehouseStockLogTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);
        $company = Company::where('name', 'GreenVit International')->first();
        $this->company = $company;
        $this->company_id = $company->id;
        $this->admin = User::where('username', 'admin')->where('company_id', $this->company_id)->first();
    }

    protected function assertArrayEquals($data, $actual)
    {
        $this->assertEquals($data['product_id'], $actual['product_id']);
        $this->assertEquals($data['warehouse_id'], $actual['warehouse_id']);
        $this->assertEquals($data['qty_before'], $actual['qty_before']);
        $this->assertEquals($data['process_date'], $actual['process_date']);
        $this->assertEquals($data['qty_in'], $actual['qty_in']);
        $this->assertEquals($data['qty_out'], $actual['qty_out']);
        $this->assertEquals($data['exp_date'], $actual['exp_date']);
    }    

    public function testAdminIndex()
    {
        $warehouse = $this->company->warehouses->random();
        $logs = $warehouse->stockLogs;
        $actual = $this->assertResponse('GET', '/warehouses/'.$warehouse->id.'/stockLogs', 200, $this->admin)->json();
        $this->assertEquals(count($logs), $actual['total']);
    }
    
    public function testAdminShow()
    {
        $warehouse = $this->company->warehouses->random();
        $log = $warehouse->stockLogs->random();
        $actual = $this->assertResponse('GET', '/warehouses/'.$warehouse->id.'/stockLogs/'.$log->id, 200, $this->admin)->json();
        $this->assertEquals($log->id, $actual['id']);
        $this->assertArrayEquals($log->toArray(), $actual);
    }

    public function testAdminStore()
    {
        $warehouse = $this->company->warehouses->random();
        $product = $this->company->productTypes->random()->products->random();
        $faker = $this->faker;
        $qty = $faker->numberBetween(10, 100);
        $exp_date = Carbon::instance($faker->dateTime('+3 year'));
        $data = [
            'warehouse_id' => $warehouse->id,
            'product_id' => $product->id,
            'process_date' => Carbon::now()->toDateTimeString(),
            'qty_before' => 0,
            'qty_in' => $qty * 2,
            'qty_out' => 0,
            'exp_date' => $exp_date->toDateTimeString(),
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/warehouses/'.$warehouse->id.'/stockLogs', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertArrayEquals($data, $actual);
    }
    
    // public function testAdminUpdate()
    // {
    //     $warehouse = $this->company->warehouses->random();
    //     $log = $warehouse->stockLogs->random();
    //     $log->qty_in = 10;
    //     $log->qty_out = 0;
    //     $data = $log->toArray();
    //     $response = $this->actingAs($this->admin)->json('PUT', '/warehouses/'.$warehouse->id.'/stockLogs/'.$log->id, $data);
    //     $this->assertStatus(200, $response);
    //     $actual = $response->json();
    //     $this->assertEquals($log->id, $actual['id']);
    //     $this->assertArrayEquals($data, $actual);
    // }    
    
    // public function testDelete()
    // {
    //     $warehouse = $this->company->warehouses->random();
    //     $log = $warehouse->stockLogs->random();
    //     $actual = $this->assertResponse('DELETE', '/warehouses/'.$warehouse->id.'/stockLogs/'.$log->id, 200, $this->admin)->json();
    //     $this->assertEquals($log->id, $actual['id']);
    //     $this->assertArrayEquals($log->toArray(), $actual);
    //     $actual = $this->assertResponse('GET', '/warehouses/'.$warehouse->id.'/stockLogs/'.$log->id, 404, $this->admin)->json();
    // }

}
