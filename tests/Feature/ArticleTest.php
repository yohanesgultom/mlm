<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use App\Article;
use App\Company;
use App\User;

class ArticleTest extends TestCase
{    
    private $admin;
    private $articles;

    public function prepare()
    {
        Storage::fake('public');
        $companies = factory(Company::class, 2)->create();        
        $this->companies = $companies;

        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();

        $this->articles = factory(Article::class, 10)->create([
            'created_by' => $this->admin->id,
            'company_id' => $this->companies->get(0)->id,
        ]);

        $this->otherArticles = factory(Article::class, 5)->create([
            'created_by' => $this->admin->id,
            'company_id' => $this->companies->get(1)->id,
        ]);

    }    

    public function testTypes()
    {
        $actual = $this->assertResponse('GET', '/articles/types', 200)->json();
        $this->assertEquals(array_flip(Article::TYPES), $actual);
    }

    public function testStatuses()
    {
        $actual = $this->assertResponse('GET', '/articles/statuses', 200)->json();
        $this->assertEquals(array_flip(Article::STATUSES), $actual);
    }

    public function testIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/companies/'.$this->companies->get(0)->id.'/articles', 200)->json();
        $this->assertEquals(count($this->articles), $actual['total']);

        $actual = $this->assertResponse('GET', '/companies/'.$this->companies->get(1)->id.'/articles', 200)->json();
        $this->assertEquals(count($this->otherArticles), $actual['total']);
    }

    public function testMemberIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/companies/'.$this->companies->get(0)->id.'/articles', 200, $this->member)->json();
        $this->assertEquals(count($this->articles), $actual['total']);
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/companies/'.$this->companies->get(0)->id.'/articles', 200, $this->admin)->json();
        $this->assertEquals(count($this->articles), $actual['total']);
    }


    public function testAdminStore()
    {
        $this->prepare();
        $data = factory(Article::class, 1)->make([
            'image_path' => UploadedFile::fake()->image('random.jpg'),
            'created_by' => $this->admin->id,
            'company_id' => $this->companies->get(0)->id,
        ])->first()->toArray();
        $response = $this->actingAs($this->admin)->json('POST', '/companies/'.$this->companies->get(0)->id.'/articles', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['title'], $actual['title']);
        $this->assertEquals(str_slug($data['title']), $actual['slug']);
        $this->assertEquals($data['html'], $actual['html']);
        $this->assertEquals($data['publish_date'], $actual['publish_date']);
        $this->assertEquals($data['type'], $actual['type']);
        $this->assertEquals(array_flip(Article::TYPES)[$data['type']], $actual['type_name']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Article::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
        $this->assertEquals($this->admin->id, $actual['created_by']);
        $this->assertTrue(starts_with($actual['image_path'], '/storage/article/image_path/'));
        Storage::disk('public')->assertExists(str_after($actual['image_path'], '/storage'));
    }

    public function testShow()
    {
        $this->prepare();
        $data = $this->articles->random();
        $actual = $this->assertResponse('GET', '/companies/'.$this->companies->get(0)->id.'/articles/'.$data->id, 200)->json();
        $this->assertEquals($data->id, $actual['id']);
        $this->assertEquals($data->title, $actual['title']);
        $this->assertEquals($data->slug, $actual['slug']);
        $this->assertEquals($data->html, $actual['html']);
        $this->assertEquals($data->type, $actual['type']);
        $this->assertEquals($data->status, $actual['status']);
        $this->assertEquals($data->publish_date->format('Y-m-d H:i:s'), $actual['publish_date']);
        $this->assertEquals($data->created_by, $actual['created_by']);
        $this->assertEquals($data->company_id, $actual['company_id']);
    }

    public function testAdminShow()
    {
        $this->prepare();
        $data = $this->articles->random();
        $actual = $this->assertResponse('GET', '/companies/'.$this->companies->get(0)->id.'/articles/'.$data->id, 200, $this->admin)->json();
        $this->assertEquals($data->id, $actual['id']);
        $this->assertEquals($data->title, $actual['title']);
        $this->assertEquals($data->slug, $actual['slug']);
        $this->assertEquals($data->html, $actual['html']);
        $this->assertEquals($data->type, $actual['type']);
        $this->assertEquals($data->status, $actual['status']);
        $this->assertEquals($data->publish_date->format('Y-m-d H:i:s'), $actual['publish_date']);
        $this->assertEquals($data->created_by, $actual['created_by']);
        $this->assertEquals($data->company_id, $actual['company_id']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $existing = $this->articles->random();
        $id = $existing->id;

        // update data
        $data = factory(Article::class, 1)->make([
            'image_path' => UploadedFile::fake()->image('random.jpg'),
            'created_by' => $this->admin->id,
            'company_id' => $this->companies->get(0)->id,
        ])->first()->toArray();
        $response = $this->actingAs($this->admin)->json('PUT', '/companies/'.$this->companies->get(0)->id.'/articles/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['title'], $actual['title']);
        $this->assertEquals(str_slug($data['title']), $actual['slug']);
        $this->assertEquals($data['html'], $actual['html']);
        $this->assertEquals($data['publish_date'], $actual['publish_date']);
        $this->assertEquals($data['type'], $actual['type']);
        $this->assertEquals(array_flip(Article::TYPES)[$data['type']], $actual['type_name']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Article::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals($this->admin->company_id, $actual['company_id']);
        $this->assertEquals($this->admin->id, $actual['created_by']);
        $this->assertTrue(starts_with($actual['image_path'], '/storage/article/image_path/'));
        Storage::disk('public')->assertExists(str_after($actual['image_path'], '/storage'));
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $data = $this->articles->first();
        $id = $data->id;
        $actual = $this->assertResponse('DELETE', '/companies/'.$this->companies->get(0)->id.'/articles/'.$id, 200, $this->admin)->json();
        $this->assertEquals($data->id, $actual['id']);
        $this->assertEquals($data->title, $actual['title']);
        $this->assertEquals($data->slug, $actual['slug']);
        $this->assertEquals($data->html, $actual['html']);
        $this->assertEquals($data->type, $actual['type']);
        $this->assertEquals($data->status, $actual['status']);
        $this->assertEquals($data->publish_date->format('Y-m-d H:i:s'), $actual['publish_date']);
        $this->assertEquals($data->created_by, $actual['created_by']);
        $this->assertEquals($data->company_id, $actual['company_id']);

        $actual = $this->assertResponse('GET', '/companies/'.$this->companies->get(0)->id.'/articles/'.$id, 404, $this->admin)->json();
    }    

}
