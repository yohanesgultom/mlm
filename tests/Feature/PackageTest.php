<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Company;
use App\User;
use App\Package;
use DB;

class PackageTest extends TestCase
{   
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'PackagesTableSeeder']);
        $this->company = Company::first();
        $this->admin = $this->company->users()->where('role', User::ROLES['ADMIN'])->first();
        $this->user = $this->company->users()->where('role', User::ROLES['MEMBER'])->first();
        $this->member_type = $this->company->member_types->random();
    } 

    public function testAdminIndex()
    {
        $expected = $this->member_type->packages;
        $actual = $this->assertResponse(
            'GET',
            "/memberTypes/{$this->member_type->id}/packages",
            200,
            $this->admin
        )->json();
        $this->assertEquals(count($expected), $actual['total']);
        $this->assertNotNull($actual['data'][0]['package_products']);
    }

    public function testAdminStore()
    {
        $products = $this->company->productTypes->random()
            ->products()->inRandomOrder()->take(3)
            ->pluck('id')
            ->sort()
            ->values()
            ->all();
        $package_products = array_map(function ($product_id) {
            return ['product_id' => $product_id, 'qty' => mt_rand(1, 3)];
        }, $products);
        $data = [
            'name' => $this->member_type->name.' '.$this->faker->word,
            'package_products' => $package_products,
        ];        
        $response = $this->actingAs($this->admin)->json('POST', "/memberTypes/{$this->member_type->id}/packages", $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();        
        $this->assertEquals($data['name'], $actual['name']);
        $actual_package_products = array_map(function ($item) {
            return ['product_id' => $item['product_id'], 'qty' => $item['qty']];
        }, $actual['package_products']);
        $this->assertEquals($package_products, $actual_package_products);
        $product_data = DB::table('products')->whereIn('id', $products)->select('id', 'base_price')->get();
        $expected_price = 0;
        foreach ($package_products as $pp) {
            $p = $product_data->where('id', $pp['product_id'])->first();
            $expected_price += ($p->base_price * $pp['qty']);
        }
        $this->assertEquals($expected_price, $actual['price']);
    }

    public function testAdminShow()
    {
        $package = $this->member_type->packages()->with('package_products.product')->inRandomOrder()->first();
        $actual = $this->assertResponse('GET', "/memberTypes/{$this->member_type->id}/packages/{$package->id}", 200, $this->admin)->json();
        $data = $package->toArray();
        $this->assertEquals($data, $actual);
    }
 
    public function testAdminUpdate()
    {
        $package = $this->member_type->packages()->inRandomOrder()->first();
        $products = $this->company->productTypes->random()
            ->products()->inRandomOrder()->take(3)
            ->pluck('id')
            ->sort()
            ->values()
            ->all();
        $package_products = array_map(function ($product_id) {
            return ['product_id' => $product_id, 'qty' => mt_rand(1, 3)];
        }, $products);
        $data = $package->toArray();
        $data['name'] = $this->member_type->name.' '.$this->faker->word;
        $data['package_products'] = $package_products;
        $response = $this->actingAs($this->admin)->json('PUT', "/memberTypes/{$this->member_type->id}/packages/{$package->id}", $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        $actual_package_products = array_map(function ($item) {
            return ['product_id' => $item['product_id'], 'qty' => $item['qty']];
        }, $actual['package_products']);
        $this->assertEquals($package_products, $actual_package_products);
        $product_data = DB::table('products')->whereIn('id', $products)->select('id', 'base_price')->get();
        $expected_price = 0;
        foreach ($package_products as $pp) {
            $p = $product_data->where('id', $pp['product_id'])->first();
            $expected_price += ($p->base_price * $pp['qty']);
        }
        $this->assertEquals($expected_price, $actual['price']);
    }

    public function testAdminDelete()
    {
        $package = $this->member_type->packages()->inRandomOrder()->first();
        $actual = $this->assertResponse(
            'DELETE',
            "/memberTypes/{$this->member_type->id}/packages/{$package->id}",
            200,
            $this->admin
        )->json();
        $data = $package->toArray();
        $this->assertEquals($data, $actual);
        $actual = $this->assertResponse(
            'GET',
            "/memberTypes/{$this->member_type->id}/packages/{$package->id}",
            404,
            $this->admin
        )->json();
    }
}
