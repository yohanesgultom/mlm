<?php

namespace Tests\Feature;

use Tests\MemberTestCase;
use App\PairingCounter;

class PairingCounterTest extends MemberTestCase
{

    public function testAdminPairingCounters()
    {
        $this->prepare();
        $member = $this->members->random();
        $pairingCounters = $member->pairingCounters;
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/pairingCounters', 200, $this->admin)->json();
        $this->assertEquals(count($pairingCounters), count($actual['data']));
        for($i = 0; $i < count($pairingCounters); $i++) {
            $this->assertEquals($pairingCounters->get($i)->id, $actual['data'][$i]['id']);
            $this->assertEquals($pairingCounters->get($i)->process_date->format('Y-m-d H:i:s'), $actual['data'][$i]['process_date']);
            $this->assertEquals($pairingCounters->get($i)->count_left, $actual['data'][$i]['count_left']);
            $this->assertEquals($pairingCounters->get($i)->count_mid, $actual['data'][$i]['count_mid']);
            $this->assertEquals($pairingCounters->get($i)->count_right, $actual['data'][$i]['count_right']);
        }
    }

    public function testMemberPairingCounters()
    {
        $this->prepare();
        $user = $this->users->get(0);
        $member = $user->member;
        $pairingCounters = $member->pairingCounters;
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/pairingCounters', 200, $user)->json();
        $this->assertEquals(count($pairingCounters), count($actual['data']));
        for($i = 0; $i < count($pairingCounters); $i++) {
            $this->assertEquals($pairingCounters->get($i)->id, $actual['data'][$i]['id']);
            $this->assertEquals($pairingCounters->get($i)->process_date->format('Y-m-d H:i:s'), $actual['data'][$i]['process_date']);
            $this->assertEquals($pairingCounters->get($i)->count_left, $actual['data'][$i]['count_left']);
            $this->assertEquals($pairingCounters->get($i)->count_mid, $actual['data'][$i]['count_mid']);
            $this->assertEquals($pairingCounters->get($i)->count_right, $actual['data'][$i]['count_right']);
        }

        // must not be able to see other member's pairingCounter
        $otherUser = $this->users->get(1);
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/pairingCounters', 403, $otherUser)->json();
    }

    public function testAdminPairingCounterShow()
    {
        $this->prepare();
        $member = $this->members->random();
        $pairingCounter = $member->pairingCounters->random();
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/pairingCounters/'.$pairingCounter->id, 200, $this->admin)->json();
        $this->assertEquals($pairingCounter->id, $actual['id']);
        $this->assertEquals($pairingCounter->process_date->format('Y-m-d H:i:s'), $actual['process_date']);
        $this->assertEquals($pairingCounter->count_left, $actual['count_left']);
        $this->assertEquals($pairingCounter->count_mid, $actual['count_mid']);
        $this->assertEquals($pairingCounter->count_right, $actual['count_right']);
    }

    public function testMemberPairingCounterShow()
    {
        $this->prepare();
        $member = $this->members->random();
        $user = $member->user;
        $pairingCounter = $member->pairingCounters->random();
        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/pairingCounters/'.$pairingCounter->id, 200, $user)->json();
        $this->assertEquals($pairingCounter->id, $actual['id']);
        $this->assertEquals($pairingCounter->process_date->format('Y-m-d H:i:s'), $actual['process_date']);
        $this->assertEquals($pairingCounter->count_left, $actual['count_left']);
        $this->assertEquals($pairingCounter->count_mid, $actual['count_mid']);
        $this->assertEquals($pairingCounter->count_right, $actual['count_right']);
    }

    public function testAdminPairingCounterStore()
    {
        $this->prepare();
        $member = $this->members->random();
        $faker = $this->faker;
        $data = [
            'process_date' => $faker->dateTime()->format('Y-m-d H:i:s'),
            'count_left' => $faker->numberBetween(0, 10),
            'count_mid' => $faker->numberBetween(0, 10),
            'count_right' => $faker->numberBetween(0, 10),
        ];
        $response = $this->actingAs($this->admin)->json('POST', '/members/'.$member->id.'/pairingCounters', $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($data['process_date'], $actual['process_date']);
        $this->assertEquals($data['count_left'], $actual['count_left']);
        $this->assertEquals($data['count_mid'], $actual['count_mid']);
        $this->assertEquals($data['count_right'], $actual['count_right']);
        $this->assertEquals($member->id, $actual['member_id']);

        // member should not be able to create pairingCounter
        $data = [
            'process_date' => $faker->dateTime()->format('Y-m-d H:i:s'),
            'count_left' => $faker->numberBetween(0, 10),
            'count_mid' => $faker->numberBetween(0, 10),
            'count_right' => $faker->numberBetween(0, 10),
        ];
        $response = $this->actingAs($member->user)->json('POST', '/members/'.$member->id.'/pairingCounters', $data);
        $response->assertStatus(403);
    }
    
    public function testAdminPairingCounterUpdate()
    {
        $this->prepare();
        $member = $this->members->random();
        $pairingCounter = $member->pairingCounters->random();

        // update data
        $faker = $this->faker;
        $data = [
            'process_date' => $faker->dateTime()->format('Y-m-d H:i:s'),
            'count_left' => $faker->numberBetween(0, 10),
            'count_mid' => $faker->numberBetween(0, 10),
            'count_right' => $faker->numberBetween(0, 10),
        ];
        $response = $this->actingAs($this->admin)->json('PUT', '/members/'.$member->id.'/pairingCounters/'.$pairingCounter->id, $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();
        $this->assertEquals($pairingCounter->id, $actual['id']);
        $this->assertEquals($member->id, $actual['member_id']);
        $this->assertEquals($data['process_date'], $actual['process_date']);
        $this->assertEquals($data['count_left'], $actual['count_left']);
        $this->assertEquals($data['count_mid'], $actual['count_mid']);
        $this->assertEquals($data['count_right'], $actual['count_right']);
        
    }    
    
    public function testAdminPairingCounterDelete()
    {
        $this->prepare();
        $member = $this->members->random();
        $pairingCounter = $member->pairingCounters->random();

        $actual = $this->assertResponse('DELETE', '/members/'.$member->id.'/pairingCounters/'.$pairingCounter->id, 200, $this->admin)->json();
        $this->assertEquals($pairingCounter->id, $actual['id']);
        $this->assertEquals($pairingCounter->process_date->format('Y-m-d H:i:s'), $actual['process_date']);
        $this->assertEquals($pairingCounter->count_left, $actual['count_left']);
        $this->assertEquals($pairingCounter->count_mid, $actual['count_mid']);
        $this->assertEquals($pairingCounter->count_right, $actual['count_right']);

        $actual = $this->assertResponse('GET', '/members/'.$member->id.'/pairingCounters/'.$pairingCounter->id, 404, $this->admin)->json();
    }
}
