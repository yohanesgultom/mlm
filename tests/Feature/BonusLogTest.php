<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\BonusRule;
use App\BonusLog;
use App\Company;
use App\Member;
use App\User;
use App\Business\BonusService;
use App\Jobs\CalculateBonus;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;

class BonusLogTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        $company = Company::where('name', 'GreenVit International')->first();
        $this->company_id = $company->id;
        $this->admin = User::where('username', 'admin')->where('company_id', $this->company_id)->first();

        // calculate bonus for members
        $bonus_rules = BonusRule::where('company_id', $this->company_id)->get();
        $dates = BonusService::getDefaultCurrentBonusDates($company);
        $date_end = Carbon::now();
        $date_start = Carbon::now();
        $date_start->subMonth();
        $process_date = $dates['process_date'];
        $bonus_service = new BonusService($bonus_rules, $date_start, $date_end, $process_date);
        $root = Member::find('0000001');
        $bonus_service->calculateBySponsorTree($root);
        $bonus_service->calculateByMemberTree($root);
    }

    public function testCycles()
    {
        $actual = $this->assertResponse('GET', '/bonusLogs/cycles', 200)->json();
        $this->assertEquals(BonusLog::CYCLES, $actual);
    }

    public function testAdminIndex()
    {
        $bonus_logs = BonusLog::byCompany($this->company_id)->get();
        $actual = $this->assertResponse('GET', '/bonusLogs', 200, $this->admin)->json();
        $this->assertEquals(count($bonus_logs), $actual['total']);

        // test filter
        $member = Member::inRandomOrder()->first();
        $cycle = $this->faker->randomElement(array_values(BonusLog::CYCLES));
        $bonus_logs = BonusLog::where('member_id', $member->id)
            ->where('cycle', $cycle)
            ->get();
        $data = ['member_id' => $member->id, 'cycle' => $cycle];
        $response = $this->actingAs($this->admin)->json('GET', '/bonusLogs', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($bonus_logs), $actual['total']);
    }

    public function testAdminShow()
    {
        $data = BonusLog::byCompany($this->company_id)
            ->inRandomOrder()
            ->first()
            ->makeVisible(['details', 'logs'])
            ->toArray();
        $actual = $this->assertResponse('GET', "/bonusLogs/{$data['id']}", 200, $this->admin)->json();
        $this->assertEquals($data['process_date'], $actual['process_date']);
        $this->assertEquals($data['member_id'], $actual['member_id']);
        $this->assertEquals($data['total'], $actual['total']);
        $this->assertEquals($data['details'], $actual['details']);
        $this->assertEquals($data['logs'], $actual['logs']);
    }

    public function testCalculateBonus()
    {        
        Queue::fake();
        $response = $this->actingAs($this->admin)->json('POST', '/bonusLogs/calculateBonus');
        $this->assertStatus(200, $response);
        Queue::assertPushed(CalculateBonus::class);
    }
}
