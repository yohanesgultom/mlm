<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Member;
use App\Company;

class UserTest extends TestCase
{   
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        $this->company = Company::first();
        $this->user = $this->company->users()->where('role', User::ROLES['MEMBER'])->first();
        $this->admin = $this->company->users()->where('role', User::ROLES['ADMIN'])->first();
    }

    private function assertArrayEquals($data, $actual)
    {
        $this->assertEquals($data['role'], $actual['role']);
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['email'], $actual['email']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals($data['verified'], $actual['verified']);
        $this->assertEquals($data['company_id'], $actual['company_id']);
    }
    
    private function _testMe($user)
    {
        $actual = $this->assertResponse('GET', '/users/me', 200, $user)->json();
        $this->assertArrayEquals($user->toArray(), $actual);
    }

    public function testRoles()
    {
        $actual = $this->assertResponse('GET', '/users/roles', 200)->json();
        $this->assertEquals(array_flip(User::ROLES), $actual);
    }
    
    public function testMe()
    {
        $this->_testMe($this->user);
        $this->_testMe($this->admin);
    }

    public function testAdminIndex()    
    {
        $count = $this
            ->company
            ->users()
            ->whereNotIn('role', [User::ROLES['MEMBER'], User::ROLES['SUPERADMIN']])
            ->where('company_id', $this->admin->company_id)
            ->count();
        $actual = $this->assertResponse('GET', '/users', 200, $this->admin)->json();
        $this->assertEquals($count, $actual['total']);
    }

    public function testAdminShow()    
    {
        $user = $this
            ->company
            ->users()
            ->whereNotIn('role', [User::ROLES['MEMBER'], User::ROLES['SUPERADMIN']])
            ->inRandomOrder()
            ->first();
        $actual = $this->assertResponse('GET', '/users/'.$user->id, 200, $this->admin)->json();
        $this->assertArrayEquals($user->toArray(), $actual);
    }

    public function testAdminDelete()
    {
        $user = $this
            ->company
            ->users()
            ->where('id', '!=', $this->admin->id)
            ->whereNotIn('role', [User::ROLES['MEMBER'], User::ROLES['SUPERADMIN']])
            ->inRandomOrder()
            ->first();
        $actual = $this->assertResponse('DELETE', '/users/'.$user->id, 200, $this->admin)->json();
        $this->assertArrayEquals($user->toArray(), $actual);
        $this->assertResponse('GET', '/users/'.$user->id, 404, $this->admin)->json();
    }

    public function testAdminCreate()
    {
        $user = factory(User::class, 1)->make(['role' => User::ROLES['ADMIN']])->first();
        $data = $user->toArray();
        $data['password'] = 'secret';
        $response = $this->actingAs($this->admin)->json('POST', '/users', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $data['company_id'] = $this->admin->company_id;
        $data['status'] = 0;
        $data['verified'] = true;
        $this->assertArrayEquals($data, $actual);
    }

    public function testAdminUpdate()
    {
        $user = $this
            ->company
            ->users()
            ->whereNotIn('role', [User::ROLES['MEMBER'], User::ROLES['SUPERADMIN']])
            ->inRandomOrder()
            ->first();
        $data = $user->toArray();
        $name = uniqid();
        $data['password'] = 'secret';
        $data['username'] = $name;
        $data['name'] = $name;
        $data['email'] = $name.'@gmail.net';
        $data['role'] = User::ROLES['ADMIN'];
        $response = $this->actingAs($this->admin)->json('PUT', '/users/'.$user->id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertArrayEquals($data, $actual);
    }    
}
