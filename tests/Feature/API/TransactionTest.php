<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use Laravel\Passport\Passport;
use DB;
use App\Country;
use App\Company;
use App\User;
use App\Transaction;
use App\Order;

class TransactionTest extends TestCase
{   

    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);
        $tx = Transaction::with('order.wallet.member.user')
            ->select('transactions.*')
            ->join('orders', 'orders.id', 'transactions.order_id')
            ->where('orders.status', Order::STATUSES['COMPLETED'])
            ->whereNull('point_date')
            ->first();
        $this->user = $tx->order->wallet->member->user;
        Passport::actingAs(
            $this->user,
            ['*']
        );
    }      

    public function testIndex()
    {
        // get all transactions
        $transactions = Transaction::with('product')
            ->select('transactions.*')
            ->join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->where('members.user_id', $this->user->id)
            ->get();
        $actual = $this->assertResponse('GET', '/api/transactions', 200)->json();
        $this->assertEquals(count($transactions), $actual['total']);

        // get unpointed transactions
        $transactions = Transaction::select('transactions.*')
            ->join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->where('members.user_id', $this->user->id)
            ->where('orders.status', Order::STATUSES['COMPLETED'])
            ->whereNull('transactions.point_date')
            ->get();

        $data = ['pointed' => false, 'completed' => true];
        $response = $this->json('GET', '/api/transactions', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($transactions), $actual['total']);
    }

    public function testPoint()
    {
        $transaction = Transaction::select('transactions.*')
            ->join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->where('members.user_id', $this->user->id)
            ->where('orders.status', Order::STATUSES['COMPLETED'])
            ->whereNull('transactions.point_date')
            ->first();
        $params = ['qty' => $transaction->qty];
        $response = $this->json('POST', "/api/transactions/{$transaction->id}/point", $params);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $data = $transaction->toArray();
        $this->assertNotNull($actual['point_date']);
        $this->assertEquals($data['id'], $actual['id']);
        $this->assertEquals($data['product_id'], $actual['product_id']);
        $this->assertEquals($data['qty'], $actual['qty']);
        $this->assertEquals($data['order_id'], $actual['order_id']);
    }
    
    public function testPointSplit()
    {
        $transaction = Transaction::select('transactions.*')
            ->join('orders', 'transactions.order_id', 'orders.id')
            ->join('wallets', 'orders.wallet_id', 'wallets.id')
            ->join('members', 'wallets.member_id', 'members.id')
            ->where('members.user_id', $this->user->id)
            ->where('orders.status', Order::STATUSES['COMPLETED'])
            ->where('transactions.qty', '>', 1)
            ->whereNull('transactions.point_date')
            ->first();
        $tx_count = $transaction->order->transactions()->count();
        $params = ['qty' => $transaction->qty - 1];
        $response = $this->json('POST', "/api/transactions/{$transaction->id}/point", $params);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $data = $transaction->toArray();
        $data['qty'] = $params['qty'];
        $this->assertNotNull($actual['point_date']);
        $this->assertEquals($data['id'], $actual['id']);
        $this->assertEquals($data['product_id'], $actual['product_id']);
        $this->assertEquals($data['qty'], $actual['qty']);
        $this->assertEquals($data['order_id'], $actual['order_id']);

        // check if new transaction created
        $txs = $transaction->order->transactions()->orderBy('transactions.id')->get();
        $new_txs = $txs->last();
        $this->assertEquals($tx_count + 1, count($txs));
        $this->assertEquals(1, $new_txs->qty);
        $this->assertNull($new_txs->point_date);
        $this->assertEquals($data['product_id'], $new_txs->product_id);
        $this->assertEquals($data['order_id'], $new_txs->order_id);
    }
}
