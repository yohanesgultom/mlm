<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use App\Article;
use App\Company;
use App\User;

class ArticleTest extends TestCase
{    
    private $admin;
    private $articles;

    public function prepare()
    {
        $companies = factory(Company::class, 2)->create();        
        $this->companies = $companies;

        $this->admin = factory(User::class, 1)->states('admin')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();
        $this->member = factory(User::class, 1)->states('member')->create([
            'company_id' => $this->companies->get(0)->id
        ])->first();

        $this->articles = factory(Article::class, 10)->create([
            'created_by' => $this->admin->id,
            'company_id' => $this->companies->get(0)->id,
        ]);

        $this->otherArticles = factory(Article::class, 5)->create([
            'created_by' => $this->admin->id,
            'company_id' => $this->companies->get(1)->id,
        ]);

    }    

    public function testTypes()
    {
        $actual = $this->assertResponse('GET', '/api/articles/types', 200)->json();
        $this->assertEquals(array_flip(Article::TYPES), $actual);
    }

    public function testStatuses()
    {
        $actual = $this->assertResponse('GET', '/api/articles/statuses', 200)->json();
        $this->assertEquals(array_flip(Article::STATUSES), $actual);
    }

    public function testIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/api/companies/'.$this->companies->get(0)->id.'/articles', 200)->json();
        $this->assertEquals(count($this->articles), $actual['total']);

        $actual = $this->assertResponse('GET', '/api/companies/'.$this->companies->get(1)->id.'/articles', 200)->json();
        $this->assertEquals(count($this->otherArticles), $actual['total']);
    }

    public function testMemberIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/api/companies/'.$this->companies->get(0)->id.'/articles', 200, $this->member)->json();
        $this->assertEquals(count($this->articles), $actual['total']);
    }

}
