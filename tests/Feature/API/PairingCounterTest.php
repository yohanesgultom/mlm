<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use App\PairingCounter;
use App\Company;
use App\Member;
use App\User;
use Laravel\Passport\Passport;
use Carbon\Carbon;

class PairingCounterTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'PairingCountersTableSeeder']);
        $company = Company::where('name', 'GreenVit International')->first();
        $this->company_id = $company->id;
        $member = Member::inRandomOrder()->first();
        Passport::actingAs(
            $member->user,
            ['*']
        );
        $this->member = $member;
    }

    public function testIndex()
    {
        $pairing_counters = PairingCounter::ownedBy($this->member->id)->get();
        $actual = $this->assertResponse('GET', '/api/pairingCounters', 200)->json();
        $this->assertEquals(count($pairing_counters), $actual['total']);

        // filter by process_date
        $pairing_counter = PairingCounter::ownedBy($this->member->id)->orderBy('process_date', 'asc')->first();
        $start = $pairing_counter->process_date->startOfMonth();
        $end = clone $start;        
        $end = $end->addMonth()->startOfMonth();
        $pairing_counters = PairingCounter::ownedBy($this->member->id)
            ->whereBetween('process_date', [$start->toDateTimeString(), $end->toDateTimeString()])
            ->get();
        $data = ['month' => $start->month, 'year' => $start->year];
        $response = $this->json('GET', '/api/pairingCounters', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($pairing_counters), $actual['total']);
        $process_date = Carbon::parse($actual['data'][0]['process_date']);
        $this->assertEquals($start->month, $process_date->month);
        $this->assertEquals($start->year, $process_date->year);
    }

    public function testShow()
    {
        $data = PairingCounter::ownedBy($this->member->id)
            ->inRandomOrder()
            ->first()
            ->toArray();
        $actual = $this->assertResponse('GET', "/api/pairingCounters/{$data['id']}", 200)->json();
        $this->assertEquals($data['process_date'], $actual['process_date']);
        $this->assertEquals($data['member_id'], $actual['member_id']);
        $this->assertEquals($data['count_left'], $actual['count_left']);
        $this->assertEquals($data['count_mid'], $actual['count_mid']);
        $this->assertEquals($data['count_right'], $actual['count_right']);
    }
}
