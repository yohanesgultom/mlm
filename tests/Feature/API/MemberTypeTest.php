<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use App\Company;

class MemberTypeTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        $this->company = Company::first();
    } 

    public function testIndex()
    {
        $actual = $this->assertResponse('GET', "/api/companies/{$this->company->id}/memberTypes", 200)->json();
        $expected = $this->company->member_types()->count();
        $this->assertEquals($expected, $actual['total']);
    }

    
    public function testShow()
    {
        $member_type = $this->company->member_types->random();
        $actual = $this->assertResponse('GET', "/api/companies/{$this->company->id}/memberTypes/{$member_type->id}", 200)->json();
        $this->assertEquals($member_type->name, $actual['name']);
        $this->assertEquals($member_type->company_id, $actual['company_id']);
    }
}
