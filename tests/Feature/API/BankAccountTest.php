<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use App\Company;
use App\BankAccount;

class BankAccountTest extends TestCase
{   

    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed', ['--class' => 'CompaniesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'BankAccountsTableSeeder']);                
    }      

    public function testIndexByCompany()
    {
        $company_id = Company::where('name', 'GreenVit International')->value('id');
        $bankAccounts = BankAccount::where('company_id', $company_id)->orderBy('id', 'asc')->get()->toArray();
        $actuals = $this->assertResponse('GET', '/api/companies/'.$company_id.'/bankAccounts', 200)->json();
        $this->assertEquals(count($bankAccounts), $actuals['total']);
        for ($i = 0; $i < count($bankAccounts); $i++) {
            $data = $bankAccounts[$i];
            $actual = $actuals['data'][$i];
            $this->assertEquals($data['account_name'], $actual['account_name']);
            $this->assertEquals($data['account_no'], $actual['account_no']);
            $this->assertEquals($data['bank_id'], $actual['bank']['id']);
        }
    }
}
