<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use App\BonusRule;
use App\BonusLog;
use App\Company;
use App\Member;
use App\User;
use App\Business\BonusService;
use Laravel\Passport\Passport;
use DB;
use Carbon\Carbon;

class BonusLogTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        $company = Company::where('name', 'GreenVit International')->first();
        $this->company_id = $company->id;
        $this->admin = User::where('username', 'admin')->where('company_id', $this->company_id)->first();

        // calculate bonus for members
        $bonus_rules = BonusRule::where('company_id', $this->company_id)->get();
        $dates = BonusService::getDefaultCurrentBonusDates($company);
        $this->date_end = Carbon::now();
        $this->date_start = Carbon::now();
        $this->date_start->subMonth();
        $this->process_date = $this->date_start;
        $bonus_service = new BonusService($bonus_rules, $this->date_start, $this->date_end, $this->process_date);
        $member = Member::find('0000001');        
        $bonus_service->calculateBySponsorTree($member);        
        $bonus_service->calculateByMemberTree($member);
        Passport::actingAs(
            $member->user,
            ['*']
        );
        $this->member = $member;
    }

    public function testIndex()
    {
        $bonus_logs = BonusLog::ownedBy($this->member->id)->get();
        $actual = $this->assertResponse('GET', '/api/bonusLogs', 200)->json();
        $this->assertEquals(count($bonus_logs), $actual['total']);
        $this->assertNotNull($actual['data'][0]['details']);
    }

    public function testIndexSearch()
    {
        $start = $this->date_start;
        $end = $this->date_end;
        $bonus_logs = BonusLog::ownedBy($this->member->id)
            ->where('process_date', '>=', $start)->where('process_date', '<', $end)
            ->get();
        $data = ['month' => $start->month, 'year' => $start->year];
        $response = $this->json('GET', '/api/bonusLogs', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($bonus_logs), $actual['total']);
        $process_date = Carbon::parse($actual['data'][0]['process_date']);
        $this->assertEquals($start->month, $process_date->month);
        $this->assertEquals($start->year, $process_date->year);
    }

    public function testAdminShow()
    {
        $data = BonusLog::ownedBy($this->member->id)
            ->inRandomOrder()
            ->first()
            ->makeVisible(['logs'])
            ->toArray();
        $actual = $this->assertResponse('GET', "/api/bonusLogs/{$data['id']}", 200)->json();
        $this->assertEquals($data['process_date'], $actual['process_date']);
        $this->assertEquals($data['member_id'], $actual['member_id']);
        $this->assertEquals($data['total'], $actual['total']);
        $this->assertEquals($data['details'], $actual['details']);
        $this->assertEquals($data['logs'], $actual['logs']);

        // must not be able to access other member's data
        $other_member = Member::find('0000002');
        $data = BonusLog::ownedBy($other_member->id)
            ->inRandomOrder()
            ->first()
            ->toArray();
        $actual = $this->assertResponse('GET', "/api/bonusLogs/{$data['id']}", 403)->json();
    }
}
