<?php

namespace Tests\Feature\API;

use Tests\MemberTestCase;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Member;

class MemberTest extends MemberTestCase
{   

    public function setUp()
    {
        parent::setUp(); 
        $this->prepare();
        $this->member = Member::with('user')->joinUsers()
            ->whereNull('members.parent_id')
            ->where('company_id', $this->companies->get(0)->id)
            ->first();
        Passport::actingAs(
            $this->member->user,
            ['*']
        );
    }
    
    public function testMe()
    {
        $actual = $this->assertResponse('GET', '/api/members/me', 200)->json();
        $this->assertNotNull($actual['achievement']);        
        $this->assertNotNull($actual['highest_achievement']);
    }

    public function testDownlines()
    {        
        $actual = $this->assertResponse('GET', '/api/downlines', 200)->json();
        $this->assertEquals(2, count($actual['downlines']));
        $this->assertEquals(2, count($actual['downlines'][0]['downlines']));
        $downlines = $actual['downlines'];
        $this->assertTrue(array_key_exists('image_path', $downlines[0]));
        $this->assertTrue(array_key_exists('reg_date', $downlines[0]));
        $this->assertTrue(array_key_exists('current_personal_points', $downlines[0]));
        $this->assertTrue(array_key_exists('current_personal_bonus', $downlines[0]));
        $this->assertTrue(array_key_exists('current_group_points', $downlines[0]));
        $this->assertTrue(array_key_exists('current_group_bonus', $downlines[0]));
        $this->assertTrue(array_key_exists('achievement_code', $downlines[0]));
        $this->assertTrue(array_key_exists('achievement', $downlines[0]));
        $this->assertTrue(array_key_exists('image_path', $downlines[0]));
        $this->assertTrue(array_key_exists('name', $downlines[0]));
    }

    public function testUpdate()
    {
        $name = $this->faker->name();
        $data = [
            'name' => $name,
            'name_printed' => $name,
            'email' => $this->faker->email(),
            'address' => $this->faker->streetAddress,
            'address_zipcode' => $this->faker->postcode,
            'image_path' => UploadedFile::fake()->image('random.jpg'),
        ];
        $response = $this->json('PUT', "/api/members/me", $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        
        $this->assertEquals($data['name'], $actual['user']['name']);
        $this->assertEquals($data['name_printed'], $actual['name_printed']);
        $this->assertEquals($data['email'], $actual['user']['email']);
        $this->assertEquals($data['address'], $actual['address']);
        $this->assertEquals($data['address_zipcode'], $actual['address_zipcode']);
        $this->assertTrue(starts_with($actual['image_path'], '/storage/member/image_path/'));
        Storage::disk('public')->assertExists(str_after($actual['image_path'], '/storage'));   
    }

    public function testChangePassword()
    {
        // wrong password
        $data = [
            'password' => 'wrongpassword',
            'password_new' => 'newpassword',
        ];
        $response = $this->json('POST', '/api/changePassword', $data);
        $this->assertStatus(403, $response);

        // correct password
        $data = [
            'password' => 'secret',
            'password_new' => 'newpassword',
        ];
        $response = $this->json('POST', '/api/changePassword', $data);
        $this->assertStatus(204, $response);
        $updated = User::find($this->member->user_id);
        $this->assertTrue(Hash::check($data['password_new'], $updated->password));
    }    

    public function testSponsors()
    {
        \Artisan::call('db:seed', ['--class' => 'MemberTypesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'PackagesTableSeeder']);

        $member_type = $this->companies->get(0)->member_types->random();
        $package = $member_type->packages->random();
        $sponsor_wallet = $this->member->wallets->random();
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email, 
            'birth_date' => $this->faker->date(),
            'gender' => $this->faker->numberBetween(0, 1),
            'bank_account_no' => $this->faker->creditCardNumber,
            'bank_account_name' => $this->faker->randomElement(['BCA', 'Bank Mandiri', 'BNI', 'BRI', 'CIMB', 'HSBC']),            
            'package_id' => $package->id,
            'sponsor_wallet_id' => $sponsor_wallet->id,
        ];
        $response = $this->json('POST', '/api/sponsors', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['email'], $actual['email']);
        $this->assertEquals($data['birth_date'], $actual['birth_date']);
        $this->assertEquals($data['gender'], $actual['gender']);
        $this->assertEquals($data['bank_account_no'], $actual['bank_account_no']);
        $this->assertEquals($data['bank_account_name'], $actual['bank_account_name']);        
        $this->assertEquals('UNPAID', $actual['status_name']);
        $this->assertEquals($data['package_id'], $actual['package']['id']);
        $this->assertEquals($sponsor_wallet->id, $actual['sponsor_wallet']['id']);
        $this->assertEquals($sponsor_wallet->member_id, $actual['sponsor_wallet']['member']['id']);
    }    
}
