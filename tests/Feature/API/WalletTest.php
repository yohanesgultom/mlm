<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use Laravel\Passport\Passport;
use DB;
use App\Member;
use App\Wallet;

class WalletTest extends TestCase
{   

    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        $this->member = Member::with('user')->inRandomOrder()->first();
        Passport::actingAs(
            $this->member->user,
            ['*']
        );
    }      

    public function testIndex()
    {
        $wallets = Wallet::where('member_id', $this->member->id)->get();
        $actual = $this->assertResponse('GET', '/api/wallets', 200)->json();
        $this->assertEquals(count($wallets), $actual['total']);
    }
}
