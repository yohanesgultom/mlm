<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use App\User;
use App\Order;
use App\Member;
use App\Wallet;
use App\Product;
use App\ProductType;
use App\Transaction;
use App\Company;
use App\Warehouse;
use App\Business\CommerceService;
use DB;
use Illuminate\Support\Collection;
use Laravel\Passport\Passport;
use Carbon\Carbon;

class OrderTest extends TestCase
{   
    protected function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'MemberStockTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehousesTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'WarehouseStocksTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'OrderRelatedTablesSeeder']);
        $this->company = Company::first();
        $this->user = $this->company->users()->where('role', User::ROLES['MEMBER'])->first();
        Passport::actingAs(
            $this->user,
            ['*']
        );
    }

    public function testStatuses()
    {
        $actual = $this->assertResponse('GET', '/api/orders/statuses', 200)->json();
        $this->assertEquals(array_flip(Order::STATUSES), $actual);
    }

    public function testIndex()
    {
        $orders = Order::select('orders.*')
            ->join('wallets', 'wallets.id', 'orders.wallet_id')
            ->where('wallets.member_id', $this->user->member->id)
            ->orderBy('orders.created_at', 'asc')
            ->get();
        $actual = $this->assertResponse('GET', '/api/orders', 200)->json();
        $this->assertEquals(count($orders), $actual['total']);

        // test range filter
        $order = $orders->first();
        $created_date_start = Carbon::instance($order->created_at)->toDateString();
        $created_date_end = Carbon::instance($order->created_at)->addYears(5)->toDateString();
        $member_id = $this->user->member->id;
        $orders = Order::join('wallets', function ($join) use ($member_id) {
            return $join->on('wallets.id', 'orders.wallet_id')
                ->where('wallets.member_id', $member_id);
            })
            ->whereBetween('orders.created_at', [$created_date_start, $created_date_end])
            ->select('orders.*')->get();
        $data = [
            'created_date_start' => $created_date_start,
            'created_date_end' => $created_date_end,
        ];
        $response = $this->json('GET', '/api/orders', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($orders), $actual['total']);
    }

    public function testStore()
    {
        $faker = $this->faker;
        $products = Product::ofCompany($this->company->id)->take(10)->get();
        $w = $this->user->member->wallets->random();
        $order = factory(Order::class, 1)
            ->make(['wallet_id' => $w->id])
            ->first();

        $stocks = CommerceService::getCentralStockAvailability($products->pluck('id')->all(), $this->company->id);
        $transactions = factory(Transaction::class, $faker->numberBetween(1, 3))
            ->make(['order_id' => $order->id, 'process_date' => new \DateTime])
            ->each(function ($t) use ($faker, $products, $stocks) {
                $p = $products->pop();
                $stock = CommerceService::getStockToSubtract($stocks, $p->id, 3);
                $qty = $stock->available;
                $t->product_id = $p->id;
                $t->qty = $qty;
                $t->amount = $p->base_price * $qty;
                $t->points = $p->point_value * $qty;                
                $t->bonus = $p->bonus_value * $qty;                
            });

        // make sure there is sufficient balance        
        $total = $transactions->sum('amount');
        $w->balance = $total;
        $w->save();

        // save initial stock qty
        $initial_stocks = CommerceService::getCentralStockAvailability($transactions->pluck('product_id')->all(), $this->company->id);

        $data = $order->toArray();
        $data['transactions'] = $transactions->toArray();

        $response = $this->json('POST', '/api/orders', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(Order::STATUSES['UNPAID'], $actual['status']);
        $this->assertEquals(array_flip(Order::STATUSES)[$actual['status']], $actual['status_name']);
        $this->assertEquals($w->id, $actual['wallet_id']);
        $this->assertEquals(count($transactions), count($actual['transactions']));
        $this->assertNull($actual['warehouse_id']);
        foreach ($data['transactions'] as $te) {
            $found = false;
            foreach ($actual['transactions'] as $ta) {
                if ($te['product_id'] == $ta['product_id'] && $te['qty'] == $ta['qty']) {
                    $this->assertEquals($te['qty'], $ta['qty']);
                    $this->assertEquals($te['amount'], $ta['amount']);
                    $this->assertEquals($te['points'], $ta['points']);
                    $this->assertEquals($te['bonus'], $ta['bonus']);
                    $this->assertEquals($te['product_id'], $ta['product_id']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }

        // stock should not changed yet
        $final_stocks = CommerceService::getCentralStockAvailability($transactions->pluck('product_id')->all(), $this->company->id);
        $this->assertTrue($initial_stocks->sum('available') == $final_stocks->sum('available'));
    }

    public function testShow()
    {
        $order = $this->user->member->wallets->random()->orders->random();
        $actual = $this->assertResponse('GET', '/api/orders/'.$order->id, 200)->json();
        $this->assertEquals($order->invoice_no, $actual['invoice_no']);
        $this->assertEquals($order->bill_no, $actual['bill_no']);
        $this->assertEquals($order->wallet_id, $actual['wallet_id']);
        $this->assertEquals($order->status, $actual['status']);
        $this->assertEquals($order->status_name, $actual['status_name']);
        $this->assertEquals(count($order->transactions), count($actual['transactions']));

        // must not be able to access other member's order
        $other_member = Member::with('wallets.orders')->where('user_id', '!=', $this->user->id)->first();
        $other_order = $other_member->wallets->random()->orders->random();
        $actual = $this->assertResponse('GET', '/api/orders/'.$other_order->id, 401)->json();        
    }
 
    public function testUpdate()
    {
        $faker = $this->faker;
        $w = $this->user->member->wallets->random();
        $order = $w->orders->random();
        if ($order->status != Order::STATUSES['UNPAID']) {
            $order->status = Order::STATUSES['UNPAID'];
            $order->save();
        }

        // update details
        $order->address = $faker->streetAddress;
        $order->address_zipcode = $faker->postcode;
        
        $companyId = $this->company->id;
        $p = Product::joinProductTypes()
            ->select('products.*')
            ->where('product_types.company_id', $companyId)
            ->whereNotIn('products.id', $order->transactions->pluck('product_id')->all())
            ->first();
        $product_ids = $order->transactions->pluck('product_id');
        $product_ids->push($p->id);
        
        // make sure there is sufficient stock
        $stocks = CommerceService::getCentralStockAvailability($product_ids->all(), $this->company->id);        
        $stock = CommerceService::getStockToSubtract($stocks, $p->id, 3);
        $qty = $stock->available;

        // new transactions        
        $transactions = factory(Transaction::class, 1)
            ->make(['order_id' => $order->id, 'process_date' => new \DateTime])
            ->each(function ($t) use ($p, $qty, $faker) {                
                $t->product_id = $p->id;
                $t->qty = $qty;
                $t->amount = $p->base_price * $qty;
                $t->points = $p->point_value * $qty;
                $t->bonus = $p->bonus_value * $qty;
            });

        // update 1 transaction (cancel the rest)
        $t = $order->transactions->pop();
        $p = Product::find($t->product_id);
        $stock = CommerceService::getStockToSubtract($stocks, $p->id, 3);
        $t->qty = $stock->available;
        $t->amount = $p->base_price * $t->qty;
        $t->points = $p->point_value * $t->qty;
        $t->bonus = $p->bonus_value * $t->qty;
        $transactions->push($t);        
                
        // make sure there is sufficient balance
        $total = $transactions->sum('amount');
        $w->balance = $total;
        $w->save();

        $data = $order->toArray();
        $data['transactions'] = $transactions->toArray();
        for ($i = 0; $i < count($data['transactions']); $i++) {
            unset($data['transactions'][$i]['product']);
        }       

        $response = $this->json('PUT', '/api/orders/'.$order->id, $data);
        $this->assertStatus(200, $response);

        $actual = $response->json();        
        $this->assertEquals($data['invoice_no'], $actual['invoice_no']);
        $this->assertEquals($data['bill_no'], $actual['bill_no']);
        $this->assertEquals($data['wallet_id'], $actual['wallet_id']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals(array_flip(Order::STATUSES)[$data['status']], $actual['status_name']);
        $this->assertEquals(count($data['transactions']), count($actual['transactions']));
        foreach ($data['transactions'] as $te) {
            $found = false;
            foreach ($actual['transactions'] as $ta) {
                if ($te['product_id'] == $ta['product_id'] && $te['qty'] == $ta['qty']) {
                    $this->assertEquals($te['qty'], $ta['qty']);
                    $this->assertEquals($te['amount'], $ta['amount']);
                    $this->assertEquals($te['points'], $ta['points']);
                    $this->assertEquals($te['bonus'], $ta['bonus']);
                    $this->assertEquals($te['product_id'], $ta['product_id']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }
    }    
    
    public function testDelete()
    {
        // can not delete if status != UNPAID
        $order = $this->user->member->wallets->random()->orders->random();
        if ($order->status == Order::STATUSES['UNPAID']) {
            $order->status = Order::STATUSES['COMPLETED'];
            $order->save();
        }
        $actual = $this->assertResponse('DELETE', '/api/orders/'.$order->id, 403)->json();

        // normal delete
        if ($order->status != Order::STATUSES['UNPAID']) {
            $order->status = Order::STATUSES['UNPAID'];
            $order->save();
        }
        $actual = $this->assertResponse('DELETE', '/api/orders/'.$order->id, 200)->json();
        $this->assertEquals($order->invoice_no, $actual['invoice_no']);
        $this->assertEquals($order->bill_no, $actual['bill_no']);
        $this->assertEquals($order->wallet_id, $actual['wallet_id']);
        $this->assertResponse('GET', '/api/orders/'.$order->id, 404)->json();

        // must not be able to delete other member's order
        $other_member = Member::with('wallets.orders')->where('user_id', '!=', $this->user->id)->first();
        $other_order = $other_member->wallets->random()->orders->random();
        if ($other_order->status != Order::STATUSES['UNPAID']) {
            $other_order->status = Order::STATUSES['UNPAID'];
            $other_order->save();
        }
        $actual = $this->assertResponse('GET', '/api/orders/'.$other_order->id, 401)->json();
    }

    public function testDirectSale()
    {
        $faker = $this->faker;
        $w = $this->user->member->wallets->random();
        $warehouse = Warehouse::ofCompany($this->company->id)->regular()->inRandomOrder()->first();
        $order = factory(Order::class, 1)
            ->make([
                'wallet_id' => $w->id,
                'warehouse_id' => $warehouse->id,
            ])
            ->first();
        
        $products = Product::ofCompany($this->company->id)->take(10)->get();
        $stocks = $warehouse->stocks()->whereIn('product_id', $products->pluck('id')->all())->get();

        $transactions = factory(Transaction::class, $faker->numberBetween(1, 3))
            ->make(['order_id' => $order->id, 'process_date' => new \DateTime])
            ->each(function ($t) use ($faker, $products, $stocks) {
                $p = $products->pop();
                $stock = CommerceService::getStockToSubtract($stocks, $p->id, 3);
                $qty = $stock->available;
                $t->product_id = $p->id;
                $t->qty = $qty;
                $t->amount = $p->base_price * $qty;
                $t->points = $p->point_value * $qty;                
                $t->bonus = $p->bonus_value * $qty;                
            });

        // make sure there is sufficient balance        
        $total = $transactions->sum('amount');
        $w->balance = $total;
        $w->save();

        // save initial stock qty
        $initial_stocks = $warehouse->stocks()->whereIn('product_id', $transactions->pluck('product_id')->all())->get();
        $initial_stocks_sum = array_sum($initial_stocks->pluck('available')->all());        

        $data = $order->toArray();
        $data['transactions'] = $transactions->toArray();

        $response = $this->json('POST', '/api/orders', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(Order::STATUSES['UNPAID'], $actual['status']);
        $this->assertEquals(array_flip(Order::STATUSES)[$actual['status']], $actual['status_name']);
        $this->assertEquals($w->id, $actual['wallet_id']);
        $this->assertEquals(count($transactions), count($actual['transactions']));
        $this->assertEquals($warehouse->id, $actual['warehouse_id']);
        foreach ($data['transactions'] as $te) {
            $found = false;
            foreach ($actual['transactions'] as $ta) {
                if ($te['product_id'] == $ta['product_id'] && $te['qty'] == $ta['qty']) {
                    $this->assertEquals($te['qty'], $ta['qty']);
                    $this->assertEquals($te['amount'], $ta['amount']);
                    $this->assertEquals($te['points'], $ta['points']);
                    $this->assertEquals($te['bonus'], $ta['bonus']);
                    $this->assertEquals($te['product_id'], $ta['product_id']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }

        // stock should not changed yet
        $final_stocks = $warehouse->stocks()->whereIn('product_id', $transactions->pluck('product_id')->all())->get();
        $final_stocks_sum = array_sum($initial_stocks->pluck('available')->all());
        $this->assertTrue($initial_stocks_sum == $final_stocks_sum);
    }    
}
