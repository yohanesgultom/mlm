<?php

namespace Tests\Feature\API;

use Laravel\Passport\Passport;
use App\Member;
use App\Product;
use App\ProductType;

class ProductTest extends \Tests\Feature\ProductTest
{   

    public function setUp()
    {
        parent::setUp(); 
        $this->prepare();
        $user = $this->member;
        Passport::actingAs(
            $user,
            ['*']
        );
    }
    
    public function testProductTypeIndex()
    {        
        $actual = $this->assertResponse('GET', '/api/productTypes', 200)->json();
        $this->assertEquals(count($this->types), $actual['total']);
    }

    public function testProductIndex()
    {        
        $type = $this->products->random()->product_type;
        $expected = Product::where('product_type_id', $type->id)->get()->toArray();
        $actual = $this->assertResponse('GET', '/api/productTypes/'.$type->id.'/products', 200)->json();
        $this->assertEquals(count($expected), $actual['total']);
    }
}
