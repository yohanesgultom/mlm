<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use Laravel\Passport\Passport;
use App\Company;
use App\Country;
use App\User;
use App\Member;
use App\Delivery;
use App\DeliveryItem;
use App\Product;
use DB;
use Carbon\Carbon;

class DeliveryTest extends TestCase
{   
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'MemberStockTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'DeliveryRelatedTablesSeeder']);
        $this->company = Company::first();
        $this->user = $this->company->users()->where('role', User::ROLES['MEMBER'])->first();
        $this->countryId = Country::where('name', 'Indonesia')->first()->id;
        Passport::actingAs(
            $this->user,
            ['*']
        );
    }   

    public function testIndex()
    {
        $deliveries = Delivery::where('member_id', $this->user->member->id)->get();
        $actual = $this->assertResponse('GET', '/api/deliveries', 200)->json();
        $this->assertEquals(count($deliveries), $actual['total']);

        // filter by delivery
        $delivery = Delivery::where('member_id', $this->user->member->id)
            ->inRandomOrder()
            ->orderBy('delivery_date', 'asc')
            ->select('delivery_date')
            ->first();            
        $delivery_date_start = Carbon::instance($delivery->delivery_date)->toDateString();
        $delivery_date_end = Carbon::instance($delivery->delivery_date)->addYears(5)->toDateString();
        $deliveries = Delivery::where('member_id', $this->user->member->id)
            ->whereBetween('delivery_date', [$delivery_date_start, $delivery_date_end])
            ->get();
        $data = [
            'delivery_date_start' => $delivery_date_start,
            'delivery_date_end' => $delivery_date_end,
        ];
        $response = $this->json('GET', '/api/deliveries', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals(count($deliveries), $actual['total']);
    }    

    public function testShow()
    {
        $delivery = Delivery::where('member_id', '!=', $this->user->member->id)->first();
        $actual = $this->assertResponse('GET', '/api/deliveries/'.$delivery->id, 401)->json();

        $delivery = $this->user->member->deliveries->random();
        $actual = $this->assertResponse('GET', '/api/deliveries/'.$delivery->id, 200)->json();
        $this->assertEquals($delivery->id, $actual['id']);
        $this->assertEquals($delivery->courier, $actual['courier']);
        $this->assertEquals($delivery->courier_ref, $actual['courier_ref']);
        $this->assertEquals($delivery->delivery_date->format('Y-m-d H:i:s'), $actual['delivery_date']);
        $this->assertEquals($delivery->address, $actual['address']);
        $this->assertEquals($delivery->address_zipcode, $actual['address_zipcode']);
        $this->assertEquals($delivery->address_city, $actual['address_city']);
        $this->assertEquals($delivery->address_state, $actual['address_state']);
        $this->assertEquals($delivery->country_id, $actual['country']['id']);
        $this->assertEquals($delivery->member_id, $actual['member_id']);
        $this->assertEquals($delivery->status, $actual['status']);
        $this->assertEquals($delivery->status_name, $actual['status_name']);
        $this->assertEquals(count($delivery->items), count($actual['items']));
    }
    
    public function testDelete()
    {
        $delivery = Delivery::where('member_id', '!=', $this->user->member->id)->first();
        $this->assertResponse('DELETE', '/api/deliveries/'.$delivery->id, 401)->json();

        $delivery = $this->user->member->deliveries->random();
        if ($delivery->status == Delivery::STATUSES['UNPAID']) {
            $delivery->status = Delivery::STATUSES['DELIVERED'];
            $delivery->save();
        }
        $actual = $this->assertResponse('DELETE', '/api/deliveries/'.$delivery->id, 403)->json();

        $delivery = $this->user->member->deliveries->random();
        if ($delivery->status != Delivery::STATUSES['UNPAID']) {
            $delivery->status = Delivery::STATUSES['UNPAID'];
            $delivery->save();
        }
        $actual = $this->assertResponse('DELETE', '/api/deliveries/'.$delivery->id, 200)->json();
        $this->assertResponse('GET', '/api/deliveries/'.$delivery->id, 404)->json();
    }    

    public function testStore()
    {
        $faker = $this->faker;
        $m = $this->user->member;

        // create valid delivery & items
        $delivery = factory(Delivery::class, 1)->make(['country_id' => $this->countryId])->first();
        $data = $delivery->toArray();
        $stocks = $m->stocks;
        $data['items'] = [];
        foreach ($stocks as $stock) {
            $item = DeliveryItem::make([
                'delivery_id' => $delivery->id,
                'product_id' => $stock->product_id,
                'qty' => $stock->qty,
            ]);
            array_push($data['items'], $item->toArray());
        }

        $response = $this->json('POST', '/api/deliveries', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['courier'], $actual['courier']);
        $this->assertNull($actual['courier_ref']);
        $this->assertNull($actual['delivery_date']);
        $this->assertNull($actual['cost']);
        $this->assertEquals($data['address'], $actual['address']);
        $this->assertEquals($data['address_zipcode'], $actual['address_zipcode']);
        $this->assertEquals($data['address_city'], $actual['address_city']);
        $this->assertEquals($data['address_state'], $actual['address_state']);
        $this->assertEquals(Delivery::STATUSES['UNPAID'], $actual['status']);
        $this->assertEquals($data['country_id'], $actual['country']['id']);
        $this->assertEquals($m->id, $actual['member_id']);
        $this->assertEquals(count($data['items']), count($actual['items']));
        foreach ($data['items'] as $ite) {
            $found = false;
            foreach ($actual['items'] as $ita) {
                if ($ite['product_id'] == $ita['product_id']) {
                    $this->assertEquals($ite['qty'], $ita['qty']);
                    $this->assertEquals($ite['product_id'], $ita['product_id']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }
    }

    public function testUpdate()
    {
        $faker = $this->faker;
        $m = $this->user->member;

        // create valid delivery & items in database
        $delivery = factory(Delivery::class, 1)->create([
            'member_id' => $m->id,
            'country_id' => $this->countryId,
            'status' => Delivery::STATUSES['UNPAID'],
        ])->first();
        $stocks = $m->stocks;
        $stock_for_update = $stocks->pop();
        foreach ($stocks as $id => $stock) {
            $item = DeliveryItem::create([
                'delivery_id' => $delivery->id,
                'product_id' => $stock->product_id,
                'qty' => $stock->qty,
            ])->first();
        }

        // update details
        $delivery->address = $faker->streetAddress;
        $delivery->address_zipcode = $faker->postcode;

        // add new item
        $items = factory(DeliveryItem::class, 1)
            ->make([
                'delivery_id' => $delivery->id,
                'product_id' => $stock_for_update->product_id,
                'qty' => $stock_for_update->qty,
            ]);

        // update 1 transaction (cancel the rest)
        $it = $delivery->items()->get()->random();
        $it->qty = $it->qty - 1;
        $items->push($it);        
        
        $data = $delivery->toArray();
        $data['items'] = $items->toArray();

        for ($i = 0; $i < count($data['items']); $i++) {
            unset($data['items'][$i]['product']);
        }

        $response = $this->json('PUT', '/api/deliveries/'.$delivery->id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['courier'], $actual['courier']);
        $this->assertEquals($data['courier_ref'], $actual['courier_ref']);
        $this->assertEquals($data['delivery_date'], $actual['delivery_date']);
        $this->assertEquals($data['cost'], $actual['cost']);
        $this->assertEquals($data['address'], $actual['address']);
        $this->assertEquals($data['address_zipcode'], $actual['address_zipcode']);
        $this->assertEquals($data['address_city'], $actual['address_city']);
        $this->assertEquals($data['address_state'], $actual['address_state']);
        $this->assertEquals($data['status'], $actual['status']);
        $this->assertEquals($data['country_id'], $actual['country']['id']);
        $this->assertEquals($m->id, $actual['member_id']);
        $this->assertEquals(count($data['items']), count($actual['items']));
        foreach ($data['items'] as $ite) {
            $found = false;
            foreach ($actual['items'] as $ita) {
                if ($ite['product_id'] == $ita['product_id']) {
                    $this->assertEquals($ite['qty'], $ita['qty']);
                    $this->assertEquals($ite['product_id'], $ita['product_id']);
                    $found = true;
                    break;
                }                
            }
            $this->assertTrue($found);
        }
    }    
}
