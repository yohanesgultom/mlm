<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use App\Company;

class PackageTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'PackagesTableSeeder']);
        $this->company = Company::with('member_types')->first();
        $this->member_type = $this->company->member_types->random();
    } 

    public function testIndex()
    {        
        $actual = $this->assertResponse('GET', "/api/companies/{$this->company->id}/memberTypes/{$this->member_type->id}/packages", 200)->json();
        $packages = $this->member_type->packages;
        $this->assertEquals(count($packages), $actual['total']);
    }
    
    public function testShow()
    {
        $package = $this->member_type->packages()->with('package_products.product')->get()->random();
        $actual = $this->assertResponse('GET', "/api/companies/{$this->company->id}/memberTypes/{$this->member_type->id}/packages/{$package->id}", 200)->json();
        $this->assertEquals($package->toArray(), $actual);
    }
}
