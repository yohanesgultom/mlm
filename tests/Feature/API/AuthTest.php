<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use App\Company;

class AuthTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'PackagesTableSeeder']);
        $this->company = Company::first();
        $this->member_type = $this->company->member_types->random();
        $this->packages = $this->member_type->packages;        
    }     

    public function testRegister()
    {
        $package = $this->packages->random();
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email, 
            'birth_date' => $this->faker->date(),
            'gender' => $this->faker->numberBetween(0, 1),
            'bank_account_no' => $this->faker->creditCardNumber,
            'bank_account_name' => $this->faker->randomElement(['BCA', 'Bank Mandiri', 'BNI', 'BRI', 'CIMB', 'HSBC']),
            'package_id' => $package->id,
        ];
        $response = $this->json('POST', '/api/register', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        $this->assertEquals($data['email'], $actual['email']);
        $this->assertEquals($data['birth_date'], $actual['birth_date']);
        $this->assertEquals($data['gender'], $actual['gender']);
        $this->assertEquals($data['bank_account_no'], $actual['bank_account_no']);
        $this->assertEquals($data['bank_account_name'], $actual['bank_account_name']);
        $this->assertEquals($data['package_id'], $actual['package']['id']);
        $this->assertEquals('UNPAID', $actual['status_name']);
    }
}
