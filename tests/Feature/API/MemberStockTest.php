<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use App\Company;
use App\User;
use App\MemberStock;
use DB;
use Illuminate\Support\Collection;
use Laravel\Passport\Passport;

class MemberStockTest extends TestCase
{   
    public function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');
        \Artisan::call('db:seed', ['--class' => 'ProductRelatedTablesSeeder']);
        \Artisan::call('db:seed', ['--class' => 'MemberStockTableSeeder']);
        $this->company = Company::first();
        $this->user = $this->company->users()->where('role', User::ROLES['MEMBER'])->first();
        Passport::actingAs(
            $this->user,
            ['*']
        );
    }    

    public function testIndex()
    {
        $member_id = $this->user->member->id;
        $stocks = $this->user->member->stocks;
        $actual = $this->assertResponse('GET', '/api/stocks', 200)->json();
        $this->assertEquals(count($stocks), $actual['total']);
    }    
}
