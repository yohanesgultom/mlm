<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Religion;
use DB;

class ReligionTest extends TestCase
{
    private $admin;
    private $data;

    protected function prepare()
    {
        $this->admin = factory(User::class, 1)->states('superadmin')->create()->first();
        $this->data = [
            ['name' => 'Islam'],
            ['name' => 'Katholik'],
            ['name' => 'Budha'],
            ['name' => 'Kristen Protestan'],
            ['name' => 'Hindu'],
            ['name' => 'Kong Hu Chu'],
        ];
        DB::table('religions')->insert($this->data);
    }

    public function testAdminIndex()
    {
        $this->prepare();
        $actual = $this->assertResponse('GET', '/religions', 200, $this->admin)->json();
        $this->assertEquals(count($this->data), count($actual['data']));
    }


    public function testAdminStore()
    {
        $this->prepare();
        $data = ['name' => $this->faker->word];
        $response = $this->actingAs($this->admin)->json('POST', '/religions', $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($data['name'], $actual['name']);
        
    }


    public function testAdminShow()
    {
        $this->prepare();
        $id = DB::table('religions')->where('name', $this->data[0]['name'])->value('id');
        $actual = $this->assertResponse('GET', '/religions/'.$id, 200, $this->admin)->json();
        $this->assertEquals($this->data[0]['name'], $actual['name']);
    }

    public function testAdminUpdate()
    {
        $this->prepare();
        $id = DB::table('religions')->where('name', $this->data[0]['name'])->value('id');
        $data = ['id' => $id, 'name' => $this->faker->word()];
        $response = $this->actingAs($this->admin)->json('PUT', 'religions/'.$id, $data);
        $this->assertStatus(200, $response);
        $actual = $response->json();
        $this->assertEquals($id, $actual['id']);
        $this->assertEquals($data['name'], $actual['name']);
    }    

    public function testAdminDelete()
    {
        $this->prepare();
        $data = ['id' => null, 'name' => $this->data[0]['name']];
        $data['id'] = DB::table('religions')->where('name', $data['name'])->value('id');
        $actual = $this->assertResponse('DELETE', '/religions/'.$data['id'], 200, $this->admin)->json();
        $this->assertEquals($data['name'], $actual['name']); 
        $actual = $this->assertResponse('GET', '/religions/'.$data['id'], 404, $this->admin)->json();
    }


}
