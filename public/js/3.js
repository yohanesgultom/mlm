webpackJsonp([3],{

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
  baseURL: Laravel.baseUrl
  // baseURL: "http://good-health-mlm.test/",
});

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HTTP; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);


// import store from '@/store'


var HTTP = __WEBPACK_IMPORTED_MODULE_0_axios___default.a.create({
  baseURL: __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL,
  headers: {
    'X-CSRF-TOKEN': Laravel.csrfToken,
    'Accept': "application/json"
  }
});

/***/ }),

/***/ 306:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(310);
var isBuffer = __webpack_require__(319);

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    return false;
  }
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  extend: extend,
  trim: trim
};


/***/ }),

/***/ 309:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__(306);
var normalizeHeaderName = __webpack_require__(321);

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__(311);
  } else if (typeof process !== 'undefined') {
    // For node use HTTP adapter
    adapter = __webpack_require__(311);
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(21)))

/***/ }),

/***/ 310:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};


/***/ }),

/***/ 311:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);
var settle = __webpack_require__(322);
var buildURL = __webpack_require__(324);
var parseHeaders = __webpack_require__(325);
var isURLSameOrigin = __webpack_require__(326);
var createError = __webpack_require__(312);
var btoa = (typeof window !== 'undefined' && window.btoa && window.btoa.bind(window)) || __webpack_require__(327);

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();
    var loadEvent = 'onreadystatechange';
    var xDomain = false;

    // For IE 8/9 CORS support
    // Only supports POST and GET calls and doesn't returns the response headers.
    // DON'T do this for testing b/c XMLHttpRequest is mocked, not XDomainRequest.
    if ("development" !== 'test' &&
        typeof window !== 'undefined' &&
        window.XDomainRequest && !('withCredentials' in request) &&
        !isURLSameOrigin(config.url)) {
      request = new window.XDomainRequest();
      loadEvent = 'onload';
      xDomain = true;
      request.onprogress = function handleProgress() {};
      request.ontimeout = function handleTimeout() {};
    }

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request[loadEvent] = function handleLoad() {
      if (!request || (request.readyState !== 4 && !xDomain)) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        // IE sends 1223 instead of 204 (https://github.com/axios/axios/issues/201)
        status: request.status === 1223 ? 204 : request.status,
        statusText: request.status === 1223 ? 'No Content' : request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED',
        request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__(328);

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ?
          cookies.read(config.xsrfCookieName) :
          undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (config.withCredentials) {
      request.withCredentials = true;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};


/***/ }),

/***/ 312:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__(323);

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};


/***/ }),

/***/ 313:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};


/***/ }),

/***/ 314:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;


/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return Auth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return Country; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return Member; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "w", function() { return Religion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "t", function() { return Product; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Achievements; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return Bank; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return BankAccounts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "u", function() { return ProductType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "n", function() { return MemberType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "o", function() { return NetworkCenterType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "B", function() { return Tags; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "E", function() { return Warehouse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "D", function() { return Wallets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Attachments; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "r", function() { return PairingCounter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "A", function() { return Stocks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "z", function() { return StockWarehouse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "p", function() { return Orders; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return Company; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Article; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return Delivery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "s", function() { return Payment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return Deposit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return BonusLog; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "C", function() { return Transaction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "x", function() { return Report; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "v", function() { return Registration; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "q", function() { return Package; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return Auditlogs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "y", function() { return StockLogs; });
var Auth = __webpack_require__(316).default;
var Country = __webpack_require__(336).default;
var Member = __webpack_require__(337).default;
var Religion = __webpack_require__(338).default;
var Product = __webpack_require__(339).default;
var Achievements = __webpack_require__(340).default;
var Bank = __webpack_require__(341).default;
var BankAccounts = __webpack_require__(342).default;
var ProductType = __webpack_require__(343).default;
var MemberType = __webpack_require__(344).default;
var NetworkCenterType = __webpack_require__(345).default;
var Tags = __webpack_require__(346).default;
var Warehouse = __webpack_require__(347).default;
var Wallets = __webpack_require__(348).default;
var Attachments = __webpack_require__(349).default;
var PairingCounter = __webpack_require__(350).default;
var Stocks = __webpack_require__(351).default;
var StockWarehouse = __webpack_require__(352).default;
var Orders = __webpack_require__(353).default;
var Company = __webpack_require__(354).default;
var Article = __webpack_require__(355).default;
var Delivery = __webpack_require__(356).default;
var Payment = __webpack_require__(357).default;
var Deposit = __webpack_require__(358).default;
var BonusLog = __webpack_require__(359).default;
var Transaction = __webpack_require__(360).default;
var Report = __webpack_require__(361).default;
var Registration = __webpack_require__(362).default;
var Package = __webpack_require__(363).default;
var Auditlogs = __webpack_require__(364).default;
var StockLogs = __webpack_require__(365).default;

/* unused harmony default export */ var _unused_webpack_default_export = ({
    Auth: Auth,
    Country: Country,
    Member: Member,
    Religion: Religion,
    Product: Product,
    Achievements: Achievements,
    Bank: Bank,
    ProductType: ProductType,
    MemberType: MemberType,
    NetworkCenterType: NetworkCenterType,
    Tags: Tags,
    BankAccounts: BankAccounts,
    Warehouse: Warehouse,
    Wallets: Wallets,
    Attachments: Attachments,
    PairingCounter: PairingCounter,
    Stocks: Stocks,
    StockWarehouse: StockWarehouse,
    Company: Company,
    Article: Article,
    Delivery: Delivery,
    Payment: Payment,
    Deposit: Deposit,
    BonusLog: BonusLog,
    Transaction: Transaction,
    Report: Report,
    Registration: Registration,
    Package: Package,
    Auditlogs: Auditlogs,
    StockLogs: StockLogs
});

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;

/* harmony default export */ __webpack_exports__["default"] = ({
	login: function login(formData) {
		return new Promise(function (resolve, reject) {

			return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/login', formData).then(function (response) {
				resolve(response);
			}).catch(function (error) {
				reject(error);
			});
		});
	},
	forgot_password: function forgot_password(formData) {
		return new Promise(function (resolve, reject) {

			return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/password/email', formData).then(function (response) {
				resolve(response);
			}).catch(function (error) {
				reject(error);
			});
		});
	},
	reset_password: function reset_password(formData) {
		return new Promise(function (resolve, reject) {

			return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/password/reset', formData).then(function (response) {
				resolve(response);
			}).catch(function (error) {
				reject(error);
			});
		});
	},
	logout: function logout() {
		return new Promise(function (resolve, reject) {

			return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/logout').then(function (response) {
				resolve(response);
			}).catch(function (error) {
				reject(error);
			});
		});
	}
});

/***/ }),

/***/ 317:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(318);

/***/ }),

/***/ 318:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);
var bind = __webpack_require__(310);
var Axios = __webpack_require__(320);
var defaults = __webpack_require__(309);

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(utils.merge(defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__(314);
axios.CancelToken = __webpack_require__(334);
axios.isCancel = __webpack_require__(313);

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__(335);

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;


/***/ }),

/***/ 319:
/***/ (function(module, exports) {

/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */

// The _isBuffer check is for Safari 5-7 support, because it's missing
// Object.prototype.constructor. Remove this eventually
module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)
}

function isBuffer (obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

// For Node v0.10 support. Remove this eventually.
function isSlowBuffer (obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))
}


/***/ }),

/***/ 320:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var defaults = __webpack_require__(309);
var utils = __webpack_require__(306);
var InterceptorManager = __webpack_require__(329);
var dispatchRequest = __webpack_require__(330);

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = utils.merge({
      url: arguments[0]
    }, arguments[1]);
  }

  config = utils.merge(defaults, this.defaults, { method: 'get' }, config);
  config.method = config.method.toLowerCase();

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;


/***/ }),

/***/ 321:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};


/***/ }),

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__(312);

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  // Note: status is not exposed by XDomainRequest
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response.request,
      response
    ));
  }
};


/***/ }),

/***/ 323:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }
  error.request = request;
  error.response = response;
  return error;
};


/***/ }),

/***/ 324:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      }

      if (!utils.isArray(val)) {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};


/***/ }),

/***/ 325:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = [
  'age', 'authorization', 'content-length', 'content-type', 'etag',
  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
  'referer', 'retry-after', 'user-agent'
];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};


/***/ }),

/***/ 326:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
  (function standardBrowserEnv() {
    var msie = /(msie|trident)/i.test(navigator.userAgent);
    var urlParsingNode = document.createElement('a');
    var originURL;

    /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
    function resolveURL(url) {
      var href = url;

      if (msie) {
        // IE needs attribute set twice to normalize properties
        urlParsingNode.setAttribute('href', href);
        href = urlParsingNode.href;
      }

      urlParsingNode.setAttribute('href', href);

      // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
      return {
        href: urlParsingNode.href,
        protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
        host: urlParsingNode.host,
        search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
        hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
        hostname: urlParsingNode.hostname,
        port: urlParsingNode.port,
        pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
                  urlParsingNode.pathname :
                  '/' + urlParsingNode.pathname
      };
    }

    originURL = resolveURL(window.location.href);

    /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
    return function isURLSameOrigin(requestURL) {
      var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
      return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
    };
  })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return function isURLSameOrigin() {
      return true;
    };
  })()
);


/***/ }),

/***/ 327:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// btoa polyfill for IE<10 courtesy https://github.com/davidchambers/Base64.js

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function E() {
  this.message = 'String contains an invalid character';
}
E.prototype = new Error;
E.prototype.code = 5;
E.prototype.name = 'InvalidCharacterError';

function btoa(input) {
  var str = String(input);
  var output = '';
  for (
    // initialize result and counter
    var block, charCode, idx = 0, map = chars;
    // if the next str index does not exist:
    //   change the mapping table to "="
    //   check if d has no fractional digits
    str.charAt(idx | 0) || (map = '=', idx % 1);
    // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
    output += map.charAt(63 & block >> 8 - idx % 1 * 8)
  ) {
    charCode = str.charCodeAt(idx += 3 / 4);
    if (charCode > 0xFF) {
      throw new E();
    }
    block = block << 8 | charCode;
  }
  return output;
}

module.exports = btoa;


/***/ }),

/***/ 328:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
  (function standardBrowserEnv() {
    return {
      write: function write(name, value, expires, path, domain, secure) {
        var cookie = [];
        cookie.push(name + '=' + encodeURIComponent(value));

        if (utils.isNumber(expires)) {
          cookie.push('expires=' + new Date(expires).toGMTString());
        }

        if (utils.isString(path)) {
          cookie.push('path=' + path);
        }

        if (utils.isString(domain)) {
          cookie.push('domain=' + domain);
        }

        if (secure === true) {
          cookie.push('secure');
        }

        document.cookie = cookie.join('; ');
      },

      read: function read(name) {
        var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
        return (match ? decodeURIComponent(match[3]) : null);
      },

      remove: function remove(name) {
        this.write(name, '', Date.now() - 86400000);
      }
    };
  })() :

  // Non standard browser env (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return {
      write: function write() {},
      read: function read() { return null; },
      remove: function remove() {}
    };
  })()
);


/***/ }),

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;


/***/ }),

/***/ 330:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);
var transformData = __webpack_require__(331);
var isCancel = __webpack_require__(313);
var defaults = __webpack_require__(309);
var isAbsoluteURL = __webpack_require__(332);
var combineURLs = __webpack_require__(333);

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Support baseURL config
  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  }

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers || {}
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};


/***/ }),

/***/ 331:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(306);

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};


/***/ }),

/***/ 332:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};


/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
};


/***/ }),

/***/ 334:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__(314);

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;


/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};


/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/countries',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/countries', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/countries/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/countries' + params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/countries/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/countries/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/members',
    postData: function postData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/members', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData() {
        var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members' + params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/members/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/members/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    downlines: function downlines(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members/' + id + '/downlines' + '?by=' + params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/religions',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/religions', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/religions/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/religions', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/religions/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/religions/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/products',

    postData: function postData(params) {
        console.log(params.values());
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/products', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/products/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData() {
        var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/products' + params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, params) {
        return new Promise(function (resolve, reject) {
            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/products/' + id, params).then(function (response) {
                console.log(params.values());
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/products/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/achievements',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/achievements', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/achievements/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/achievements', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/achievements/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/achievements/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/banks',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/banks', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/banks/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData() {
    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/banks', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/banks/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/banks/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/bankAccounts',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/bankAccounts', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/bankAccounts/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/bankAccounts', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/bankAccounts/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/bankAccounts/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/productTypes',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/productTypes', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/productTypes/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/productTypes', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/productTypes/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/productTypes/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/memberTypes',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/memberTypes', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/memberTypes/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/memberTypes', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/memberTypes/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/memberTypes/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/networkCenterTypes',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/networkCenterTypes', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/networkCenterTypes/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/networkCenterTypes', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/networkCenterTypes/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/networkCenterTypes/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 346:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/tags',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/tags', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/tags/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/tags', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/tags/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/tags/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 347:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/warehouses',

    postData: function postData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/warehouses', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/warehouses/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/warehouses', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/warehouses/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/warehouses/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    transfer: function transfer(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/warehouses/' + id + '/transfer', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    sell: function sell(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/warehouses/' + id + '/sell', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 348:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl,

  postData: function postData(member_id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/members/' + member_id + '/wallets', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(member_id, id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members/' + member_id + '/wallets/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(member_id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members/' + member_id + '/wallets', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, member_id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/members/' + member_id + '/wallets/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id, member_id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/members/' + member_id + '/wallets/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 349:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl,

    postData: function postData(member_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/members/' + member_id + '/attachments', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id, member_id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members/' + member_id + '/attachments/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(id, member_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/attachments', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, member_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/members/' + member_id + '/attachments/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id, member_id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/members/' + member_id + '/attachments/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    download: function download(id, member_id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members/' + member_id + '/attachments/' + id + '/download').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 350:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl,

  postData: function postData(member_id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/members/' + member_id + '/pairingCounters', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id, member_id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members/' + member_id + '/pairingCounters/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(id, member_id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/pairingCounters', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, member_id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/members/' + member_id + '/pairingCounters/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id, member_id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/members/' + member_id + '/pairingCounters/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl,

    postData: function postData(member_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/members/' + member_id + '/stocks', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(member_id, id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members/' + member_id + '/stocks/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(member_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/members/' + member_id + '/stocks', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, member_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/members/' + member_id + '/stocks/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id, member_id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/members/' + member_id + '/stocks/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl,

  postData: function postData(warehouse_id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/warehouses/' + warehouse_id + '/stocks', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(warehouse_id, id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/warehouses/' + warehouse_id + '/stocks/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/stocks', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, warehouse_id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/warehouses/' + warehouse_id + '/stocks/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id, warehouse_id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/warehouses/' + warehouse_id + '/stocks/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/orders',
    postData: function postData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/orders', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/orders/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/orders', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/orders/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/orders/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getStatus: function getStatus() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/orders/statuses').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

  pathUrl: pathUrl + '/companies',

  postData: function postData(params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/companies', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  showData: function showData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/companies/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  getData: function getData() {
    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/companies', params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateData: function updateData(id, params) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/companies/' + id, params).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  deleteData: function deleteData(id) {
    return new Promise(function (resolve, reject) {

      return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/companies/' + id).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl,

    postData: function postData(company_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/companies/' + company_id + '/articles', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id, company_id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/companies/' + company_id + '/articles/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(id, company_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/companies/' + company_id + '/articles', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, company_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/companies/' + company_id + '/articles/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id, company_id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/companies/' + company_id + '/articles/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getStatus: function getStatus() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/articles/statuses').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getType: function getType() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/articles/types').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/deliveries',
    postData: function postData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/deliveries', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/deliveries/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/deliveries', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/deliveries/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/deliveries/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getStatus: function getStatus() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/deliveries/statuses').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/payments',

    postData: function postData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/payments', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/payments/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/payments', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/payments/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/payments/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getStatus: function getStatus() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/payments/statuses').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getType: function getType() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/payments/types').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/deposits',

    postData: function postData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/deposits', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/deposits/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/deposits', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/deposits/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/deposits/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getStatus: function getStatus() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/deposits/statuses').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/bonusLogs',

    postData: function postData(params) {
        return new Promise(function (resolve, reject) {
            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/bonusLogs/calculateBonus', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id) {
        return new Promise(function (resolve, reject) {
            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/bonusLogs/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(id, params) {
        return new Promise(function (resolve, reject) {
            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/bonusLogs', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

	pathUrl: pathUrl + '/transactions',

	showData: function showData(id) {
		return new Promise(function (resolve, reject) {

			return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/transactions/' + id).then(function (response) {
				resolve(response);
			}).catch(function (error) {
				reject(error);
			});
		});
	},
	transfer: function transfer(id, params) {
		return new Promise(function (resolve, reject) {

			return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/transactions/' + id + '/transfer', params).then(function (response) {
				resolve(response);
			}).catch(function (error) {
				reject(error);
			});
		});
	}
});

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({
    getOrders: function getOrders() {
        var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

        return new Promise(function (resolve, reject) {
            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/reports/orders' + params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getProductLogs: function getProductLogs(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/reports/productStockLogs', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getProductStocks: function getProductStocks(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/reports/productStocks', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getWarehouses: function getWarehouses(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/reports/warehouses', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/registrations',
    postData: function postData(params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/registrations', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/registrations/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData() {
        var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/registrations' + params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/registrations/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/registrations/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getStatus: function getStatus() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/registrations/statuses').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl,

    postData: function postData(member_type_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/memberTypes/' + member_type_id + '/packages', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(id, member_type_id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/memberTypes/' + member_type_id + '/packages/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(member_type_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/memberTypes/' + member_type_id + '/packages', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    updateData: function updateData(member_type_id, id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].put(baseUrl + '/memberTypes/' + member_type_id + '/packages/' + id, params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    deleteData: function deleteData(id, member_type_id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].delete(baseUrl + '/memberTypes/' + member_type_id + '/packages/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getStatus: function getStatus() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/packages/statuses').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getType: function getType() {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/packages/types').then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl + '/auditLogs',

    showData: function showData(id) {
        return new Promise(function (resolve, reject) {
            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/auditLogs/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(id, params) {
        return new Promise(function (resolve, reject) {
            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/auditLogs', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_common_js__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(304);



var baseUrl = __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */].baseURL;
var pathUrl = baseUrl;

/* harmony default export */ __webpack_exports__["default"] = ({

    pathUrl: pathUrl,

    postData: function postData(warehouse_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].post(baseUrl + '/warehouses/' + warehouse_id + '/stockLogs', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    showData: function showData(warehouse_id, id) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/warehouses/' + warehouse_id + '/stockLogs/' + id).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    },
    getData: function getData(warehouse_id, params) {
        return new Promise(function (resolve, reject) {

            return __WEBPACK_IMPORTED_MODULE_0__http_common_js__["a" /* HTTP */].get(baseUrl + '/warehouses/' + warehouse_id + '/stockLogs', params).then(function (response) {
                resolve(response);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
});

/***/ }),

/***/ 372:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.VueForm = factory());
}(this, (function () { 'use strict';

var emailRegExp = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i; // from angular
var urlRegExp = /^(http\:\/\/|https\:\/\/)(.{4,})$/;

var email = function email(value, attrValue, vnode) {
  return emailRegExp.test(value);
};
email._allowNulls = true;

var number = function number(value, attrValue, vnode) {
  return !isNaN(value);
};
number._allowNulls = true;

var url = function url(value, attrValue, vnode) {
  return urlRegExp.test(value);
};
url._allowNulls = true;

var validators = {
  email: email,
  number: number,
  url: url,
  required: function required(value, attrValue, vnode) {
    if (attrValue === false) {
      return true;
    }

    if (value === 0) {
      return true;
    }

    if (vnode.data.attrs && typeof vnode.data.attrs.bool !== 'undefined' || vnode.componentOptions && vnode.componentOptions.propsData && typeof vnode.componentOptions.propsData.bool !== 'undefined') {
      // bool attribute is present, allow false pass validation
      if (value === false) {
        return true;
      }
    }

    if (Array.isArray(value)) {
      return !!value.length;
    }
    return !!value;
  },
  minlength: function minlength(value, length) {
    return value.length >= length;
  },
  maxlength: function maxlength(value, length) {
    return length >= value.length;
  },
  pattern: function pattern(value, _pattern) {
    var patternRegExp = new RegExp('^' + _pattern + '$');
    return patternRegExp.test(value);
  },
  min: function min(value, _min, vnode) {
    if ((vnode.data.attrs.type || '').toLowerCase() == 'number') {
      return +value >= +_min;
    }
    return value >= _min;
  },
  max: function max(value, _max, vnode) {
    if ((vnode.data.attrs.type || '').toLowerCase() == 'number') {
      return +_max >= +value;
    }
    return _max >= value;
  }
};

var config = {
  validators: validators,
  formComponent: 'vueForm',
  formTag: 'form',
  messagesComponent: 'fieldMessages',
  messagesTag: 'div',
  showMessages: '',
  validateComponent: 'validate',
  validateTag: 'div',
  fieldComponent: 'field',
  fieldTag: 'div',
  formClasses: {
    dirty: 'vf-form-dirty',
    pristine: 'vf-form-pristine',
    valid: 'vf-form-valid',
    invalid: 'vf-form-invalid',
    touched: 'vf-form-touched',
    untouched: 'vf-form-untouched',
    focused: 'vf-form-focused',
    submitted: 'vf-form-submitted',
    pending: 'vf-form-pending'
  },
  validateClasses: {
    dirty: 'vf-field-dirty',
    pristine: 'vf-field-pristine',
    valid: 'vf-field-valid',
    invalid: 'vf-field-invalid',
    touched: 'vf-field-touched',
    untouched: 'vf-field-untouched',
    focused: 'vf-field-focused',
    submitted: 'vf-field-submitted',
    pending: 'vf-field-pending'
  },
  inputClasses: {
    dirty: 'vf-dirty',
    pristine: 'vf-pristine',
    valid: 'vf-valid',
    invalid: 'vf-invalid',
    touched: 'vf-touched',
    untouched: 'vf-untouched',
    focused: 'vf-focused',
    submitted: 'vf-submitted',
    pending: 'vf-pending'
  },
  Promise: typeof Promise === 'function' ? Promise : null
};

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();





var defineProperty = function (obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};



var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

function getClasses(classConfig, state) {
  var _ref;

  return _ref = {}, defineProperty(_ref, classConfig.dirty, state.$dirty), defineProperty(_ref, classConfig.pristine, state.$pristine), defineProperty(_ref, classConfig.valid, state.$valid), defineProperty(_ref, classConfig.invalid, state.$invalid), defineProperty(_ref, classConfig.touched, state.$touched), defineProperty(_ref, classConfig.untouched, state.$untouched), defineProperty(_ref, classConfig.focused, state.$focused), defineProperty(_ref, classConfig.pending, state.$pending), defineProperty(_ref, classConfig.submitted, state.$submitted), _ref;
}

function addClass(el, className) {
  if (el.classList) {
    el.classList.add(className);
  } else {
    el.className += ' ' + className;
  }
}

function removeClass(el, className) {
  if (el.classList) {
    el.classList.remove(className);
  } else {
    el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
  }
}

function vModelValue(data) {
  if (data.model) {
    return data.model.value;
  }
  return data.directives.filter(function (v) {
    return v.name === 'model';
  })[0].value;
}

function getVModelAndLabel(nodes, config) {
  var foundVnodes = {
    vModel: [],
    label: null,
    messages: null
  };

  if (!nodes) {
    return foundVnodes;
  }

  function traverse(nodes) {
    for (var i = 0; i < nodes.length; i++) {
      var node = nodes[i];

      if (node.componentOptions) {
        if (node.componentOptions.tag === hyphenate(config.messagesComponent)) {
          foundVnodes.messages = node;
        }
      }

      if (node.tag === 'label' && !foundVnodes.label) {
        foundVnodes.label = node;
      }

      if (node.data) {
        if (node.data.model) {
          // model check has to come first. If a component has
          // a directive and v-model, the directive will be in .directives
          // and v-modelstored in .model
          foundVnodes.vModel.push(node);
        } else if (node.data.directives) {
          var match = node.data.directives.filter(function (v) {
            return v.name === 'model';
          });
          if (match.length) {
            foundVnodes.vModel.push(node);
          }
        }
      }
      if (node.children) {
        traverse(node.children);
      } else if (node.componentOptions && node.componentOptions.children) {
        traverse(node.componentOptions.children);
      }
    }
  }

  traverse(nodes);

  return foundVnodes;
}

function getName(vnode) {
  if (vnode.data && vnode.data.attrs && vnode.data.attrs.name) {
    return vnode.data.attrs.name;
  } else if (vnode.componentOptions && vnode.componentOptions.propsData && vnode.componentOptions.propsData.name) {
    return vnode.componentOptions.propsData.name;
  }
}

var hyphenateRE = /([^-])([A-Z])/g;
function hyphenate(str) {
  return str.replace(hyphenateRE, '$1-$2').replace(hyphenateRE, '$1-$2').toLowerCase();
}

function randomId() {
  return Math.random().toString(36).substr(2, 10);
}

// https://davidwalsh.name/javascript-debounce-function
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
        args = arguments;
    var later = function later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

function isShallowObjectDifferent(a, b) {
  var aValue = '';
  var bValue = '';
  Object.keys(a).sort().filter(function (v) {
    return typeof a[v] !== 'function';
  }).forEach(function (v) {
    return aValue += a[v];
  });
  Object.keys(b).sort().filter(function (v) {
    return typeof a[v] !== 'function';
  }).forEach(function (v) {
    return bValue += b[v];
  });
  return aValue !== bValue;
}

var vueFormConfig = 'VueFormProviderConfig' + randomId();
var vueFormState = 'VueFormProviderState' + randomId();
var vueFormParentForm = 'VueFormProviderParentForm' + randomId();

var hasOwn = Object.prototype.hasOwnProperty;
var toStr = Object.prototype.toString;
var defineProperty$1 = Object.defineProperty;
var gOPD = Object.getOwnPropertyDescriptor;

var isArray = function isArray(arr) {
	if (typeof Array.isArray === 'function') {
		return Array.isArray(arr);
	}

	return toStr.call(arr) === '[object Array]';
};

var isPlainObject = function isPlainObject(obj) {
	if (!obj || toStr.call(obj) !== '[object Object]') {
		return false;
	}

	var hasOwnConstructor = hasOwn.call(obj, 'constructor');
	var hasIsPrototypeOf = obj.constructor && obj.constructor.prototype && hasOwn.call(obj.constructor.prototype, 'isPrototypeOf');
	// Not own constructor property must be Object
	if (obj.constructor && !hasOwnConstructor && !hasIsPrototypeOf) {
		return false;
	}

	// Own properties are enumerated firstly, so to speed up,
	// if last one is own, then all properties are own.
	var key;
	for (key in obj) { /**/ }

	return typeof key === 'undefined' || hasOwn.call(obj, key);
};

// If name is '__proto__', and Object.defineProperty is available, define __proto__ as an own property on target
var setProperty = function setProperty(target, options) {
	if (defineProperty$1 && options.name === '__proto__') {
		defineProperty$1(target, options.name, {
			enumerable: true,
			configurable: true,
			value: options.newValue,
			writable: true
		});
	} else {
		target[options.name] = options.newValue;
	}
};

// Return undefined instead of __proto__ if '__proto__' is not an own property
var getProperty = function getProperty(obj, name) {
	if (name === '__proto__') {
		if (!hasOwn.call(obj, name)) {
			return void 0;
		} else if (gOPD) {
			// In early versions of node, obj['__proto__'] is buggy when obj has
			// __proto__ as an own property. Object.getOwnPropertyDescriptor() works.
			return gOPD(obj, name).value;
		}
	}

	return obj[name];
};

var extend = function extend() {
	var options, name, src, copy, copyIsArray, clone;
	var target = arguments[0];
	var i = 1;
	var length = arguments.length;
	var deep = false;

	// Handle a deep copy situation
	if (typeof target === 'boolean') {
		deep = target;
		target = arguments[1] || {};
		// skip the boolean and the target
		i = 2;
	}
	if (target == null || (typeof target !== 'object' && typeof target !== 'function')) {
		target = {};
	}

	for (; i < length; ++i) {
		options = arguments[i];
		// Only deal with non-null/undefined values
		if (options != null) {
			// Extend the base object
			for (name in options) {
				src = getProperty(target, name);
				copy = getProperty(options, name);

				// Prevent never-ending loop
				if (target !== copy) {
					// Recurse if we're merging plain objects or arrays
					if (deep && copy && (isPlainObject(copy) || (copyIsArray = isArray(copy)))) {
						if (copyIsArray) {
							copyIsArray = false;
							clone = src && isArray(src) ? src : [];
						} else {
							clone = src && isPlainObject(src) ? src : {};
						}

						// Never move original objects, clone them
						setProperty(target, { name: name, newValue: extend(deep, clone, copy) });

					// Don't bring in undefined values
					} else if (typeof copy !== 'undefined') {
						setProperty(target, { name: name, newValue: copy });
					}
				}
			}
		}
	}

	// Return the modified object
	return target;
};

var vueForm = {
  render: function render(h) {
    var _this = this;

    var attrs = {};

    if (typeof window !== 'undefined') {
      attrs.novalidate = '';
    }

    return h(this.tag || this.vueFormConfig.formTag, {
      on: {
        submit: function submit(event) {
          if (_this.state.$pending) {
            event.preventDefault();
            _this.vueFormConfig.Promise.all(_this.promises).then(function () {
              _this.state._submit();
              _this.$emit('submit', event);
              _this.promises = [];
            });
          } else {
            _this.state._submit();
            _this.$emit('submit', event);
          }
        },
        reset: function reset(event) {
          _this.state._reset();
          _this.$emit('reset', event);
        }
      },
      class: this.className,
      attrs: attrs
    }, [this.$slots.default]);
  },

  props: {
    state: {
      type: Object,
      required: true
    },
    tag: String,
    showMessages: String
  },
  inject: { vueFormConfig: vueFormConfig },
  provide: function provide() {
    var _ref;

    return _ref = {}, defineProperty(_ref, vueFormState, this.state), defineProperty(_ref, vueFormParentForm, this), _ref;
  },

  data: function data() {
    return {
      promises: []
    };
  },
  created: function created() {
    var _this2 = this;

    if (!this.state) {
      return;
    }
    var controls = {};
    var state = this.state;
    var formstate = {
      $dirty: false,
      $pristine: true,
      $valid: true,
      $invalid: false,
      $submitted: false,
      $touched: false,
      $untouched: true,
      $focused: false,
      $pending: false,
      $error: {},
      $submittedState: {},
      _id: '',
      _submit: function _submit() {
        _this2.state.$submitted = true;
        _this2.state._cloneState();
      },
      _cloneState: function _cloneState() {
        var cloned = JSON.parse(JSON.stringify(state));
        delete cloned.$submittedState;
        Object.keys(cloned).forEach(function (key) {
          _this2.$set(_this2.state.$submittedState, key, cloned[key]);
        });
      },
      _addControl: function _addControl(ctrl) {
        controls[ctrl.$name] = ctrl;
        _this2.$set(state, ctrl.$name, ctrl);
      },
      _removeControl: function _removeControl(ctrl) {
        delete controls[ctrl.$name];
        _this2.$delete(_this2.state, ctrl.$name);
        _this2.$delete(_this2.state.$error, ctrl.$name);
      },
      _validate: function _validate() {
        Object.keys(controls).forEach(function (key) {
          controls[key]._validate();
        });
      },
      _reset: function _reset() {
        state.$submitted = false;
        state.$pending = false;
        state.$submittedState = {};
        Object.keys(controls).forEach(function (key) {
          var control = controls[key];
          control._hasFocused = false;
          control._setUntouched();
          control._setPristine();
          control.$submitted = false;
          control.$pending = false;
        });
      }
    };

    Object.keys(formstate).forEach(function (key) {
      _this2.$set(_this2.state, key, formstate[key]);
    });

    this.$watch('state', function () {
      var isDirty = false;
      var isValid = true;
      var isTouched = false;
      var isFocused = false;
      var isPending = false;
      Object.keys(controls).forEach(function (key) {
        var control = controls[key];

        control.$submitted = state.$submitted;

        if (control.$dirty) {
          isDirty = true;
        }
        if (control.$touched) {
          isTouched = true;
        }
        if (control.$focused) {
          isFocused = true;
        }
        if (control.$pending) {
          isPending = true;
        }
        if (!control.$valid) {
          isValid = false;
          // add control to errors
          _this2.$set(state.$error, control.$name, control);
        } else {
          _this2.$delete(state.$error, control.$name);
        }
      });

      state.$dirty = isDirty;
      state.$pristine = !isDirty;
      state.$touched = isTouched;
      state.$untouched = !isTouched;
      state.$focused = isFocused;
      state.$valid = isValid;
      state.$invalid = !isValid;
      state.$pending = isPending;
    }, {
      deep: true,
      immediate: true
    });

    /* watch pristine? if set to true, set all children to pristine
    Object.keys(controls).forEach((ctrl) => {
      controls[ctrl].setPristine();
    });*/
  },

  computed: {
    className: function className() {
      var classes = getClasses(this.vueFormConfig.formClasses, this.state);
      return classes;
    }
  },
  methods: {
    reset: function reset() {
      this.state._reset();
    },
    validate: function validate() {
      this.state._validate();
    }
  }
};

var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};





function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var scope_eval = createCommonjsModule(function (module) {
// Generated by CoffeeScript 1.10.0
(function() {
  var hasProp = {}.hasOwnProperty,
    slice = [].slice;

  module.exports = function(source, scope) {
    var key, keys, value, values;
    keys = [];
    values = [];
    for (key in scope) {
      if (!hasProp.call(scope, key)) continue;
      value = scope[key];
      if (key === 'this') {
        continue;
      }
      keys.push(key);
      values.push(value);
    }
    return Function.apply(null, slice.call(keys).concat(["return eval(" + (JSON.stringify(source)) + ")"])).apply(scope["this"], values);
  };

}).call(commonjsGlobal);
});

function findLabel(nodes) {
  if (!nodes) {
    return;
  }
  for (var i = 0; i < nodes.length; i++) {
    var vnode = nodes[i];
    if (vnode.tag === 'label') {
      return nodes[i];
    } else if (nodes[i].children) {
      return findLabel(nodes[i].children);
    }
  }
}

var messages = {
  inject: { vueFormConfig: vueFormConfig, vueFormState: vueFormState, vueFormParentForm: vueFormParentForm },
  render: function render(h) {
    var _this = this;

    var children = [];
    var field = this.formstate[this.fieldname];
    if (field && field.$error && this.isShown) {
      Object.keys(field.$error).forEach(function (key) {
        if (_this.$slots[key] || _this.$scopedSlots[key]) {
          var out = _this.$slots[key] || _this.$scopedSlots[key](field);
          if (_this.autoLabel) {
            var label = findLabel(out);
            if (label) {
              label.data = label.data || {};
              label.data.attrs = label.data.attrs || {};
              label.data.attrs.for = field._id;
            }
          }
          children.push(out);
        }
      });
      if (!children.length && field.$valid) {
        if (this.$slots.default || this.$scopedSlots.default) {
          var out = this.$slots.default || this.$scopedSlots.default(field);
          if (this.autoLabel) {
            var label = findLabel(out);
            if (label) {
              label.data = label.data || {};
              label.data.attrs = label.data.attrs || {};
              label.data.attrs.for = field._id;
            }
          }
          children.push(out);
        }
      }
    }
    return h(this.tag || this.vueFormConfig.messagesTag, children);
  },

  props: {
    state: Object,
    name: String,
    show: {
      type: String,
      default: ''
    },
    tag: {
      type: String
    },
    autoLabel: Boolean
  },
  data: function data() {
    return {
      formstate: null,
      fieldname: ''
    };
  },
  created: function created() {
    this.fieldname = this.name;
    this.formstate = this.state || this.vueFormState;
  },

  computed: {
    isShown: function isShown() {
      var field = this.formstate[this.fieldname];
      var show = this.show || this.vueFormParentForm.showMessages || this.vueFormConfig.showMessages;

      if (!show || !field) {
        return true;
      }

      return scope_eval(show, field);
    }
  }
};

var validate = {
  render: function render(h) {
    var _this = this;

    var foundVnodes = getVModelAndLabel(this.$slots.default, this.vueFormConfig);
    var vModelnodes = foundVnodes.vModel;
    var attrs = {
      for: null
    };
    if (vModelnodes.length) {
      this.name = getName(vModelnodes[0]);

      if (foundVnodes.messages) {
        this.$nextTick(function () {
          var messagesVm = foundVnodes.messages.componentInstance;
          if (messagesVm) {
            messagesVm.fieldname = messagesVm.fieldname || _this.name;
          }
        });
      }

      if (this.autoLabel) {
        var id = vModelnodes[0].data.attrs.id || this.fieldstate._id;
        this.fieldstate._id = id;
        vModelnodes[0].data.attrs.id = id;
        if (foundVnodes.label) {
          foundVnodes.label.data = foundVnodes.label.data || {};
          foundVnodes.label.data.attrs = foundVnodes.label.data.attrs || {};
          foundVnodes.label.data.attrs.for = id;
        } else if (this.tag === 'label') {
          attrs.for = id;
        }
      }
      vModelnodes.forEach(function (vnode) {
        if (!vnode.data.directives) {
          vnode.data.directives = [];
        }
        vnode.data.directives.push({ name: 'vue-form-validator', value: { fieldstate: _this.fieldstate, config: _this.vueFormConfig } });
        vnode.data.attrs['vue-form-validator'] = '';
        vnode.data.attrs['debounce'] = _this.debounce;
      });
    } else {
      //console.warn('Element with v-model not found');
    }
    return h(this.tag || this.vueFormConfig.validateTag, { 'class': this.className, attrs: attrs }, this.$slots.default);
  },

  props: {
    state: Object,
    custom: null,
    autoLabel: Boolean,
    tag: {
      type: String
    },
    debounce: Number
  },
  inject: { vueFormConfig: vueFormConfig, vueFormState: vueFormState, vueFormParentForm: vueFormParentForm },
  data: function data() {
    return {
      name: '',
      formstate: null,
      fieldstate: {}
    };
  },

  methods: {
    getClasses: function getClasses$$1(classConfig) {
      var s = this.fieldstate;
      return Object.keys(s.$error).reduce(function (classes, error) {
        classes[classConfig.invalid + '-' + hyphenate(error)] = true;
        return classes;
      }, getClasses(classConfig, s));
    }
  },
  computed: {
    className: function className() {
      return this.getClasses(this.vueFormConfig.validateClasses);
    },
    inputClassName: function inputClassName() {
      return this.getClasses(this.vueFormConfig.inputClasses);
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    this.fieldstate.$name = this.name;
    this.formstate._addControl(this.fieldstate);

    var vModelEls = this.$el.querySelectorAll('[vue-form-validator]');

    // add classes to the input element
    this.$watch('inputClassName', function (value, oldValue) {
      var out = void 0;

      var _loop = function _loop(i, el) {
        if (oldValue) {
          Object.keys(oldValue).filter(function (k) {
            return oldValue[k];
          }).forEach(function (k) {
            return removeClass(el, k);
          });
        }
        out = [];
        Object.keys(value).filter(function (k) {
          return value[k];
        }).forEach(function (k) {
          out.push(k);
          addClass(el, k);
        });
      };

      for (var i = 0, el; el = vModelEls[i++];) {
        _loop(i, el);
      }
      _this2.fieldstate._className = out;
    }, {
      deep: true,
      immediate: true
    });

    this.$watch('name', function (value, oldValue) {
      _this2.formstate._removeControl(_this2.fieldstate);
      _this2.fieldstate.$name = value;
      _this2.formstate._addControl(_this2.fieldstate);
    });
  },
  created: function created() {
    var _this4 = this;

    this.formstate = this.state || this.vueFormState;
    var vm = this;
    var pendingValidators = [];
    var _val = void 0;
    var prevVnode = void 0;
    this.fieldstate = {
      $name: '',
      $dirty: false,
      $pristine: true,
      $valid: true,
      $invalid: false,
      $touched: false,
      $untouched: true,
      $focused: false,
      $pending: false,
      $submitted: false,
      $error: {},
      $attrs: {},
      _className: null,
      _id: 'vf' + randomId(),
      _setValidatorValidity: function _setValidatorValidity(validator, isValid) {
        if (isValid) {
          vm.$delete(this.$error, validator);
        } else {
          vm.$set(this.$error, validator, true);
        }
      },
      _setValidity: function _setValidity(isValid) {
        this.$valid = isValid;
        this.$invalid = !isValid;
      },
      _setDirty: function _setDirty() {
        this.$dirty = true;
        this.$pristine = false;
      },
      _setPristine: function _setPristine() {
        this.$dirty = false;
        this.$pristine = true;
      },
      _setTouched: function _setTouched() {
        this.$touched = true;
        this.$untouched = false;
      },
      _setUntouched: function _setUntouched() {
        this.$touched = false;
        this.$untouched = true;
      },
      _setFocused: function _setFocused(value) {
        this.$focused = typeof value === 'boolean' ? value : false;
        if (this.$focused) {
          this._setHasFocused();
        } else {
          this._setTouched();
        }
      },
      _setHasFocused: function _setHasFocused() {
        this._hasFocused = true;
      },

      _hasFocused: false,
      _validators: {},
      _validate: function _validate(vnode) {
        var _this3 = this;

        if (!vnode) {
          vnode = prevVnode;
        } else {
          prevVnode = vnode;
        }
        this.$pending = true;
        var isValid = true;
        var emptyAndRequired = false;
        var value = vModelValue(vnode.data);
        _val = value;

        var pending = {
          promises: [],
          names: []
        };

        pendingValidators.push(pending);

        var attrs = vnode.data.attrs || {};
        var childvm = vnode.componentInstance;
        if (childvm && childvm._vfValidationData_) {
          attrs = extend({}, attrs, childvm._vfValidationData_);
        }

        var propsData = vnode.componentOptions && vnode.componentOptions.propsData ? vnode.componentOptions.propsData : {};

        Object.keys(this._validators).forEach(function (validator) {
          // when value is empty and current validator is not the required validator, the field is valid
          if ((value === '' || value === undefined || value === null) && validator !== 'required') {
            _this3._setValidatorValidity(validator, true);
            emptyAndRequired = true;
            // return early, required validator will
            // fall through if it is present
            return;
          }

          var attrValue = typeof attrs[validator] !== 'undefined' ? attrs[validator] : propsData[validator];
          var isFunction = typeof _this3._validators[validator] === 'function';

          // match vue behaviour, ignore if attribute is null or undefined. But for type=email|url|number and custom validators, the value will be null, so allow with _allowNulls
          if (isFunction && (attrValue === null || typeof attrValue === 'undefined') && !_this3._validators[validator]._allowNulls) {
            return;
          }

          if (attrValue) {
            _this3.$attrs[validator] = attrValue;
          }

          var result = isFunction ? _this3._validators[validator](value, attrValue, vnode) : vm.custom[validator];

          if (typeof result === 'boolean') {
            if (result) {
              _this3._setValidatorValidity(validator, true);
            } else {
              isValid = false;
              _this3._setValidatorValidity(validator, false);
            }
          } else {
            pending.promises.push(result);
            pending.names.push(validator);
            vm.vueFormParentForm.promises.push(result);
          }
        });

        if (pending.promises.length) {
          vm.vueFormConfig.Promise.all(pending.promises).then(function (results) {

            // only concerned with the last promise results, in case
            // async responses return out of order
            if (pending !== pendingValidators[pendingValidators.length - 1]) {
              //console.log('ignoring old promise', pending.promises);
              return;
            }

            pendingValidators = [];

            results.forEach(function (result, i) {
              var name = pending.names[i];
              if (result) {
                _this3._setValidatorValidity(name, true);
              } else {
                isValid = false;
                _this3._setValidatorValidity(name, false);
              }
            });
            _this3._setValidity(isValid);
            _this3.$pending = false;
          });
        } else {
          this._setValidity(isValid);
          this.$pending = false;
        }
      }
    };

    // add custom validators
    if (this.custom) {
      Object.keys(this.custom).forEach(function (prop) {
        if (typeof _this4.custom[prop] === 'function') {
          _this4.custom[prop]._allowNulls = true;
          _this4.fieldstate._validators[prop] = _this4.custom[prop];
        } else {
          _this4.fieldstate._validators[prop] = _this4.custom[prop];
        }
      });
    }

    this.$watch('custom', function (v, oldV) {
      if (!oldV) {
        return;
      }
      if (isShallowObjectDifferent(v, oldV)) {
        _this4.fieldstate._validate();
      }
    }, {
      deep: true
    });
  },
  destroyed: function destroyed() {
    this.formstate._removeControl(this.fieldstate);
  }
};

var field = {
  inject: { vueFormConfig: vueFormConfig },
  render: function render(h) {
    var foundVnodes = getVModelAndLabel(this.$slots.default, this.vueFormConfig);
    var vModelnodes = foundVnodes.vModel;
    var attrs = {
      for: null
    };
    if (vModelnodes.length) {
      if (this.autoLabel) {
        var id = vModelnodes[0].data.attrs && vModelnodes[0].data.attrs.id || 'vf' + randomId();
        vModelnodes[0].data.attrs.id = id;
        if (foundVnodes.label) {
          foundVnodes.label.data = foundVnodes.label.data || {};
          foundVnodes.label.data.attrs = foundVnodes.label.data.attrs = {};
          foundVnodes.label.data.attrs.for = id;
        } else if (this.tag === 'label') {
          attrs.for = id;
        }
      }
    }
    return h(this.tag || this.vueFormConfig.fieldTag, { attrs: attrs }, this.$slots.default);
  },

  props: {
    tag: {
      type: String
    },
    autoLabel: {
      type: Boolean,
      default: true
    }
  }
};

var debouncedValidators = {};

function addValidators(attrs, validators, fieldValidators) {
  Object.keys(attrs).forEach(function (attr) {
    var prop = attr === 'type' ? attrs[attr].toLowerCase() : attr.toLowerCase();

    if (validators[prop] && !fieldValidators[prop]) {
      fieldValidators[prop] = validators[prop];
    }
  });
}

function compareChanges(vnode, oldvnode, validators) {

  var hasChanged = false;
  var attrs = vnode.data.attrs || {};
  var oldAttrs = oldvnode.data.attrs || {};
  var out = {};

  if (vModelValue(vnode.data) !== vModelValue(oldvnode.data)) {
    out.vModel = true;
    hasChanged = true;
  }

  Object.keys(validators).forEach(function (validator) {
    if (attrs[validator] !== oldAttrs[validator]) {
      out[validator] = true;
      hasChanged = true;
    }
  });

  // if is a component
  if (vnode.componentOptions && vnode.componentOptions.propsData) {
    var _attrs = vnode.componentOptions.propsData;
    var _oldAttrs = oldvnode.componentOptions.propsData;
    Object.keys(validators).forEach(function (validator) {
      if (_attrs[validator] !== _oldAttrs[validator]) {
        out[validator] = true;
        hasChanged = true;
      }
    });
  }

  if (hasChanged) {
    return out;
  }
}

var vueFormValidator = {
  name: 'vue-form-validator',
  bind: function bind(el, binding, vnode) {
    var fieldstate = binding.value.fieldstate;
    var validators = binding.value.config.validators;

    var attrs = vnode.data.attrs || {};
    var inputName = getName(vnode);

    if (!inputName) {
      console.warn('vue-form: name attribute missing');
      return;
    }

    if (attrs.debounce) {
      debouncedValidators[fieldstate._id] = debounce(function (fieldstate, vnode) {
        if (fieldstate._hasFocused) {
          fieldstate._setDirty();
        }
        fieldstate._validate(vnode);
      }, attrs.debounce);
    }

    // add validators
    addValidators(attrs, validators, fieldstate._validators);

    // if is a component, a validator attribute could be a prop this component uses
    if (vnode.componentOptions && vnode.componentOptions.propsData) {
      addValidators(vnode.componentOptions.propsData, validators, fieldstate._validators);
    }

    fieldstate._validate(vnode);

    // native listeners
    el.addEventListener('blur', function () {
      fieldstate._setFocused(false);
    }, false);
    el.addEventListener('focus', function () {
      fieldstate._setFocused(true);
    }, false);

    // component listeners
    var vm = vnode.componentInstance;
    if (vm) {
      vm.$on('blur', function () {
        fieldstate._setFocused(false);
      });
      vm.$on('focus', function () {
        fieldstate._setFocused(true);
      });

      vm.$once('vf:addFocusListeners', function () {
        el.addEventListener('focusout', function () {
          fieldstate._setFocused(false);
        }, false);
        el.addEventListener('focusin', function () {
          fieldstate._setFocused(true);
        }, false);
      });

      vm.$on('vf:validate', function (data) {
        if (!vm._vfValidationData_) {
          addValidators(data, validators, fieldstate._validators);
        }
        vm._vfValidationData_ = data;
        fieldstate._validate(vm.$vnode);
      });
    }
  },
  update: function update(el, binding, vnode, oldVNode) {
    var validators = binding.value.config.validators;

    var changes = compareChanges(vnode, oldVNode, validators);
    var fieldstate = binding.value.fieldstate;


    var attrs = vnode.data.attrs || {};
    var vm = vnode.componentInstance;
    if (vm && vm._vfValidationData_) {
      attrs = extend({}, attrs, vm[vm._vfValidationData_]);
    }

    if (vnode.elm.className.indexOf(fieldstate._className[0]) === -1) {
      vnode.elm.className = vnode.elm.className + ' ' + fieldstate._className.join(' ');
    }

    if (!changes) {
      return;
    }

    if (changes.vModel) {
      // re-validate all
      if (attrs.debounce) {
        fieldstate.$pending = true;
        debouncedValidators[fieldstate._id](fieldstate, vnode);
      } else {
        if (fieldstate._hasFocused) {
          fieldstate._setDirty();
        }
        fieldstate._validate(vnode);
      }
    } else {
      // attributes have changed
      // to do: loop through them and re-validate changed ones
      //for(let prop in changes) {
      //  fieldstate._validate(vnode, validator);
      //}
      // for now
      fieldstate._validate(vnode);
    }
  }
};

function VueFormBase(options) {
  var _components;

  var c = extend(true, {}, config, options);
  this.provide = function () {
    return defineProperty({}, vueFormConfig, c);
  };
  this.components = (_components = {}, defineProperty(_components, c.formComponent, vueForm), defineProperty(_components, c.messagesComponent, messages), defineProperty(_components, c.validateComponent, validate), defineProperty(_components, c.fieldComponent, field), _components);
  this.directives = { vueFormValidator: vueFormValidator };
}

var VueForm = function (_VueFormBase) {
  inherits(VueForm, _VueFormBase);

  function VueForm() {
    classCallCheck(this, VueForm);
    return possibleConstructorReturn(this, (VueForm.__proto__ || Object.getPrototypeOf(VueForm)).apply(this, arguments));
  }

  createClass(VueForm, null, [{
    key: 'install',
    value: function install(Vue, options) {
      Vue.mixin(new this(options));
    }
  }, {
    key: 'installed',
    get: function get$$1() {
      return !!this.install.done;
    },
    set: function set$$1(val) {
      this.install.done = val;
    }
  }]);
  return VueForm;
}(VueFormBase);

VueFormBase.call(VueForm);
// temp fix for vue 2.3.0
VueForm.options = new VueForm();

return VueForm;

})));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(9)))

/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(671)
  __webpack_require__(673)
  __webpack_require__(675)
}
var normalizeComponent = __webpack_require__(8)
/* script */
var __vue_script__ = __webpack_require__(677)
/* template */
var __vue_template__ = __webpack_require__(678)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b93c74f2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/components/auth/login.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b93c74f2", Component.options)
  } else {
    hotAPI.reload("data-v-b93c74f2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 559:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var options = {
    validators: {
        checkbox: function checkbox(value, attrValue, vnode) {
            // return true to set input as $valid, false to set as $invalid
            return value;
        },
        sameas: function sameas(value, attrValue, vnode) {
            return value == attrValue;
        }
    }
};

/* harmony default export */ __webpack_exports__["a"] = (options);

/***/ }),

/***/ 671:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(672);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(7)("6c3558b8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b93c74f2\",\"scoped\":false,\"hasInlineConfig\":true}!./bootstrapValidator.min.css", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b93c74f2\",\"scoped\":false,\"hasInlineConfig\":true}!./bootstrapValidator.min.css");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 672:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(6)(false);
// imports


// module
exports.push([module.i, "/*!\n * BootstrapValidator (http://bootstrapvalidator.com)\n * The best jQuery plugin to validate form fields. Designed to use with Bootstrap 3\n *\n * @version     v0.5.3, built on 2014-11-05 9:14:18 PM\n * @author      https://twitter.com/nghuuphuoc\n * @copyright   (c) 2013 - 2014 Nguyen Huu Phuoc\n * @license     Commercial: http://bootstrapvalidator.com/license/\n *              Non-commercial: http://creativecommons.org/licenses/by-nc-nd/3.0/\n */\n.bv-form .help-block{margin-bottom:0\n}\n.bv-form .tooltip-inner{text-align:left\n}\n.nav-tabs li.bv-tab-success>a{color:#3c763d\n}\n.nav-tabs li.bv-tab-error>a{color:#a94442\n}\n.bv-form .bv-icon-no-label{top:0\n}\n.bv-form .bv-icon-input-group{top:0;z-index:100\n}", ""]);

// exports


/***/ }),

/***/ 673:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(674);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(7)("91d9aaee", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b93c74f2\",\"scoped\":true,\"hasInlineConfig\":true}!./login.css", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b93c74f2\",\"scoped\":true,\"hasInlineConfig\":true}!./login.css");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 674:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(6)(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,600);", ""]);

// module
exports.push([module.i, "/* Fonts Imported from Google */\n\n/*   Core: General style\n----------------------------*/\nbody[data-v-b93c74f2] {\n    font-family: 'Open Sans', sans-serif;\n    height: 100%;\n    padding: 6.5% 0;\n}\n#sign-in[data-v-b93c74f2],\n#sign-up[data-v-b93c74f2] {\n    background: radial-gradient(ellipse at center, #5A93AF 0%, #004E74 100%);\n}\n.login-form[data-v-b93c74f2] {\n    background-color: rgba(255, 255, 255, .9);\n    border-radius: 4px;\n    padding-top: 15px;\n    padding-bottom: 15px;\n}\n.btn-primary[data-v-b93c74f2],\n.primary[data-v-b93c74f2],\n.tags a.primary[data-v-b93c74f2] {\n    border-color: #509DE0;\n    background: #509DE0 !important;\n}\n.btn[data-v-b93c74f2]{\n    cursor: pointer;\n}\n.btn-primary[data-v-b93c74f2]:hover {\n    border-color: #509DE0;\n    background: #509DE0 !important;\n}\n.icheckbox_minimal-blue[data-v-b93c74f2] {\n    background-position: -20px 0;\n}\na[data-v-b93c74f2]:hover,\na[data-v-b93c74f2]:focus {\n    text-decoration: none;\n}\n.social a[data-v-b93c74f2] {\n    border-radius: 50px;\n    padding: 10px 12px 2px 12px;\n    font-size: 25px;\n}\n.btn-facebook[data-v-b93c74f2],\n.btn-facebook[data-v-b93c74f2]:hover,\n.btn-facebook[data-v-b93c74f2]:focus {\n    color: #5F7AB3;\n    border: 1px solid #5F7AB3;\n}\n.btn-twitter[data-v-b93c74f2],\n.btn-twitter[data-v-b93c74f2]:hover,\n.btn-twitter[data-v-b93c74f2]:focus {\n    background: #3BACF2;\n    color: rgba(255, 255, 255, .85);\n    border: 1px solid #3BACF2;\n}\n.has-error .help-block[data-v-b93c74f2],\n.has-error .control-label[data-v-b93c74f2],\n.has-error .radio[data-v-b93c74f2],\n.has-error .checkbox[data-v-b93c74f2],\n.has-error .radio-inline[data-v-b93c74f2],\n.has-error .checkbox-inline[data-v-b93c74f2] {\n    color: #ff6666;\n}\n.has-error.radio label[data-v-b93c74f2],\n.has-error.checkbox label[data-v-b93c74f2],\n.has-success.radio label[data-v-b93c74f2],\n.has-success.checkbox label[data-v-b93c74f2] {\n    color: #333;\n}\n.radio[data-v-b93c74f2],\n.checkbox[data-v-b93c74f2] {\n    display: block;\n}\n.has-error .form-control[data-v-b93c74f2],\n.has-error .form-control[data-v-b93c74f2]:focus {\n    border-color: #ff6666;\n    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\n}\n.has-success .form-control[data-v-b93c74f2],\n.has-success .form-control[data-v-b93c74f2]:focus {\n    border-color: #66cc99;\n    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\n}\n.btn-google[data-v-b93c74f2],\n.btn-google[data-v-b93c74f2]:hover,\n.btn-google[data-v-b93c74f2]:focus {\n    color: #dd4b39;\n    border: 1px solid #dd4b39;\n}\n.social .alter[data-v-b93c74f2] {\n    font-size: 21px;\n    color: #666;\n    margin-top: 15px;\n    margin-bottom: 15px;\n}\n#forgot[data-v-b93c74f2]:hover {\n    color: #2a6496 !important;\n}\n@media screen and (max-width: 400px) {\n.mar-left5[data-v-b93c74f2] {\n        margin-left: 18px !important;\n        margin-top: -10px;\n}\n.mar-top4[data-v-b93c74f2] {\n        margin-top: -10px;\n}\n.mar-left[data-v-b93c74f2] {\n        margin-left: -5px !important;\n}\n.sign-up[data-v-b93c74f2] {\n        float: left !important;\n}\n#forgot[data-v-b93c74f2]::after {\n        content: \"\\A\";\n        white-space: pre;\n}\n}\n\n\n/* Chrome, Safari, Opera */\n@-webkit-keyframes CAnimation-data-v-b93c74f2 {\nfrom {\n        -webkit-filter: hue-rotate(0deg);\n}\nto {\n        -webkit-filter: hue-rotate(-360deg);\n}\n}\n\n\n/* Standard syntax */\n@keyframes CAnimation-data-v-b93c74f2 {\nfrom {\n        -webkit-filter: hue-rotate(0deg);\n}\nto {\n        -webkit-filter: hue-rotate(-360deg);\n}\n}\n.forgot[data-v-b93c74f2] {\n    color: #428BCA !important;\n}\na[data-v-b93c74f2]:hover {\n    text-decoration: none;\n}\n.radio label[data-v-b93c74f2],\n.checkbox label[data-v-b93c74f2] {\n    padding-left: 0;\n}\n@media (min-width: 768px) {\n.form_width[data-v-b93c74f2] {\n        margin-left: 12%;\n}\n}\n\n\n/* ===== Preloader =====*/\n.preloader[data-v-b93c74f2] {\n    position: fixed;\n    width: 100%;\n    height: 100%;\n    top: 0;\n    left: 0;\n    z-index: 100000;\n    backface-visibility: hidden;\n    background: #ffffff;\n}\n.loader_img[data-v-b93c74f2] {\n    width: 50px;\n    height: 50px;\n    position: absolute;\n    left: 50%;\n    top: 50%;\n    background-position: center;\n    margin: -25px 0 0 -25px;\n}\n", ""]);

// exports


/***/ }),

/***/ 675:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(676);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(7)("00c62809", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b93c74f2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=2!./login.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b93c74f2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=2!./login.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 676:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(6)(false);
// imports


// module
exports.push([module.i, "\n.login[data-v-b93c74f2] {\n    padding-top: 6.5%;\n    padding-bottom: 2%;\n    width: 100%;\n    height: 100vh;\n    top: 0;\n    bottom: 0;\n    left: 0;\n    right: 0;\n    background: radial-gradient(ellipse at center, #5A93AF 0%, #004E74 100%);\n    overflow-y: auto;\n}\n", ""]);

// exports


/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__validations_validations_js__ = __webpack_require__(559);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api__ = __webpack_require__(315);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vue_form___default.a, __WEBPACK_IMPORTED_MODULE_2__validations_validations_js__["a" /* default */]);

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "login",
    data: function data() {
        return {
            formstate: {},
            model: {
                username: "",
                password: ""

            },
            showDismissibleAlert: false
        };
    },

    methods: {
        onSubmit: function onSubmit() {
            var _this = this;

            if (this.formstate.$invalid) {
                return;
            } else {
                __WEBPACK_IMPORTED_MODULE_3__api__["e" /* Auth */].login(this.model).then(function (response) {
                    // console.log(response)
                    _this.handleLogin(response.data);
                    // window.axios.defaults.headers.common['X-CSRF-TOKEN'] = response.data.csrfToken;
                    _this.$router.push("/admin/index1");
                }).catch(function (error) {
                    _this.showDismissibleAlert = true;
                });
                // 
            }
        },
        handleLogin: function handleLogin(data) {

            this.$store.dispatch('setUser', data);
            // this.$store.dispatch('setAuthority', authority)
            this.$store.dispatch('setLoggedIn', true);
            localStorage.setItem('user', JSON.stringify(data));
        }
    },
    mounted: function mounted() {},
    destroyed: function destroyed() {}
});

/***/ }),

/***/ 678:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid login" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-lg-4 col-10 col-sm-8 mx-auto login-form" },
          [
            _vm._m(0),
            _vm._v(" "),
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "row" }, [
                _c(
                  "div",
                  { staticClass: "col-12" },
                  [
                    _c(
                      "b-alert",
                      {
                        attrs: {
                          variant: "danger",
                          dismissible: "",
                          show: _vm.showDismissibleAlert
                        },
                        on: {
                          dismissed: function($event) {
                            _vm.showDismissibleAlert = false
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                            These credentials do not match our records.\n                            "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "vue-form",
                      {
                        attrs: { state: _vm.formstate },
                        on: {
                          submit: function($event) {
                            $event.preventDefault()
                            return _vm.onSubmit($event)
                          }
                        }
                      },
                      [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-sm-12 mt-3 " }, [
                            _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c(
                                  "validate",
                                  { attrs: { tag: "div" } },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.model.username,
                                          expression: "model.username"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        name: "username",
                                        id: "username",
                                        type: "text",
                                        required: "",
                                        autofocus: "",
                                        placeholder: "Username"
                                      },
                                      domProps: { value: _vm.model.username },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.model,
                                            "username",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "field-messages",
                                      {
                                        staticClass: "text-danger",
                                        attrs: {
                                          name: "email",
                                          show: "$invalid && $submitted"
                                        }
                                      },
                                      [
                                        _c(
                                          "div",
                                          {
                                            attrs: { slot: "required" },
                                            slot: "required"
                                          },
                                          [
                                            _vm._v(
                                              "Username is a required field"
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            attrs: { slot: "Username" },
                                            slot: "Username"
                                          },
                                          [_vm._v("Username is not valid")]
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-sm-12" }, [
                            _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c(
                                  "validate",
                                  { attrs: { tag: "div" } },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.model.password,
                                          expression: "model.password"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        name: "password",
                                        id: "password",
                                        type: "password",
                                        required: "",
                                        placeholder: "Password",
                                        minlength: "6",
                                        maxlength: "10"
                                      },
                                      domProps: { value: _vm.model.password },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.model,
                                            "password",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "field-messages",
                                      {
                                        staticClass: "text-danger",
                                        attrs: {
                                          name: "password",
                                          show: "$invalid && $submitted"
                                        }
                                      },
                                      [
                                        _c(
                                          "div",
                                          {
                                            attrs: { slot: "required" },
                                            slot: "required"
                                          },
                                          [_vm._v("Password is required")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            attrs: { slot: "minlength" },
                                            slot: "minlength"
                                          },
                                          [
                                            _vm._v(
                                              "Password should be atleast 6 characters long"
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            attrs: { slot: "maxlength" },
                                            slot: "maxlength"
                                          },
                                          [
                                            _vm._v(
                                              "Password should be atmost 10 characters long"
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-sm-12" }, [
                            _c("div", { staticClass: "form-group checkbox" }, [
                              _c("label", { attrs: { for: "remember" } }, [
                                _c("input", {
                                  attrs: {
                                    type: "checkbox",
                                    name: "remember",
                                    id: "remember"
                                  }
                                }),
                                _vm._v(
                                  "  Remember Me\n                                            "
                                )
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            staticClass: "btn btn-primary btn-block mb-3",
                            attrs: { type: "submit", value: "Sign In" }
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-sm-6" },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "float-left",
                                  attrs: { to: "/forgot_password", exact: "" }
                                },
                                [_vm._v("Forgot Password ? ")]
                              )
                            ],
                            1
                          )
                        ])
                      ]
                    )
                  ],
                  1
                )
              ])
            ])
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h2", { staticClass: "text-center logo_h2" }, [
      _c("img", {
        attrs: {
          src: __webpack_require__(679),
          alt: "Logo"
        }
      })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b93c74f2", module.exports)
  }
}

/***/ }),

/***/ 679:
/***/ (function(module, exports) {

module.exports = "/images/logo-gvi.png?b0ee468f06a33c34397e220e504435d6";

/***/ })

});