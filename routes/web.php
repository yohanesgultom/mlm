<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// adjusted from Illuminate\Routing\Router.php Router.auth()
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// constants
Route::get('users/roles', 'UserController@roles');
Route::get('members/genders', 'MemberController@genders');
Route::get('members/statuses', 'MemberController@statuses');
Route::get('attachments/types', 'AttachmentController@types');
Route::get('wallets/types', 'WalletController@types');
Route::get('articles/types', 'ArticleController@types');
Route::get('articles/statuses', 'ArticleController@statuses');
Route::get('payments/types', 'PaymentController@types');
Route::get('payments/statuses', 'PaymentController@statuses');
Route::get('deposits/statuses', 'DepositController@statuses');
Route::get('orders/statuses', 'OrderController@statuses');
Route::get('deliveries/statuses', 'DeliveryController@statuses');
Route::get('registrations/statuses', 'RegistrationController@statuses');
Route::get('bonusLogs/cycles', 'BonusLogController@cycles');
Route::get('warehouses/types', 'WarehouseController@types');

Route::apiResource('companies.articles', 'ArticleController', ['only' => ['index', 'show']]);
Route::get('companies/{id}/bankAccounts', 'BankAccountController@indexByCompany');

Route::group(['middleware' => 'auth'], function() {
    Route::get('users/me', 'UserController@me');
});

// TODO: not being used until member can access trough web
// if member never access through web, these can be removed
Route::group(['middleware' => 'can:access-member-functions'], function() {
    Route::get('members/me', 'MemberController@me');
    Route::get('members/{id}/downlines', 'MemberController@downlines');
    Route::get('members/{member_id}/attachments/{id}/download', 'AttachmentController@download');

    Route::apiResource('companies', 'CompanyController', ['only' => ['show']]);
    Route::apiResource('religions', 'ReligionController', ['only' => ['index', 'show']]);
    Route::apiResource('countries', 'CountryController', ['only' => ['index', 'show']]);
    Route::apiResource('banks', 'BankController', ['only' => ['index', 'show']]);
    Route::apiResource('members', 'MemberController', ['only' => ['index', 'show']]);
    Route::apiResource('members.wallets', 'WalletController', ['only' => ['index', 'show']]);
    Route::apiResource('members.attachments', 'AttachmentController', ['only' => ['index', 'show', 'store', 'destroy']]);
    Route::apiResource('members.stocks', 'MemberStockController', ['only' => ['index', 'show']]);
    Route::apiResource('members.pairingCounters', 'PairingCounterController', ['only' => ['index', 'show']]);
    Route::apiResource('products', 'ProductController', ['only' => ['index', 'show']]);
    Route::apiResource('productTypes', 'ProductTypeController', ['only' => ['index', 'show']]);
    Route::apiResource('tags', 'TagController', ['only' => ['index', 'show']]);
    Route::apiResource('bankAccounts', 'BankAccountController', ['only' => ['index', 'show']]);
    Route::apiResource('achievements', 'AchievementController', ['only' => ['index', 'show']]);
    Route::apiResource('warehouses', 'WarehouseController', ['only' => ['index', 'show']]);
    Route::apiResource('memberTypes', 'MemberTypeController', ['only' => ['index', 'show']]);
});

Route::group(['middleware' => 'can:access-superadmin-functions'], function() {
    Route::apiResource('companies', 'CompanyController', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::apiResource('religions', 'ReligionController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('countries', 'CountryController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('banks', 'BankController', ['only' => ['store', 'update', 'destroy']]);
});

Route::group(['middleware' => 'can:access-admin-functions'], function() {
    Route::post('transactions/{id}/transfer', 'TransactionController@transfer');
    Route::post('bonusLogs/calculateBonus', 'BonusLogController@calculateBonus');
    Route::post('warehouses/{id}/transfer', 'WarehouseController@transfer');

    Route::get('reports/orders', 'ReportController@orders');
    Route::get('reports/warehouses', 'ReportController@warehouses');
    Route::get('reports/productStocks', 'ReportController@productStocks');
    Route::get('reports/productStockLogs', 'ReportController@productStockLogs');
    Route::get('reports/bonus', 'ReportController@bonus');
    Route::get('reports/transactions', 'ReportController@transactions');
    
    Route::get('jobs', 'JobController@index');

    Route::apiResource('transactions', 'TransactionController', ['only' => ['index', 'show']]);
    Route::apiResource('members', 'MemberController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('members.wallets', 'WalletController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('members.pairingCounters', 'PairingCounterController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('members.stocks', 'MemberStockController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('products', 'ProductController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('productTypes', 'ProductTypeController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('tags', 'TagController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('bankAccounts', 'BankAccountController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('achievements', 'AchievementController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('warehouses', 'WarehouseController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('memberTypes', 'MemberTypeController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('memberTypes.packages', 'PackageController');
    Route::apiResource('companies.articles', 'ArticleController', ['only' => ['store', 'update', 'destroy']]);
    Route::apiResource('warehouses.stocks', 'WarehouseStockController', ['except' => ['update', 'store', 'delete']]);
    Route::apiResource('warehouses.stockLogs', 'WarehouseStockLogController', ['except' => ['update', 'delete']]);
    Route::apiResource('networkCenterTypes', 'NetworkCenterTypeController');
    Route::apiResource('orders', 'OrderController');
    Route::apiResource('payments', 'PaymentController');
    Route::apiResource('deposits', 'DepositController');
    Route::apiResource('deliveries', 'DeliveryController');
    Route::apiResource('bonusLogs', 'BonusLogController', ['only' => ['index', 'show']]);
    Route::apiResource('users', 'UserController');
    Route::apiResource('auditLogs', 'AuditLogController', ['only' => ['index', 'show']]);
    Route::apiResource('registrations', 'RegistrationController');
});
