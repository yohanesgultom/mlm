<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'API\AuthController@login');
Route::post('register', 'API\AuthController@register');

Route::get('users/roles', 'UserController@roles');
Route::get('members/genders', 'MemberController@genders');
Route::get('members/statuses', 'MemberController@statuses');
Route::get('attachments/types', 'AttachmentController@types');
Route::get('wallets/types', 'WalletController@types');
Route::get('articles/types', 'ArticleController@types');
Route::get('articles/statuses', 'ArticleController@statuses');
Route::get('payments/types', 'PaymentController@types');
Route::get('payments/statuses', 'PaymentController@statuses');
Route::get('deposits/statuses', 'DepositController@statuses');
Route::get('orders/statuses', 'OrderController@statuses');
Route::get('deliveries/statuses', 'DeliveryController@statuses');
Route::get('registrations/statuses', 'RegistrationController@statuses');

Route::get('companies/{id}/bankAccounts', 'BankAccountController@indexByCompany');
Route::resource('companies.articles', 'ArticleController', ['only' => ['index', 'show']]);
Route::resource('companies.memberTypes', 'API\MemberTypeController', ['only' => ['index', 'show']]);
Route::resource('companies.memberTypes.packages', 'API\PackageController', ['only' => ['index', 'show']]);

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('sponsors', 'API\MemberController@sponsors');
	Route::post('refresh', 'API\AuthController@refresh');
	Route::post('logout', 'API\AuthController@logout');
	Route::post('changePassword', 'API\MemberController@changePassword');
	Route::post('transactions/{id}/point', 'API\TransactionController@point');
	Route::put('members/me', 'API\MemberController@updateMe');	
	Route::get('members/me', 'MemberController@me');
	Route::get('me', 'MemberController@me');
	Route::get('downlines', 'API\MemberController@memberDownlines');
	Route::get('productTypes', 'API\ProductTypeController@index');
	Route::get('productTypes/{product_type_id}/products', 'API\ProductController@index');
	Route::get('transactions', 'API\TransactionController@index');
	Route::apiResource('bonusLogs', 'API\BonusLogController', ['only' => ['index', 'show']]);
	Route::apiResource('stocks', 'API\MemberStockController', ['only' => ['index']]);
	Route::apiResource('orders', 'API\OrderController');
	Route::apiResource('deliveries', 'API\DeliveryController');
	Route::apiResource('transactions', 'API\TransactionController', ['only' => ['index', 'show']]);
	Route::apiResource('wallets', 'API\WalletController', ['only' => ['index']]);
	Route::apiResource('pairingCounters', 'API\PairingCounterController', ['only' => ['index', 'show']]);
});